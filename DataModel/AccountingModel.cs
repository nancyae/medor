﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class AccountingModel
    {

        public Guid ConsultationID { get; set; }
        public String PFullName { get; set; }
        public Boolean CCheckBox { get; set; }
        public decimal CFees { get; set; }
        public string Ctitle { get; set; }
        public string Ccurrency { get; set; }
        public DateTime CCreatedOn { get; set; }

        public decimal TotalLBPAmount { get; set; }
        public decimal TotalUSAmount { get; set; }

        public DateTime SetTime { get; set; }
        public DateTime TillDate { get; set; }

        public IEnumerable<DataModel.AccountingModel> AccountingModels { get; set; }
        public Guid SelectedClinicID { get; set; }
        public IEnumerable<ClinicModel> ClinicModels { get; set; }
        public Guid SelectedDoctorID { get; set; }
        public IEnumerable<DoctorModel> DoctorModels { get; set; }
    }
}
