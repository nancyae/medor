﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class BP_PetScanModel
    {

        public Guid ID { get; set; }
        public Guid BP_ID { get; set; }
        public Boolean CheckBox { get; set; }
        public string Description { get; set; }
        public String FileName { get; set; }
        public Byte[] Data { get; set; }
        public String ContentType { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
