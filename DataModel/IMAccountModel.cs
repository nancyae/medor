﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
  public  class IMAccountModel
    {
        public Guid AccountID { get; set; }
        public Guid PatientID { get; set; }


        public string Currency { get; set; }
        public decimal? TotalFees { get; set; }
        public DateTime CreatedON { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? ModifiedON { get; set; }
        public Guid? ModifiedBy { get; set; }
        

        public decimal TotalListAccounting { get; set; }

    }
}
