﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class UserModel
    {
        public Guid UserID { get; set; }
        public int UserProfileID { get; set; }
        public Guid RoleID { get; set; }
        public Guid Role { get; set; }
        public String RoleName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public Boolean Gender { get; set; }
        public String Phone { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String Country { get; set; }
        public Guid CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public DateTime CreatedOn { get; set; }
        public Boolean IsSuperAdmin { get; set; }
        public Byte[] Image { get; set; }
        public Guid? ModifiedBy { get; set; }
        public String ModifiedByName { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? LastLoggedIn { get; set; }
        public String LoggedInSince { get; set; }
        public String Username { get; set; }
        public String Email { get; set; }
        public String OldPassword { get; set; }
        public String Password { get; set; }

        public IEnumerable< String >SelectedDescription { get; set; }
        public int SelectedQuantity { get; set; }
        public decimal SelectedUnitCost { get; set; }
        public decimal SelectedTotal { get; set; }
       

        public IEnumerable<String> SelectedClinicNames { get; set; }
        public IEnumerable<String> SelectedDoctorNames { get; set; }
        public IEnumerable<Guid> SelectedClinicIDs { get; set; }
        public IEnumerable<Guid> SelectedDoctorIDs { get; set; }
        public IEnumerable<Guid> SelectedPatientIDs { get; set; }
        public IEnumerable<ClinicModel> ClinicModels { get; set; }
        public IEnumerable<DoctorModel> DoctorModels { get; set; }
        public IEnumerable<RoleModel> RoleModels { get; set; }
       public IEnumerable<PaitentModel> PaitentModels { get; set; }
        public IEnumerable<InvoicesModel> InvoicesModels { get; set; }
        public IEnumerable<InvoiceItemModel> InvoiceItemModels { get; set; }
        public SecurityModel SecurityModel { get; set; }
    }
}
