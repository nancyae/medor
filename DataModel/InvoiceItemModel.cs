﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public  class InvoiceItemModel
    {
        public Guid InvoiceItemID { get; set; }
        public Guid InvoiceID { get; set; }
        public Guid InventoryID { get; set; }
        public Guid ConsultationID { get; set; }
        public decimal UnitCost { get; set; }
        public decimal Total { get; set; }
        public int? Discount { get; set; }
        public String Description { get; set; }
        public int Quantity { get; set; }
        public IEnumerable<InvoicesModel> InvoicesModels { get; set; }
   


    }
}
