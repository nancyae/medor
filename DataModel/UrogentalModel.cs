﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public  class UrogentalModel
    {
        public Guid UrogentalID { get; set; }
        public Guid ReviewOfSystemID { get; set; }
        public Boolean DiabeteCheck { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }




    }
}
