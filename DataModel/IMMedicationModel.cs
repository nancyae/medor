﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class IMMedicationModel
    {

        public Guid MedicationID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        public string MedicationName { get; set; }
        public DateTime ?DateStart { get; set; }
        public DateTime ?DateStop { get; set; }
        public string Doze { get; set; }
        public Boolean Morning { get; set; }
        public Boolean Afternoon { get; set; }
        public Boolean Evening { get; set; }
        public Boolean BedTime { get; set; }
        public Boolean AsNeeded { get; set; }
        public string HowMuch { get; set; }
        public String Reason { get; set; }
        public String AdditionalInformation { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
