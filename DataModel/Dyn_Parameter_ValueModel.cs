﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public  class Dyn_Parameter_ValueModel
    {
       public  Guid ParameterValueID { get; set; }
        public Guid ParameterDateID { get; set; }
        public Guid ParameterID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        public string ParameterName { get; set; }

        public Boolean CheckBox { get; set; }
        public DateTime Date { get; set; }
        public string Value { get; set; }
        public IList<Dyn_Parameter_ValueModel> Dyn_Parameter_ValueModelst { get; set; }


    }
}
