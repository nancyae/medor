﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
 public   class ICD10PatientModel
    {

        public Guid ICD10PatientID { get; set; }
        public Guid IMconsultationID { get; set; }
        public Guid PatientID { get; set; }
        public string CodeDescriptionICD10 { get; set; }
        public string Note { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid ModifiedBy { get; set; }
    }
}
