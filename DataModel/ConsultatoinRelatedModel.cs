﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class ConsultatoinRelatedModel
    {
        //Consultation
        public Guid IMconsultationID { get; set; }
        public String Ctitle { get; set; }
        public String Cnote { get; set; }
        public Guid CcreatedBy { get; set; }
        public DateTime CcreatedOn { get; set; }
        public Guid CmodifiedBy { get; set; }
        public DateTime CmodifiedOn { get; set; }

        //Complaint History
        public Guid IMcomplaintHistoryID { get; set; }
        public String CHtitle { get; set; }
        public String CHnote { get; set; }
        public Guid CHcreatedBy { get; set; }
        public DateTime CHcreatedOn { get; set; }
        public Guid CHmodifiedBy { get; set; }
        public DateTime CHmodifiedOn { get; set; }

        //IMCardiovasular Risk Vascular
        public Guid CRVascularID { get; set; }
        public Boolean CRVDiabetes { get; set; }
        public Boolean CRVDyslipidemia { get; set; }
        public Boolean CRVAlcohol { get; set; }
        public Boolean CRVObesity { get; set; }
        public Boolean CRVFamilialHistory { get; set; }
        public String CRVDiabetesDescription { get; set; }
        public String CRVDyslipidemiaDescription { get; set; }
        public String CRVAlcoholDescription { get; set; }
        public String CRVObesityDescription { get; set; }
        public String CRVFamilialHistoryDescription { get; set; }
        public Guid CRVcreatedBy { get; set; }
        public DateTime CRVcreatedOn { get; set; }
        public Guid CRVmodifiedBy { get; set; }
        public DateTime CRVmodifiedOn { get; set; }

        //IMmedicalHistory

        public Guid MedicaHistoryID { get; set; }
        public String MHAllergies { get; set; }
        public String MHGynecology { get; set; }
        public String MHMedical { get; set; }
        public String MHFamily { get; set; }
        public String MHSurgical { get; set; }
        public Guid MHcreatedBy { get; set; }
        public DateTime MHcreatedOn { get; set; }
        public Guid MHmodifiedBy { get; set; }
        public DateTime MHmodifiedOn { get; set; }

        //IMsystemReview
        public Guid RSReviewSystemID { get; set; }
        public Boolean RSHEENT { get; set; }
        public Boolean RSPulmonary { get; set; }
        public Boolean RSDigestive { get; set; }
        public Boolean RSCardiovascular { get; set; }
        public Boolean RSUrogenital { get; set; }
        public Boolean RSNervouSystem { get; set; }
        public Boolean SRSkin { get; set; }
        public Boolean RSMusculoskeletal { get; set; }
        public Boolean RSPsychosocial { get; set; }
        public String RSHEENTDescription { get; set; }
        public String RSPulmonaryDescription { get; set; }
        public String RSDigestiveDescription { get; set; }
        public String RSCardiovascularDescriptionical { get; set; }
        public String RSUrogenitalDescription { get; set; }
        public String RSNervouSystemDescription { get; set; }
        public String RSSkinDescription { get; set; }
        public String RSMusculoskeletalDescription { get; set; }
        public String RSPsychosocialDescription { get; set; }
        public int RSAnorexia { get; set; }
        public int RSFever { get; set; }
        public int RSWeight { get; set; }
        public int RSFatigue { get; set; }

        //Phisycal History

        public Guid PhysicalHistoryID { get; set; }

        public Boolean PHHEENT { get; set; }
        public Boolean PHPulmonary { get; set; }
        public Boolean PHDigestive { get; set; }
        public Boolean PHCardiovascular { get; set; }
        public Boolean PHUrogenital { get; set; }
        public Boolean PHNervouSystem { get; set; }
        public Boolean PHSkin { get; set; }
        public Boolean PHMusculoskeletal { get; set; }
        public Boolean PHPsychosocial { get; set; }
        public String PHHEENTDescription { get; set; }
        public String PHPulmonaryDescription { get; set; }
        public String PHDigestiveDescription { get; set; }
        public String PHCardiovascularDescriptionical { get; set; }
        public String PHUrogenitalDescription { get; set; }
        public String PHNervousSystemDescription { get; set; }
        public String PHSkinDescription { get; set; }
        public String PHMusculoskeletalDescription { get; set; }
        public String PHPsychosocialDescription { get; set; }
        public int PHRespiratoryRate { get; set; }
        public int PHBloodPressure { get; set; }
        public int PHWeight { get; set; }
        public Guid PHcreatedBy { get; set; }
        public DateTime PHcreatedOn { get; set; }
        public Guid PHmodifiedBy { get; set; }
        public DateTime PHmodifiedOn { get; set; }

        //Diagnostics

        //public Guid MDiagnosticID { get; set; }
        //public String MCCD_ICD10 { get; set; }
        //public String MNotes { get; set; }
        //public Guid McreatedBy { get; set; }
        //public DateTime McreatedOn { get; set; }
        //public Guid MmodifiedBy { get; set; }
        //public DateTime MmodifiedOn { get; set; }

        //Medication

        public Guid MmedicationID { get; set; }
        public string MMedicationName { get; set; }
        public DateTime MDateStart { get; set; }
        public DateTime MDateStop { get; set; }
        public int MDoze { get; set; }
        public Boolean MMorning { get; set; }
        public Boolean MAfternoon { get; set; }
        public Boolean MEvening { get; set; }
        public Boolean MBedTime { get; set; }
        public Boolean MAsNeeded { get; set; }
        public int MHowMuch { get; set; }
        public String MReason { get; set; }
        public String MAdditionalInformation { get; set; }
        public Guid McreatedBy { get; set; }
        public DateTime McreatedOn { get; set; }
        public Guid MmodifiedBy { get; set; }
        public DateTime MmodifiedOn { get; set; }



        //Hospitalization

        public Guid HospitalSummaryID { get; set; }
        public string HSAdmissionName { get; set; }
        public DateTime HSAdmissionDate { get; set; }
        public DateTime HSDeparture { get; set; }
        public int HSLengthStay { get; set; }
        public string HSDescription { get; set; }
        public Guid HSCreatedBy { get; set; }
        public DateTime HSCreatedOn { get; set; }
        public Guid HSCmodifiedBy { get; set; }
        public DateTime HSCmodifiedOn { get; set; }

        //file
        public Guid FileID { get; set; }
        public string FileName { get; set; }

        public Guid FCreatedBy { get; set; }
        public DateTime FCreatedOn { get; set; }
        public Guid FmodifiedBy { get; set; }
        public DateTime FmodifiedOn { get; set; }

        //Management
        public Guid ManagementID { get; set; }
        public string MTitle { get; set; }
        public String Mdescription { get; set; }
        public Guid MCreatedBy { get; set; }
        public DateTime MCreatedOn { get; set; }
        public Guid MCmodifiedBy { get; set; }
        public DateTime MCmodifiedOn { get; set; }


        // Biological

        public Guid BiologicalID { get; set; }

        public Boolean BPBiology { get; set; }
        public Boolean BPconventionalRadiography { get; set; }
        public Boolean BPEchography { get; set; }
        public Boolean BPScan { get; set; }
        public Boolean BPMRI { get; set; }
        public Boolean BPscintigraphy { get; set; }
        public Boolean BPPetScan { get; set; }
        public Boolean BPOther { get; set; }

        public String BPconventionalRadiographyNote { get; set; }
        public String BPEchographyNote { get; set; }
        public String BPScanNote { get; set; }
        public String BPMRINote { get; set; }
        public String BPscintigraphyNote { get; set; }
        public String BPOtherNote { get; set; }

        public String BPconventionalRadiographyFile { get; set; }
        public String BPEchographyFile { get; set; }
        public String BPScanFile { get; set; }
        public String BPMRIFile { get; set; }
        public String BPscintigraphyFile { get; set; }
        public Boolean BPPetScanFile { get; set; }
        public String BPOtherFile { get; set; }

        public Guid BPCreatedBy { get; set; }
        public DateTime BPCreatedOn { get; set; }
        public Guid BPCmodifiedBy { get; set; }
        public DateTime BPCmodifiedOn { get; set; }
    }
}
