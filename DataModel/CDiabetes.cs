﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class CDiabetes

    {
        public Guid DiabeteID { get; set; }

        public Guid CardiovascularID { get; set; }

        public Boolean DiabeteCheck { get; set; }

        public string Description { get; set; }

        public DateTime ?CreatedOn { get; set; }


    }
}