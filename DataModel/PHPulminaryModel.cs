﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PHPulminaryModel
    {
        public Guid PulminaryID { get; set; }
        public Guid ReviewOfSystemID { get; set; }
        public Boolean DiabeteCheck { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
