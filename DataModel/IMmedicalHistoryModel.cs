﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
  public  class IMmedicalHistoryModel
    {

        public Guid MedicalHistoryID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }
        
        public String Allergies { get; set; }
        public String Gynecology { get; set; }
        public String Medical { get; set; }
        public String Family { get; set; }
        public String Surgical { get; set; }

        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }



    }
}
