﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class  IMedicineModifiedModel
    {

        public DataModel.IMConsultationModel IMconsultationModel { get; set; }
        public DataModel.IMcomplaintHistory IMcomplaintHistoryModel { get; set; }



        //Cardiovascular
        public DataModel.CDiabetes CDiabetes { get; set; }
        public DataModel.CDyslipidemia CDyslipidemia { get; set; }
        public DataModel.CAlcoholModel CAlcoholModel { get; set; }
        public DataModel.CObesityModel CObesityModel { get; set; }
        public DataModel.FamilialHistoryModel FamilialHistoryModel { get; set; }

        // medicalHistory
        public DataModel.IMmedicalHistoryModel IMmedicalHistoryModels { get; set; }
        public DataModel.AllergiesModel AllergiesModels { get; set; }
        public DataModel.FamilyModel FamilyModels { get; set; }
        public DataModel.GynecologyModel GynecologyModels { get; set; }
        public DataModel.SurgicalModel SurgicalModels { get; set; }
        public DataModel.MHMedicalModel MHMedicalModels { get; set; }


        // Review of system
        public DataModel.RSHEENTModel RSHEENTModel { get; set; }
        public DataModel.RSPulminaryModel RSPulminaryModel { get; set; }
        public DataModel.RSCardiovascularModel RSCardiovascularModel { get; set; }
        public DataModel.RSDigestiveModel RSDigestiveModel { get; set; }
        public DataModel.RSUrogentalModel RSUrogentalModel { get; set; }
        public DataModel.RSNervousSystemModel RSNervousSystemModel { get; set; }
        public DataModel.RSSkinModel RSSkinModel { get; set; }
        public DataModel.RSMusculoskeletalModel RSMusculoskeletalModel { get; set; }
        public DataModel.RSPsychosocialeModel RSPsychosocialeModel { get; set; }
        public DataModel.AnorexiasModel AnorexiasModel { get; set; }
        public DataModel.RSFeverModel RSFeverModel { get; set; }
        public DataModel.RSFatigueModel RSFatigueModel { get; set; }
        public DataModel.RSWeightModel RSWeightModel { get; set; }


        // Physical History

        public DataModel.PHHEENTModel PHHEENTModel { get; set; }
        public DataModel.PHPulminaryModel PHPulminaryModel { get; set; }
        public DataModel.PHCardiovascularModelcs PHCardiovascularModel { get; set; }
        public DataModel.PHDigestiveModel PHDigestiveModel { get; set; }
        public DataModel.PHUrogentalModel PHUrogentalModel { get; set; }
        public DataModel.PHNervousSystemModel PHNervousSystemModel { get; set; }
        public DataModel.PHRSSkinModel PHSkinModel { get; set; }
        public DataModel.PHMusculoskeletalModel PHMusculoskeletalModel { get; set; }
        public DataModel.PHPsychosocialeModel PHPsychosocialeModel { get; set; }

        public DataModel.BloodPressureModel BloodPressureModel { get; set; }
        public DataModel.PHWeightModel PHWeightModel { get; set; }
        public DataModel.PHRespiratoryRateModel PHRespiratoryRateModel { get; set; }


        public DataModel.ICD10PatientModel ICD10PatientModel { get; set; }

        public DataModel.IMFileModel IMFileModel { get; set; }

        public DataModel.IMMedicationModel IMMedicationModel { get; set; }

        public DataModel.IMHospitalSummaryModel IMHospitalSummaryModel { get; set; }

        public DataModel.IMManagement IMManagement { get; set; }

    }
}
