﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class CalendarModel
    {
        public Guid SelectedClinicID { get; set; }
        public IEnumerable<ClinicModel> ClinicModels { get; set; }
        public IEnumerable<AppointmentModel> AppointmentModels { get; set; }
    }
}
