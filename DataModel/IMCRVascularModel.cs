﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
  public class IMCRVascularModel
    {

        public Guid CRVascularID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        public Boolean Diabetes { get; set; }
        public Boolean Dyslipidemia { get; set; }
        public Boolean Alcohol { get; set; }
        public Boolean Obesity { get; set; }
        public Boolean FamilialHistory { get; set; }

        public String  DiabetesDescription { get; set; }
        public String DyslipidemiaDescription { get; set; }
        public String AlcoholDescription { get; set; }
        public String ObesityDescription { get; set; }
        public String FamilialHistoryDescription { get; set; }

        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }




    }
}
