﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class IMcomplaintHistory
    {
        public Guid complaintHistoryID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        public String title { get; set; }
        public String note { get; set; }
        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime? modifiedOn { get; set; }

    }
}
