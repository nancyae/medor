﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
 public    class RSFeverModel
    {
      
        public Guid FeverID { get; set; }
       
      
        public string Fever { get; set; }
     
   
     
        public DateTime? CreatedOn { get; set; }
    
    }
}
