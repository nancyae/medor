﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class RoleModel
    {
        public Guid RoleID { get; set; }
        public String Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid CreatedBy { get; set; }
        public String CreatedByName { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public Guid? ModifiedBy { get; set; }
        public String ModifiedByName { get; set; }
        public Boolean IsSuperAdmin { get; set; }

        public SecurityModel SecurityModel { get; set; }
        public IList<PermissionModel> PermissionModels { get; set; }
        public IEnumerable<Guid> SelectedUserIDs { get; set; }
        public IEnumerable<UserModel> UserModels { get; set; }
    }
}
