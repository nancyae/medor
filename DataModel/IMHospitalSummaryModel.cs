﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class IMHospitalSummaryModel
    {


        public Guid HospitalSummaryID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }
        public string AdmissionName { get; set; }
        public DateTime ?AdmissionDate { get; set; }
        public DateTime ?Departure { get; set; }
        public int ?LengthStay { get; set; }
        public string Description { get; set; }
        public String FileName { get; set; }
        public Byte[] Data { get; set; }
        public String ContentType { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }


    }
}
