﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class IMedicneModel
    {
        public Guid MedicalInternalID { get; set; }
        public Guid PatientID { get; set; }
        public String TableName { get; set; }
        public String TapName { get; set; }

       // public HttpPostedFileBase SomeFile { get; set; }

        public Guid ConsultationIDTap { get; set; }
        public Guid FileID_BP { get; set; }


        //Consultation
        public Guid IMconsultationID { get; set; }
        public Guid FconsultationID { get; set; }
        public String Ctitle { get; set; }
        public String Cnote { get; set; }
        public decimal CFees { get; set; }
        public String Ccurency { get; set; }
        public Boolean CCheckBox { get; set; }
        //INvoices
        public Guid  InCreatedBy { get; set; }



        public Guid CcreatedBy { get; set; }
        public DateTime CcreatedOn { get; set; }
        public Guid CmodifiedBy { get; set; }
        public DateTime CmodifiedOn { get; set; }

        //Complaint History
        public Guid IMcomplaintHistoryID { get; set; }
        public String CHtitle { get; set; }
        public String CHnote { get; set; }
        public Guid CHcreatedBy { get; set; }
        public DateTime CHcreatedOn { get; set; }
        public Guid CHmodifiedBy { get; set; }
        public DateTime CHmodifiedOn { get; set; }

        //IMCardiovasular Risk Vascular
        public Guid CRVascularID { get; set; }
        public Boolean CRVDiabetes { get; set; }
        public Boolean CRVDyslipidemia { get; set; }
        public Boolean CRVAlcohol { get; set; }
        public Boolean CRVObesity { get; set; }
        public Boolean CRVFamilialHistory { get; set; }
        public String CRVDiabetesDescription { get; set; }
        public String CRVDyslipidemiaDescription { get; set; }
        public String CRVAlcoholDescription { get; set; }
        public String CRVObesityDescription { get; set; }
        public String CRVFamilialHistoryDescription { get; set; }
        public Guid CRVcreatedBy { get; set; }
        public DateTime CRVcreatedOn { get; set; }
        public Guid CRVmodifiedBy { get; set; }
        public DateTime CRVmodifiedOn { get; set; }

        //IMmedicalHistory
        public Guid MedicaHistoryID { get; set; }
        public String MHAllergies { get; set; }
        public String MHGynecology { get; set; }
        public String MHMedical { get; set; }
        public String MHFamily { get; set; }
        public String MHSurgical { get; set; }
        public Guid MHcreatedBy { get; set; }
        public DateTime MHcreatedOn { get; set; }
        public Guid MHmodifiedBy { get; set; }
        public DateTime MHmodifiedOn { get; set; }

        //IMsystemReview
        public Guid RSReviewSystemID { get; set; }
        public Boolean RSHEENT { get; set; }
        public Boolean RSPulmonary { get; set; }
        public Boolean RSDigestive { get; set; }
        public Boolean RSCardiovascular { get; set; }
        public Boolean RSUrogenital { get; set; }
        public Boolean RSNervouSystem { get; set; }
        public Boolean SRSkin { get; set; }
        public Boolean RSMusculoskeletal { get; set; }
        public Boolean RSPsychosocial { get; set; }
        public String RSHEENTDescription { get; set; }
        public String RSPulmonaryDescription { get; set; }
        public String RSDigestiveDescription { get; set; }
        public String RSCardiovascularDescriptionical { get; set; }
        public String RSUrogenitalDescription { get; set; }
        public String RSNervouSystemDescription { get; set; }
        public String RSSkinDescription { get; set; }
        public String RSMusculoskeletalDescription { get; set; }
        public String RSPsychosocialDescription { get; set; }
        public String RSAnorexia { get; set; }
        public String RSFever { get; set; }
        public String RSWeight { get; set; }
        public String RSFatigue { get; set; }

        //Phisycal History
        public Guid PhysicalHistoryID { get; set; }
        public Boolean PHHEENT { get; set; }
        public Boolean PHPulmonary { get; set; }
        public Boolean PHDigestive { get; set; }
        public Boolean PHCardiovascular { get; set; }
        public Boolean PHUrogenital { get; set; }
        public Boolean PHNervouSystem { get; set; }
        public Boolean PHSkin { get; set; }
        public Boolean PHMusculoskeletal { get; set; }
        public Boolean PHPsychosocial { get; set; }
        public String PHHEENTDescription { get; set; }
        public String PHPulmonaryDescription { get; set; }
        public String PHDigestiveDescription { get; set; }
        public String PHCardiovascularDescriptionical { get; set; }
        public String PHUrogenitalDescription { get; set; }
        public String PHNervousSystemDescription { get; set; }
        public String PHSkinDescription { get; set; }
        public String PHMusculoskeletalDescription { get; set; }
        public String PHPsychosocialDescription { get; set; }
        public string PHRespiratoryRate { get; set; }
        public string PHBloodPressure { get; set; }
        public string PHWeight { get; set; }
        public Guid PHcreatedBy { get; set; }
        public DateTime PHcreatedOn { get; set; }
        public Guid PHmodifiedBy { get; set; }
        public DateTime PHmodifiedOn { get; set; }


        public Guid MmedicationID { get; set; }
        public string MMedicationName { get; set; }
        public DateTime MDateStart { get; set; }
        public DateTime MDateStop { get; set; }
        public String MDoze { get; set; }
        public Boolean MMorning { get; set; }
        public Boolean MAfternoon { get; set; }
        public Boolean MEvening { get; set; }
        public Boolean MBedTime { get; set; }
        public Boolean MAsNeeded { get; set; }
        public String MHowMuch { get; set; }
        public String MReason { get; set; }
        public String MAdditionalInformation { get; set; }
        public Guid McreatedBy { get; set; }
        public DateTime McreatedOn { get; set; }
        public Guid MmodifiedBy { get; set; }
        public DateTime MmodifiedOn { get; set; }

        //Hospitalization
        public Guid HospitalSummaryID { get; set; }
        public string HSAdmissionName { get; set; }
        public DateTime HSAdmissionDate { get; set; }
        public DateTime HSDeparture { get; set; }
        public int HSLengthStay { get; set; }
        public string HSDescription { get; set; }
        public Guid HSCreatedBy { get; set; }
        public DateTime HSCreatedOn { get; set; }
        public Guid HSCmodifiedBy { get; set; }
        public DateTime HSCmodifiedOn { get; set; }

        //file
        public Guid FileID { get; set; }
        public string FileName { get; set; }

        public Guid FCreatedBy { get; set; }
        public DateTime FCreatedOn { get; set; }
        public Guid FmodifiedBy { get; set; }
        public DateTime FmodifiedOn { get; set; }

        //Management
        public Guid ManagementID { get; set; }
        public String MTitle { get; set; }
        public String Mdescription { get; set; }
        public Guid MaCreatedBy { get; set; }
        public DateTime MaCreatedOn { get; set; }
        public Guid MCmodifiedBy { get; set; }
        public DateTime MCmodifiedOn { get; set; }


        // Biological

        public Guid BiologicalID { get; set; }

        public Boolean BPBiology { get; set; }
        public Boolean BPconventionalRadiography { get; set; }
        public Boolean BPEchography { get; set; }
        public Boolean BPScan { get; set; }
        public Boolean BPMRI { get; set; }
        public Boolean BPscintigraphy { get; set; }
        public Boolean BPPetScan { get; set; }
        public Boolean BPOther { get; set; }

        public String BPconventionalRadiographyNote { get; set; }
        public String BPEchographyNote { get; set; }
        public String BPScanNote { get; set; }
        public String BPPetScanNote { get; set; }
        public String BPMRINote { get; set; }
        public String BPscintigraphyNote { get; set; }
        public String BPOtherNote { get; set; }

        public String BPconventionalRadiographyFile { get; set; }
        public String BPEchographyFile { get; set; }
        public String BPScanFile { get; set; }
        public String BPMRIFile { get; set; }
        public String BPscintigraphyFile { get; set; }
        public Boolean BPPetScanFile { get; set; }
        public String BPOtherFile { get; set; }

        public Guid BPCreatedBy { get; set; }
        public DateTime BPCreatedOn { get; set; }
        public Guid BPCmodifiedBy { get; set; }
        public DateTime BPCmodifiedOn { get; set; }

        public DataModel.IMConsultationModel IMconsultationModelN { get; set; }
        public DataModel.IMcomplaintHistory IMcomplaintHistoryModelN { get; set; }
        
        public IEnumerable<DataModel.IMConsultationModel> IMconsultationModels { get; set; }
        public IEnumerable<DataModel.IMcomplaintHistory> IMcomplaintHistoryModels { get; set; }
        public DataModel.IMCRVascularModel IMCRVascularModels { get; set; }
        public IEnumerable < DataModel.CDiabetes > CDiabetes { get; set; }
        public IEnumerable<DataModel.CDyslipidemia> CDyslipidemia { get; set; }
        public IEnumerable<DataModel.CAlcoholModel> CAlcoholModel { get; set; }
        public IEnumerable<DataModel.CObesityModel> CObesityModel { get; set; }
        public IEnumerable<DataModel.FamilialHistoryModel> FamilialHistoryModel { get; set; }
        public DataModel.IMmedicalHistoryModel IMmedicalHistoryModels { get; set; }
        public IEnumerable<DataModel.AllergiesModel> AllergiesModels { get; set; }
        public IEnumerable<DataModel.FamilyModel> FamilyModels { get; set; }
        public IEnumerable<DataModel.GynecologyModel> GynecologyModels { get; set; }
        public IEnumerable<DataModel.SurgicalModel> SurgicalModels { get; set; }
        public IEnumerable<DataModel.MHMedicalModel> MHMedicalModels { get; set; }
        public DataModel.IMreviewSystemModel IMreviewSystemModels { get; set; }


        public IEnumerable<DataModel.RSEntryModel> RSEntryModels { get; set; }
        public IEnumerable<DataModel.RSHEENTModel> RSHEENTModels { get; set; }
        public IEnumerable<DataModel.RSPulminaryModel> RSPulminaryModels { get; set; }
        public IEnumerable<DataModel.RSCardiovascularModel> RSCardiovascularModels { get; set; }
        public IEnumerable<DataModel.RSDigestiveModel> RSDigestiveModels { get; set; }
        public IEnumerable<DataModel.RSUrogentalModel> RSUrogentalModels { get; set; }
        public IEnumerable<DataModel.RSNervousSystemModel> RSNervousSystemModels { get; set; }
        public IEnumerable<DataModel.RSSkinModel> RSSkinModels { get; set; }
        public IEnumerable<DataModel.RSMusculoskeletalModel> RSMusculoskeletalModels { get; set; }
        public IEnumerable<DataModel.RSPsychosocialeModel> RSPsychosocialeModels { get; set; }
        public DataModel.AnorexiasModel AnorexiasModel { get; set; }

        public IEnumerable<DataModel.AnorexiasModel> AnorexiasModels { get; set; }
        public IEnumerable<DataModel.RSFeverModel> RSFeverModels { get; set; }
        public IEnumerable<DataModel.RSFatigueModel> RSFatigueModels { get; set; }
        public IEnumerable<DataModel.RSWeightModel> RSWeightModels { get; set; }
        
        public DataModel.RSEntryModel RSEntryModel { get; set; }
        public DataModel.RSHEENTModel RSHEENTModel { get; set; }
        public DataModel.RSPulminaryModel RSPulminaryModel { get; set; }
        public DataModel.RSCardiovascularModel RSCardiovascularModel { get; set; }
        public DataModel.RSDigestiveModel RSDigestiveModel { get; set; }
        public DataModel.RSUrogentalModel RSUrogentalModel { get; set; }
        public DataModel.RSNervousSystemModel RSNervousSystemModel { get; set; }
        public DataModel.RSSkinModel RSSkinModel { get; set; }
        public DataModel.RSMusculoskeletalModel RSMusculoskeletalModel { get; set; }
        public DataModel.RSPsychosocialeModel RSPsychosocialeModel { get; set; }

        public DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModels { get; set; }


        public DataModel.BloodPressureModel BloodPressureModel { get; set; }
        public DataModel.PHWeightModel PHWeightModel { get; set; }
        public DataModel.PHRespiratoryRateModel PHRespiratoryRateModel { get; set; }

        public IEnumerable<DataModel.BloodPressureModel> BloodPressureModels { get; set; }
        public IEnumerable<DataModel.PHWeightModel> PHWeightModels { get; set; }
        public IEnumerable<DataModel.PHRespiratoryRateModel> PHRespiratoryRateModels { get; set; }

        public IEnumerable<DataModel.PHEntryModel> PHEntryModels { get; set; }
        public IEnumerable<DataModel.PHHEENTModel> PHHEENTModels { get; set; }
        public IEnumerable<DataModel.PHPulminaryModel> PHPulminaryModels { get; set; }
        public IEnumerable <DataModel.PHCardiovascularModelcs> PHCardiovascularModels { get; set; }
        public IEnumerable<DataModel.PHDigestiveModel> PHDigestiveModels { get; set; }
        public IEnumerable<DataModel.PHUrogentalModel> PHUrogentalModels { get; set; }
        public IEnumerable<DataModel.PHNervousSystemModel> PHNervousSystemModels { get; set; }
        public IEnumerable<DataModel.PHRSSkinModel> PHSkinModels { get; set; }
        public IEnumerable<DataModel.PHMusculoskeletalModel> PHMusculoskeletalModels { get; set; }
        public IEnumerable<DataModel.PHPsychosocialeModel> PHPsychosocialeModels { get; set; }

        public DataModel.PHHEENTModel PHHEENTModel { get; set; }
        public DataModel.PHPulminaryModel PHPulminaryModel{ get; set; }
        public DataModel.PHCardiovascularModelcs PHCardiovascularModel { get; set; }
        public DataModel.PHDigestiveModel PHDigestiveModel { get; set; }
        public DataModel.PHUrogentalModel PHUrogentalModel { get; set; }
        public DataModel.PHNervousSystemModel PHNervousSystemModel { get; set; }
        public DataModel.PHRSSkinModel PHSkinModel { get; set; }
        public DataModel.PHMusculoskeletalModel PHMusculoskeletalModel { get; set; }
        public DataModel.PHPsychosocialeModel PHPsychosocialeModel { get; set; }

        public DataModel.IMBiologicalModel IMBiologicalModels { get; set; }
        public IEnumerable<DataModel.Conventional_RadiographyModel> Conventional_RadiographyModels { get; set; }
        public IEnumerable<DataModel.EchographyModel> BPEchographyModels { get; set; }
        public IEnumerable<DataModel.BPOtherModel> BPOtherModels { get; set; }
        public IEnumerable<DataModel.BPScanModel> BPScanModels { get; set; }
        public IEnumerable<DataModel.BPMRIModel> BPMRIModels { get; set; }
        public IEnumerable<DataModel.BPScintigraphyModel> BPScintigraphyModels { get; set; }
        public IEnumerable<DataModel.BPPetScanModel> BPPetScanModels { get; set; }
        public Guid ParameterDateID { get; set; }

        public IEnumerable<DataModel.DaynParametersModel> DaynParametersModels { get; set; }
        public DataModel.DaynParametersModel DaynParametersModel { get; set; }
        
        public IEnumerable<DataModel.Dyn_Parameter_ValueModel> Dyn_Parameter_ValueModels { get; set; }
        public IList<Dyn_Parameter_ValueModel> Dyn_Parameter_ValueModelst { get; set; }
        public IList<ParameterValueDateModel> ParameterValueDateModelist { get; set; }
        public IList<ParameterValueDateModel> ParameterValueDateModel { get; set; }
        public DataModel.Dyn_Parameter_ValueModel Dyn_Parameter_ValueModel { get; set; }
        public string DaynParametersvalue { get; set; }
        public Guid DaynParametersID { get; set; }

        public DataModel.IMMedicationModel IMMedicationModel { get; set; }
        public IList<IMMedicationModel> IMMedicationModels { get; set; }
        public IList<BP_CRMoldel> BP_CRMoldels { get; set; }
        public IList<BP_EchographyModel> BP_EchographyModels { get; set; }
        public IList<BP_MRIModel> BP_MRIModels { get; set; }
        public IList<BP_ScanModel> BP_ScanModels { get; set; }
        public IList<BP_scintigraphyModel> BP_scintigraphyModels { get; set; }
        public IList<BP_OtherModel> BP_OtherModels { get; set; }
        public IList<BP_PetScanModel> BP_PetScanModels { get; set; }

        public IEnumerable<DataModel.IMHospitalSummaryModel> IMHospitalSummaryModels { get; set; }
        public DataModel.IMHospitalSummaryModel IMHospitalSummaryModel { get; set; }
        //public DataModel.IMHospitalSummaryModel IMHospitalSummaryModels { get; set; }
        public IEnumerable<DataModel.ICD10PatientModel> ICD10PatientModels { get; set; }
        public DataModel.ICD10PatientModel ICD10PatientModel { get; set; }
        public IEnumerable<DataModel.IMFileModel> IMFileModels { get; set; }
        public DataModel.IMFileModel IMFileModel { get; set; }
        public DataModel.IMManagement IMManagement { get; set; }
        public IEnumerable<DataModel.IMManagement> IMManagementMOdels { get; set; }
        public IEnumerable<DataModel.IMAccountModel> IMAccountModels { get; set; }

        //public IEnumerable<String> SelectedClinicNames { get; set; }
        //public IEnumerable<Guid> SelectedClinicIDs { get; set; }

        public IEnumerable<String> SelectedDoctorNames { get; set; }
        public Guid SelectedDoctorID { get; set; }
        public IEnumerable<DoctorModel> DoctorModels { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }

        public SecurityModel SecurityModel { get; set; }

    }

    public class Date
    {
    }
}