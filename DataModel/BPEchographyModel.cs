﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    class BPEchographyModel
    {
        public Guid EchographyID { get; set; }
        public Guid BiologicalID { get; set; }
        public Boolean CheckBox { get; set; }
        public string Description { get; set; }
        public String FileName { get; set; }
        public Byte[] Data { get; set; }

        public String ContentType { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
