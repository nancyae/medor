﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public  class ExpenseModel
    {
        public Guid ExpenseID { get; set; }
        public Guid UserClinicID { get; set; }
        public Decimal Amount { get; set; }
        public DateTime CreatedON { get; set; }
        public Guid CreatedBy { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }



        public String CreatedByName { get; set; }
        public Decimal Income { get; set; }
        public Decimal TotalAmount { get; set; }
        public IEnumerable< DataModel.InvoicesModel> InvoicesModels { get; set; }


    }
}
