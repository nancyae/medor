﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class HospitalizationModel
    {
        public Guid HospitalizationID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        //DescrptionCheckBox
        public String HospitalizationName { get; set; }
        public DateTime Admissiondate { get; set; }
        public DateTime DepartureDate { get; set; }
        public int Length { get; set; }
        public String Description { get; set; }
        public String File { get; set; }


        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }
    }
}
