﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
 public  class IMFileModel
    {
        public Guid FileID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }
     
        public String FileName { get; set; }
        public Byte[] Data { get; set; }

        public String ContentType { get; set; }
        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid? modifiedBy { get; set; }
        public DateTime? modifiedOn { get; set; }



    }
}
