﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
public class IMreviewSystemModel
    {
        public Guid ReviewSystemID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        //Checkbox
        public Boolean HEENT { get; set; }
        public Boolean Pulmonary { get; set; }
        public Boolean Digestive { get; set; }
        public Boolean Cardiovascular { get; set; }
        public Boolean Urogenital { get; set; }
        public Boolean NervouSystem { get; set; }
        public Boolean Skin { get; set; }
        public Boolean Musculoskeletal { get; set; }
        public Boolean Psychosocial { get; set; }

        //DescrptionCheckBox
        public String HEENTDescription { get; set; }
        public String PulmonaryDescription { get; set; }
        public String DigestiveDescription { get; set; }
        public String CardiovascularDescriptionical { get; set; }
        public String UrogenitalDescription { get; set; }
        public String NervousSystemDescription { get; set; }
        public String SkinDescription { get; set; }
        public String MusculoskeletalDescription { get; set; }
        public String PsychosocialDescription { get; set; }

        //Input
        public string Anorexia { get; set; }
        public string Fever { get; set; }
        public string Weight { get; set; }
        public string Fatigue { get; set; }

        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }



    }
}
