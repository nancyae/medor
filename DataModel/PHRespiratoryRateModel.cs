﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PHRespiratoryRateModel
    {
        public Guid RespiratoryRateID { get; set; }
        public Guid ReviewOfSystemID { get; set; }
        public string RespiratoryRate { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
