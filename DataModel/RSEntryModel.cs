﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
public  class RSEntryModel
    {

        public Guid AnorexiaID { get; set; }
        public Guid FeverID { get; set; }
        public Guid WeightID { get; set; }
        public Guid FatigueID { get; set; }

        public Guid ReviewOfSystemID { get; set; }
        public string Anorexia { get; set; }
        public string  Fever { get; set; }
        public string Weight { get; set; }
        public string Fatigue { get; set; }

        public DateTime? ACreatedOn { get; set; }
        public DateTime? FCreatedOn { get; set; }
        public DateTime? WCreatedOn { get; set; }
        public DateTime? FaCreatedOn { get; set; }





    }
}
