﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class GynecologyModel
    {
        public Guid GynecologyID { get; set; }
        public Guid MedicalHistoryID { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
