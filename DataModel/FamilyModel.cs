﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
   public class FamilyModel
    {
        public Guid FamilyID { get; set; }
        public Guid MedicalHistoryID { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
