﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PHEntryModel
    {
        public Guid RespiratoryRateID { get; set; }
        public Guid BloodPressureID { get; set; }
        public Guid WeightID { get; set; }
       

        public Guid ReviewOfSystemID { get; set; }
        public string Weight { get; set; }
        public string RespiratoryRate { get; set; }
        public string BloodPressure { get; set; }
      

        public DateTime? RCreatedOn { get; set; }
        public DateTime? BCreatedOn { get; set; }
        public DateTime? WCreatedOn { get; set; }
     
    }
}
