﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class IMDiagnosticsModel
    {

        public Guid DiagnosticID { get; set; }
        public Guid PatientID { get; set; }

         
        //DescrptionCheckBox
        public String CCD_ICD10 { get; set; }
        public String Notes { get; set; }
    
        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }


    }
}
