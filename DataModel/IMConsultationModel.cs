﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class IMConsultationModel
    {

        public Guid IMconsultationID { get; set; }
        public Guid PatientID { get; set; }
        public Guid DoctorID { get; set; }
        public String title { get; set; }
        public String note { get; set; }
        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public decimal Fees { get; set; }
        public Boolean CheckBox { get; set; }
        public String Curency { get; set; }
        public Guid ?modifiedBy { get; set; }
        public DateTime ?modifiedOn { get; set; }

        public string CreatedByName { get; set; }
        public string modifiedByName { get; set; }


        public string FullName { get; set; }
        public  DataModel.IMcomplaintHistory IMcomplaintHistoryModels { get; set; }
        public DataModel.IMConsultationModel IMConsultationModels { get; set; }
        public DataModel.IMmedicalHistoryModel IMmedicalHistoryModels { get; set; }

    }
}
