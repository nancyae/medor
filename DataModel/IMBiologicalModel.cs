﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class IMBiologicalModel
    {

        public Guid BiologicalID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }


        public Boolean Biology { get; set; }
        public Boolean conventionalRadiography { get; set; }
        public Boolean Echography { get; set; }
        public Boolean Scan { get; set; }
        public Boolean MRI { get; set; }
        public Boolean scintigraphy { get; set; }
        public Boolean PetScan { get; set; }
        public Boolean Other { get; set; }

        public String conventionalRadiographyNote { get; set; }
        public String EchographyNote { get; set; }
        public String ScanNote { get; set; }
        public String MRINote { get; set; }
        public String scintigraphyNote { get; set; }
        public String PetScanNote { get; set; }
        public String OtherNote { get; set; }



        public String conventionalRadiographyFile { get; set; }
        public String EchographyFile { get; set; }
        public String ScanFile { get; set; }
        public String MRIFile { get; set; }
        public String scintigraphyFile { get; set; }
        public String PetScanFile { get; set; }
        public String OtherFile { get; set; }



        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }


        public String ContentType { get; set; }
        public String FileName { get; set; }
        public Byte[] Data { get; set; }

    }
}
