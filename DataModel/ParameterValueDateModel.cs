﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
public  class ParameterValueDateModel
    {
public Guid ParameterDateID { get; set; }
public DateTime Date { get; set; }
public decimal Count { set; get; } 

    }
}
