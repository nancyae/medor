﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
  public  class AnorexiasModel
    {
        public Guid AnorexiaID { get; set; }
        public string Anorexia { get; set; }
        public DateTime? CreatedOn { get; set; }

    }
}
