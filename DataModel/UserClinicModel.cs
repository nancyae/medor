﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class UserClinicModel
    {
        public Guid UserClinicID { get; set; }
        public Guid UserID { get; set; }
        public Guid ClinicID { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
