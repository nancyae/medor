﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
    public class PediatricMedicalHistoryModel
    {
        public Guid MedicalHistoryID { get; set; }
        public Guid PatientID { get; set; }
        public String MedicalHistory { get; set; }
        public String SurgicalHistory { get; set; }
        public String FamilyHistory { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Guid ModifiedBy { get; set; }
        public String GeneralNote { get; set; }
    }
}
