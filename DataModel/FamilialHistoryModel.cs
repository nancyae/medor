﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
  public class FamilialHistoryModel
    {


        public Guid FamilialHistoryID { get; set; }

        public Guid CardiovascularID { get; set; }

        public Boolean CheckBox { get; set; }

        public string Description { get; set; }

        public DateTime? CreatedOn { get; set; }



    }
}
