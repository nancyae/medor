﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
 public   class BloodPressureModel
    {
     
        public Guid BloodPressureID { get; set; }
 


        public Guid ReviewOfSystemID { get; set; }
  
        public string BloodPressure { get; set; }



        public DateTime? CreatedOn { get; set; }
    
        }
}
