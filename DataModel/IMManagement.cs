﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataModel
{
  public  class IMManagement
    {

        public Guid ManagementID { get; set; }
        public Guid PatientID { get; set; }
        public Guid ConsultationID { get; set; }

        //DescrptionCheckBox
        public String Description { get; set; }
        public String Title { get; set; }

        public Guid createdBy { get; set; }
        public DateTime createdOn { get; set; }
        public Guid modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }





    }
}
