﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Web.Mvc;
using System.Collections.Specialized;
//using SignalRChat.Common;



namespace MedOR.Hubs
{

    public class ChatHub : Hub
    {
        static List<DataModel.MessageDetail> CurrentMessage = new List<DataModel.MessageDetail>();
        static List<DataModel.UserDetail> ConnectedUsers = new List<DataModel.UserDetail>();

        public void Connect(string userName, string IdUserDB)
        {
            var id = Context.ConnectionId;
            


            if (ConnectedUsers.Count(x => x.UserId == IdUserDB) == 0)
            {
                ConnectedUsers.Add(new DataModel.UserDetail { ConnectionId = id,UserId= IdUserDB, UserName = userName });
                // send to caller
                Clients.Caller.onConnected(id, userName, ConnectedUsers, CurrentMessage);
                // send to all except caller client
                Clients.AllExcept(id).onNewUserConnected(id, userName);
            }
            else
            {
               // Clients.Caller.onConnected(id, userName, ConnectedUsers, CurrentMessage);
                // send to all except caller client
               // Clients.AllExcept(id).onNewUserConnected(id, userName);

            }
        }

        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            Clients.All.addNewMessageToPage(name, message);
        }



        public void sendPrivateMessage(string toUserId, string message, string IDprofileUser)

        {

            // Guid UserID = new Guid(HttpContext.User.Identity.Name);
            var fromUserId = Context.ConnectionId; 
            var toUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == toUserId);
            var fromUser = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == fromUserId);
            if (toUser != null && fromUser != null)
            {

                // send to caller user
                Clients.Caller.sendPrivateMessage(toUserId, fromUser.UserName, message);

                // send to 

                Clients.Client(toUserId).sendPrivateMessage(toUserId, fromUser.UserName, message);



            }
        }

        public override System.Threading.Tasks.Task OnDisconnected()
        {
            var item = ConnectedUsers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                ConnectedUsers.Remove(item);

                var id = Context.ConnectionId;
                Clients.All.onUserDisconnected(id, item.UserName);
            }
            return base.OnDisconnected();
        }

        private void AddMessageinCache(string userName, string message)
        {
            CurrentMessage.Add(new DataModel.MessageDetail { UserName = userName, Message = message });

            if (CurrentMessage.Count > 100)
                CurrentMessage.RemoveAt(0);
        }
    }
}