﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class ConsultationController : Controller
    {
        // GET: /Consultation/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public ConsultationController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CreateNewConsultation()
        {
            return View();
        }
        public ActionResult EditConsultation(Guid ConsultationID, DataModel.IMedicneModel Medicine,string Tabname, string fullname)
        {

            if (ConsultationID != null && ConsultationID != Guid.Empty)
            {


                var IMedicine = _MedOrSvc.GetConsultationByID(ConsultationID);
                IMedicine.IMconsultationID = ConsultationID;
                
                
                if (IMedicine.IMconsultationModelN != null)
                {
                    IMedicine.IMconsultationModelN.IMconsultationID = ConsultationID;
                }
                if (fullname != null & IMedicine.IMconsultationModelN != null)
                { IMedicine.IMconsultationModelN.FullName = fullname; }

                if (IMedicine == null)
                    IMedicine = new DataModel.IMedicneModel();
               
                if (Tabname != null)
                    return Redirect(Url.Action("EditConsultation", new { ConsultationID = ConsultationID,/* patientID = patientID*/ }) + "#" + Tabname);
                else
                return View(IMedicine);
            }
            return View();
        }
        public ActionResult ADDDescription(string block, Guid ConsultationID)
        {
            Guid UserID = new Guid(HttpContext.User.Identity.Name);
            return View();
        }
        [HttpPost]
        public ActionResult ADDDescription(DataModel.IMedicneModel IMedicneModel,string TapName)

        {

            var success = _MedOrSvc.NewDescription(IMedicneModel);

            //if (success != 0)
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#"+ success);


           // return View();
        }
        [HttpPost]
        public ActionResult EditConsultation(DataModel.IMedicneModel IMedicneModel)
        {
            //var success = _MedOrSvc.NewDescription(IMedicneModel);
            IMedicneModel.CmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            var success = _MedOrSvc.UpdateTapConsultation(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab2");
        }
        [HttpPost]
        public ActionResult EditTapsConsultation(DataModel.IMedicneModel IMedicneModel)
        {

            IMedicneModel.CmodifiedBy= new Guid(HttpContext.User.Identity.Name);
            var success = _MedOrSvc.UpdateTapConsultation(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab12");
        }
        public ActionResult EditTapsComplaint(DataModel.IMedicneModel IMedicneModel)
        {
            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            var success = _MedOrSvc.UpdateTapComplaint(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab4");
        }
        public ActionResult EditReviewSystem(DataModel.IMedicneModel IMedicneModel)
        {
            // var success = _MedOrSvc.UpdateReviewSystem(IMedicneModel);
            IMedicneModel.CmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            _MedOrSvc.UpdateRHEntry(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab5");
        }
        public ActionResult EditPhysicalHistory(DataModel.IMedicneModel IMedicneModel)
        {
            // var success = _MedOrSvc.UpdateReviewSystem(IMedicneModel);
            IMedicneModel.CmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            _MedOrSvc.UpdatePHEntry(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab6");
        }
        public ActionResult EditMedeication(DataModel.IMedicneModel IMedicneModel)
        {
            // var success = _MedOrSvc.UpdateReviewSystem(IMedicneModel);
            IMedicneModel.CmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            _MedOrSvc.UpdateMedication(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab8");
        }
        public ActionResult EditHospitalization(DataModel.IMedicneModel IMedicneModel)
        {
            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            var success = _MedOrSvc.UpdateHospitalizations(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab13");
        }
        public ActionResult EditPaidConsultation(int CFees, Guid ConsultationID, string e, string Currency)
        {
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
            int xCFees = Convert.ToInt32(CFees);
            IMedicneModel.CFees = xCFees;

            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            // IMedicneModel.PatientID = PatientID;
            IMedicneModel.ConsultationIDTap = ConsultationID;
            IMedicneModel.Ccurency = Currency;
            IMedicneModel.InCreatedBy = new Guid(HttpContext.User.Identity.Name);
            if (e == "paid")
            {
                IMedicneModel.CCheckBox = true;
                IMedicneModel.CmodifiedBy = new Guid(HttpContext.User.Identity.Name);

            }
            if (e == "EditPaidConsultation")
            {
                IMedicneModel.CCheckBox =false;
                IMedicneModel.CmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            }
                
            _MedOrSvc.SetFeeConsultation(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = ConsultationID,/* patientID = patientID*/ }) + "#panel_tab14");
        }
        public ActionResult EditDescriptionBiology(string BPconventionalRadiographyNote , string BPEchographyNote, string BPScanNote, 
         string BPMRINote, string BPscintigraphyNote, string BPPetScanNote, string BPOtherNote, Guid ConsultationID,Guid PatientID,DateTime CcreatedOn)
        {


           

            DataModel.IMBiologicalModel IMBiologicalModel = new DataModel.IMBiologicalModel();

            if (BPOtherNote != "" || BPconventionalRadiographyNote != "" || BPEchographyNote != "" || BPScanNote != "" || BPMRINote != "" ||
           
                BPscintigraphyNote != "" || BPPetScanNote != "")
            {
                IMBiologicalModel.conventionalRadiographyNote = BPconventionalRadiographyNote;
                IMBiologicalModel.EchographyNote = BPEchographyNote;
                IMBiologicalModel.ScanNote = BPScanNote;
                IMBiologicalModel.MRINote = BPMRINote;
                IMBiologicalModel.scintigraphyNote = BPscintigraphyNote;
                IMBiologicalModel.PetScanNote = BPPetScanNote;
                IMBiologicalModel.OtherNote = BPOtherNote;

                if (IMBiologicalModel != null)
                {
                    // UpdateBPEchographie 
                    IMBiologicalModel.BiologicalID = Guid.NewGuid();
                    IMBiologicalModel.createdOn = CcreatedOn;
                    IMBiologicalModel.ConsultationID = ConsultationID;
                    IMBiologicalModel.PatientID = PatientID;
                    IMBiologicalModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                    IMBiologicalModel.modifiedBy = new Guid(HttpContext.User.Identity.Name);
                    var result = _MedOrSvc.checkConsultation(ConsultationID);
                    // var resultEchography = _MedOrSvc.checkEchography(consultationID);

                    if (result == null)
                    {
                        //00000000-0000-0000-0000-000000000000
                        IMBiologicalModel.BiologicalID = _MedOrSvc.SetNewBiological(IMBiologicalModel);

                        if (BPOtherNote != "")
                        {
                            _MedOrSvc.SetNewBPOther(IMBiologicalModel);
                        }

                        if (BPconventionalRadiographyNote != "")
                        {
                            _MedOrSvc.SetNewBPconventionalRadiography(IMBiologicalModel);
                        }

                        if (BPEchographyNote != "")
                        {
                            _MedOrSvc.SetNewBPEchography(IMBiologicalModel);
                        }

                        if (BPScanNote != "")
                        {
                            _MedOrSvc.SetNewBPScan(IMBiologicalModel);
                        }

                        if (BPMRINote != "")
                        {
                            _MedOrSvc.SetNewBPMRI(IMBiologicalModel);
                        }

                        if (BPscintigraphyNote != "")
                        {
                            _MedOrSvc.SetNewBPscintigraphy(IMBiologicalModel);
                        }

                        if (BPPetScanNote != "")
                        {
                            _MedOrSvc.SetNewBPPetScan(IMBiologicalModel);
                        }

                    }

                    else
                    {
                        IMBiologicalModel.BiologicalID = result.BiologicalID;



                        //if (BPOtherNote != "")
                        //{
                        //    _MedOrSvc.SetUpdateBPOther(IMBiologicalModel);
                        //}

                        if (BPconventionalRadiographyNote != "")
                        {
                            _MedOrSvc.UpdateBPconventionalRadiography(IMBiologicalModel);
                        }

                        if (BPEchographyNote != "")
                        {
                            _MedOrSvc.UpdateBPEchographies(IMBiologicalModel);
                        }

                        if (BPScanNote != "")
                        {
                            _MedOrSvc.UpdateBPScan(IMBiologicalModel);
                        }

                        if (BPMRINote != "")
                        {
                            _MedOrSvc.SetNewBPMRI(IMBiologicalModel);
                        }

                        if (BPscintigraphyNote != "")
                        {
                            _MedOrSvc.SetNewBPscintigraphy(IMBiologicalModel);
                        }

                        if (BPPetScanNote != "")
                        {
                            _MedOrSvc.SetNewBPPetScan(IMBiologicalModel);
                        }
                    }

                }
            }
                return Redirect(Url.Action("EditConsultation", new { ConsultationID = ConsultationID,/* patientID = patientID*/ }) + "#panel_tab13");
        }
        public ActionResult IMdeleteDescription(Guid DescriptionID, Guid ConsultationID,string Tabname)
        {
             Tabname = _MedOrSvc.DeleteDescription(DescriptionID, Tabname);
            if (Tabname != null)
            return Redirect(Url.Action("EditConsultation", new { id = "", ConsultationID = ConsultationID }) + "#" + Tabname);
            return View();
        }
        public ActionResult ICD10Call(string ICD10s, Guid ConsultationID)
        {
           
              //  return Redirect(Url.Action("EditConsultation", new { id = "", ConsultationID = ConsultationID }) + "#panel_tab2");

            return View();
        }
        public ActionResult UploadFilesTap(HttpPostedFileBase uploadFile, Guid id, string Source)

        {
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
            DataModel.IMHospitalSummaryModel IMHospitalSummaryModel = new DataModel.IMHospitalSummaryModel();
            DataModel.IMFileModel IMFileModel = new DataModel.IMFileModel();


            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            IMFileModel.ConsultationID = id;

            Guid patientID = _MedOrSvc.GetPatientID(id);
            int newfile = 0;

            if (ModelState.IsValid)
            {
                if (uploadFile != null && uploadFile.ContentLength > 0)
                {
                    IMFileModel.Data = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(IMFileModel.Data, 0, uploadFile.ContentLength);
                    IMFileModel.FileName = uploadFile.FileName;
                    IMFileModel.ContentType = uploadFile.ContentType;
                }
                IMFileModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                IMFileModel.createdOn = DateTime.Now;
                IMFileModel.PatientID = patientID;

                if (Source == "Hospitalization")
                {
                    IMHospitalSummaryModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                    IMHospitalSummaryModel.CreatedOn = DateTime.Now;
                    IMHospitalSummaryModel.ConsultationID = id;
                    IMHospitalSummaryModel.PatientID = patientID;
                    IMHospitalSummaryModel.Data = new byte[uploadFile.ContentLength];
                    IMHospitalSummaryModel.ContentType = uploadFile.ContentType;
                    IMHospitalSummaryModel.FileName = uploadFile.FileName;

                    newfile = _MedOrSvc.SetHospitalization(IMHospitalSummaryModel, Source);
                    IMedicneModel.IMconsultationID = id;
                    return Redirect(Url.Action("EditConsultation", new { id = "", ConsultationID = id }) + "#panel_tab11");
                }

                if (Source == null)
                {
                    newfile = _MedOrSvc.SetNewFile(IMFileModel);
                    IMedicneModel.IMconsultationID = id;
                    return Redirect(Url.Action("EditConsultation", new { id = "", ConsultationID = id }) + "#panel_tab11");
                }

            //    if (newfile != 0)
                   
           }
            //   after successfully uploading redirect the user
            return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID }) + "#panel_tab10");
        }
        public ActionResult FileUpload(HttpPostedFileBase uploadFile, Guid id, string Source, Guid FileID_BP, DateTime ccreatedon)

        {
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
            DataModel.IMHospitalSummaryModel IMHospitalSummaryModel = new DataModel.IMHospitalSummaryModel();
            DataModel.IMFileModel IMFileModel = new DataModel.IMFileModel();
            DataModel.Conventional_RadiographyModel Conventional_RadiographyModel = new DataModel.Conventional_RadiographyModel();
            DataModel.IMBiologicalModel IMBiologicalModel = new DataModel.IMBiologicalModel();
            DataModel.BP_FilesModel BP_FilesModel = new DataModel.BP_FilesModel();
            Guid FileID = Guid.NewGuid();

            IMFileModel.ConsultationID = id;
            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            Guid patientID = _MedOrSvc.GetPatientID(id);
            int newfile = 0;

            if (ModelState.IsValid)
            {
                if (uploadFile != null && uploadFile.ContentLength > 0)
                {

                    IMFileModel.Data = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(IMFileModel.Data, 0, uploadFile.ContentLength);
                    IMFileModel.FileName = uploadFile.FileName;
                    IMFileModel.ContentType = uploadFile.ContentType;

                }
                IMFileModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                IMFileModel.createdOn = DateTime.Now;
                IMFileModel.PatientID = patientID;

                if (Source == "Hospitalization")
                {
                    IMHospitalSummaryModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                    IMHospitalSummaryModel.CreatedOn = DateTime.Now;
                    IMHospitalSummaryModel.ConsultationID = id;
                    IMHospitalSummaryModel.PatientID = patientID;
                    IMHospitalSummaryModel.Data = new byte[uploadFile.ContentLength];
                    IMHospitalSummaryModel.ContentType = uploadFile.ContentType;
                    IMHospitalSummaryModel.FileName = uploadFile.FileName;

                    newfile = _MedOrSvc.SetHospitalization(IMHospitalSummaryModel, Source);
                    IMedicneModel.IMconsultationID = id;
                }

                if (Source != "Hospitalization" & Source != "")
                {

                    IMBiologicalModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                    IMBiologicalModel.ConsultationID = id;
                    IMBiologicalModel.PatientID = patientID;

                    IMBiologicalModel.createdOn = DateTime.Now;

                    BP_FilesModel.Data = new byte[uploadFile.ContentLength];
                    BP_FilesModel.ContentType = uploadFile.ContentType;

                    BP_FilesModel.FileName = uploadFile.FileName;


                    //check IMBiology if new
                    var result1 = _MedOrSvc.checkConsultation(id);
                    if (result1 == null)// if IMBiology Not yet Created
                    {
                        IMBiologicalModel.BiologicalID = Guid.NewGuid();
                        IMBiologicalModel.BiologicalID = _MedOrSvc.SetNewBiological(IMBiologicalModel);
                    }//if resultF == null OR if the IMBiology not yet created

                    else //if IMBiology is created
                    {
                        IMBiologicalModel.BiologicalID = result1.BiologicalID;
                    }

                    var resultF = _MedOrSvc.checkFileID_BP(FileID_BP, Source);
                    Guid gt = new Guid("00000000-0000-0000-0000-000000000000");

                    if (resultF == gt)//if conventional or scan ... not yet created
                    {
                        if (Source == "BPCFiles")

                            FileID = _MedOrSvc.SetNewBPconventionalRadiography(IMBiologicalModel);

                        if (Source == "Echography")
                            FileID = _MedOrSvc.SetNewBPEchography(IMBiologicalModel);

                        if (Source == "Scan")
                            FileID = _MedOrSvc.SetNewBPScan(IMBiologicalModel);

                        if (Source == "MRI")
                            FileID = _MedOrSvc.SetNewBPMRI(IMBiologicalModel);

                        if (Source == "Other")
                            FileID = _MedOrSvc.SetNewBPOther(IMBiologicalModel);

                        if (Source == "PetScan")
                            FileID = _MedOrSvc.SetNewBPPetScan(IMBiologicalModel);

                        if (Source == "scintigraphy")
                            FileID = _MedOrSvc.SetNewBPscintigraphy(IMBiologicalModel);

                        BP_FilesModel.BP_ID = FileID;
                    }//if conventional or scan ... not yet created

                    else // if scan or petscan... created
                    {
                        BP_FilesModel.BP_ID = resultF;
                    }

                    _MedOrSvc.SetNewBP_Files(BP_FilesModel, Source);
                    IMedicneModel.IMconsultationID = id;
                    newfile = 1;

                }

                if (Source == null)
                {
                    newfile = _MedOrSvc.SetNewFile(IMFileModel);
                    IMedicneModel.IMconsultationID = id;
                }

                if (newfile != 0)

                    return Redirect(Url.Action("EditConsultation", new { id = "", patientID = patientID, ConsultationID = id, FileID1 = FileID }) + "#panel_tab13");
            }
            //   after successfully uploading redirect the user
            return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID }) + "#panel_tab13");
        }
        public ActionResult IMdeleteItem(Guid ConsultationID, Guid ID,string fullname)
        {
            var deleted = _MedOrSvc.IMdeleteItem(ConsultationID, ID);
            return Redirect(Url.Action("EditConsultation", new { id = "", ConsultationID = ConsultationID, fullname=fullname }) + deleted);
   
        }
        public ActionResult EditTapsMnagement(DataModel.IMedicneModel IMedicneModel)
        {
            IMedicneModel.CHmodifiedBy = new Guid(HttpContext.User.Identity.Name);
            var success = _MedOrSvc.UpdateTapManagement(IMedicneModel);
            return Redirect(Url.Action("EditConsultation", new { ConsultationID = IMedicneModel.IMconsultationID,/* patientID = patientID*/ }) + "#panel_tab13");
        }

        [HttpPost]
        public JsonResult ImedicineJson( DateTime CcreatedOn,string ICD10, string ICD10Note, Guid consultationID, Guid PatientID,string fullname)
        {
            DataModel.ICD10PatientModel ICD10PatientModel = new DataModel.ICD10PatientModel();
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
  
            //if (CHtitle !="" || CHnote != "")
            string source = "";
            var ListICD10 = ICD10.Split('|');
            var ListICD10Note = ICD10Note.Split('|');

            List<string> ListICD101 = new List<string>();
            List<string> ListICD101Note = new List<string>();
            for (int i = 0; i < ListICD10.Length; i++)
            {
                ListICD10[i] = ListICD10[i].Replace("null", "");
                ListICD10[i] = ListICD10[i].Replace("]", "");
                ListICD10[i] = ListICD10[i].Replace("[", "");
                ListICD10[i] = ListICD10[i].Replace("\"", "");
                if (ListICD10[i] != "")
                { ListICD101.Add(ListICD10[i]); }
            }
            for (int i = 0; i < ListICD10Note.Length; i++)
            {
                ListICD10Note[i] = ListICD10Note[i].Replace("null", "");
                ListICD10Note[i] = ListICD10Note[i].Replace("]", "");
                ListICD10Note[i] = ListICD10Note[i].Replace("[", "");
                ListICD10Note[i] = ListICD10Note[i].Replace("\"", "");
                if (ListICD10[i] != "")
                { ListICD101Note.Add(ListICD10Note[i]); }
            }
            int updateMedical = 0;
            for (int i = 0; i < ListICD101.Count; i++)
            {
                ICD10PatientModel.ICD10PatientID = Guid.NewGuid();
                ICD10PatientModel.PatientID = PatientID;
                ICD10PatientModel.IMconsultationID = consultationID;
                ICD10PatientModel.CodeDescriptionICD10 = ListICD10[i];
                ICD10PatientModel.Note = ListICD10Note[i];
                ICD10PatientModel.CreatedOn = CcreatedOn;
                ICD10PatientModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                updateMedical = _MedOrSvc.SetIMDiagnostic(ICD10PatientModel);
            }
            //if (PanelTaps == null)
                return Json(Url.Action("EditConsultation", new { id = "", PatientID = PatientID, ConsultationID = consultationID, CcreatedOn = CcreatedOn, fullname= fullname, }) + "#panel_tab8");

        }
    }
}
