﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class PediatricMedicalController : Controller
    {
        //
        // GET: /MedicalHistory/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public PediatricMedicalController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View MedicalHistory")]
        public ActionResult Medical(String firstname, String lastname, Guid patientID)
        {
            if (patientID != null && patientID != Guid.Empty)
            {
                var medical = _MedOrSvc.GetMedicalInfoAndHistoryByPatientID(patientID);
                if (medical == null)
                    medical = new DataModel.PediatricMedicalModel();
                medical.FirstName = firstname;
                medical.LastName = lastname;
                return View(medical);
            }
            else
                return View();
        }

        [HttpPost]
        [Comm.AuthorizeUser(AccessLevel = "Create MedicalHistory")]
        [Comm.AuthorizeUser(AccessLevel = "Edit MedicalHistory")]
        public ActionResult Medical(DataModel.PediatricMedicalModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                var updateMedical = _MedOrSvc.UpdateMedicalInfoAndHistory(model);

                if (updateMedical != 0)
                    return Redirect("/Patient/ListOfPatients");
            }
            return View(model);
        }
    }
}
