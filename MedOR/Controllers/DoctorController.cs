﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class DoctorController : Controller
    {
        //
        // GET: /Doctor/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public DoctorController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "View Doctors")]
        public ActionResult ListOfDoctors()
        {
            var doctors = _MedOrSvc.GetDoctorsByUserID(new Guid(HttpContext.User.Identity.Name));
            return View(doctors);
        }

        [Comm.AuthorizeUser(AccessLevel = "Create Doctors")]
        public ActionResult CreateNewDoctor()
        {
            var model = _MedOrSvc.GetSpecialties();
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateNewDoctor(DataModel.DoctorModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                var newDoctor = _MedOrSvc.AddNewDoctor(model);

                if (newDoctor != 0)
                    return Redirect("/Doctor/ListOfDoctors");
            }
            model = _MedOrSvc.GetSpecialties();
            return View(model);
        }

        [Comm.AuthorizeUser(AccessLevel = "Edit Doctors")]
        public ActionResult EditDoctor(Guid id)
        {
            var model = _MedOrSvc.GetDoctorByID(id);
            if (model == null)
            {
                // return HttpNotFound();...
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult EditDoctor(DataModel.DoctorModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = new Guid(HttpContext.User.Identity.Name);
                model.ModifiedOn = DateTime.Now;
                var editDoctor = _MedOrSvc.UpdateDoctor(model);

                if (editDoctor != 0)
                    return Redirect("/Doctor/ListOfDoctors");
            }
            return View();
        }

        [Comm.AuthorizeUser(AccessLevel = "Delete Doctors")]
        public ActionResult DeleteDoctor(Guid id)
        {
            var deleted = _MedOrSvc.DeleteDoctorByID(id);
            if (deleted != 0)
                return Redirect("/Doctor/ListOfDoctors");
            return View();
        }
    }
}
