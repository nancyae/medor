﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;

namespace MedOR.Controllers
{
    [Authorize]
    public class AccountingController : Controller
    {
        //
        // GET: /Consultation/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public AccountingController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Account()
        {

            var Accounts = _MedOrSvc.GetAllPendingPayment(new Guid(HttpContext.User.Identity.Name));


            return View(Accounts);
        }
        [HttpGet]
        public ActionResult AccountPaid(Guid ConsultationID)
        {
            var Accounts = _MedOrSvc.PaidConsultation(ConsultationID);
            if (Accounts==1)
            return Redirect(Url.Action("Account", new { id = "" }));
            return View(Accounts);

        }

     // [Comm.AuthorizeUser(AccessLevel = "View Accounting")]
     [HttpGet]
        public ActionResult ListOfAccounting(DataModel.AccountingModel AccountingModel)
        {
            //DataModel.AccountingModel AccountingModel = new DataModel.AccountingModel();
            //SetTime = SetTime.Date;
            //TillDate = TillDate.Date;
            //AccountingModel.SetTime = SetTime;
            //AccountingModel.TillDate = TillDate;

            var Accounts = _MedOrSvc.ListAccounting(new Guid(HttpContext.User.Identity.Name), AccountingModel);
          
            return View(Accounts);
        }
        
        

        //  [Comm.AuthorizeUser(AccessLevel = "View Expense")]
        public ActionResult ListOfExpense()
        {
            var Expense = _MedOrSvc.GetAllExpense(new Guid(HttpContext.User.Identity.Name));
            
            return View(Expense);
        }

        public ActionResult CreateNewExpense()
        {
            return View();
        }
        [HttpPost]
       // [Comm.AuthorizeUser(AccessLevel = "Create Expense")]
        public ActionResult CreateNewExpense(DataModel.ExpenseModel model)
        {
           
            if (ModelState.IsValid)
            {
          

                model.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                model.CreatedON = DateTime.Now;
                model.UserClinicID= new Guid("57CED3EB-0DF4-4A39-8075-CE3B7C22A728");
                var newUser = _MedOrSvc.AddNewExpense(model);

                if (newUser != 0)
                    return Redirect("/Accounting/ListOfExpense");
            }
            return View(model);
        }

    [Comm.AuthorizeUser(AccessLevel = "View Accounting")]
        public ActionResult Accounts()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetAppointments(Guid ClinicID)
        {
            var eventList = new List<DataModel.AppointmentModel>();
            var appointments = _MedOrSvc.GetAppointmentsByUserIDAndClinicID(new Guid(HttpContext.User.Identity.Name), ClinicID);
            //DateTime currStart;
            //DateTime currEnd;
            //foreach (var appointment in appointments)
            //{
            //    currStart = Convert.ToDateTime(appointment.StartDateTime);
            //    currEnd = Convert.ToDateTime(appointment.EndDateTime);
            //    eventList.Add(new DataModel.AppointmentModel()
            //    {
            //        id = appointment.AppointmentID.ToString(),
            //        title = appointment.Title,
            //        start = currStart.ToString("s"),
            //        end = currEnd.ToString("s"),
            //        allDay = appointment.AllDay,
            //        // url = "/Schedule/Details/" + oc.ID.ToString() + "/"
            //    });
            //}

            var rows = eventList.ToArray();

            return Json(rows, JsonRequestBehavior.AllowGet);
        }


    }
}
