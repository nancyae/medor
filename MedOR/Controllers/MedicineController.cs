﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;


namespace MedOR.Controllers
{
    public class MedicineController : Controller
    {   Guid neConsultationID = Guid.NewGuid();
        #region Constructor
        private DataServices.Service _MedOrSvc;

        public MedicineController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion
        //GET: /Medicine/
        //    [Comm.AuthorizeUser(AccessLevel = "View MedicalHistory")]
        public ActionResult IMedicine(String firstname, String lastname, Guid patientID, Guid ConsultationID, 
                                      Guid FileID1,string Tapname, DateTime CcreatedOn,string valueparameter,Guid? ParameterDateID)

        {
              Guid UserID = new Guid(HttpContext.User.Identity.Name);
            if (patientID != null && patientID != Guid.Empty)
             {
                var IMedicine = _MedOrSvc.GetMedicalInternalByPatientID(patientID, UserID);
                 var groupedList = IMedicine.Dyn_Parameter_ValueModelst.GroupBy(x => (x.ParameterDateID))
                           .Select(x =>
                                        new {
                                            ParameterDateID = x.Key,
                                            Items = x,
                                           
                                            Count = x.Count()
                                        }
                                  );
                var i = 1;
                IList<DataModel.ParameterValueDateModel> ParameterValueDateModel = new List<DataModel.ParameterValueDateModel>();
               
                    if (IMedicine == null)
                    IMedicine = new DataModel.IMedicneModel();
                    IMedicine.FirstName = firstname;
                    IMedicine.LastName = lastname;
                if(CcreatedOn !=null)
                IMedicine.CcreatedOn = CcreatedOn;
                if (ParameterDateID != null)
                    IMedicine.ParameterDateID = ParameterDateID.Value;
                if (FileID1 != null)
                    IMedicine.ConsultationIDTap = ConsultationID;
                IMedicine.FileID_BP = FileID1;
                IMedicine.DaynParametersvalue= valueparameter;
                //if (Tapname == null)
                //    Tapname = "#panel_tab12";
                if (Tapname !=null)
                    return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID, CcreatedOn= CcreatedOn, ConsultationID = ConsultationID, FileID1 = Guid.Empty }) + "#"+ Tapname);
                //return View(IMedicine + Tapname) ;
                else {return View(IMedicine ); }
                
            }
            else
                return View();
        }
        public ActionResult IMdeleteItem(Guid patientID, Guid ID, DateTime CcreatedOn)
        {
            var deleted = _MedOrSvc.IMdeleteItem(patientID, ID);
           // if (deleted != 0)
                //return Redirect("/Patient/ListOfPatients");
            return Redirect(Url.Action("IMedicine", new { id = "", patientID= patientID ,ConsultationID=ID, FileID1 = Guid.Empty, CcreatedOn = CcreatedOn }) + "#panel_tab12");
             //  return View();
            //return View($"{Url.Action("EditConsultation", new { IMedicine = IMedicine })}#panel_tab9");
        }
        public ActionResult IMdeleteDiabete(Guid DescriptionID)
        {
            string valuetap = " ";
            string deleted = _MedOrSvc.DeleteDescription(DescriptionID, valuetap);
            if (deleted != null)
                return Redirect("/Patient/ListOfPatients");
                //return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID }) + "#panel_tab12");
            return View();
        }        
        public ActionResult AddDescription(string DiabeteDescription, string block1, string block2, string block3, string block4,Guid ConsultationID)
        {
            DataModel.IMCRVascularModel IMCRVascularModel = new DataModel.IMCRVascularModel();
            //IMCRVascularModel.ConsultationID = newConsultationID;
            //IMCRVascularModel.PatientID = PatientID;

            IMCRVascularModel.DiabetesDescription = DiabeteDescription;       
            IMCRVascularModel.DyslipidemiaDescription = block1;
            IMCRVascularModel.AlcoholDescription = block2;
   
            IMCRVascularModel.ObesityDescription = block3;
            IMCRVascularModel.FamilialHistoryDescription = block4;

            IMCRVascularModel.createdOn = DateTime.Now;
                       
            //IMCRVascularModel.CRVascularID = _MedOrSvc.SetNewCardiovascular(IMCRVascularModel);
            _MedOrSvc.SetNewDiabetes(IMCRVascularModel);
            _MedOrSvc.SetNewDyslipidemia(IMCRVascularModel);
            _MedOrSvc.SetNewAlcohol(IMCRVascularModel);
            _MedOrSvc.SetNewObesity(IMCRVascularModel);
            _MedOrSvc.SetNewFamilialHistory(IMCRVascularModel);

                return Redirect("/Patient/ListOfPatients");
            //return View();

        }
        public ActionResult ConsultationDetails(Guid ConsultationID)
        {
                Guid UserID = new Guid(HttpContext.User.Identity.Name);
                if (ConsultationID != null && ConsultationID != Guid.Empty)
                {
                    var IMedicineModifiedModel = _MedOrSvc.GetLastModifiedConsultationByID(ConsultationID);
                    if (IMedicineModifiedModel == null)
                    IMedicineModifiedModel = new DataModel.IMedicineModifiedModel();
                    return View(IMedicineModifiedModel);
                }
                return View();
        }
        public ActionResult DynamicValuesInput(Guid ConsultationID, DateTime CcreatedOn, string FirstName, string lastname, Guid PatientID, 
            string valueparameter1, Guid DaynParametersvalueID, DateTime DateParametersvalue, Guid ParameterDateID)
        {
           
        DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();

            ParameterDateID = _MedOrSvc.DynamicInput(valueparameter1, ConsultationID, PatientID, DateParametersvalue, DaynParametersvalueID, ParameterDateID);


        



            if (ParameterDateID != Guid.Empty)
            IMedicneModel.ParameterDateID = ParameterDateID;

            return Redirect(Url.Action("IMedicine", new { id = "", patientID = PatientID, ConsultationID = ConsultationID, FileID1 = Guid.Empty, CcreatedOn = CcreatedOn, ParameterDateID = ParameterDateID }) + "#" + "panel_tab13");


        }
        public ActionResult DynamicValues(Guid ConsultationID , DateTime CcreatedOn, string FirstName, string lastname, Guid PatientID,string valueparameter, Guid ParameterDateID)
        {

            //    DataModel.IMFileModel FileModel = new DataModel.IMFileModel();
               valueparameter = _MedOrSvc.Dynamic(valueparameter, ConsultationID, PatientID);
            return Redirect(Url.Action("IMedicine", new { id = "", patientID = PatientID, ConsultationID = ConsultationID, FileID1 = Guid.Empty, valueparameter= valueparameter, CcreatedOn=CcreatedOn, ParameterDateID = ParameterDateID }) + "#" + "panel_tab13");

           
        }
        [HttpPost]
        public void Upload()
        {
            for (int i = 0; i < Request.Files.Count; i++)
            {
              
            }
        }
        public ActionResult UploadFilesTap(HttpPostedFileBase uploadFile, Guid id, string Source, DateTime? ccreatedon)

        {
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
            DataModel.IMHospitalSummaryModel IMHospitalSummaryModel = new DataModel.IMHospitalSummaryModel();
            DataModel.IMFileModel IMFileModel = new DataModel.IMFileModel();

            IMFileModel.ConsultationID = id;

            Guid patientID = _MedOrSvc.GetPatientID(id);
            int newfile = 0;

            if (ModelState.IsValid)
            {
                if (uploadFile != null && uploadFile.ContentLength > 0)
                {

                    IMFileModel.Data = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(IMFileModel.Data, 0, uploadFile.ContentLength);
                    IMFileModel.FileName = uploadFile.FileName;
                    IMFileModel.ContentType = uploadFile.ContentType;

                }
                IMFileModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                IMFileModel.createdOn = DateTime.Now;
                IMFileModel.PatientID = patientID;

                if (Source == "Hospitalization")
                {
                    IMHospitalSummaryModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                    IMHospitalSummaryModel.CreatedOn = DateTime.Now;
                    IMHospitalSummaryModel.ConsultationID = id;
                    IMHospitalSummaryModel.PatientID = patientID;
                    IMHospitalSummaryModel.Data = new byte[uploadFile.ContentLength];
                    IMHospitalSummaryModel.ContentType = uploadFile.ContentType;
                    IMHospitalSummaryModel.FileName = uploadFile.FileName;

                    newfile = _MedOrSvc.SetHospitalization(IMHospitalSummaryModel, Source);
                    IMedicneModel.IMconsultationID = id;
                }

                if (Source == null)
                {
                    newfile = _MedOrSvc.SetNewFile(IMFileModel);
                    IMedicneModel.IMconsultationID = id;
                }

                if (newfile != 0)
                    return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID, ConsultationID = id ,FileID1 = Guid.Empty, CcreatedOn = ccreatedon }) + "#panel_tab11");
            }
            //   after successfully uploading redirect the user
            return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID }) + "#panel_tab10");
        }
        public ActionResult FileUpload(HttpPostedFileBase uploadFile , Guid id,string Source,Guid FileID_BP, DateTime ccreatedon)

        {
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
            DataModel.IMHospitalSummaryModel IMHospitalSummaryModel = new DataModel.IMHospitalSummaryModel();
            DataModel.IMFileModel IMFileModel = new DataModel.IMFileModel();
            DataModel.Conventional_RadiographyModel Conventional_RadiographyModel = new DataModel.Conventional_RadiographyModel();
            DataModel.IMBiologicalModel IMBiologicalModel = new DataModel.IMBiologicalModel();
            DataModel.BP_FilesModel BP_FilesModel = new DataModel.BP_FilesModel();
            Guid FileID = Guid.NewGuid();

            IMFileModel.ConsultationID= id;
           
            Guid patientID = _MedOrSvc.GetPatientID(id);
            int newfile =0;

            if (ModelState.IsValid)
            {
                if (uploadFile != null && uploadFile.ContentLength > 0)
                {

                    IMFileModel.Data = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(IMFileModel.Data, 0, uploadFile.ContentLength);
                    IMFileModel.FileName = uploadFile.FileName;
                    IMFileModel.ContentType = uploadFile.ContentType;

                }
                IMFileModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                IMFileModel.createdOn = DateTime.Now;
                IMFileModel.PatientID = patientID;

                if(Source== "Hospitalization")
                {
                    IMHospitalSummaryModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                    IMHospitalSummaryModel.CreatedOn = ccreatedon;
                    IMHospitalSummaryModel.ConsultationID = id;
                    IMHospitalSummaryModel.PatientID = patientID;
                    IMHospitalSummaryModel.Data = new byte[uploadFile.ContentLength];
                    IMHospitalSummaryModel.ContentType = uploadFile.ContentType;
                    IMHospitalSummaryModel.FileName = uploadFile.FileName;
                  
                    newfile = _MedOrSvc.SetHospitalization(IMHospitalSummaryModel,Source);
                    IMedicneModel.IMconsultationID = id;
                }

                if (Source != "Hospitalization" & Source != "")
                {

                    IMBiologicalModel.createdBy = new Guid(HttpContext.User.Identity.Name);
                    IMBiologicalModel.ConsultationID = id;
                    IMBiologicalModel.PatientID = patientID;

                    IMBiologicalModel.createdOn = ccreatedon;

                    BP_FilesModel.Data = new byte[uploadFile.ContentLength];
                    BP_FilesModel.ContentType = uploadFile.ContentType;

                    BP_FilesModel.FileName = uploadFile.FileName;


                    //check IMBiology if new
                    var result1 = _MedOrSvc.checkConsultation(id);
                    if (result1 == null)// if IMBiology Not yet Created
                    {
                        IMBiologicalModel.BiologicalID = Guid.NewGuid();
                        IMBiologicalModel.BiologicalID = _MedOrSvc.SetNewBiological(IMBiologicalModel);
                    }//if resultF == null OR if the IMBiology not yet created

                    else //if IMBiology is created
                    {
                        IMBiologicalModel.BiologicalID = result1.BiologicalID;
                    }

                    var resultF = _MedOrSvc.checkFileID_BP(FileID_BP, Source);
                    Guid gt = new Guid("00000000-0000-0000-0000-000000000000");

                    if (resultF == gt)//if conventional or scan ... not yet created
                    {
                        if (Source == "BPCFiles")

                            FileID = _MedOrSvc.SetNewBPconventionalRadiography(IMBiologicalModel);
                      
                        if (Source == "Echography")
                            FileID = _MedOrSvc.SetNewBPEchography(IMBiologicalModel);

                        if (Source == "Scan")
                            FileID = _MedOrSvc.SetNewBPScan(IMBiologicalModel);

                        if (Source == "MRI")
                            FileID = _MedOrSvc.SetNewBPMRI(IMBiologicalModel);

                        if (Source == "Other")
                            FileID = _MedOrSvc.SetNewBPOther(IMBiologicalModel);

                        if (Source == "PetScan")
                            FileID = _MedOrSvc.SetNewBPPetScan(IMBiologicalModel);

                        if (Source == "scintigraphy")
                            FileID = _MedOrSvc.SetNewBPscintigraphy(IMBiologicalModel);

                        BP_FilesModel.BP_ID = FileID;
                    }//if conventional or scan ... not yet created

                    else // if scan or petscan... created
                    {
                        BP_FilesModel.BP_ID = resultF;
                    }

                    _MedOrSvc.SetNewBP_Files(BP_FilesModel, Source);
                    IMedicneModel.IMconsultationID = id;
                    newfile = 1;

                }
             
                if (Source == null)
                {
                     newfile = _MedOrSvc.SetNewFile(IMFileModel);
                    IMedicneModel.IMconsultationID = id;
                }

                if (newfile != 0)
                  
               return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID, ConsultationID= id,FileID1= FileID, CcreatedOn= ccreatedon }) + "#panel_tab13");
            }
         //   after successfully uploading redirect the user
            return Redirect(Url.Action("IMedicine", new { id = "", patientID = patientID }) + "#panel_tab13");
        }
        [HttpGet]  
        public FileResult DownLoadFile(Guid id,string name)  
            {
            DataModel.IMFileModel FileModel = new DataModel.IMFileModel();
           
                //FileModel.FileID = id;
                var IMedicine = _MedOrSvc.GetFileByID(id,name);
           
            return File(IMedicine.Data, "application/pdf", IMedicine.FileName);

        }
        [HttpPost]
        public JsonResult ImedicineJson(Guid DoctorID, string Ctitle, string Cnote, DateTime CcreatedOn,
        string CHtitle, string CHnote,
                                   string ICD10, string ICD10Note,
                                   string MMedicationName, DateTime MDateStart, DateTime MDateStop, string MDoze, Boolean MMorning, Boolean MAfternoon,
                                    Boolean MEvening, Boolean MBedTime, Boolean MAsNeeded, string MHowMuch,string MReason,string MAdditionalInformation, Guid PatientID,
                                   string HSAdmissionName, DateTime HSAdmissionDate, DateTime HSDeparture, int LengthStay, string HSDescription,
                                   Boolean CRVDiabetes, Boolean CRVDyslipidemia, Boolean CRVAlcohol, string block, string block1, string block2, string block3, string block4, Boolean CRVObesity, Boolean CRVFamilialHistory,
                                    string MHAllergies, string MHGynecology, string MHMedical, string MHFamily, string MHSurgical,
                                   string Anorexia, string Fever, string Weight, string Fatigue,
                                   Boolean RSHEENT, Boolean RSPulmonary, Boolean RSCardiovascular,
                                   string RSHEENTDescription, string RSPulminaryDescription, string RSCardiovascularDescription,
                                   Boolean RSDigestive, Boolean RSUrogenital, Boolean RSNervouSystem,
                                   string RSDigestiveDescription, string RSUrogenitalDescription, string RSNervousSystemDescription,
                                   Boolean RSSkin, Boolean RSMusculoskeletal, Boolean RSPsychosocial,
                                   string RSSkinDescription, string RSMusculoskeletalDescription, string RSPsychosocialDescription
                 , string RespiratoryRate, string BloodPressure, string PHWeight,
                   Boolean PHHEENT, Boolean PHPulmonary, Boolean PHCardiovascular,
                   string PHHEENTDescription, string PHPulminaryDescription, string PHCardiovascularDescription,
                   Boolean PHDigestive, Boolean PHUrogenital, Boolean PHNervouSystem,
                   string PHDigestiveDescription, string PHUrogenitalDescription, string PHNervousSystemDescription,
                   Boolean PHSkin, Boolean PHMusculoskeletal, Boolean PHPsychosocial,
                   string PHSkinDescription, string PHMusculoskeletalDescription, string PHPsychosocialDescription
, Boolean BPconventionalRadiography, Boolean BPEchography, Boolean BPScan,
string BPconventionalRadiographyNote, string BPEchographyNote, string BPScanNote,
Boolean BPMRI, Boolean BPscintigraphy, Boolean BPPetScan,
string BPMRINote, string BPscintigraphyNote, string BPPetScanNote,
Boolean BPOther, string BPOtherNote,
 string Mdescription,
string Currency, string CFees,
string PanelTaps,Guid consultationID,Guid FileID_BP
//string ColumnDynamicHeader,string RowDynamicHeader, string contentRow
            )
        {
           
            DataModel.ICD10PatientModel ICD10PatientModel = new DataModel.ICD10PatientModel();
            DataModel.IMedicneModel IMedicneModel = new DataModel.IMedicneModel();
            DataModel.IMmedicalHistoryModel IMmedicalHistoryModel = new DataModel.IMmedicalHistoryModel();
            DataModel.IMMedicationModel IMMedicationModel = new DataModel.IMMedicationModel();
            DataModel.IMManagement IMManagementModel = new DataModel.IMManagement();
            DataModel.IMHospitalSummaryModel HospitalizationModel = new DataModel.IMHospitalSummaryModel();
            DataModel.IMCRVascularModel IMCRVascularModel = new DataModel.IMCRVascularModel();
            DataModel.IMreviewSystemModel IMreviewSystemModel = new DataModel.IMreviewSystemModel();
            DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel = new DataModel.IMPhysicalHistoryModel();
            DataModel.IMcomplaintHistory IMcomplaintHistory = new DataModel.IMcomplaintHistory();
            DataModel.IMBiologicalModel IMBiologicalModel = new DataModel.IMBiologicalModel();

            Guid FileID1 = Guid.NewGuid();

            //if (CHtitle !="" || CHnote != "")
            string source = "";
            Guid newConsultationID = Guid.NewGuid();
            if (Ctitle == "" || Cnote == "")
            newConsultationID = consultationID;
            IMedicneModel.CcreatedBy = new Guid(HttpContext.User.Identity.Name);



            //DoctorID = Guid.Empty;
            IMedicneModel.SelectedDoctorID = DoctorID ;
            IMedicneModel.IMconsultationID = newConsultationID;
            ICD10PatientModel.PatientID = PatientID;
            if (Ctitle != "" || Cnote != "")
            { 
                IMedicneModel.Cnote = Cnote;
            IMedicneModel.Ctitle = Ctitle;
            IMedicneModel.CHnote = CHnote;
            IMedicneModel.CHtitle = CHtitle;
                IMedicneModel.CcreatedOn = CcreatedOn;
            _MedOrSvc.SetIMConsultation(IMedicneModel, ICD10PatientModel, IMMedicationModel);
            }
            // Complaint History
            if (CHnote != "" || CHtitle != "")
            {
                IMcomplaintHistory.note = CHnote;
                IMcomplaintHistory.title = CHtitle;
                IMcomplaintHistory.ConsultationID = newConsultationID;
                IMcomplaintHistory.PatientID = PatientID;
                IMcomplaintHistory.createdBy = IMedicneModel.CcreatedBy;
                IMcomplaintHistory.createdOn = CcreatedOn;
               _MedOrSvc.SetNewComlaint(IMcomplaintHistory);
            }

            // Medication 
            if (MMedicationName != "")
            {
                IMMedicationModel.ConsultationID = newConsultationID;
                IMMedicationModel.PatientID = PatientID;
                IMMedicationModel.MedicationName = MMedicationName;
                IMMedicationModel.DateStart = MDateStart;
                IMMedicationModel.DateStop = MDateStop;
                IMMedicationModel.Doze = MDoze;
                IMMedicationModel.Morning = MMorning;
                IMMedicationModel.Afternoon = MAfternoon;
                IMMedicationModel.Evening = MEvening;
                IMMedicationModel.BedTime = MBedTime;
                IMMedicationModel.AsNeeded = MAsNeeded;
                IMMedicationModel.HowMuch = MHowMuch;

                IMMedicationModel.AdditionalInformation = MAdditionalInformation;
                IMMedicationModel.Reason = MReason;
                IMMedicationModel.CreatedBy = IMedicneModel.CcreatedBy;
                IMMedicationModel.CreatedOn = CcreatedOn;

                _MedOrSvc.SetNewMedicationntModel(IMMedicationModel);
            }

            if (Mdescription != "")
            {
                IMManagementModel.Description = Mdescription;
                IMManagementModel.createdOn = CcreatedOn;
                IMManagementModel.ConsultationID = newConsultationID;
                IMManagementModel.PatientID = PatientID;
                IMManagementModel.createdBy = IMedicneModel.CcreatedBy;

                _MedOrSvc.SetIMManagement(IMManagementModel);
            }

            if (HSDescription != "" || HSAdmissionName != "")
            {
              
                HospitalizationModel.Description = HSDescription;
                HospitalizationModel.CreatedOn = CcreatedOn;
                HospitalizationModel.ConsultationID = newConsultationID;
                HospitalizationModel.PatientID = PatientID;
                HospitalizationModel.CreatedBy = IMedicneModel.CcreatedBy;
                HospitalizationModel.AdmissionDate = HSAdmissionDate;
                HospitalizationModel.Departure = HSDeparture;
                HospitalizationModel.Description = HSDescription;
                HospitalizationModel.LengthStay = LengthStay;
                HospitalizationModel.AdmissionName = HSAdmissionName;
               _MedOrSvc.SetHospitalization(HospitalizationModel,source);
            }
            //Medical History
          
            if (MHAllergies != "" || MHGynecology != "" || MHMedical != "" || MHFamily != "" || MHSurgical != "")
            {

                IMmedicalHistoryModel.createdOn = CcreatedOn;
                IMmedicalHistoryModel.createdBy = IMedicneModel.CcreatedBy;
                //IMmedicalHistoryModel.MedicalHistoryID = newConsultationID;
                IMmedicalHistoryModel.PatientID = PatientID;
                IMmedicalHistoryModel.ConsultationID = newConsultationID;

                IMmedicalHistoryModel.Allergies = MHAllergies;
                IMmedicalHistoryModel.Gynecology = MHGynecology;
                IMmedicalHistoryModel.Medical = MHMedical;
                IMmedicalHistoryModel.Family = MHFamily;
                IMmedicalHistoryModel.Surgical = MHSurgical;
               
                IMmedicalHistoryModel.MedicalHistoryID= _MedOrSvc.SetNewMedicalHistory(IMmedicalHistoryModel);
                if (MHAllergies != "")
                { _MedOrSvc.SetNewAllergies(IMmedicalHistoryModel); }

                if (MHGynecology != "")
                { _MedOrSvc.SetNewGynecology(IMmedicalHistoryModel); }

                if (MHMedical != "")
                { _MedOrSvc.SetNewMedical(IMmedicalHistoryModel); }

                if (MHFamily != "")
                { _MedOrSvc.SetNewFamily(IMmedicalHistoryModel); }

                if (MHSurgical != "")
                { _MedOrSvc.SetNewSurgical(IMmedicalHistoryModel); }
            }

            //CRVascular
                IMCRVascularModel.DiabetesDescription = block;
                IMCRVascularModel.DyslipidemiaDescription = block1;
                IMCRVascularModel.AlcoholDescription = block2;
                IMCRVascularModel.ObesityDescription = block3;
                IMCRVascularModel.FamilialHistoryDescription = block4;

                if (block != "" || block1 != "" || block2 != "" || block3 != "" || block4 != "")
                {
                    IMCRVascularModel.PatientID = PatientID;
                    IMCRVascularModel.ConsultationID = newConsultationID;
                    IMCRVascularModel.createdOn = CcreatedOn;
                    IMCRVascularModel.createdBy = IMedicneModel.CcreatedBy;
                   
                IMCRVascularModel.Diabetes = CRVDiabetes;
                IMCRVascularModel.Alcohol = CRVAlcohol;
                IMCRVascularModel.Obesity = CRVObesity;
                IMCRVascularModel.FamilialHistory = CRVFamilialHistory;
                IMCRVascularModel.Dyslipidemia = CRVDyslipidemia;

                IMCRVascularModel.CRVascularID = _MedOrSvc.SetNewCardiovascular(IMCRVascularModel);

                if (block != "")
                    {  _MedOrSvc.SetNewDiabetes(IMCRVascularModel);}

                    if (block1 != "")
                    {        _MedOrSvc.SetNewDyslipidemia(IMCRVascularModel);}

                    if (block2 != "")
                    { _MedOrSvc.SetNewAlcohol(IMCRVascularModel);}

                    if (block3 != "")
                    {  _MedOrSvc.SetNewObesity(IMCRVascularModel);}

                    if (block4 != "")
                    {    _MedOrSvc.SetNewFamilialHistory(IMCRVascularModel);}
            }

            if (Anorexia != "")
            {
                //int xAnorexia = Convert.ToInt32(Anorexia);
                IMreviewSystemModel.Anorexia = Anorexia;
            }

            if (Fever != "")
            {
                //int xFever = Convert.ToInt32(Fever);
                IMreviewSystemModel.Fever = Fever;
            }

            if (Weight != "")
            {
                //int xWeight = Convert.ToInt32(Weight);
                IMreviewSystemModel.Weight = Weight;
            }

            if (Fatigue != "")
            {
                //int xFatigue = Convert.ToInt32(Fatigue);
                IMreviewSystemModel.Fatigue = Fatigue;
            }

                IMreviewSystemModel.HEENTDescription = RSHEENTDescription;
                IMreviewSystemModel.PulmonaryDescription = RSPulminaryDescription;
                IMreviewSystemModel.CardiovascularDescriptionical = RSCardiovascularDescription;
                IMreviewSystemModel.DigestiveDescription = RSDigestiveDescription;
                IMreviewSystemModel.NervousSystemDescription = RSNervousSystemDescription;
                IMreviewSystemModel.UrogenitalDescription = RSUrogenitalDescription;
                IMreviewSystemModel.SkinDescription = RSSkinDescription;
                IMreviewSystemModel.MusculoskeletalDescription = RSMusculoskeletalDescription;
                IMreviewSystemModel.PsychosocialDescription = RSPsychosocialDescription;

            if (RSHEENTDescription != "" || RSPulminaryDescription != "" || RSCardiovascularDescription != "" || RSDigestiveDescription != "" || RSNervousSystemDescription != "" ||
                RSUrogenitalDescription != "" || RSSkinDescription != "" || RSMusculoskeletalDescription != "" || RSPsychosocialDescription != "" ||
                Fatigue != "" || Weight != "" || Fever != "" || Anorexia != "") 
                
            {
            IMreviewSystemModel.HEENT = RSHEENT;
            IMreviewSystemModel.Pulmonary = RSPulmonary;
            IMreviewSystemModel.Cardiovascular = RSCardiovascular;
            IMreviewSystemModel.Digestive = RSDigestive;
            IMreviewSystemModel.NervouSystem = RSNervouSystem;
            IMreviewSystemModel.Urogenital = RSUrogenital;
            IMreviewSystemModel.Skin = RSSkin;
            IMreviewSystemModel.Musculoskeletal = RSMusculoskeletal;
            IMreviewSystemModel.Psychosocial = RSPsychosocial;

                IMreviewSystemModel.createdBy = IMedicneModel.CcreatedBy;
                IMreviewSystemModel.ConsultationID = newConsultationID;
                  IMreviewSystemModel.PatientID = PatientID;
                IMreviewSystemModel.createdOn = CcreatedOn;
                IMreviewSystemModel.ReviewSystemID = _MedOrSvc.SetNewREviewSystem(IMreviewSystemModel);

                //if (Anorexia != "")
                //{
                //}
                    _MedOrSvc.SetNewRSEntry(IMreviewSystemModel); 
                //if (Fever != "")
                //{ _MedOrSvc.SetNewRSEntry(IMreviewSystemModel); }
                //if (Weight != "")
                //{ _MedOrSvc.SetNewRSEntry(IMreviewSystemModel); }
                //if (Fatigue != "")
                //{ _MedOrSvc.SetNewRSEntry(IMreviewSystemModel); }


                if (RSHEENTDescription != "" )
                { _MedOrSvc.SetNewRSHEENT(IMreviewSystemModel); }

                if(RSPulminaryDescription != "")
                { _MedOrSvc.SetNewRSPulminary(IMreviewSystemModel); }

                if (RSSkinDescription != "")
                { _MedOrSvc.SetNewRSSkin(IMreviewSystemModel);  }

                if (RSCardiovascularDescription != "")
                {_MedOrSvc.SetNewRSCardiovascular(IMreviewSystemModel);}

                if (RSDigestiveDescription != "" )
                { _MedOrSvc.SetNewRSDigestive(IMreviewSystemModel); }

                if (RSUrogenitalDescription != "")
                {   _MedOrSvc.SetNewRSUrogental(IMreviewSystemModel);}

                if (RSNervousSystemDescription != "")
                { _MedOrSvc.SetNewRSNervousSystem(IMreviewSystemModel);}

                if (RSMusculoskeletalDescription != "" )
                { _MedOrSvc.SetNewRSMusculoskeletal(IMreviewSystemModel);}

                if (RSPsychosocialDescription != "")
                {    _MedOrSvc.SetNewPsychosociale(IMreviewSystemModel); }
            }

            // Physical History
                if (RespiratoryRate != "")
                {
                   // int xRespiratoryRate = Convert.ToInt32(RespiratoryRate);
                    IMPhysicalHistoryModel.RespiratoryRate = RespiratoryRate;
                }

                if (BloodPressure != "")
                {
                    //int xBloodPressure = Convert.ToInt32(BloodPressure);
                    IMPhysicalHistoryModel.BloodPressure = BloodPressure;
                }

                if (PHWeight != "")
                {
                    //int xPHWeight = Convert.ToInt32(PHWeight);
                    IMPhysicalHistoryModel.Weight = PHWeight;
                }
                IMPhysicalHistoryModel.HEENTDescription = PHHEENTDescription;
                IMPhysicalHistoryModel.DigestiveDescription = PHDigestiveDescription;
                IMPhysicalHistoryModel.UrogenitalDescription = PHUrogenitalDescription;
                IMPhysicalHistoryModel.NervousSystemDescription = PHNervousSystemDescription;
                IMPhysicalHistoryModel.MusculoskeletalDescription = PHMusculoskeletalDescription;
                IMPhysicalHistoryModel.PsychosocialDescription = PHPsychosocialDescription;
                IMPhysicalHistoryModel.SkinDescription = PHSkinDescription;
                IMPhysicalHistoryModel.PulmonaryDescription = PHPulminaryDescription;
                IMPhysicalHistoryModel.CardiovascularDescriptionical = PHCardiovascularDescription;

            if (PHHEENTDescription != "" || PHPulminaryDescription != "" || PHCardiovascularDescription != "" || PHDigestiveDescription != "" || PHNervousSystemDescription != "" ||
           PHUrogenitalDescription != "" || PHSkinDescription != "" || PHMusculoskeletalDescription != "" || PHPsychosocialDescription != "" ||
           RespiratoryRate != "" || BloodPressure != "" || PHWeight != "" )
            {
                IMPhysicalHistoryModel.HEENT = PHHEENT;
                IMPhysicalHistoryModel.Pulmonary = PHPulmonary;
                IMPhysicalHistoryModel.Cardiovascular = PHCardiovascular;
                IMPhysicalHistoryModel.Digestive = PHDigestive;
                IMPhysicalHistoryModel.Urogenital = PHUrogenital;
                IMPhysicalHistoryModel.NervouSystem = PHNervouSystem;
                IMPhysicalHistoryModel.Skin = PHSkin;
                IMPhysicalHistoryModel.Musculoskeletal = PHMusculoskeletal;
                IMPhysicalHistoryModel.Psychosocial = PHPsychosocial;

                    IMPhysicalHistoryModel.createdOn = CcreatedOn;
                    IMPhysicalHistoryModel.ConsultationID = newConsultationID;
                    IMPhysicalHistoryModel.PatientID = PatientID;
                    IMPhysicalHistoryModel.createdBy = IMedicneModel.CcreatedBy;

                    IMPhysicalHistoryModel.IMPhysicalHistoryID = _MedOrSvc.SetPhisycalHistory(IMPhysicalHistoryModel);
                _MedOrSvc.SetNewPHEntry(IMPhysicalHistoryModel);


                if (PHHEENTDescription != "")
                    { _MedOrSvc.SetNewPHHEENT(IMPhysicalHistoryModel); }

                    if (PHPulminaryDescription != "")
                    {   _MedOrSvc.SetNewPHPulminary(IMPhysicalHistoryModel);}

                    if (PHDigestiveDescription != "")
                    {    _MedOrSvc.SetNewPHDigestive(IMPhysicalHistoryModel); }

                    if (PHCardiovascularDescription != "")
                    {  _MedOrSvc.SetNewPHCardiovascular(IMPhysicalHistoryModel);}

                    if (PHUrogenitalDescription != "")
                    { _MedOrSvc.SetNewPHUrogental(IMPhysicalHistoryModel);}

                    if (PHNervousSystemDescription != "")
                    {   _MedOrSvc.SetNewPHNervousSystem(IMPhysicalHistoryModel); }

                    if (PHSkinDescription != "")
                    { _MedOrSvc.SetNewPHSkin(IMPhysicalHistoryModel); }

                    if (PHMusculoskeletalDescription != "")
                    { _MedOrSvc.SetNewPHMusculoskeletal(IMPhysicalHistoryModel); }

                    if (PHPsychosocialDescription != "")
                    {   _MedOrSvc.SetNewPHPsychosociale(IMPhysicalHistoryModel); }
            
                }

            //Biological Parameters



            //    DataModel.IMFileModel FileModel = new DataModel.IMFileModel();
            //    var success = _MedOrSvc.Dynamic( name);

            if (BPOtherNote != "" || BPconventionalRadiographyNote != "" || BPEchographyNote != "" || BPScanNote != "" || BPMRINote != "" ||
             BPscintigraphyNote != "" || BPPetScanNote != "")
            {
                
                IMBiologicalModel.conventionalRadiographyNote = BPconventionalRadiographyNote;
                IMBiologicalModel.EchographyNote = BPEchographyNote;
                IMBiologicalModel.ScanNote = BPScanNote;
                IMBiologicalModel.MRINote = BPMRINote;
                IMBiologicalModel.scintigraphyNote = BPscintigraphyNote;
                IMBiologicalModel.PetScanNote = BPPetScanNote;
                IMBiologicalModel.OtherNote = BPOtherNote;


              
                //var ListColumnDynamicHeader = ColumnDynamicHeader.Split('|');
                //var ListRowDynamicHeader = RowDynamicHeader.Split('|');
                //var ListcontentRow = contentRow.Split('|');
                //List<string> ListColumnDynamicHeader101 = new List<string>();
                //List<string> ListRowDynamicHeader1 = new List<string>();
                //List<string> ListcontentRow1 = new List<string>();
                //for (int i = 0; i < ListColumnDynamicHeader.Length; i++)
                //{
                //    ListColumnDynamicHeader[i] = ListColumnDynamicHeader[i].Replace("null", "");
                //    ListColumnDynamicHeader[i] = ListColumnDynamicHeader[i].Replace("]", "");
                //    ListColumnDynamicHeader[i] = ListColumnDynamicHeader[i].Replace("[", "");
                //    ListColumnDynamicHeader[i] = ListColumnDynamicHeader[i].Replace("\"", "");
                //    if (ListColumnDynamicHeader[i] != "")
                //    { ListColumnDynamicHeader101.Add(ListColumnDynamicHeader[i]); }
                //}
                //for (int i = 0; i < ListRowDynamicHeader.Length; i++)
                //{
                //    ListRowDynamicHeader[i] = ListRowDynamicHeader[i].Replace("null", "");
                //    ListRowDynamicHeader[i] = ListRowDynamicHeader[i].Replace("]", "");
                //    ListRowDynamicHeader[i] = ListRowDynamicHeader[i].Replace("[", "");
                //    ListRowDynamicHeader[i] = ListRowDynamicHeader[i].Replace("\"", "");
                //    if (ListRowDynamicHeader[i] != "")
                //    { ListRowDynamicHeader1.Add(ListRowDynamicHeader[i]); }
                //}
                //for (int i = 0; i < ListcontentRow.Length; i++)
                //{
                //    ListcontentRow[i] = ListcontentRow[i].Replace("null", "");
                //    ListcontentRow[i] = ListcontentRow[i].Replace("]", "");
                //    ListcontentRow[i] = ListcontentRow[i].Replace("[", "");
                //    ListcontentRow[i] = ListcontentRow[i].Replace("\"", "");
                //    if (ListcontentRow[i] != "")
                //    { ListcontentRow1.Add(ListcontentRow[i]); }
                //}
                


                if (IMBiologicalModel != null)
                {
                   // UpdateBPEchographie 
                    IMBiologicalModel.BiologicalID = Guid.NewGuid();
                               IMBiologicalModel.conventionalRadiography = BPconventionalRadiography;
                    IMBiologicalModel.Echography = BPEchography;
                    IMBiologicalModel.Scan = BPScan;
                    IMBiologicalModel.MRI = BPMRI;
                    IMBiologicalModel.scintigraphy = BPscintigraphy;
                    IMBiologicalModel.PetScan = BPPetScan;
                    IMBiologicalModel.Other = BPOther;

                    IMBiologicalModel.createdOn = CcreatedOn;
                    IMBiologicalModel.ConsultationID = newConsultationID;
                    IMBiologicalModel.PatientID = PatientID;
                    IMBiologicalModel.createdBy = IMedicneModel.CcreatedBy;
                        var result = _MedOrSvc.checkConsultation(consultationID);
                   // var resultEchography = _MedOrSvc.checkEchography(consultationID);

                    if (result == null)
                        {
                            //00000000-0000-0000-0000-000000000000
                     IMBiologicalModel.BiologicalID = _MedOrSvc.SetNewBiological(IMBiologicalModel);

                    if (BPOtherNote != "")
                    { _MedOrSvc.SetNewBPOther(IMBiologicalModel);
                    }

                    if (BPconventionalRadiographyNote != "")
                    { _MedOrSvc.SetNewBPconventionalRadiography(IMBiologicalModel); }

                    if (BPEchographyNote != "")
                    { _MedOrSvc.SetNewBPEchography(IMBiologicalModel); }

                    if (BPScanNote != "")
                    { _MedOrSvc.SetNewBPScan(IMBiologicalModel); }

                    if (BPMRINote != "")
                    { _MedOrSvc.SetNewBPMRI(IMBiologicalModel); }

                    if (BPscintigraphyNote != "")
                    { _MedOrSvc.SetNewBPscintigraphy(IMBiologicalModel); }

                    if (BPPetScanNote != "")
                    { _MedOrSvc.SetNewBPPetScan(IMBiologicalModel); }

                    }

                  else {
                       IMBiologicalModel.BiologicalID = result.BiologicalID;



                        //if (BPOtherNote != "")
                        //{
                        //    _MedOrSvc.SetUpdateBPOther(IMBiologicalModel);
                        //}

                        if (BPconventionalRadiographyNote != "")
                        { _MedOrSvc.UpdateBPconventionalRadiography(IMBiologicalModel); }

                        if (BPEchographyNote != "")
                        { _MedOrSvc.UpdateBPEchographies(IMBiologicalModel); }

                        if (BPScanNote != "")
                        { _MedOrSvc.UpdateBPScan(IMBiologicalModel); }

                        if (BPMRINote != "")
                        { _MedOrSvc.SetNewBPMRI(IMBiologicalModel); }

                        if (BPscintigraphyNote != "")
                        { _MedOrSvc.SetNewBPscintigraphy(IMBiologicalModel); }

                        if (BPPetScanNote != "")
                        { _MedOrSvc.SetNewBPPetScan(IMBiologicalModel); }
                    }
                }
            }



            if (CFees!= "0")
            {
                if (CFees != "0")
                {
                    int xCFees = Convert.ToInt32(CFees);
                    IMedicneModel.CFees = xCFees;
                }

                IMedicneModel.PatientID = PatientID;
                IMedicneModel.ConsultationIDTap = consultationID;
                IMedicneModel.Ccurency = Currency;
                IMedicneModel.InCreatedBy = new Guid(HttpContext.User.Identity.Name);
                if (PanelTaps== "panel_tab14")
                {
                    IMedicneModel.CCheckBox = true;
                }
                _MedOrSvc.SetFeeConsultation(IMedicneModel);
            }


            int updateMedical = 0;
            var ListICD10 = ICD10.Split('|');
            var ListICD10Note = ICD10Note.Split('|');
     
            List<string> ListICD101 = new List<string>();
            List<string> ListICD101Note = new List<string>();
            for (int i = 0; i < ListICD10.Length; i++)
            {
                ListICD10[i] = ListICD10[i].Replace("null", "");
                ListICD10[i] = ListICD10[i].Replace("]", "");
                ListICD10[i] = ListICD10[i].Replace("[", "");
                ListICD10[i] = ListICD10[i].Replace("\"", "");
                if (ListICD10[i] != "")
                { ListICD101.Add(ListICD10[i]); }
            }
            for (int i = 0; i < ListICD10Note.Length; i++)
            {
                ListICD10Note[i] = ListICD10Note[i].Replace("null", "");
                ListICD10Note[i] = ListICD10Note[i].Replace("]", "");
                ListICD10Note[i] = ListICD10Note[i].Replace("[", "");
                ListICD10Note[i] = ListICD10Note[i].Replace("\"", "");
                if (ListICD10[i] != "")
                { ListICD101Note.Add(ListICD10Note[i]); }
            }

            for (int i = 0; i < ListICD101.Count; i++)
            {
                ICD10PatientModel.ICD10PatientID = Guid.NewGuid();
                ICD10PatientModel.PatientID = PatientID;
                ICD10PatientModel.IMconsultationID = newConsultationID;
                ICD10PatientModel.CodeDescriptionICD10 = ListICD10[i];
                ICD10PatientModel.Note = ListICD10Note[i];
                ICD10PatientModel.CreatedOn = CcreatedOn;
                ICD10PatientModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
                updateMedical = _MedOrSvc.SetIMDiagnostic(ICD10PatientModel);
            }
            if (PanelTaps == null)
                return Json(Url.Action("IMedicine", new { id = "", PatientID = PatientID, ConsultationID=Guid.Empty, FileID1, CcreatedOn = CcreatedOn }) + "#panel_tab14");
            neConsultationID = newConsultationID;
            IMedicneModel.FconsultationID = neConsultationID;
              return Json(Url.Action("IMedicine", new { id = "", PatientID = PatientID, ConsultationID = newConsultationID,FileID1, CcreatedOn= CcreatedOn }) + "#" + PanelTaps);
        }
        public JsonResult GetSeearchICD10(string searchICD10)
        {
            DataAccess.MedOREntities _MedORDB = new DataAccess.MedOREntities();
            ListDictionary list = new ListDictionary();
            var ICD2 = _MedORDB.ICD_10.Where(x => (x.Code_Description.Contains(searchICD10) || x.Code_Category.Contains(searchICD10) || x.Code_ICD10.Contains(searchICD10)))
                 .Select(y => new { Code = y.Code_ICD10, Description = y.Code_Description, Category = y.Code_Category }).Take(8).ToList();

            return Json(ICD2, JsonRequestBehavior.AllowGet);
        }

    }
}
