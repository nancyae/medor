﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MedOR.Common;
using WebMatrix.Data;
using WebMatrix.WebData;
using System.Web.Security;
using Newtonsoft.Json;

namespace MedOR.Views.Invoices
{
    public class InvoicesController : Controller
    {
        //
        // GET: /Invoices///

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public InvoicesController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ListOfInvoices()
        {

            var Invoices = _MedOrSvc.GetAllInvoices(new Guid(HttpContext.User.Identity.Name));
            return View(Invoices);

        }
        public ActionResult ListOfInvoiceItems(Guid? id, DataModel.InvoiceItemModel model)
         {
            var Invoices = _MedOrSvc.GetItemsByInvoiceID(id.Value);
            var Doctorname = "";
                foreach (var inv in Invoices)
            {
                foreach(var inv1 in inv.InvoicesModels)
                    {
                    Doctorname = inv1.DoctorName;
                }
            }

            ViewBag.DoctorName = Doctorname;
            return View(Invoices);
        }
        public ActionResult EditInvoice(DataModel.InvoicesModel model)
        {
            var Invoices = _MedOrSvc.GetAllInvoices(new Guid(HttpContext.User.Identity.Name));
            return View(Invoices);

        }
        public ActionResult DeleteInvoice(Guid id)
        {
            var deleted = _MedOrSvc.DeleteInvoiceByID(id);
            if (deleted != 0)
                return Redirect("/Invoices/ListOfInvoices");
            return View();
        }
        public ActionResult DeleteInvoiceItemByID(Guid InvoiceItemID, Guid InvoiceID, decimal SubTotal, decimal GTotal)
        {
            var deleted = _MedOrSvc.DeleteInvoiceItemByID(InvoiceItemID);

            var editInvoice = _MedOrSvc.UpdateInvoice(InvoiceID, SubTotal, GTotal);
            if (deleted != 0)
            {
                return Redirect("/Invoices/ListOfInvoices");
                
            }
            return View();
        }
       
        public ActionResult ViewInvoices(DataModel.UserModel model)
        {

            DataModel.UserModel userModel = new DataModel.UserModel();
            userModel = _MedOrSvc.GetAllAccess();

            //var Invoices = _MedOrSvc.AddNewItemInvoice(model);

            return View(userModel);

}

        public ActionResult AddItem()
        {
          //if (model.InvoicesModels!=null)
          //  //    return Redirect("/Invoices/ViewInvoices?" + model);
          //  return RedirectToAction("ViewInvoices", model);

            return View();
        }


        public JsonResult SaveInputsInvoices( string SelectedDescription, string SelectedQuantity, string SelectedUnitCost, string SelectedTotal,
                                              string SelectedDoctorIDs,string SelectedPatientIDs,string GTotal,string TotalInt,string SelectedTitleInvoiceID, DataModel.UserModel model)
        {


            DataModel.InvoiceItemModel InvoiceItemModel = new DataModel.InvoiceItemModel();
            DataModel.InvoicesModel InvoicesModel = new DataModel.InvoicesModel();

            decimal GTotal1 = Convert.ToDecimal(GTotal);
            decimal TotalInt1 = Convert.ToDecimal(TotalInt);
            Guid newInvoiceID= Guid.NewGuid();

            InvoicesModel.InvoiceID = newInvoiceID;
            InvoicesModel.PatientID = new Guid(SelectedPatientIDs);
            InvoicesModel.DoctorID = new Guid(SelectedDoctorIDs);
            InvoicesModel.Total = GTotal1;
            //InvoicesModel.VAT = 11;
            InvoicesModel.Title = SelectedTitleInvoiceID;
            InvoicesModel.SubTotal = TotalInt1;
            InvoicesModel.CreatedBy = new Guid(HttpContext.User.Identity.Name);
            InvoicesModel.CreatedON = DateTime.Now ;

            _MedOrSvc.AddNewInvoice(InvoicesModel);
            //InvoicesModel.CreatedBy = TotalInt1;




                var ListSelectedDescription = SelectedDescription.Split(',');
                var ListSelectedQuantity = SelectedQuantity.Split(',');
                var ListSelectedUnitCost = SelectedUnitCost.Split(',');
                var ListSelectedTotal = SelectedTotal.Split(',');
             
            List<string> ListSelectedDescription1 = new List<string>();
            List<string> ListSelectedQuantity1 = new List<string>();
            List<string> ListSelectedUnitCost1 = new List<string>();
            List<string> ListSelectedTotal1 = new List<string>();

            for (int i = 0; i < ListSelectedDescription.Length; i++)
            {
                ListSelectedDescription[i] = ListSelectedDescription[i].Replace("null", "");
                ListSelectedDescription[i] = ListSelectedDescription[i].Replace("]", "");
                ListSelectedDescription[i] = ListSelectedDescription[i].Replace("[", "");
                ListSelectedDescription[i] = ListSelectedDescription[i].Replace("\"", "");
                if (ListSelectedDescription[i] != "")
                { ListSelectedDescription1.Add(ListSelectedDescription[i]); }
            }

            for (int i = 0; i < ListSelectedQuantity.Length; i++)
            {
                ListSelectedQuantity[i] = ListSelectedQuantity[i].Replace("null", "");
                ListSelectedQuantity[i] = ListSelectedQuantity[i].Replace("]", "");
                ListSelectedQuantity[i] = ListSelectedQuantity[i].Replace("[", "");
                ListSelectedQuantity[i] = ListSelectedQuantity[i].Replace("\"", "");
                if (ListSelectedQuantity[i] != "")
                {
                    ListSelectedQuantity1.Add(ListSelectedQuantity[i]);
                }
            }

            for (int i = 0; i < ListSelectedUnitCost.Length; i++)
            {
                ListSelectedUnitCost[i] = ListSelectedUnitCost[i].Replace("null", "");
                ListSelectedUnitCost[i] = ListSelectedUnitCost[i].Replace("]", "");
                ListSelectedUnitCost[i] = ListSelectedUnitCost[i].Replace("[", "");
                ListSelectedUnitCost[i] = ListSelectedUnitCost[i].Replace("\"", "");
                if (ListSelectedUnitCost[i] != "")
                {
                    ListSelectedUnitCost1.Add(ListSelectedUnitCost[i]);
                }
            }
            for (int i = 0; i < ListSelectedTotal.Length; i++)
            {
                ListSelectedTotal[i] = ListSelectedTotal[i].Replace("null", "");
                ListSelectedTotal[i] = ListSelectedTotal[i].Replace("]", "");
                ListSelectedTotal[i] = ListSelectedTotal[i].Replace("[", "");
                ListSelectedTotal[i] = ListSelectedTotal[i].Replace("\"", "");
                if (ListSelectedTotal[i] != "")
                {
                    ListSelectedTotal1.Add(ListSelectedTotal[i]);

                }
            }
            
            for (int i = 0; i < ListSelectedTotal1.Count; i++)
            {
                InvoiceItemModel.InvoiceItemID = Guid.NewGuid();
                InvoiceItemModel.InvoiceID = newInvoiceID;
                InvoiceItemModel.ConsultationID = new Guid("293B5BC6-4043-4558-B9B2-555B7B5EFC87");
                InvoiceItemModel.InventoryID = Guid.NewGuid();
                InvoiceItemModel.Description = ListSelectedDescription1[i];
                InvoiceItemModel.Quantity = int.Parse(ListSelectedQuantity1[i]);
                InvoiceItemModel.UnitCost = int.Parse(ListSelectedUnitCost1[i]);
                InvoiceItemModel.Total = int.Parse(ListSelectedTotal1[i]) ;
                _MedOrSvc.AddNewInvoiceItem(InvoiceItemModel);


            }



            //foreach(var e in dr)
            //{ }

            //var rsesult = SelectedDescription.Replace( "\\" ,"xxx ");

            //var Invoices = _MedOrSvc.AddNewItemInvoice(model);
            //return Json(new { Url = "Invoices/ListOfInvoices" });
            //return Json(new { Url = "Invoices/ListOfInvoices" });
            //return Redirect("Invoices/ListOfInvoices");
            //return PartialView("ListOfInvoices");
            return Json("Invoices/ListOfInvoices", JsonRequestBehavior.AllowGet);
        }




    }
}
