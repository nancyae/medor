﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MedOR.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        //
        // GET: /Consultation/

        #region Constructor
        private DataServices.Service _MedOrSvc;

        public ReportController()
        {
            _MedOrSvc = new DataServices.Service();
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        // [Comm.AuthorizeUser(AccessLevel = "View Reports")]
        public ActionResult ListOfReports()
        {
            return View();
        }

    }
}
