﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace DataServices
{
    public class Service : BaseService
    {
        private object HttpContext ;
        public Service()
        { }
        #region Login
        //using simple membership.
        #endregion
        #region Accounting     
        public IEnumerable<DataModel.ExpenseModel> GetAllExpense(Guid UserID)
        {
            //DateTime BeforeMonth = ' 11/2/2015 11:56:08 ' ;
            //System.DateTime date1 = new System.DateTime(2018, 3, 10, 22, 15, 0);
            //DateTime currentTime = DateTime.Now;

            var ExpenseModel = (from p in _MedORDB.Expenses
                                select new DataModel.ExpenseModel
                                {
                                    ExpenseID = p.ExpenseID,
                                    UserClinicID = p.UserClinicID,
                                    Title = p.Title,
                                    Description = p.Description,
                                    Amount = p.Amount,
                                    CreatedON = p.CreatedON,
                                    CreatedBy = p.CreatedBy,

                                    CreatedByName = (from u in _MedORDB.Users where u.UserID == p.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                    //Income = (from x in _MedORDB.Invoices select x.Total).Sum(),
                                    TotalAmount = (from x in _MedORDB.Expenses select x.Amount).Sum(),
                                    //InvoicesModels=(from x in _MedORDB.Invoices select new DataModel.InvoicesModel                                                                                                     { DoctorID= x.DoctorID })
                                    //InvoicesModels = (from x in _MedORDB.Invoices
                                    //                      //where x.CreatedON > date1.Subtract(currentTime)
                                    //                  group x by new { x.DoctorID } into gr
                                    //                  select new DataModel.InvoicesModel
                                    //                  {
                                    //                      DoctorID = gr.Key.DoctorID,
                                    //                      Total = gr.Sum(q => q.Total),
                                    //                      DoctorName = (from x in _MedORDB.Doctors where x.DoctorID == x.DoctorID select x.FirstName + " " + x.LastName).FirstOrDefault()
                                    //                  }
                                    //                           )

                                });


            return ExpenseModel;
        }
        public Int32 AddNewExpense(DataModel.ExpenseModel model)
        {
            DataAccess.Expense newExpense = new DataAccess.Expense();
            newExpense.ExpenseID = Guid.NewGuid();
            newExpense.Title = model.Title;
            newExpense.Description = model.Description;
            newExpense.Amount = model.Amount;
            newExpense.UserClinicID = model.UserClinicID;
            newExpense.CreatedBy = model.CreatedBy;
            newExpense.CreatedON = model.CreatedON;
            _MedORDB.Expenses.AddObject(newExpense);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public DataModel.AccountingModel GetAllPendingPayment(Guid UserID)
        {
            DataModel.AccountingModel Accounting = new DataModel.AccountingModel();
        Accounting.AccountingModels = (from m in _MedORDB.IMconsultations where m.Checkbox== false
                                       //orderby m.CreatedOn descending
                                       select (new DataModel.AccountingModel
                                                                             {    ConsultationID= m.IMconsultationID,
                                                                                 PFullName  = (from u in _MedORDB.Patients where m.PatientID == u.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                                                 CCheckBox = m.Checkbox,
                                                                                 Ctitle = m.Title,
                                                                                 CFees = m.Fees,
                                                                                 Ccurrency = m.Currency
                                                                                 ,CCreatedOn=m.CreatedOn

                                                                             })).OrderByDescending(m => m.CCreatedOn.Day);
            
            var success = _MedORDB.SaveChanges();
            return Accounting;
        }
        public DataModel.AccountingModel GetAllPendingPaymentForPatient(Guid PatientID)
        {
            DataModel.AccountingModel Accounting = new DataModel.AccountingModel();
            Accounting.AccountingModels = (from m in _MedORDB.IMconsultations
                                           where m.Checkbox == false && m.PatientID== PatientID
                                           //orderby m.CreatedOn descending
                                           select (new DataModel.AccountingModel
                                           {
                                               ConsultationID = m.IMconsultationID,
                                               PFullName = (from u in _MedORDB.Patients where m.PatientID == u.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                               CCheckBox = m.Checkbox,
                                               Ctitle = m.Title,
                                               CFees = m.Fees,
                                               Ccurrency = m.Currency
                                                                                     ,
                                               CCreatedOn = m.CreatedOn

                                           })).OrderByDescending(m => m.CCreatedOn.Day);
            Accounting.PFullName = (from u in _MedORDB.Patients where u.PatientID == PatientID select u.FirstName + " " + u.LastName).FirstOrDefault();
                                      
                                  

            var success = _MedORDB.SaveChanges();
            return Accounting;
        }
        public Int32 PaidConsultation(Guid Consultation)
        {
            var editConsultauiom = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == Consultation).FirstOrDefault();
            editConsultauiom.Checkbox = true;
            var success = _MedORDB.SaveChanges();
         //   Guid Patient= from m in _MedORDB.IMconsultations where m.IMconsultationID==
            var editAccount = _MedORDB.IMAccounts.Where(x => x.PatientID == editConsultauiom.PatientID & x.Currency==editConsultauiom.Currency).FirstOrDefault();
            if (editAccount!=null)
            { editAccount.TotalFees = editAccount.TotalFees - editConsultauiom.Fees;
                success = _MedORDB.SaveChanges();

            }
            return success;
        }
        public DataModel.AccountingModel ListAccounting(Guid UserID,DataModel.AccountingModel AccountingModel)
        {
            DataModel.AccountingModel Accounting1 = new DataModel.AccountingModel();



            if (AccountingModel.SetTime.Date == DateTime.Now.Date && AccountingModel.TillDate.Date == DateTime.Now.Date)
            {
               DateTime LastMonth1 = DateTime.Now.AddMonths(-1);

                Accounting1.AccountingModels = (from m in _MedORDB.IMconsultations
                                               where m.CreatedOn > LastMonth1
                                               orderby m.CreatedOn descending
                                               select (new DataModel.AccountingModel
                                               {
                                                   PFullName = (from u in _MedORDB.Patients where m.PatientID == u.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                   CCheckBox = m.Checkbox,
                                                   Ctitle = m.Title,
                                                   CFees = m.Fees,
                                                   Ccurrency = m.Currency,
                                                   CCreatedOn = m.CreatedOn
                                                   ,SetTime=LastMonth1
                                               }));
            }
            else
            {
                Accounting1.AccountingModels = (from m in _MedORDB.IMconsultations
                                               where m.CreatedOn >= AccountingModel.SetTime && m.CreatedOn <= AccountingModel.TillDate && AccountingModel.SelectedDoctorID==m.DoctorID
                                                orderby m.CreatedOn descending
                                               select (new DataModel.AccountingModel
                                               {
                                                   PFullName = (from u in _MedORDB.Patients where m.PatientID == u.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                   CCheckBox = m.Checkbox,
                                                   Ctitle = m.Title,
                                                   CFees = m.Fees,
                                                   Ccurrency = m.Currency,
                                                   CCreatedOn = m.CreatedOn,
                                                   SetTime= AccountingModel.SetTime,
                                                   TillDate=AccountingModel.TillDate,
                                                  // DoctorModels=(from t in _MedORDB)
                                                   
                                               }));
               
                                           };

            //Accounting1.ClinicModels = from uc in _MedORDB.UserClinics
            //                           where uc.UserID == UserID
            //                           from c in _MedORDB.Clinics
            //                           where c.ClinicID == uc.ClinicID
            //                           select new DataModel.ClinicModel
            //                           {

            //                               Name = c.Name
            //                           };
            Accounting1.DoctorModels = 
                from ud in _MedORDB.UserDoctors
                where ud.UserID == UserID
                from d in _MedORDB.Doctors
                where ud.DoctorID == d.DoctorID
                select new DataModel.DoctorModel { FullName = d.FirstName + " " + d.LastName, DoctorID = d.DoctorID };


            Decimal LBPAmount = 0;
            Decimal USAmount = 0;
            foreach (var ii in Accounting1.AccountingModels )
            {
                if (ii.Ccurrency=="LBP")
                 LBPAmount = ii.CFees + LBPAmount;
                if (ii.Ccurrency == "USD")
                    USAmount = ii.CFees + USAmount;
            }

            Accounting1.TotalLBPAmount = LBPAmount;
            Accounting1.TotalUSAmount = USAmount;

            return Accounting1;
        }
        
        #endregion
        #region UserProfile
        public int GetUserProfileIDByUsername(String username)
        {
            var user = _MedORDB.UserProfiles.Where(x => x.UserName == username).Select(x => x.UserId).FirstOrDefault();
            return user;
        }
        public String GetEmailByUsername(String username)
        {
            var email = _MedORDB.UserProfiles.Where(x => x.UserName == username).Select(x => x.EmailId).FirstOrDefault();
            return email;
        }
        public Int32 UpdateUserProfile(int userProfileID, String email)
        {
            var updateUserProfile = _MedORDB.UserProfiles.Where(x => x.UserId == userProfileID).FirstOrDefault();
            int success = 0;
            if (userProfileID != 0)
            {
                updateUserProfile.EmailId = email;
                success = _MedORDB.SaveChanges();
            }

            return success;
        }
        #endregion
        #region WebpagesMembership
        public Boolean CheckMatchUserIDAndToken(int userid, String rt)
        {
            var any = _MedORDB.webpages_Membership.Where(x => x.UserId == userid && x.PasswordVerificationToken == rt).Any();  //&& (j.PasswordVerificationTokenExpirationDate < DateTime.Now)
            return any;
        }
        #endregion
        #region User
        public DataModel.UserModel GetUserByUserProfileID(int userProfileID)
        {
            var user = (from u in _MedORDB.Users
                        where u.UserProfileID == userProfileID
                        select new DataModel.UserModel
                        {
                            UserID = u.UserID,
                            Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                            Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault()
                        }).FirstOrDefault();
            return user;
        }
        public int GetUserProfileIDByUserID(Guid userID)
        {
            var userProfileID = _MedORDB.Users.Where(x => x.UserID == userID).Select(x => x.UserProfileID).FirstOrDefault();
            return userProfileID;
        }
        public String GetUsernameByUserID(Guid userID)
        {
            var username = (from u in _MedORDB.Users
                            where u.UserID == userID
                            from p in _MedORDB.UserProfiles
                            where p.UserId == u.UserProfileID
                            select p.UserName).FirstOrDefault();
            return username;
        }
        public IEnumerable<DataModel.UserModel> GetAllUsers(Guid user)
        {
            // Guid re = NewGuid('A6916BE3 - 3601 - 4978 - ADA9 - 146C5EDB37B8');

            var userModel = (from u in _MedORDB.Users
                             from r in _MedORDB.Roles
                             where r.RoleID == u.RoleID && r.IsSuperAdmin == null
                             select new DataModel.UserModel
                             {
                                 UserID = u.UserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Gender = u.Gender,
                                 Phone = u.Phone,
                                 Address = u.Address,
                                 City = u.City,
                                 Country = u.Country,
                                 Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                                 Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault(),
                                 Image = u.Image,
                                 RoleID = u.RoleID,
                                 RoleName = r.Name,
                                 CreatedBy = u.CreatedBy,
                                 CreatedByName = (from m in _MedORDB.Users where m.UserID == u.CreatedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 CreatedOn = u.CreatedOn,
                                 ModifiedBy = u.ModifiedBy,
                                 ModifiedByName = (from m in _MedORDB.Users where m.UserID == u.ModifiedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 ModifiedOn = u.ModifiedOn,
                                 SelectedClinicIDs = from us in _MedORDB.UserClinics
                                                     where us.UserID == u.UserID
                                                     from c in _MedORDB.Clinics
                                                     where us.ClinicID == c.ClinicID
                                                     select c.ClinicID,
                                 SelectedDoctorIDs = from us in _MedORDB.UserDoctors
                                                     where us.UserID == u.UserID
                                                     from d in _MedORDB.Doctors
                                                     where us.DoctorID == d.DoctorID
                                                     select d.DoctorID,
                                 SelectedClinicNames = from us in _MedORDB.UserClinics
                                                       where us.UserID == u.UserID
                                                       from c in _MedORDB.Clinics
                                                       where us.ClinicID == c.ClinicID
                                                       select c.Name + ", ",
                                 SelectedDoctorNames = from us in _MedORDB.UserDoctors
                                                       where us.UserID == u.UserID
                                                       from d in _MedORDB.Doctors
                                                       where us.DoctorID == d.DoctorID
                                                       select d.FirstName + " " + d.LastName + ", ",
                                 SecurityModel = (
                                        from s in _MedORDB.Securities
                                        where s.RoleID == (from Ro in _MedORDB.Users where Ro.UserID == user select Ro.RoleID).FirstOrDefault()
                                        //from p in _MedORDB.Permissions
                                        //where p.PermissionName == "Users"
                                        select new DataModel.SecurityModel
                                        {
                                            View = s.View,
                                            Create = s.Create,
                                            Edit = s.Edit,
                                            Delete = s.Delete
                                        }).FirstOrDefault()



                                        //from p in _MedORDB.Permissions
                                        //    where p.PermissionName == "Users"
                                        //    from s in _MedORDB.Securities
                                        //    where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                        //    //Guid.Parse("765307EC-7C9F-4C5B-8336-0A01737586E7")
                                        //    select new DataModel.SecurityModel
                                        //    {
                                        //        View = s.View,
                                        //        Create = s.Create,
                                        //        Edit = s.Edit,
                                        //        Delete = s.Delete
                                        //    }).FirstOrDefault()
                             }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName);
            return userModel;
        }
        //from u in _MedORDB.Users
                          

        //                     where u.UserID == UserID
        //                             from l in _MedORDB.Roles
        //                            where l.RoleID == u.RoleID
        //                             // from p in _MedORDB.Permissions
        //                             // where p.PermissionName == "Roles"
        //                             from s in _MedORDB.Securities
        //                             where s.RoleID == l.RoleID
        //                         from p in _MedORDB.Permissions
        //                         where p.PermissionID==s.PermissionID && p.PermissionName=="Roles"
        public object checkFileID_BP(string v)
        {
            throw new NotImplementedException();
        }
        public DataModel.UserModel GetAllAccess()
        {
            var userModel = new DataModel.UserModel
            {
                DoctorModels = (from d in _MedORDB.Doctors select new DataModel.DoctorModel { FullName = d.FirstName + " " + d.LastName, DoctorID = d.DoctorID }).OrderBy(o => o.FullName),
                ClinicModels = (from c in _MedORDB.Clinics select new DataModel.ClinicModel { Name = c.Name, ClinicID = c.ClinicID }).OrderBy(o => o.Name),
                RoleModels = (from r in _MedORDB.Roles where r.IsSuperAdmin==null select new DataModel.RoleModel { Name = r.Name, RoleID = r.RoleID }).OrderBy(o => o.Name),
                PaitentModels = (from t in _MedORDB.Patients select new DataModel.PaitentModel { FullName = t.FirstName + " " + t.LastName, PatientID = t.PatientID }).OrderBy(o => o.FullName),

            };
            return userModel;
        }
        public DataModel.UserModel GetUserByID(Guid UserID)
        {
            var userModel = (from u in _MedORDB.Users
                             where u.UserID == UserID
                             select new DataModel.UserModel
                             {
                                 UserID = u.UserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Gender = u.Gender,
                                 Phone = u.Phone,
                                 Address = u.Address,
                                 City = u.City,
                                 Country = u.Country,
                                 Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                                 Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault(),
                                 Role = u.RoleID,
                                 RoleID = u.RoleID,
                                 RoleName = (from r in _MedORDB.Roles
                                             where r.RoleID == u.RoleID
                                             select r.Name).FirstOrDefault(),
                                 //CreatedBy = u.CreatedBy,
                                 //CreatedByName = (from m in _MedORDB.Users where m.UserID == u.CreatedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 //CreatedOn = u.CreatedOn,
                                 //ModifiedBy = u.ModifiedBy,
                                 //ModifiedByName = (from m in _MedORDB.Users where m.UserID == u.ModifiedBy select m.FirstName + " " + m.LastName).FirstOrDefault(),
                                 //ModifiedOn = u.ModifiedOn,
                                 SelectedClinicIDs = from us in _MedORDB.UserClinics
                                                     where us.UserID == u.UserID
                                                     from c in _MedORDB.Clinics
                                                     where us.ClinicID == c.ClinicID
                                                     select c.ClinicID,
                                 SelectedDoctorIDs = from us in _MedORDB.UserDoctors
                                                     where us.UserID == u.UserID
                                                     from d in _MedORDB.Doctors
                                                     where us.DoctorID == d.DoctorID
                                                     select d.DoctorID,
                                 SelectedClinicNames = from us in _MedORDB.UserClinics
                                                       where us.UserID == u.UserID
                                                       from c in _MedORDB.Clinics
                                                       where us.ClinicID == c.ClinicID
                                                       select c.Name + ", ",
                                 SelectedDoctorNames = from us in _MedORDB.UserDoctors
                                                       where us.UserID == u.UserID
                                                       from d in _MedORDB.Doctors
                                                       where us.DoctorID == d.DoctorID
                                                       select d.FirstName + " " + d.LastName + ", ",
                                 LastLoggedIn = u.LastLoggedIn,
                                 Image = u.Image
                             }).FirstOrDefault();

            userModel.LoggedInSince = userModel.LastLoggedIn.HasValue ? TimeAgo(userModel.LastLoggedIn.Value) : "never";

            return userModel;
        }
        public static string TimeAgo(DateTime date)
        {
            TimeSpan timeSince = DateTime.Now.Subtract(date);
            if (timeSince.TotalMilliseconds < 1) return "not yet";
            if (timeSince.TotalMinutes < 1) return "just now";
            if (timeSince.TotalMinutes < 2) return "1 minute ago";
            if (timeSince.TotalMinutes < 60) return string.Format("{0} minutes ago", timeSince.Minutes);
            if (timeSince.TotalMinutes < 120) return "1 hour ago";
            if (timeSince.TotalHours < 24) return string.Format("{0} hours ago", timeSince.Hours);
            if (timeSince.TotalDays < 2) return "yesterday";
            if (timeSince.TotalDays < 7) return string.Format("{0} days ago", timeSince.Days);
            if (timeSince.TotalDays < 14) return "last week";
            if (timeSince.TotalDays < 21) return "2 weeks ago";
            if (timeSince.TotalDays < 28) return "3 weeks ago";
            if (timeSince.TotalDays < 60) return "last month";
            if (timeSince.TotalDays < 365) return string.Format("{0} months ago", Math.Round(timeSince.TotalDays / 30));
            if (timeSince.TotalDays < 730) return "last year"; //last but not least...
            return string.Format("{0} years ago", Math.Round(timeSince.TotalDays / 365));
        }
        public Byte[] GetUserImage(Guid UserID)
        {
            var userImage = (from u in _MedORDB.Users
                             where u.UserID == UserID
                             select u.Image).FirstOrDefault();
            return userImage;
        }
        public DataModel.UserModel GetUserByIDForLockScreen(Guid UserID)
        {
            var userModel = (from u in _MedORDB.Users
                             where u.UserID == UserID
                             select new DataModel.UserModel
                             {
                                 UserID = u.UserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Username = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.UserName).FirstOrDefault(),
                                 Email = (from m in _MedORDB.UserProfiles where m.UserId == u.UserProfileID select m.EmailId).FirstOrDefault(),
                             }).FirstOrDefault();
            return userModel;
        }
        public Int32 AddNewUser(DataModel.UserModel model)
        {
            DataAccess.User newUser = new DataAccess.User();
            newUser.UserID = Guid.NewGuid();
            newUser.UserProfileID = model.UserProfileID;
            newUser.RoleID = model.Role;
            newUser.FirstName = model.FirstName;
            newUser.LastName = model.LastName;
            newUser.Gender = model.Gender;
            newUser.Phone = model.Phone;
            newUser.Address = model.Address;
            newUser.City = model.City;
            newUser.Country = model.Country;
            newUser.Image = model.Image;
            newUser.CreatedBy = model.CreatedBy;
            newUser.CreatedOn = model.CreatedOn;
            _MedORDB.Users.AddObject(newUser);
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                if (model.SelectedClinicIDs != null)
                {
                    foreach (var clinicID in model.SelectedClinicIDs)
                    {
                        AddNewUserClinic(newUser.UserID, clinicID, model.CreatedBy, model.CreatedOn);
                    }
                }

                if (model.SelectedDoctorIDs != null)
                {
                    foreach (var doctorID in model.SelectedDoctorIDs)
                    {
                        AddNewUserDoctor(newUser.UserID, doctorID, model.CreatedBy, model.CreatedOn);
                    }
                }
            }

            return success;
        }
        public Int32 UpdateUser(DataModel.UserModel model)
        {
            var updateUser = _MedORDB.Users.Where(x => x.UserID == model.UserID).FirstOrDefault();
            updateUser.RoleID = model.Role;
            updateUser.FirstName = model.FirstName;
            updateUser.LastName = model.LastName;
            updateUser.Gender = model.Gender;
            updateUser.Phone = model.Phone;
            updateUser.Address = model.Address;
            updateUser.City = model.City;
            updateUser.Country = model.Country;
            updateUser.Image = model.Image;
            updateUser.ModifiedBy = model.ModifiedBy;
            updateUser.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                //Clinics access
                success = UpdateUserClinic(model.UserID, model.SelectedClinicIDs, model.ModifiedBy.Value, model.ModifiedOn.Value);

                //Doctors access
                success = UpdateUserDoctor(model.UserID, model.SelectedDoctorIDs, model.ModifiedBy.Value, model.ModifiedOn.Value);
            }

            return success;
        }
        public Int32 UpdateUserLastLoggedIn(DataModel.UserModel model)
        {
            var updateUser = _MedORDB.Users.Where(x => x.UserID == model.UserID).FirstOrDefault();
            updateUser.LastLoggedIn = model.LastLoggedIn;
            updateUser.ModifiedBy = model.ModifiedBy;
            updateUser.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public Int32 DeleteUserByID(Guid UserID)
        {
            var user = _MedORDB.Users.Where(x => x.UserID == UserID).FirstOrDefault();
            int success = 0;
            if (user != null)
            {
                //Delete all userClinics related to this user
                var userClinics = _MedORDB.UserClinics.Where(x => x.UserID == UserID);
                foreach (var userClinic in userClinics)
                    _MedORDB.UserClinics.DeleteObject(userClinic);

                //Delete all userDoctors related to this user
                var userDoctors = _MedORDB.UserDoctors.Where(x => x.UserID == UserID);
                foreach (var userDoctor in userDoctors)
                    _MedORDB.UserDoctors.DeleteObject(userDoctor);

                _MedORDB.Users.DeleteObject(user);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion
        #region Invoices
        public DataModel.InvoicesModel GetInvoicesByID(Guid CreatedBy)
        {
            var InvoicesModel = (from pi in _MedORDB.Invoices
                                 where pi.CreatedBy == CreatedBy
                                 select new DataModel.InvoicesModel
                                 {
                                     InvoiceID = pi.InvoiceID,
                                     PatientID = pi.PatientID,
                                     DoctorID = pi.DoctorID,
                                     Total = pi.Total,
                                     VAT = (int)pi.VAT,
                                     SubTotal = pi.SubTotal,
                                     CreatedON = pi.CreatedON,
                                     CreatedBy = pi.CreatedBy,
                                     ModifiedON = pi.ModifiedOn,
                                     ModifiedBy = pi.ModifiedBy

                                 }).FirstOrDefault();
            return InvoicesModel;
        }
        public IEnumerable<DataModel.InvoiceItemModel> GetItemsByInvoiceID(Guid InvoiceID)
        {
            var ItemsInvoice = (from pi in _MedORDB.InvoiceItems
                                where pi.InvoiceID == InvoiceID
                                select new DataModel.InvoiceItemModel
                                {
                                    InvoiceID = pi.InvoiceID,
                                    InvoiceItemID = pi.InvoiceItemID,
                                    InventoryID = pi.InventoryID,
                                    Total = pi.Total,
                                    Description = pi.Description,
                                    Discount = pi.Discount,
                                    UnitCost = pi.Unicost,
                                    Quantity = pi.Quantity,
                                    //TotalInvoice = (from u in _MedORDB.Invoices where u.InvoiceID == InvoiceID select u.Total ).FirstOrDefault(),
                                    //SubTotalInvoice = (from x in _MedORDB.Invoices where x.InvoiceID == InvoiceID select x.SubTotal).FirstOrDefault(),
                                    InvoicesModels = from dd in _MedORDB.Invoices
                                                     where dd.InvoiceID == InvoiceID
                                                     select new DataModel.InvoicesModel
                                                     {
                                                         InvoiceID = dd.InvoiceID,
                                                         PatientID = dd.PatientID,
                                                         DoctorID = dd.DoctorID,
                                                         SubTotal = dd.SubTotal,
                                                         Total = dd.Total,
                                                         Title = dd.Title,
                                                         FullNamePatient = (from u in _MedORDB.Patients where dd.PatientID == u.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                         DoctorName = (from x in _MedORDB.Doctors where dd.DoctorID == x.DoctorID select x.FirstName + " " + x.LastName).FirstOrDefault(),
                                                         //VAT= dd.VAT
                                                     }
                                });
            return ItemsInvoice;
        }
        public IEnumerable<DataModel.InvoicesModel> GetAllInvoices(Guid UserID)
        {
            var InvoicesModel = (from p in _MedORDB.Invoices
                                 select new DataModel.InvoicesModel
                                 {
                                     PatientID = p.PatientID,
                                     InvoiceID = p.InvoiceID,
                                     DoctorID = p.DoctorID,
                                     Total = p.Total,
                                     SubTotal = p.SubTotal,
                                     ModifiedON = p.ModifiedOn,
                                     CreatedON = p.CreatedON,
                                     CreatedBy = p.CreatedBy,
                                     ModifiedBy = p.ModifiedBy,
                                     Title = p.Title,
                                     FullNamePatient = (from u in _MedORDB.Patients where u.PatientID == p.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                     DoctorName = (from x in _MedORDB.Doctors where x.DoctorID == p.DoctorID select x.FirstName + " " + x.LastName).FirstOrDefault(),

                                     SecurityModel = (from u in _MedORDB.Users
                                                      where u.UserID == UserID
                                                      from r in _MedORDB.Roles
                                                      where r.RoleID == u.RoleID
                                                      from m in _MedORDB.Permissions
                                                      where m.PermissionName == "Invoices"
                                                      from s in _MedORDB.Securities
                                                      where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID
                                                      select new DataModel.SecurityModel
                                                      {
                                                          View = (from m in _MedORDB.Permissions where m.PermissionName == "Invoices" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.View).FirstOrDefault(),
                                                          Create = (from m in _MedORDB.Permissions where m.PermissionName == "Invoices" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Create).FirstOrDefault(),
                                                          Edit = (from m in _MedORDB.Permissions where m.PermissionName == "Invoices" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Edit).FirstOrDefault(),
                                                          Delete = (from m in _MedORDB.Permissions where m.PermissionName == "Invoices" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Delete).FirstOrDefault(),
                                                      }).FirstOrDefault()

                                 });

            return InvoicesModel;
        }
        public Int32 UpdateInvoice(Guid InvoiceID, decimal SubTotal, decimal GTotal)
        {
            var editInvoice = _MedORDB.Invoices.Where(x => x.InvoiceID == InvoiceID).FirstOrDefault();
            editInvoice.SubTotal = SubTotal;
            editInvoice.Total = GTotal;
            editInvoice.ModifiedOn = DateTime.Now;


            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteInvoiceByID(Guid InvoiceID)
        {
            var Invoice = _MedORDB.Invoices.Where(x => x.InvoiceID == InvoiceID).FirstOrDefault();
            var Items = _MedORDB.InvoiceItems.Where(x => x.InvoiceID == InvoiceID);
            int success = 0;

            if (Invoice != null)
            {
                _MedORDB.Invoices.DeleteObject(Invoice);
                foreach (var Item in Items)
                    _MedORDB.InvoiceItems.DeleteObject(Item);
                success = _MedORDB.SaveChanges();
            }

            return success;
        }
        public Int32 DeleteInvoiceItemByID(Guid InvoiceItemID)
        {

            var InvoiceItem = _MedORDB.InvoiceItems.Where(x => x.InvoiceItemID == InvoiceItemID).FirstOrDefault(); ;
            int success = 0;

            if (InvoiceItem != null)
            {
                _MedORDB.InvoiceItems.DeleteObject(InvoiceItem);

                success = _MedORDB.SaveChanges();
            }

            return success;
        }



        #endregion
        #region InvoiceItem
        public DataModel.InvoiceItemModel GetInvoicesModelByInvoice(Guid InvoiceID)
        {
            var InvoiceItemModel = (from pi in _MedORDB.InvoiceItems
                                    where pi.InvoiceID == InvoiceID
                                    select new DataModel.InvoiceItemModel
                                    {
                                        InvoiceItemID = pi.InvoiceItemID,
                                        InvoiceID = pi.InvoiceID,
                                        InventoryID = pi.InventoryID,
                                        ConsultationID = pi.ConsultationID,
                                        Description = pi.Description,
                                        UnitCost = pi.Unicost,
                                        Total = pi.Total,
                                        Discount = (int)pi.Discount,
                                        Quantity = pi.Quantity




                                    }).FirstOrDefault();
            return InvoiceItemModel;
        }


        public Int32 AddNewInvoice(DataModel.InvoicesModel model)
        {
            DataAccess.Invoice newInvoice = new DataAccess.Invoice();
            newInvoice.InvoiceID = model.InvoiceID;
            newInvoice.DoctorID = model.DoctorID;
            newInvoice.PatientID = model.PatientID;

            //newInvoice.VAT = model.VAT;
            newInvoice.Total = model.Total;
            newInvoice.SubTotal = model.SubTotal;
            newInvoice.CreatedON = model.CreatedON;
            newInvoice.CreatedBy = model.CreatedBy;
            newInvoice.Title = model.Title;


            _MedORDB.Invoices.AddObject(newInvoice);
            var success = _MedORDB.SaveChanges();

            return success;
        }


        public Int32 AddNewInvoiceItem(DataModel.InvoiceItemModel model)
        {
            DataAccess.InvoiceItem newInvoiceItem = new DataAccess.InvoiceItem();
            newInvoiceItem.InvoiceID = model.InvoiceID;
            newInvoiceItem.InvoiceItemID = model.InvoiceItemID;
            newInvoiceItem.ConsultationID = model.ConsultationID;

            //newInvoice.VAT = model.VAT;
            newInvoiceItem.Description = model.Description;
            newInvoiceItem.Quantity = model.Quantity;
            newInvoiceItem.Unicost = model.UnitCost;
            newInvoiceItem.Total = model.Total;
            //newInvoice.ModifiedBy = model.ModifiedBy;


            _MedORDB.InvoiceItems.AddObject(newInvoiceItem);
            var success = _MedORDB.SaveChanges();

            return success;
        }
        #endregion
        #region UserClinic
        public Int32 AddNewUserClinic(Guid userID, Guid clinicID, Guid createdBy, DateTime createdOn)
        {
            DataAccess.UserClinic newUserClinic = new DataAccess.UserClinic();
            newUserClinic.UserClinicID = Guid.NewGuid();
            newUserClinic.UserID = userID;
            newUserClinic.ClinicID = clinicID;
            newUserClinic.CreatedBy = createdBy;
            newUserClinic.CreatedOn = createdOn;
            _MedORDB.UserClinics.AddObject(newUserClinic);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateUserClinic(Guid userID, IEnumerable<Guid> selectedClinicIDs, Guid createdBy, DateTime createdOn)
        {
            var success = 1;
            var previousClinicIDs = _MedORDB.UserClinics.Where(x => x.UserID == userID).Select(x => x.ClinicID);
            var previousUserClinics = _MedORDB.UserClinics.Where(x => x.UserID == userID);
            IQueryable<Guid> removeClinicIDs;

            if (selectedClinicIDs != null)
                removeClinicIDs = previousClinicIDs.Except(selectedClinicIDs);
            else
                removeClinicIDs = previousClinicIDs;

            if (removeClinicIDs.Count() != 0)
            {
                foreach (var removeClinicID in removeClinicIDs)
                {
                    _MedORDB.UserClinics.DeleteObject(previousUserClinics.Where(x => x.ClinicID == removeClinicID).FirstOrDefault());
                }
                success = _MedORDB.SaveChanges();
            }

            if (selectedClinicIDs != null)
            {
                var addClinicIDs = selectedClinicIDs.Except(previousClinicIDs);
                foreach (var addClinicID in addClinicIDs)
                    success = AddNewUserClinic(userID, addClinicID, createdBy, createdOn);
            }

            return success;
        }
        #endregion
        #region UserDoctor
        public Int32 AddNewUserDoctor(Guid userID, Guid doctorID, Guid createdBy, DateTime createdOn)
        {
            DataAccess.UserDoctor newUserDoctor = new DataAccess.UserDoctor();
            newUserDoctor.UserDoctorID = Guid.NewGuid();
            newUserDoctor.UserID = userID;
            newUserDoctor.DoctorID = doctorID;
            newUserDoctor.CreatedBy = createdBy;
            newUserDoctor.CreatedOn = createdOn;
            _MedORDB.UserDoctors.AddObject(newUserDoctor);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateUserDoctor(Guid userID, IEnumerable<Guid> selectedDoctorIDs, Guid createdBy, DateTime createdOn)
        {
            var success = 1;
            var previousDoctorIDs = _MedORDB.UserDoctors.Where(x => x.UserID == userID).Select(x => x.DoctorID);
            var previousUserDoctors = _MedORDB.UserDoctors.Where(x => x.UserID == userID);
            IQueryable<Guid> removeDoctorIDs;

            if (selectedDoctorIDs != null)
                removeDoctorIDs = previousDoctorIDs.Except(selectedDoctorIDs);
            else
                removeDoctorIDs = previousDoctorIDs;

            if (removeDoctorIDs.Count() != 0)
            {
                foreach (var removeDoctorID in removeDoctorIDs)
                {
                    _MedORDB.UserDoctors.DeleteObject(previousUserDoctors.Where(x => x.DoctorID == removeDoctorID).FirstOrDefault());
                }
                success = _MedORDB.SaveChanges();
            }

            if (selectedDoctorIDs != null)
            {
                var addDoctorIDs = selectedDoctorIDs.Except(previousDoctorIDs);
                foreach (var addDoctorID in addDoctorIDs)
                    success = AddNewUserDoctor(userID, addDoctorID, createdBy, createdOn);
            }

            return success;
        }
        #endregion
        #region Clinic
        public IEnumerable<DataModel.ClinicModel> GetClinicsByUserID(Guid UserID)
        {
            var clinicModel = (from uc in _MedORDB.UserClinics
                               where uc.UserID == UserID
                               from c in _MedORDB.Clinics
                               where c.ClinicID == uc.ClinicID
                               select new DataModel.ClinicModel
                               {
                                   ClinicID = c.ClinicID,
                                   Name = c.Name,
                                   Address = c.Address,
                                   City = c.City,
                                   Country = c.Country,
                                   Phone = c.Phone,
                                   CreatedBy = c.CreatedBy,
                                   CreatedByName = (from u in _MedORDB.Users where u.UserID == c.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   CreatedOn = c.CreatedOn,
                                   ModifiedBy = c.ModifiedBy,
                                   ModifiedByName = (from u in _MedORDB.Users where u.UserID == c.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   ModifiedOn = c.ModifiedOn,
                                   SecurityModel = (from u in _MedORDB.Users
                                                    where u.UserID == UserID
                                                    from r in _MedORDB.Roles
                                                    where r.RoleID == u.RoleID
                                                    from p in _MedORDB.Permissions
                                                    where p.PermissionName == "Clinics"
                                                    from s in _MedORDB.Securities
                                                    where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                    select new DataModel.SecurityModel
                                                    {
                                                        View = s.View,
                                                        Create = s.Create,
                                                        Edit = s.Edit,
                                                        Delete = s.Delete
                                                    }).FirstOrDefault(),
                               }).OrderBy(o => o.Name);
            return clinicModel;
        }
        public Int32 AddNewClinic(DataModel.ClinicModel model)
        {
            DataAccess.Clinic newClinic = new DataAccess.Clinic();

            newClinic.ClinicID = Guid.NewGuid();
            newClinic.Name = model.Name;
            newClinic.Address = model.Address;
            newClinic.City = model.City;
            newClinic.Country = model.Country;
            newClinic.Phone = model.Phone;
            newClinic.CreatedBy = model.CreatedBy;
            newClinic.CreatedOn = model.CreatedOn;
            _MedORDB.Clinics.AddObject(newClinic);
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                AddNewUserClinic(model.CreatedBy, newClinic.ClinicID, model.CreatedBy, model.CreatedOn);
            }
            return success;
        }
        public DataModel.ClinicModel GetClinicByID(Guid ClinicID)
        {
            var clinicModel = (from c in _MedORDB.Clinics
                               where c.ClinicID == ClinicID
                               select new DataModel.ClinicModel
                               {
                                   ClinicID = c.ClinicID,
                                   Name = c.Name,
                                   Address = c.Address,
                                   City = c.City,
                                   Country = c.Country,
                                   Phone = c.Phone,
                               }).FirstOrDefault();
            return clinicModel;
        }
        public Int32 UpdateClinic(DataModel.ClinicModel model)
        {
            var editClinic = _MedORDB.Clinics.Where(x => x.ClinicID == model.ClinicID).FirstOrDefault();
            editClinic.Name = model.Name;
            editClinic.Address = model.Address;
            editClinic.City = model.City;
            editClinic.Country = model.Country;
            editClinic.Phone = model.Phone;
            editClinic.ModifiedBy = model.ModifiedBy;
            editClinic.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteClinicByID(Guid ClinicID)
        {
            var clinic = _MedORDB.Clinics.Where(x => x.ClinicID == ClinicID).FirstOrDefault();
            int success = 0;
            if (clinic != null)
            {
                _MedORDB.Clinics.DeleteObject(clinic);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion
        #region Security
        public DataModel.RoleModel GetAllPermissionsAndUsersForRoleModel()
        {
            var roleModel = new DataModel.RoleModel
            {
                PermissionModels = (from p in _MedORDB.Permissions
                                    select new DataModel.PermissionModel
                                    {
                                        PermissionID = p.PermissionID,
                                        PermissionName = p.PermissionName
                                    }).OrderBy(o => o.PermissionName).ToList(),
                UserModels = (from u in _MedORDB.Users
                            where u.RoleID == null
                              select new DataModel.UserModel
                              {
                                  UserID = u.UserID,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName
                              }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName),
            };
            return roleModel;
        }
        public IEnumerable<DataModel.RoleModel> GetRoleAndPermissions(Guid UserID)
        {
            var RoleModels = (from r in _MedORDB.Roles
                              where r.IsSuperAdmin == null
                              select new DataModel.RoleModel
                              {
                                  RoleID = r.RoleID,
                                  Name = r.Name,
                                  CreatedBy = r.CreatedBy,
                                  CreatedByName = (from us in _MedORDB.Users where us.UserID == r.CreatedBy select us.FirstName + " " + us.LastName).FirstOrDefault(),
                                  CreatedOn = r.CreatedOn,
                                  ModifiedBy = r.ModifiedBy,
                                  ModifiedByName = (from us in _MedORDB.Users where us.UserID == r.ModifiedBy select us.FirstName + " " + us.LastName).FirstOrDefault(),
                                  ModifiedOn = r.ModifiedOn,
                                  SecurityModel = (
                                  from u in _MedORDB.Users
                                      //where u.UserID == UserID
                                      //from l in _MedORDB.Roles
                                      //where l.RoleID == u.RoleID
                                      //from p in _MedORDB.Permissions
                                      //where p.PermissionName == "Roles"
                                      //from s in _MedORDB.Securities
                                      //where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                     
                              where u.UserID == UserID
                                      from l in _MedORDB.Roles
                                     where l.RoleID == u.RoleID
                                    // from p in _MedORDB.Permissions
                                    // where p.PermissionName == "Roles"
                                      from s in _MedORDB.Securities
                                      where s.RoleID == l.RoleID
                                  from p in _MedORDB.Permissions
                                  where p.PermissionID==s.PermissionID && p.PermissionName=="Roles"


                                  select new DataModel.SecurityModel
                                                   {
                                                       View = s.View,
                                                       Create = s.Create,
                                                       Edit = s.Edit,
                                                       Delete = s.Delete
                                                   }).FirstOrDefault()
                              }).OrderBy(o => o.Name);
            return RoleModels;
        }
        public List<DataModel.PermissionModel> GetAllPermissionsForRoleModel(Guid roleID)
        {
            var permissionModels = (from p in _MedORDB.Permissions
                                    select new DataModel.PermissionModel
                                    {
                                        PermissionID = p.PermissionID,
                                        PermissionName = p.PermissionName,
                                        View = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.View).FirstOrDefault(),
                                        Create = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.Create).FirstOrDefault(),
                                        Edit = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.Edit).FirstOrDefault(),
                                        Delete = (from s in _MedORDB.Securities where s.RoleID == roleID && s.PermissionID == p.PermissionID select s.Delete).FirstOrDefault(),
                                    }).OrderBy(o => o.PermissionName).ToList();
            return permissionModels;
        }
        public DataModel.RoleModel GetRoleByID(Guid RoleID)
        {
            var roleModel = (from r in _MedORDB.Roles
                             where r.RoleID == RoleID
                             select new DataModel.RoleModel
                             {
                                 RoleID = r.RoleID,
                                 Name = r.Name,
                                 SelectedUserIDs = from e in _MedORDB.Users
                                                   where e.RoleID == r.RoleID 
                                                   select e.UserID,
                                 UserModels = (
                                 
                                 from u in _MedORDB.Users
                                 from ro in _MedORDB.Roles
                                 where ro.IsSuperAdmin == null && u.RoleID==ro.RoleID

                                 select new DataModel.UserModel
                                 {
                                     UserID = u.UserID,
                                     FirstName = u.FirstName,
                                     LastName = u.LastName
                                 }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName),
                             }).FirstOrDefault();
            return roleModel;
        }

        public Int32 AddNewSecurityRole(DataModel.RoleModel model)
        {
            var success = 0;

            DataAccess.Role newRole = new DataAccess.Role();
            newRole.RoleID = Guid.NewGuid();
            newRole.Name = model.Name;
            newRole.CreatedOn = model.CreatedOn;
            newRole.CreatedBy = model.CreatedBy;
            _MedORDB.Roles.AddObject(newRole);
            success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                foreach (var permission in model.PermissionModels)
                {
                    DataAccess.Security newSecurity = new DataAccess.Security();
                    newSecurity.SecurityID = Guid.NewGuid();
                    newSecurity.RoleID = newRole.RoleID;
                    newSecurity.PermissionID = permission.PermissionID;
                    newSecurity.View = permission.View;
                    newSecurity.Create = permission.Create;
                    newSecurity.Edit = permission.Edit;
                    newSecurity.Delete = permission.Delete;
                    newSecurity.CreatedOn = model.CreatedOn;
                    newSecurity.CreatedBy = model.CreatedBy;
                    _MedORDB.Securities.AddObject(newSecurity);
                }
                success = _MedORDB.SaveChanges();
            }

            if (success != 0)
            {
                if (model.SelectedUserIDs != null)
                {
                    foreach (var selectedUserID in model.SelectedUserIDs)
                    {
                        var updateUser = _MedORDB.Users.Where(x => x.UserID == selectedUserID).FirstOrDefault();
                        updateUser.RoleID = newRole.RoleID;
                        updateUser.ModifiedBy = model.CreatedBy;
                        updateUser.ModifiedOn = model.CreatedOn;
                    }
                    success = _MedORDB.SaveChanges();
                }
            }
            return success;
        }
        //public Int32 UpdateSecurityRole(DataModel.RoleModel model)
        //{
        //    var success = 1;

        //    var updateRole = _MedORDB.Roles.Where(x => x.RoleID == model.RoleID).FirstOrDefault();
        //    updateRole.Name = model.Name;
        //    updateRole.ModifiedOn = model.ModifiedOn;
        //    updateRole.ModifiedBy = model.ModifiedBy;
        //    success = _MedORDB.SaveChanges();

        //    if (success != 0)
        //    {
        //        foreach (var permission in model.PermissionModels)
        //        {
        //            var updateSecurity = _MedORDB.Securities.Where(x => x.RoleID == model.RoleID && x.PermissionID == permission.PermissionID).FirstOrDefault();
        //            if (updateSecurity != null)
        //            {
        //                updateSecurity.View = permission.View;
        //                updateSecurity.Create = permission.Create;
        //                updateSecurity.Edit = permission.Edit;
        //                updateSecurity.Delete = permission.Delete;
        //                updateSecurity.ModifiedOn = model.ModifiedOn;
        //                updateSecurity.ModifiedBy = model.ModifiedBy;
        //            }
        //            else
        //            {
        //                DataAccess.Security newSecurity = new DataAccess.Security();
        //                newSecurity.SecurityID = Guid.NewGuid();
        //                newSecurity.RoleID = model.RoleID;
        //                newSecurity.PermissionID = permission.PermissionID;
        //                newSecurity.View = permission.View;
        //                newSecurity.Create = permission.Create;
        //                newSecurity.Edit = permission.Edit;
        //                newSecurity.Delete = permission.Delete;
        //                newSecurity.CreatedOn = model.ModifiedOn.Value;
        //                newSecurity.CreatedBy = model.ModifiedBy.Value;
        //                _MedORDB.Securities.AddObject(newSecurity);
        //            }
        //            success = _MedORDB.SaveChanges();
        //        }
        //    }

        //    if (success != 0)
        //    {
        //        var previousUsersID = _MedORDB.Users.Where(x => x.RoleID == model.RoleID).Select(u => u.UserID);


        //        if (model.SelectedUserIDs != null)
        //        {


        //            var removeUsersID = previousUsersID.Except(model.SelectedUserIDs);
        //        foreach (var removeUserID in removeUsersID)
        //        {
        //            var updateUser = _MedORDB.Users.Where(x => x.UserID == removeUserID).FirstOrDefault();
        //            updateUser.RoleID = Guid.Empty;
        //            updateUser.ModifiedBy = model.ModifiedBy;
        //            updateUser.ModifiedOn = model.ModifiedOn;
        //        }



        //        foreach (var selectedUserID in model.SelectedUserIDs)
        //        {
        //            var updateUser = _MedORDB.Users.Where(x => x.UserID == selectedUserID).FirstOrDefault();
        //            updateUser.RoleID = model.RoleID;
        //            updateUser.ModifiedBy = model.ModifiedBy;
        //            updateUser.ModifiedOn = model.ModifiedOn;
        //        }



        //       success = _MedORDB.SaveChanges();
        //        }






        //    }


        //    return success;
        //}
        public Int32 UpdateSecurityRole(DataModel.RoleModel model)
        {
            var success = 1;

            var updateRole = _MedORDB.Roles.Where(x => x.RoleID == model.RoleID).FirstOrDefault();
            updateRole.Name = model.Name;
            updateRole.ModifiedOn = model.ModifiedOn;
            updateRole.ModifiedBy = model.ModifiedBy;
            success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                foreach (var permission in model.PermissionModels)
                {
                    var updateSecurity = _MedORDB.Securities.Where(x => x.RoleID == model.RoleID && x.PermissionID == permission.PermissionID).FirstOrDefault();
                    if (updateSecurity != null)
                    {
                        updateSecurity.View = permission.View;
                        updateSecurity.Create = permission.Create;
                        updateSecurity.Edit = permission.Edit;
                        updateSecurity.Delete = permission.Delete;
                        updateSecurity.ModifiedOn = model.ModifiedOn;
                        updateSecurity.ModifiedBy = model.ModifiedBy;
                    }
                    else
                    {
                        DataAccess.Security newSecurity = new DataAccess.Security();
                        newSecurity.SecurityID = Guid.NewGuid();
                        newSecurity.RoleID = model.RoleID;
                        newSecurity.PermissionID = permission.PermissionID;
                        newSecurity.View = permission.View;
                        newSecurity.Create = permission.Create;
                        newSecurity.Edit = permission.Edit;
                        newSecurity.Delete = permission.Delete;
                        newSecurity.CreatedOn = model.ModifiedOn.Value;
                        newSecurity.CreatedBy = model.ModifiedBy.Value;
                        _MedORDB.Securities.AddObject(newSecurity);
                    }
                    success = _MedORDB.SaveChanges();
                }
            }

            if (success != 0)
            {
                var previousUsersID = _MedORDB.Users.Where(x => x.RoleID == model.RoleID).Select(u => u.UserID);

                if (model.SelectedUserIDs != null)
                {
                    var removeUsersID = previousUsersID.Except(model.SelectedUserIDs);

                    foreach (var removeUserID in removeUsersID)
                    {
                        var updateUser = _MedORDB.Users.Where(x => x.UserID == removeUserID).FirstOrDefault();
                        updateUser.RoleID = Guid.Empty;
                        updateUser.ModifiedBy = model.ModifiedBy;
                        updateUser.ModifiedOn = model.ModifiedOn;
                    }

                    foreach (var selectedUserID in model.SelectedUserIDs)
                    {
                        var updateUser = _MedORDB.Users.Where(x => x.UserID == selectedUserID).FirstOrDefault();
                        updateUser.RoleID = model.RoleID;
                        updateUser.ModifiedBy = model.ModifiedBy;
                        updateUser.ModifiedOn = model.ModifiedOn;
                    }
                    success = _MedORDB.SaveChanges();
                }
                else if (previousUsersID.Count() != 0)
                {
                    var removeUsersID = previousUsersID;

                    foreach (var removeUserID in removeUsersID)
                    {
                        var updateUser = _MedORDB.Users.Where(x => x.UserID == removeUserID).FirstOrDefault();
                        updateUser.RoleID = Guid.Empty;
                        updateUser.ModifiedBy = model.ModifiedBy;
                        updateUser.ModifiedOn = model.ModifiedOn;
                    }
                    success = _MedORDB.SaveChanges();
                }
                else { }
            }
            return success;
        }


        public Int32 DeleteRoleByID(Guid RoleID)
        {
            var role = _MedORDB.Roles.Where(x => x.RoleID == RoleID).FirstOrDefault();
            int success = 0;
            if (role != null)
            {
                _MedORDB.Roles.DeleteObject(role);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion
        #region Doctor
        public IEnumerable<DataModel.DoctorModel> GetDoctorsByUserID(Guid UserID)
        {
            var doctorModel = (from ud in _MedORDB.UserDoctors
                               where ud.UserID == UserID
                               from d in _MedORDB.Doctors
                               where d.DoctorID == ud.DoctorID
                               select new DataModel.DoctorModel
                               {
                                   DoctorID = d.DoctorID,
                                   FirstName = d.FirstName,
                                   LastName = d.LastName,
                                   Gender = d.Gender,
                                   Email = d.Email,
                                   MobilePhone = d.MobilePhone,
                                   WorkPhone = d.WorkPhone,
                                   SpecialtyID = d.SpecialtyID,
                                   SpecialtyName = (from dd in _MedORDB.DropDowns where dd.DropDownID == d.SpecialtyID select dd.Value).FirstOrDefault(),
                                   CreatedBy = d.CreatedBy,
                                   CreatedByName = (from u in _MedORDB.Users where u.UserID == d.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   CreatedOn = d.CreatedOn,
                                   ModifiedBy = d.ModifiedBy,
                                   ModifiedByName = (from u in _MedORDB.Users where u.UserID == d.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                   ModifiedOn = d.ModifiedOn,
                                   SecurityModel = (from u in _MedORDB.Users
                                                    where u.UserID == UserID
                                                    from r in _MedORDB.Roles
                                                    where r.RoleID == u.RoleID
                                                    from p in _MedORDB.Permissions
                                                    where p.PermissionName == "Doctors"
                                                    from s in _MedORDB.Securities
                                                    where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                    select new DataModel.SecurityModel
                                                    {
                                                        View = s.View,
                                                        Create = s.Create,
                                                        Edit = s.Edit,
                                                        Delete = s.Delete
                                                    }).FirstOrDefault()
                               }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName);
            return doctorModel;
        }
        public Int32 AddNewDoctor(DataModel.DoctorModel model)
        {
            DataAccess.Doctor newDoctor = new DataAccess.Doctor();
            newDoctor.DoctorID = Guid.NewGuid();
            newDoctor.FirstName = model.FirstName;
            newDoctor.LastName = model.LastName;
            newDoctor.Gender = model.Gender;
            newDoctor.Email = model.Email;
            newDoctor.MobilePhone = model.MobilePhone;
            newDoctor.WorkPhone = model.WorkPhone;
            newDoctor.SpecialtyID = model.Specialty;
            newDoctor.CreatedBy = model.CreatedBy;
            newDoctor.CreatedOn = model.CreatedOn;
            _MedORDB.Doctors.AddObject(newDoctor);
            var success = _MedORDB.SaveChanges();

            if (success != 0)
            {
                AddNewUserDoctor(model.CreatedBy, newDoctor.DoctorID, model.CreatedBy, model.CreatedOn);
            }

            return success;
        }
        public DataModel.DoctorModel GetDoctorByID(Guid DoctorID)
        {
            var doctorModel = (from d in _MedORDB.Doctors
                               where d.DoctorID == DoctorID
                               select new DataModel.DoctorModel
                               {
                                   DoctorID = d.DoctorID,
                                   FirstName = d.FirstName,
                                   LastName = d.LastName,
                                   Gender = d.Gender,
                                   Email = d.Email,
                                   MobilePhone = d.MobilePhone,
                                   WorkPhone = d.WorkPhone,
                                   SpecialtyID = d.SpecialtyID,
                                   Specialty = d.SpecialtyID,
                                   DropDownModel = from dd in _MedORDB.DropDowns
                                                   where dd.Type == "Specialty"
                                                   select new DataModel.DropDownModel
                                                   {
                                                       DropDownID = dd.DropDownID,
                                                       Value = dd.Value
                                                   }
                               }).FirstOrDefault();
            return doctorModel;
        }
        public Int32 UpdateDoctor(DataModel.DoctorModel model)
        {
            var editDoctor = _MedORDB.Doctors.Where(x => x.DoctorID == model.DoctorID).FirstOrDefault();
            editDoctor.FirstName = model.FirstName;
            editDoctor.LastName = model.LastName;
            editDoctor.Gender = model.Gender;
            editDoctor.Email = model.Email;
            editDoctor.MobilePhone = model.MobilePhone;
            editDoctor.WorkPhone = model.WorkPhone;
            editDoctor.SpecialtyID = model.Specialty;
            editDoctor.ModifiedBy = model.ModifiedBy;
            editDoctor.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteDoctorByID(Guid DoctorID)
        {
            var doctor = _MedORDB.Doctors.Where(x => x.DoctorID == DoctorID).FirstOrDefault();
            int success = 0;
            if (doctor != null)
            {
                _MedORDB.Doctors.DeleteObject(doctor);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        public DataModel.DoctorModel GetSpecialties()
        {
            var doctorModel = new DataModel.DoctorModel
            {
                DropDownModel = from dd in _MedORDB.DropDowns
                                where dd.Type == "Specialty"
                                select new DataModel.DropDownModel
                                {
                                    DropDownID = dd.DropDownID,
                                    Value = dd.Value
                                }
            };
            return doctorModel;
        }
        #endregion  
        #region Patient
        public IEnumerable<DataModel.PaitentModel> GetAllPatients(Guid UserID)
        {
            var patientModel = (from p in _MedORDB.Patients
                                select new DataModel.PaitentModel
                                {
                                    PatientID = p.PatientID,
                                    FirstName = p.FirstName,
                                    MiddleName = p.MiddleName,
                                    LastName = p.LastName,
                                    PatientEmail = p.Email,
                                    City = p.City,
                                    Image = p.Image,
                                    MobilePhone = p.MobilePhone,
                                    HomePhone = p.HomePhone,
                                    CreatedBy = p.CreatedBy,
                                    CreatedByName = (from u in _MedORDB.Users where u.UserID == p.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                    CreatedOn = p.CreatedOn,
                                    ModifiedBy = p.ModifiedBy,
                                    ModifiedByName = (from u in _MedORDB.Users where u.UserID == p.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                    ModifiedOn = p.ModifiedOn,
                                    SecurityModel = (from u in _MedORDB.Users
                                                     where u.UserID == UserID
                                                     from r in _MedORDB.Roles
                                                     where r.RoleID == u.RoleID
                                                     //from m in _MedORDB.Permissions
                                                     //where m.PermissionName == "Patients"
                                                     //from s in _MedORDB.Securities
                                                     //where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID
                                                     select new DataModel.SecurityModel
                                                     {
                                                         View = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.View).FirstOrDefault(),
                                                         Create = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Create).FirstOrDefault(),
                                                         Edit = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Edit).FirstOrDefault(),
                                                         Delete = (from m in _MedORDB.Permissions where m.PermissionName == "Patients" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.Delete).FirstOrDefault(),
                                                         MedicalHistoryView = (from m in _MedORDB.Permissions where m.PermissionName == "MedicalHistory" from s in _MedORDB.Securities where s.PermissionID == m.PermissionID && s.RoleID == r.RoleID select s.View).FirstOrDefault(),
                                                     }).FirstOrDefault()
                                }).OrderBy(o => o.FirstName).ThenBy(o => o.LastName);
            return patientModel;
        }
        public Int32 AddNewPatient(DataModel.PaitentModel model)
        {
            DataAccess.Patient newPatient = new DataAccess.Patient();
            newPatient.PatientID = Guid.NewGuid();
            newPatient.FirstName = model.FirstName;
            newPatient.MiddleName = model.MiddleName;
            newPatient.LastName = model.LastName;
            newPatient.Profession = model.Profession;
            newPatient.DateOfBirth = model.DateOfBirth;
            newPatient.Gender = model.Gender;
            newPatient.Email = model.PatientEmail;
            newPatient.MobilePhone = model.MobilePhone;
            newPatient.HomePhone = model.HomePhone;
            newPatient.Address = model.Address;
            newPatient.City = model.City;
            newPatient.Image = model.Image;
            newPatient.Country = model.Country;
            newPatient.CreatedBy = model.CreatedBy;
            newPatient.CreatedOn = model.CreatedOn;
            _MedORDB.Patients.AddObject(newPatient);
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public DataModel.PaitentModel GetPatientByID(Guid PatientID)
        {
            var patientModel = (from p in _MedORDB.Patients
                                where p.PatientID == PatientID
                                select new DataModel.PaitentModel
                                {
                                    PatientID = p.PatientID,
                                    FirstName = p.FirstName,
                                    MiddleName = p.MiddleName,
                                    LastName = p.LastName,
                                    Profession = p.Profession,
                                    Gender = p.Gender,
                                    PatientEmail = p.Email,
                                    MobilePhone = p.MobilePhone,
                                    HomePhone = p.HomePhone,
                                    DateOfBirth = p.DateOfBirth,
                                    Address = p.Address,
                                    Image = p.Image,
                                    City = p.City,
                                    Country = p.Country,
                                }).FirstOrDefault();
            return patientModel;
        }
        public Guid GetPatientID(Guid ID)
        {
            var ConsultationModel = (from p in _MedORDB.IMconsultations
                                where p.IMconsultationID == ID
                                select new DataModel.PaitentModel
                                {
                                    PatientID = p.PatientID,
                                }).FirstOrDefault();
            Guid ConsultationID = ConsultationModel.PatientID;
            return ConsultationID ;
        }
        public Byte[] GetPatientImage(Guid PatientID)
        {
            var patientImage = (from p in _MedORDB.Patients
                                where p.PatientID == PatientID
                                select p.Image).FirstOrDefault();
            return patientImage;
        }
        public Int32 UpdatePatient(DataModel.PaitentModel model)
        {
            var editPatient = _MedORDB.Patients.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            editPatient.FirstName = model.FirstName;
            editPatient.MiddleName = model.MiddleName;
            editPatient.LastName = model.LastName;
            editPatient.Profession = model.Profession;
            editPatient.Gender = model.Gender;
            editPatient.Email = model.PatientEmail;
            editPatient.MobilePhone = model.MobilePhone;
            editPatient.HomePhone = model.HomePhone;
            editPatient.DateOfBirth = model.DateOfBirth;
            editPatient.Address = model.Address;
            editPatient.City = model.City;
            editPatient.Country = model.Country;
            editPatient.Image = model.Image;
            editPatient.ModifiedBy = model.ModifiedBy;
            editPatient.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewFile(DataModel.IMFileModel model)
        {
            DataAccess.IMFile IMFile = new DataAccess.IMFile();
            IMFile.FileID = Guid.NewGuid();
            IMFile.PatientID = model.PatientID;
            IMFile.Consultation = model.ConsultationID;

            IMFile.Data = model.Data;
            IMFile.ContentType = model.ContentType;
            IMFile.FileName = model.FileName;
            
            IMFile.CreatedBy = model.createdBy;
            IMFile.CreatedOn = model.createdOn;
            _MedORDB.IMFiles.AddObject(IMFile);
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public Int32 DeletePatientByID(Guid PatientID)
        {
            var patient = _MedORDB.Patients.Where(x => x.PatientID == PatientID).FirstOrDefault();
            int success = 0;
            if (patient != null)
            {
                _MedORDB.Patients.DeleteObject(patient);
                success = _MedORDB.SaveChanges();
            }
            return success;
        }
        #endregion
        #region Calendar
        public DataModel.CalendarModel GetCalendarByUserID(Guid UserID)
        {
            var calendarModel = new DataModel.CalendarModel
            {
                ClinicModels = (from us in _MedORDB.UserClinics
                                where us.UserID == UserID
                                from c in _MedORDB.Clinics
                                where us.ClinicID == c.ClinicID
                                select new DataModel.ClinicModel
                                {
                                    Name = c.Name,
                                    ClinicID = c.ClinicID
                                }).OrderBy(o => o.Name)
            };
            return calendarModel;
        }
        public IEnumerable<DataModel.AppointmentModel> GetAppointmentsByUserIDAndClinicID(Guid UserID, Guid? ClinicID)
        {
            var appointmentModel = (from uc in _MedORDB.UserClinics
                                    where uc.UserID == UserID && uc.ClinicID == ClinicID
                                    from a in _MedORDB.Appointments
                                    where a.ClinicID == uc.ClinicID
                                    select new DataModel.AppointmentModel
                                    {
                                        AppointmentID = a.AppointmentID,
                                        PatientID = a.PatientID,
                                        DoctorID = a.DoctorID,
                                        ClinicID = a.ClinicID,
                                        StartDateTime = a.StartDateTime.Value,
                                        EndDateTime = a.EndDateTime.Value,
                                        title = (((from p in _MedORDB.Patients where p.PatientID == a.PatientID select p.FirstName + " " + p.LastName).FirstOrDefault())+(a.title)),
                                        NewPatientName=a.title,

                                        AllDay = a.AllDay,
                                        Note = a.Note,
                                        ClinicModels = (from us in _MedORDB.UserClinics
                                                        where us.UserID == UserID
                                                        from c in _MedORDB.Clinics
                                                        where us.ClinicID == c.ClinicID
                                                        select new DataModel.ClinicModel
                                                        {
                                                            ClinicID = us.ClinicID,
                                                            Name = c.Name
                                                        }
                                                      ).OrderBy(o => o.Name),
                                        PatientName = (from u in _MedORDB.Patients where a.PatientID == u.PatientID select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                        DoctorName = (from x in _MedORDB.Doctors where a.DoctorID == x.DoctorID select x.FirstName + " " + x.LastName).FirstOrDefault(),


                                        SecurityModel = (from u in _MedORDB.Users
                                                         where u.UserID == UserID
                                                         from r in _MedORDB.Roles
                                                         where r.RoleID == u.RoleID
                                                         from p in _MedORDB.Permissions
                                                         where p.PermissionName == "Appointments"
                                                         from s in _MedORDB.Securities
                                                         where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                         select new DataModel.SecurityModel
                                                         {
                                                             View = s.View,
                                                             Create = s.Create,
                                                             Edit = s.Edit,
                                                             Delete = s.Delete
                                                         }).FirstOrDefault()
                                    }).OrderBy(o => o.StartDateTime);
            return appointmentModel;
        }
        public DataModel.AppointmentModel GetAppointmentByID(Guid AppointmentID)
        {
            DataModel.AppointmentModel appointmentModel = new DataModel.AppointmentModel() ;
            var patientID = (from p in _MedORDB.Appointments where p.AppointmentID == AppointmentID select p.PatientID).FirstOrDefault();

            //NewPatient
            if (patientID == Guid.Empty) {
                appointmentModel = (from a in _MedORDB.Appointments
                                    where a.AppointmentID == AppointmentID
                                    select new DataModel.AppointmentModel
                                    {
                                        AppointmentID = a.AppointmentID,
                                        PatientID = a.PatientID,
                                        DoctorID = a.DoctorID,
                                        ClinicID = a.ClinicID,
                                        StartDateTime = a.StartDateTime,
                                        EndDateTime = a.EndDateTime,
                                        title = a.title ,
                                        PhoneNumber = a.PhoneNumber,
                                        AllDay = a.AllDay,
                                        Note = a.Note,
                                    }).FirstOrDefault();

            }
            else { 

            appointmentModel = (from a in _MedORDB.Appointments
                                    where a.AppointmentID == AppointmentID
                                    select new DataModel.AppointmentModel
                                    {
                                        AppointmentID = a.AppointmentID,
                                        PatientID = a.PatientID,
                                        DoctorID = a.DoctorID,
                                        ClinicID = a.ClinicID,
                                        StartDateTime = a.StartDateTime,
                                        EndDateTime = a.EndDateTime,
                                        title =  (from p in _MedORDB.Patients where p.PatientID==a.PatientID select p.FirstName + " "+ p.LastName).FirstOrDefault() ,
                                        PhoneNumber=a.PhoneNumber+" " +(from p in _MedORDB.Patients where p.PatientID == a.PatientID select p.MobilePhone).FirstOrDefault(),
                                        AllDay = a.AllDay,
                                        Note = a.Note,
                                    }).FirstOrDefault();
               
            }
            return appointmentModel;
        }
        public Int32 AddNewAppointment(DataModel.AppointmentModel model)
        {
            DataAccess.Appointment newAppointment = new DataAccess.Appointment();
            newAppointment.AppointmentID = Guid.NewGuid();
            newAppointment.PatientID = model.PatientID;
            newAppointment.DoctorID = model.DoctorID;
            newAppointment.ClinicID = model.ClinicID;
            newAppointment.title = model.title;
            newAppointment.PhoneNumber = model.PhoneNumber;
            newAppointment.Note = model.Note;
            newAppointment.AllDay = model.allDay;
            newAppointment.StartDateTime = model.StartDateTime;
            newAppointment.EndDateTime = model.EndDateTime;
            newAppointment.CreatedBy = model.CreatedBy;
            newAppointment.CreatedOn = model.CreatedOn;
            _MedORDB.Appointments.AddObject(newAppointment);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateAppointment(DataModel.AppointmentModel model)
        {
            var editAppointment = _MedORDB.Appointments.Where(x => x.AppointmentID == model.AppointmentID).FirstOrDefault();
            var editPatient = _MedORDB.Patients.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            if(editPatient!=null)
            editPatient.MobilePhone = model.PhoneNumber;

            editAppointment.PatientID = model.PatientID;
            editAppointment.DoctorID = model.DoctorID;
            editAppointment.title = model.title;
            editAppointment.Note = model.Note;
            if (editPatient == null)
                editAppointment.PhoneNumber = model.PhoneNumber;
            editAppointment.StartDateTime = model.StartDateTime;
            editAppointment.EndDateTime = model.EndDateTime;
            editAppointment.AllDay = model.allDay;
            editAppointment.ModifiedBy = model.ModifiedBy;
            editAppointment.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateAppointmentDateTime(DataModel.AppointmentModel model)
        {
            var editAppointment = _MedORDB.Appointments.Where(x => x.AppointmentID == model.AppointmentID).FirstOrDefault();
            editAppointment.EndDateTime = model.EndDateTime;
            editAppointment.StartDateTime = model.StartDateTime;
            editAppointment.AllDay = model.AllDay;
            editAppointment.ModifiedBy = model.ModifiedBy;
            editAppointment.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateAppointmentEndDateTime(DataModel.AppointmentModel model)
        {
            var editAppointment = _MedORDB.Appointments.Where(x => x.AppointmentID == model.AppointmentID).FirstOrDefault();
            editAppointment.EndDateTime = model.EndDateTime;
            editAppointment.ModifiedBy = model.ModifiedBy;
            editAppointment.ModifiedOn = model.ModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 DeleteAppointmentByID(Guid AppointmentID)
        {
            var appointment = _MedORDB.Appointments.Where(x => x.AppointmentID == AppointmentID).FirstOrDefault();
            var success = 0;
            if (appointment != null)
            {
                _MedORDB.Appointments.DeleteObject(appointment);
             
            }
            success = _MedORDB.SaveChanges();
            return success;
        }
        #endregion
        #region Layout
        public DataModel.LayoutStyleModel GetLayoutStyleByUserID(Guid UserID)
        {
            var layoutStyleModel = (from l in _MedORDB.LayoutStyles
                                    where l.UserID == UserID
                                    select new DataModel.LayoutStyleModel
                                    {
                                        LayoutStyleID = l.LayoutStyleID,
                                        Layout = l.Layout,
                                        Rtl = l.Rtl,
                                        Header = l.Header,
                                        Footer = l.Footer,
                                        Background = l.Background,
                                        PredefinedColorScheme = l.PredefinedColorScheme,
                                        Basic = l.Basic,
                                        Text = l.Text,
                                        Elements = l.Elements
                                    }).FirstOrDefault();
            return layoutStyleModel;
        }

        public Int32 UpdateLayoutStyle(DataModel.LayoutStyleModel model)
        {
            var editLayoutStyle = _MedORDB.LayoutStyles.Where(x => x.UserID == model.UserID).FirstOrDefault();
            if (editLayoutStyle != null)
            {
                editLayoutStyle.Rtl = model.Rtl;
                editLayoutStyle.Layout = model.Layout;
                editLayoutStyle.Header = model.Header;
                editLayoutStyle.Footer = model.Footer;
                editLayoutStyle.Background = model.Background;
                editLayoutStyle.PredefinedColorScheme = model.PredefinedColorScheme;
                editLayoutStyle.Basic = model.Basic;
                editLayoutStyle.Text = model.Text;
                editLayoutStyle.Elements = model.Elements;
            }
            else
            {
                DataAccess.LayoutStyle newLayoutStyleModel = new DataAccess.LayoutStyle();
                newLayoutStyleModel.LayoutStyleID = Guid.NewGuid();
                newLayoutStyleModel.UserID = model.UserID;
                newLayoutStyleModel.Rtl = model.Rtl;
                newLayoutStyleModel.Layout = model.Layout;
                newLayoutStyleModel.Header = model.Header;
                newLayoutStyleModel.Footer = model.Footer;
                newLayoutStyleModel.Background = model.Background;
                newLayoutStyleModel.PredefinedColorScheme = model.PredefinedColorScheme;
                newLayoutStyleModel.Basic = model.Basic;
                newLayoutStyleModel.Text = model.Text;
                newLayoutStyleModel.Elements = model.Elements;
                _MedORDB.LayoutStyles.AddObject(newLayoutStyleModel);
            }
            var success = _MedORDB.SaveChanges();
            return success;
        }
        #endregion
        #region MedicalInfoAndHistory
        public DataModel.PediatricMedicalModel GetMedicalInfoAndHistoryByPatientID(Guid PatientID)
        {
            var medicalInfoAndHistory = (from p in _MedORDB.PediatricMedicalInfoes
                                         where p.PatientID == PatientID
                                         select new DataModel.PediatricMedicalModel
                                         {
                                             MedicalInfoID = p.MedicalInfoID,
                                             BloodType = p.BloodType,
                                             Consanguinity = p.Consanguinity,
                                             PNormal = p.PNormal,
                                             PAlbuminuria = p.PAlbuminuria,
                                             PInfections = p.PInfections,
                                             PChildbirthThreat = p.PChildbirthThreat,
                                             POthers = p.POthers,
                                             PMedicines = p.PMedicines,
                                             CBTerm = p.CBTerm,
                                             CBVaginally = p.CBVaginally,
                                             CBCSection = p.CBCSection,
                                             CBForceps = p.CBForceps,
                                             BApgar = p.BApgar,
                                             BCyanosis = p.BCyanosis,
                                             BReanimation = p.BReanimation,
                                             BNeonatalWeight = p.BNeonatalWeight,
                                             BHeight = p.BHeight,
                                             BCranialPerimeter = p.BCranialPerimeter,
                                             BPlacenta = p.BPlacenta,
                                             NPGuthrie = p.NPGuthrie,
                                             NPPhysiologicalIcterus = p.NPPhysiologicalIcterus,
                                             NPNICU = p.NPNICU,
                                             NPCongenitalAbnormalities = p.NPCongenitalAbnormalities,
                                             NPBreastMilk = p.NPBreastMilk,
                                             NPArtificialMilk = p.NPArtificialMilk,
                                             NPArtificialMilkName = p.NPArtificialMilkName,
                                             PDFirstSmile = p.PDFirstSmile,
                                             PDSitUp = p.PDSitUp,
                                             PDSteps = p.PDSteps,
                                             PDSchooling = p.PDSchooling,
                                             PDFirstWord = p.PDFirstWord,
                                             PDStandUp = p.PDStandUp,
                                             PDDiurnalCleanliness = p.PDDiurnalCleanliness,
                                             DMilk = p.DMilk,
                                             DSolidFood = p.DSolidFood,
                                             DIron = p.DIron,
                                             DFluor = p.DFluor,
                                             DVitaminD = p.DVitaminD,
                                             DMultivitamin = p.DMultivitamin,
                                             AFood = p.AFood,
                                             AFoodTypes = p.AFoodTypes,
                                             AMedications = p.AMedications,
                                             AMedicationTypes = p.AMedicationTypes,
                                             AAsthma = p.AAsthma,
                                             AHives = p.AHives,
                                             AOthers = p.AOthers,
                                             CDWhoopingCough = p.CDWhoopingCough,
                                             CDMumps = p.CDMumps,
                                             CDMeasles = p.CDMeasles,
                                             CDRubella = p.CDRubella,
                                             CDScarlatine = p.CDScarlatine,
                                             CDChickenPox = p.CDChickenPox,
                                             CDRoseola = p.CDRoseola,
                                             CDOthers = p.CDOthers,
                                             MIGeneralNote = p.GeneralNote,
                                             ModifiedBy = p.ModifiedBy,
                                             ModifiedOn = p.ModifiedOn,
                                             MedicalHistoryID = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.MedicalHistoryID).FirstOrDefault(),
                                             MedicalHistory = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.MedicalHistory).FirstOrDefault(),
                                             SurgicalHistory = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.SurgicalHistory).FirstOrDefault(),
                                             FamilyHistory = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.FamilyHistory).FirstOrDefault(),
                                             MHGeneralNote = (from m in _MedORDB.PediatricMedicalHistories where m.PatientID == PatientID select m.GeneralNote).FirstOrDefault(),
                                         }).FirstOrDefault();
            return medicalInfoAndHistory;
        }
        public Int32 UpdateMedicalInfoAndHistory(DataModel.PediatricMedicalModel model)
        {
            var editMedical = _MedORDB.PediatricMedicalInfoes.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            if (editMedical == null)
            {
                var newMedical = new DataAccess.PediatricMedicalInfo();
                newMedical.MedicalInfoID = Guid.NewGuid();
                newMedical.PatientID = model.PatientID;
                newMedical.BloodType = model.BloodType;
                newMedical.Consanguinity = model.Consanguinity;
                newMedical.PNormal = model.PNormal;
                newMedical.PAlbuminuria = model.PAlbuminuria;
                newMedical.PInfections = model.PInfections;
                newMedical.PChildbirthThreat = model.PChildbirthThreat;
                newMedical.POthers = model.POthers;
                newMedical.PMedicines = model.PMedicines;
                newMedical.CBTerm = model.CBTerm;
                newMedical.CBVaginally = model.CBVaginally;
                newMedical.CBCSection = model.CBCSection;
                newMedical.CBForceps = model.CBForceps;
                newMedical.BApgar = model.BApgar;
                newMedical.BCyanosis = model.BCyanosis;
                newMedical.BReanimation = model.BReanimation;
                newMedical.BNeonatalWeight = model.BNeonatalWeight;
                newMedical.BHeight = model.BHeight;
                newMedical.BCranialPerimeter = model.BCranialPerimeter;
                newMedical.BPlacenta = model.BPlacenta;
                newMedical.NPGuthrie = model.NPGuthrie;
                newMedical.NPPhysiologicalIcterus = model.NPPhysiologicalIcterus;
                newMedical.NPNICU = model.NPNICU;
                newMedical.NPCongenitalAbnormalities = model.NPCongenitalAbnormalities;
                newMedical.NPBreastMilk = model.NPBreastMilk;
                newMedical.NPArtificialMilk = model.NPArtificialMilk;
                newMedical.NPArtificialMilkName = model.NPArtificialMilkName;
                newMedical.PDFirstSmile = model.PDFirstSmile;
                newMedical.PDSitUp = model.PDSitUp;
                newMedical.PDSteps = model.PDSteps;
                newMedical.PDSchooling = model.PDSchooling;
                newMedical.PDFirstWord = model.PDFirstWord;
                newMedical.PDStandUp = model.PDStandUp;
                newMedical.PDDiurnalCleanliness = model.PDDiurnalCleanliness;
                newMedical.DMilk = model.DMilk;
                newMedical.DSolidFood = model.DSolidFood;
                newMedical.DIron = model.DIron;
                newMedical.DFluor = model.DFluor;
                newMedical.DVitaminD = model.DVitaminD;
                newMedical.DMultivitamin = model.DMultivitamin;
                newMedical.AFood = model.AFood;
                newMedical.AFoodTypes = model.AFoodTypes;
                newMedical.AMedications = model.AMedications;
                newMedical.AMedicationTypes = model.AMedicationTypes;
                newMedical.AAsthma = model.AAsthma;
                newMedical.AHives = model.AHives;
                newMedical.AOthers = model.AOthers;
                newMedical.CDWhoopingCough = model.CDWhoopingCough;
                newMedical.CDMumps = model.CDMumps;
                newMedical.CDMeasles = model.CDMeasles;
                newMedical.CDRubella = model.CDRubella;
                newMedical.CDScarlatine = model.CDScarlatine;
                newMedical.CDChickenPox = model.CDChickenPox;
                newMedical.CDRoseola = model.CDRoseola;
                newMedical.CDOthers = model.CDOthers;
                newMedical.GeneralNote = model.MIGeneralNote;
                newMedical.ModifiedBy = model.ModifiedBy;
                newMedical.ModifiedOn = model.ModifiedOn;
                _MedORDB.PediatricMedicalInfoes.AddObject(newMedical);
            }
            else
            {
                editMedical.BloodType = model.BloodType;
                editMedical.Consanguinity = model.Consanguinity;
                editMedical.PNormal = model.PNormal;
                editMedical.PAlbuminuria = model.PAlbuminuria;
                editMedical.PInfections = model.PInfections;
                editMedical.PChildbirthThreat = model.PChildbirthThreat;
                editMedical.POthers = model.POthers;
                editMedical.PMedicines = model.PMedicines;
                editMedical.CBTerm = model.CBTerm;
                editMedical.CBVaginally = model.CBVaginally;
                editMedical.CBCSection = model.CBCSection;
                editMedical.CBForceps = model.CBForceps;
                editMedical.BApgar = model.BApgar;
                editMedical.BCyanosis = model.BCyanosis;
                editMedical.BReanimation = model.BReanimation;
                editMedical.BNeonatalWeight = model.BNeonatalWeight;
                editMedical.BHeight = model.BHeight;
                editMedical.BCranialPerimeter = model.BCranialPerimeter;
                editMedical.BPlacenta = model.BPlacenta;
                editMedical.NPGuthrie = model.NPGuthrie;
                editMedical.NPPhysiologicalIcterus = model.NPPhysiologicalIcterus;
                editMedical.NPNICU = model.NPNICU;
                editMedical.NPCongenitalAbnormalities = model.NPCongenitalAbnormalities;
                editMedical.NPBreastMilk = model.NPBreastMilk;
                editMedical.NPArtificialMilk = model.NPArtificialMilk;
                editMedical.NPArtificialMilkName = model.NPArtificialMilkName;
                editMedical.PDFirstSmile = model.PDFirstSmile;
                editMedical.PDSitUp = model.PDSitUp;
                editMedical.PDSteps = model.PDSteps;
                editMedical.PDSchooling = model.PDSchooling;
                editMedical.PDFirstWord = model.PDFirstWord;
                editMedical.PDStandUp = model.PDStandUp;
                editMedical.PDDiurnalCleanliness = model.PDDiurnalCleanliness;
                editMedical.DMilk = model.DMilk;
                editMedical.DSolidFood = model.DSolidFood;
                editMedical.DIron = model.DIron;
                editMedical.DFluor = model.DFluor;
                editMedical.DVitaminD = model.DVitaminD;
                editMedical.DMultivitamin = model.DMultivitamin;
                editMedical.AFood = model.AFood;
                editMedical.AFoodTypes = model.AFoodTypes;
                editMedical.AMedications = model.AMedications;
                editMedical.AMedicationTypes = model.AMedicationTypes;
                editMedical.AAsthma = model.AAsthma;
                editMedical.AHives = model.AHives;
                editMedical.AOthers = model.AOthers;
                editMedical.CDWhoopingCough = model.CDWhoopingCough;
                editMedical.CDMumps = model.CDMumps;
                editMedical.CDMeasles = model.CDMeasles;
                editMedical.CDRubella = model.CDRubella;
                editMedical.CDScarlatine = model.CDScarlatine;
                editMedical.CDChickenPox = model.CDChickenPox;
                editMedical.CDRoseola = model.CDRoseola;
                editMedical.CDOthers = model.CDOthers;
                editMedical.GeneralNote = model.MIGeneralNote;
                editMedical.ModifiedBy = model.ModifiedBy;
                editMedical.ModifiedOn = model.ModifiedOn;
            }


            var editMedicalHistory = _MedORDB.PediatricMedicalHistories.Where(x => x.PatientID == model.PatientID).FirstOrDefault();
            if (editMedicalHistory == null)
            {
                var newMedicalHistory = new DataAccess.PediatricMedicalHistory();
                newMedicalHistory.MedicalHistoryID = Guid.NewGuid();
                newMedicalHistory.PatientID = model.PatientID;
                newMedicalHistory.MedicalHistory = model.MedicalHistory;
                newMedicalHistory.SurgicalHistory = model.SurgicalHistory;
                newMedicalHistory.FamilyHistory = model.FamilyHistory;
                newMedicalHistory.GeneralNote = model.MHGeneralNote;
                newMedicalHistory.ModifiedBy = model.ModifiedBy;
                newMedicalHistory.ModifiedOn = model.ModifiedOn;
                _MedORDB.PediatricMedicalHistories.AddObject(newMedicalHistory);
            }
            else
            {
                editMedicalHistory.MedicalHistory = model.MedicalHistory;
                editMedicalHistory.SurgicalHistory = model.SurgicalHistory;
                editMedicalHistory.FamilyHistory = model.FamilyHistory;
                editMedicalHistory.GeneralNote = model.MHGeneralNote;
                editMedicalHistory.ModifiedBy = model.ModifiedBy;
                editMedicalHistory.ModifiedOn = model.ModifiedOn;
            }

            var success = _MedORDB.SaveChanges();
            return success;
        }
        #endregion
        #region IMedicine
        public DataModel.IMFileModel GetFileByID(Guid ID,string name)
        {
            DataModel.IMFileModel IMFileModel = new DataModel.IMFileModel();
            if (name=="Files")
            {
               
                IMFileModel = (from m in _MedORDB.IMFiles
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileID = m.FileID,
                                   FileName = m.FileName,
                                   Data = m.Data,

                               })).FirstOrDefault();
               
            }
            if (name == "Hospitalization")
            {
                
                IMFileModel = (from m in _MedORDB.IMHopitalizations
                               where (m.HospitalizationID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                 
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
                
            }
            if (name == "CR")
            {
                IMFileModel = (from m in _MedORDB.BP_CR_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }

            if (name == "Other")
            {
                IMFileModel = (from m in _MedORDB.BP_Other_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }

            if (name == "Scin")
            {
                IMFileModel = (from m in _MedORDB.BP_scintigraphy_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }

            if (name == "PetScan")
            {
                IMFileModel = (from m in _MedORDB.BP_PetScan_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }

            if (name == "MRI")
            {
                IMFileModel = (from m in _MedORDB.BP_MRI_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }

            if (name == "Scan")
            {
                IMFileModel = (from m in _MedORDB.BP_Scan_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }


            if (name == "ECH")
            {
                IMFileModel = (from m in _MedORDB.BP_Echography_Files
                               where (m.FileID == ID)
                               orderby m.CreatedOn descending
                               select (new DataModel.IMFileModel
                               {
                                   FileName = m.FileName,
                                   Data = m.File,

                               })).FirstOrDefault();
            }
            return IMFileModel;
        }
        public string IMdeleteItem(Guid PatientID,Guid ID)
        {
            var IMConsultation = _MedORDB.IMconsultations.Where(x => x.PatientID == PatientID && x.IMconsultationID== ID).FirstOrDefault();
            //if (PatientID==Guid.Empty)
            //{
            //    var IMConsultation = _MedORDB.IMconsultations.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();


            //}
            var success = "";
            if (IMConsultation != null)
            {
                _MedORDB.IMconsultations.DeleteObject(IMConsultation);
                 _MedORDB.SaveChanges();
                success = "#panel_tab12";
            }

            var ComplaintHistory = _MedORDB.IMcomplaintHistories.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();
            if (ComplaintHistory != null)
            {
                _MedORDB.IMcomplaintHistories.DeleteObject(ComplaintHistory);
                _MedORDB.SaveChanges();
                success = "#panel_tab3";
            }

            //Cardiovascular
            var IMCRVasculars = _MedORDB.IMCRVasculars.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();
            var CDiabetes = _MedORDB.CDiabetes.Where(x => x.CadiovascularID == IMCRVasculars.CRVascularID);
            var CAlcohols = _MedORDB.CAlcohols.Where(x => x.CadiovascularID == IMCRVasculars.CRVascularID);
            var CDyslipidemias = _MedORDB.CDyslipidemias.Where(x => x.CadiovascularID == IMCRVasculars.CRVascularID);
            var CObesities = _MedORDB.CObesities.Where(x => x.CadiovascularID == IMCRVasculars.CRVascularID);
            var CFamilialHistories = _MedORDB.CFamilialHistories.Where(x => x.CadiovascularID == IMCRVasculars.CRVascularID);   
            if (IMCRVasculars != null)
            {
                _MedORDB.IMCRVasculars.DeleteObject(IMCRVasculars);
                foreach (var Item in CDiabetes) _MedORDB.CDiabetes.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in CAlcohols) _MedORDB.CAlcohols.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in CDyslipidemias) _MedORDB.CDyslipidemias.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in CObesities) _MedORDB.CObesities.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in CFamilialHistories) _MedORDB.CFamilialHistories.DeleteObject(Item); _MedORDB.SaveChanges();
                 _MedORDB.SaveChanges();
                success = "#panel_tab3";
            }

            var IMmedicalHistories = _MedORDB.IMmedicalHistories.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();
            var MHAllergies = _MedORDB.MHAllergies.Where(x => x.MedicaHistoryID == IMmedicalHistories.MedicaHistoryID);
            var MHFamilies = _MedORDB.MHFamilies.Where(x => x.MedicaHistoryID == IMmedicalHistories.MedicaHistoryID);
            var MHGynecologies = _MedORDB.MHGynecologies.Where(x => x.MedicaHistoryID == IMmedicalHistories.MedicaHistoryID);
            var MHSurgicals = _MedORDB.MHSurgicals.Where(x => x.MedicaHistoryID == IMmedicalHistories.MedicaHistoryID);
            var MHMedicals = _MedORDB.MHMedicals.Where(x => x.MedicaHistoryID == IMmedicalHistories.MedicaHistoryID);

            if (IMmedicalHistories != null)
            {
                _MedORDB.IMmedicalHistories.DeleteObject(IMmedicalHistories);
                foreach (var Item in MHAllergies) _MedORDB.MHAllergies.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in MHFamilies) _MedORDB.MHFamilies.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in MHGynecologies) _MedORDB.MHGynecologies.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in MHSurgicals) _MedORDB.MHSurgicals.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in MHMedicals) _MedORDB.MHMedicals.DeleteObject(Item); _MedORDB.SaveChanges();
                 _MedORDB.SaveChanges();
                success = "#panel_tab3";
            }

            var IMMedications = _MedORDB.IMMedications.Where(x =>  x.MedicationID == ID).FirstOrDefault();
            if (IMMedications != null)
            {
                _MedORDB.IMMedications.DeleteObject(IMMedications);
                _MedORDB.SaveChanges();
                success = "#panel_tab7";
            }
            // delete for consultation x
            var IMMedications1 = _MedORDB.IMMedications.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();
            if (IMMedications1 != null)
            {
                _MedORDB.IMMedications.DeleteObject(IMMedications1);
                _MedORDB.SaveChanges();
                success = "#panel_tab7";
            }


            var IMReviewSystems = _MedORDB.IMReviewSystems.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();
            var RSHEENTs = _MedORDB.RSHEENTs.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID );
            var RSPulminaries = _MedORDB.RSPulminaries.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSCardiovasculars = _MedORDB.RSCardiovasculars.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSDigestives = _MedORDB.RSDigestives.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSUrogentals = _MedORDB.RSUrogentals.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSNervousSystems = _MedORDB.RSNervousSystems.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSMusculoskeletals = _MedORDB.RSMusculoskeletals.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSPsychosociales = _MedORDB.RSPsychosociales.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSWeights = _MedORDB.RSWeights.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSFevers = _MedORDB.RSFevers.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSFatigues = _MedORDB.RSFatigues.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);
            var RSAnorexias = _MedORDB.RSAnorexias.Where(x => x.ReviewOfSystemID == IMReviewSystems.ReviewOfSystemID);

            if (IMReviewSystems != null)
            {
                _MedORDB.IMReviewSystems.DeleteObject(IMReviewSystems);
                foreach (var Item in RSHEENTs) _MedORDB.RSHEENTs.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in RSPulminaries)_MedORDB.RSPulminaries.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in RSCardiovasculars) _MedORDB.RSCardiovasculars.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in RSDigestives) _MedORDB.RSDigestives.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in RSUrogentals) _MedORDB.RSUrogentals.DeleteObject(Item);_MedORDB.SaveChanges();
                foreach (var Item in RSNervousSystems) _MedORDB.RSNervousSystems.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in RSMusculoskeletals) _MedORDB.RSMusculoskeletals.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in RSPsychosociales) _MedORDB.RSPsychosociales.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in RSWeights) _MedORDB.RSWeights.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in RSFevers) _MedORDB.RSFevers.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in RSFatigues) _MedORDB.RSFatigues.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in RSAnorexias) _MedORDB.RSAnorexias.DeleteObject(Item);  _MedORDB.SaveChanges();
                 _MedORDB.SaveChanges();
                success = "#panel_tab6";
            }
            //delete Physical exam history for specific consultation 
            var IMPhysicalExamHistories = _MedORDB.IMPhysicalExamHistories.Where(x => x.PatientID == PatientID && x.IMconsultation == ID).FirstOrDefault();
            var PHHEENTs = _MedORDB.PHHEENTs.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHPulminaries = _MedORDB.PHPulminaries.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHCardiovasculars = _MedORDB.PHCardiovasculars.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHDigestives = _MedORDB.PHDigestives.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHUrogentals = _MedORDB.PHUrogentals.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHNervousSystems = _MedORDB.PHNervousSystems.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHMusculoskeletals = _MedORDB.PHMusculoskeletals.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
            var PHSPsychosociales = _MedORDB.PHPsychosociales.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);

             var PHWeights = _MedORDB.PHWeights.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
             var PHRespiratoryRates = _MedORDB.PHRespiratoryRates.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);
             var PHBloodPressures = _MedORDB.PHBloodPressures.Where(x => x.PhysicalExamHistoryID == IMPhysicalExamHistories.PhysicalExamHistoryID);


            if (IMPhysicalExamHistories != null)
            {
                _MedORDB.IMPhysicalExamHistories.DeleteObject(IMPhysicalExamHistories);
                foreach (var Item in PHHEENTs) _MedORDB.PHHEENTs.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in PHPulminaries) _MedORDB.PHPulminaries.DeleteObject(Item);_MedORDB.SaveChanges();
                foreach (var Item in PHCardiovasculars) _MedORDB.PHCardiovasculars.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in PHDigestives) _MedORDB.PHDigestives.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in PHUrogentals) _MedORDB.PHUrogentals.DeleteObject(Item);_MedORDB.SaveChanges();
                foreach (var Item in PHNervousSystems) _MedORDB.PHNervousSystems.DeleteObject(Item);_MedORDB.SaveChanges();
                foreach (var Item in PHMusculoskeletals) _MedORDB.PHMusculoskeletals.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in PHSPsychosociales) _MedORDB.PHPsychosociales.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in PHWeights) _MedORDB.PHWeights.DeleteObject(Item); _MedORDB.SaveChanges();
                foreach (var Item in PHRespiratoryRates) _MedORDB.PHRespiratoryRates.DeleteObject(Item);  _MedORDB.SaveChanges();
                foreach (var Item in PHBloodPressures) _MedORDB.PHBloodPressures.DeleteObject(Item); _MedORDB.SaveChanges();

                _MedORDB.SaveChanges();
                success = "#panel_tab6";
            }
            var ICD10_Patient = _MedORDB.ICD10_Patient.Where(x =>  x.ICD10PatientID == ID).FirstOrDefault();
            if (ICD10_Patient != null)
            {
                _MedORDB.ICD10_Patient.DeleteObject(ICD10_Patient);
              _MedORDB.SaveChanges();
                success = "#panel_tab8";
            }

            //for delete consultation x
            var ICD10_Patient1 = _MedORDB.ICD10_Patient.Where(x => x.PatientID == PatientID && x.IMconsultationID == ID).FirstOrDefault();
            if (ICD10_Patient1 != null)
            {
                _MedORDB.ICD10_Patient.DeleteObject(ICD10_Patient1);
                _MedORDB.SaveChanges();
                success = "#panel_tab8";
            }
           // if (PatientID = Guid.Empty) { }
            //delete Biological Parameter for specific consultation 
            var IMBiologicals = _MedORDB.IMBiologicals.Where(x =>


           x.PatientID == PatientID && x.ConsultaionID == ID).FirstOrDefault();
           
            

            if (IMBiologicals != null)
            {
                var BPConventionalRadiographies = _MedORDB.BPConventionalRadiographies.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault();
                var BPMRIs = _MedORDB.BPMRIs.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault();
                var BPOthers = _MedORDB.BPOthers.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault(); ;
                var BPPetScans = _MedORDB.BPPetScans.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault(); ;
                var BPScans = _MedORDB.BPScans.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault(); ;
                var BPScintigraphies = _MedORDB.BPScintigraphies.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault(); ;
                var BPEchographies = _MedORDB.BPEchographies.Where(x => x.BiologicalID == IMBiologicals.BiologicalID).FirstOrDefault(); ;

                var BP_CR_Files = _MedORDB.BP_CR_Files.Where(x => x.ConventionalRadiographyID == BPConventionalRadiographies.ConventionalRadiographyID);
                var BP_Scan_Files = _MedORDB.BP_Scan_Files.Where(x => x.ScanID == BPScans.ScanID);
                var BP_PetScan_Files = _MedORDB.BP_PetScan_Files.Where(x => x.PetScan == BPPetScans.PetScanID);
                var BP_scintigraphy_Files = _MedORDB.BP_scintigraphy_Files.Where(x => x.scintigraphyID == BPScintigraphies.ScintigraphyID);
                var BP_Other_Files = _MedORDB.BP_Other_Files.Where(x => x.Other == BPConventionalRadiographies.ConventionalRadiographyID);
                var BP_MRI_Files = _MedORDB.BP_MRI_Files.Where(x => x.MRIID == BPMRIs.MRIID);
                var BP_Echography_Files = _MedORDB.BP_Echography_Files.Where(x => x.Echography == BPEchographies.EchographyID);
                _MedORDB.IMBiologicals.DeleteObject(IMBiologicals);
          
          
            if (BPConventionalRadiographies != null)
            {
                _MedORDB.BPConventionalRadiographies.DeleteObject(BPConventionalRadiographies);
                foreach (var Item in BP_CR_Files) _MedORDB.BP_CR_Files.DeleteObject(Item);
                    _MedORDB.SaveChanges();
                    success = "#panel_tab13";

                }
            if (BPMRIs != null)
            {
                _MedORDB.BPMRIs.DeleteObject(BPMRIs);
                foreach (var Item in BP_MRI_Files) _MedORDB.BP_MRI_Files.DeleteObject(Item);
                    _MedORDB.SaveChanges();
                    success = "#panel_tab13";

                }
            if (BPOthers != null)
            {
                _MedORDB.BPOthers.DeleteObject(BPOthers);
                foreach (var Item in BP_Other_Files) _MedORDB.BP_Other_Files.DeleteObject(Item);
                    _MedORDB.SaveChanges();
                    success = "#panel_tab13";

                }
            if (BPPetScans != null)
            {
                _MedORDB.BPPetScans.DeleteObject(BPPetScans);
                foreach (var Item in BP_PetScan_Files) _MedORDB.BP_PetScan_Files.DeleteObject(Item);
                        _MedORDB.SaveChanges();
                    success = "#panel_tab13";

                }
            if (BPScans != null)
            {
                _MedORDB.BPScans.DeleteObject(BPScans);
                foreach (var Item in BP_Scan_Files) _MedORDB.BP_Scan_Files.DeleteObject(Item);
                    _MedORDB.SaveChanges();
                    success = "#panel_tab13";

                }
            if (BPScintigraphies != null)
            {
                _MedORDB.BPScintigraphies.DeleteObject(BPScintigraphies);
                foreach (var Item in BP_scintigraphy_Files) _MedORDB.BP_scintigraphy_Files.DeleteObject(Item);
               _MedORDB.SaveChanges();
                    success = "#panel_tab13";

                }
            if (BPEchographies != null)
            {
                _MedORDB.BPEchographies.DeleteObject(BPEchographies);
                foreach (var Item in BP_Echography_Files) _MedORDB.BP_Echography_Files.DeleteObject(Item); 
                        _MedORDB.SaveChanges();
                    success ="#panel_tab13";

                }
             }

            var IMHsummary = _MedORDB.IMHopitalizations.Where(x =>x.HospitalizationID == ID).FirstOrDefault();
            if (IMHsummary != null)
            {
                _MedORDB.IMHopitalizations.DeleteObject(IMHsummary);
                 _MedORDB.SaveChanges();
                success = "#panel_tab11";
            }

            //for delete consultation x
            var IMHsummary1 = _MedORDB.IMHopitalizations.Where(x => x.PatientID == PatientID && x.ConsultationID == ID).FirstOrDefault();
            if (IMHsummary1 != null)
            {
                _MedORDB.IMHopitalizations.DeleteObject(IMHsummary1);
                 _MedORDB.SaveChanges();
                success = "#panel_tab11";
            }
            //for delete File x 
            var IMFiles = _MedORDB.IMFiles.Where(x => x.PatientID == PatientID && x.FileID == ID).FirstOrDefault();
            if (IMFiles != null)
            {
                _MedORDB.IMFiles.DeleteObject(IMFiles);
            _MedORDB.SaveChanges();
                success = "#panel_tab10";
            }
            var IMFiles1 = _MedORDB.IMFiles.Where(x => x.PatientID == PatientID && x.Consultation == ID).FirstOrDefault();
            if (IMFiles1 != null)
            {
                _MedORDB.IMFiles.DeleteObject(IMFiles1);
                _MedORDB.SaveChanges();
                success = "#panel_tab10";
            }

            //for delete Management x 
            var IMManagements = _MedORDB.IMManagements.Where(x => x.ManagementID == ID).FirstOrDefault();
            if (IMManagements != null)
            {
                _MedORDB.IMManagements.DeleteObject(IMManagements);
           _MedORDB.SaveChanges();
                success = "#panel_tab9";
            }

            //for delete consultation x
            var IMManagements1 = _MedORDB.IMManagements.Where(x => x.PatientID == PatientID && x.ConsultationID == ID).FirstOrDefault();
            if (IMManagements1 != null)
            {
                _MedORDB.IMManagements.DeleteObject(IMManagements1);
                _MedORDB.SaveChanges();
                success = "#panel_tab9";
            }


            return success;
        }
        public string DeleteDescription( Guid DescriptionID,string ValueTap)
        {
            string success = null;
            

           
          
            if (ValueTap == "panel_tab2")
            {
                var CDiabete = _MedORDB.CDiabetes.Where(x => x.DiabetesID == DescriptionID).FirstOrDefault();
                var Dyslipidemias = _MedORDB.CDyslipidemias.Where(x => x.CDyslipidemiaID == DescriptionID).FirstOrDefault();
                var CAlcohol = _MedORDB.CAlcohols.Where(x => x.AlcoholID == DescriptionID).FirstOrDefault();
                var CObesities = _MedORDB.CObesities.Where(x => x.ObesityID == DescriptionID).FirstOrDefault();
                var CFamilialHistories = _MedORDB.CFamilialHistories.Where(x => x.FamilialHistoryID == DescriptionID).FirstOrDefault();
                if (CDiabete != null)
                {
                    _MedORDB.CDiabetes.DeleteObject(CDiabete);
                    _MedORDB.SaveChanges();
                    success = "panel_tab2";
                }

                if (Dyslipidemias != null)
                {
                    _MedORDB.CDyslipidemias.DeleteObject(Dyslipidemias);
                    _MedORDB.SaveChanges();
                    success = "panel_tab2";
                }
                if (CAlcohol != null)
                {
                    _MedORDB.CAlcohols.DeleteObject(CAlcohol);
                    _MedORDB.SaveChanges();
                    success = "panel_tab2";
                }

                if (CObesities != null)
                {
                    _MedORDB.CObesities.DeleteObject(CObesities);
                    _MedORDB.SaveChanges();
                    success = "panel_tab2";
                }

                if (CFamilialHistories != null)
                {
                    _MedORDB.CFamilialHistories.DeleteObject(CFamilialHistories);
                    _MedORDB.SaveChanges();
                    success = "panel_tab2";
                }


               
            }
            if (ValueTap == "panel_tab3")
            {
                var MHAllergies = _MedORDB.MHAllergies.Where(x => x.MHAllergiesID == DescriptionID).FirstOrDefault();
                var MHGynecologies = _MedORDB.MHGynecologies.Where(x => x.GynecologyID == DescriptionID).FirstOrDefault();
                var MHFamilies = _MedORDB.MHFamilies.Where(x => x.FamilyID == DescriptionID).FirstOrDefault();
                var MHMedicals = _MedORDB.MHMedicals.Where(x => x.MedicalID == DescriptionID).FirstOrDefault();
                var MHSurgicals = _MedORDB.MHSurgicals.Where(x => x.SurgicalID == DescriptionID).FirstOrDefault();
                if (MHGynecologies != null)
                {
                    _MedORDB.MHGynecologies.DeleteObject(MHGynecologies);
                    _MedORDB.SaveChanges();
                    success = "panel_tab3";
                }

                if (MHSurgicals != null)
                {
                    _MedORDB.MHSurgicals.DeleteObject(MHSurgicals);
                    _MedORDB.SaveChanges();
                    success = "panel_tab3";
                }
                if (MHMedicals != null)
                {
                    _MedORDB.MHMedicals.DeleteObject(MHMedicals);
                    _MedORDB.SaveChanges();
                    success = "panel_tab3";
                }

                if (MHAllergies != null)
                {
                    _MedORDB.MHAllergies.DeleteObject(MHAllergies);
                    _MedORDB.SaveChanges();
                    success = "panel_tab3";
                }

                if (MHFamilies != null)
                {
                    _MedORDB.MHFamilies.DeleteObject(MHFamilies);
                    _MedORDB.SaveChanges();
                    success = "panel_tab3";
                }
            }


            if (ValueTap == "panel_tab5")
            {
                var RSAnorexia = _MedORDB.RSAnorexias.Where(x => x.AnorexiaID == DescriptionID).FirstOrDefault();
                var RSWeight = _MedORDB.RSWeights.Where(x => x.WeightID == DescriptionID).FirstOrDefault();
                var RSFever = _MedORDB.RSFevers.Where(x => x.FeverID == DescriptionID).FirstOrDefault();
                var Fatigue = _MedORDB.RSFatigues.Where(x => x.FatigueID == DescriptionID).FirstOrDefault();

                var RSHEENTs = _MedORDB.RSHEENTs.Where(x => x.HEENTID == DescriptionID).FirstOrDefault();
                var RSPulminaries = _MedORDB.RSPulminaries.Where(x => x.PulminaryID == DescriptionID).FirstOrDefault();
                var RSCardiovasculars = _MedORDB.RSCardiovasculars.Where(x => x.CardiovascularID == DescriptionID).FirstOrDefault();
                var RSDigestives = _MedORDB.RSDigestives.Where(x => x.DigestiveID == DescriptionID).FirstOrDefault();
                var RSUrogentals = _MedORDB.RSUrogentals.Where(x => x.UrogentalID == DescriptionID).FirstOrDefault();
                var RSNervousSystems = _MedORDB.RSNervousSystems.Where(x => x.NervousSystemID == DescriptionID).FirstOrDefault();
                var RSSkins = _MedORDB.RSSkins.Where(x => x.SkinID == DescriptionID).FirstOrDefault();
                var RSMusculoskeletals = _MedORDB.RSMusculoskeletals.Where(x => x.MusculoskeletalID == DescriptionID).FirstOrDefault();
                var RSPsychosociales = _MedORDB.RSPsychosociales.Where(x => x.PsychosocialeID == DescriptionID).FirstOrDefault();

                if (RSAnorexia != null)
                {
                    _MedORDB.RSAnorexias.DeleteObject(RSAnorexia);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSWeight != null)
                {
                    _MedORDB.RSWeights.DeleteObject(RSWeight);
                    _MedORDB.SaveChanges();
                    success = ValueTap; 
                }
                if (Fatigue != null)
                {
                    _MedORDB.RSFatigues.DeleteObject(Fatigue);
                    _MedORDB.SaveChanges();
                    success = ValueTap; 
                }
                if (RSFever != null)
                {
                    _MedORDB.RSFevers.DeleteObject(RSFever);
                   _MedORDB.SaveChanges();
                    success = ValueTap;
                }


                if (RSHEENTs != null)
                {
                    _MedORDB.RSHEENTs.DeleteObject(RSHEENTs);
                    _MedORDB.SaveChanges();
                    success = success = ValueTap;
                }
                if (RSPulminaries != null)
                {
                    _MedORDB.RSPulminaries.DeleteObject(RSPulminaries);
                     _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSCardiovasculars != null)
                {
                    _MedORDB.RSCardiovasculars.DeleteObject(RSCardiovasculars);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSDigestives != null)
                {
                    _MedORDB.RSDigestives.DeleteObject(RSDigestives);
                     _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSUrogentals != null)
                {
                    _MedORDB.RSUrogentals.DeleteObject(RSUrogentals);
                   _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSNervousSystems != null)
                {
                    _MedORDB.RSNervousSystems.DeleteObject(RSNervousSystems);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSSkins != null)
                {
                    _MedORDB.RSSkins.DeleteObject(RSSkins);
                 _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (RSMusculoskeletals != null)
                {
                    _MedORDB.RSMusculoskeletals.DeleteObject(RSMusculoskeletals);
                     _MedORDB.SaveChanges();
                    success = ValueTap;
                }

                if (RSPsychosociales != null)
                {
                    _MedORDB.RSPsychosociales.DeleteObject(RSPsychosociales);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
            }
            if (ValueTap == "panel_tab6")
            {
                var Respiratory = _MedORDB.PHRespiratoryRates.Where(x => x.RespiratoryRateID == DescriptionID).FirstOrDefault();
                var BloodPressure = _MedORDB.PHBloodPressures.Where(x => x.BloodPressureID == DescriptionID).FirstOrDefault();
                var Weight = _MedORDB.PHWeights.Where(x => x.WeightID == DescriptionID).FirstOrDefault();

                var PHHEENTs = _MedORDB.PHHEENTs.Where(x => x.HEENTID == DescriptionID).FirstOrDefault();
                var PHPulminaries = _MedORDB.PHPulminaries.Where(x => x.PulminaryID == DescriptionID).FirstOrDefault();
                var PHCardiovasculars = _MedORDB.PHCardiovasculars.Where(x => x.CardiovascularID == DescriptionID).FirstOrDefault();
                var PHDigestives = _MedORDB.PHDigestives.Where(x => x.DigestiveID == DescriptionID).FirstOrDefault();
                var PHUrogentals = _MedORDB.PHUrogentals.Where(x => x.UrogentalID == DescriptionID).FirstOrDefault();
                var PHNervousSystems = _MedORDB.PHNervousSystems.Where(x => x.NervousSystemID == DescriptionID).FirstOrDefault();
                var PHSkins = _MedORDB.PHSkins.Where(x => x.SkinID == DescriptionID).FirstOrDefault();
                var PHMusculoskeletals = _MedORDB.PHMusculoskeletals.Where(x => x.MusculoskeletalID == DescriptionID).FirstOrDefault();
                var PHPsychosociales = _MedORDB.PHPsychosociales.Where(x => x.PsychosocialeID == DescriptionID).FirstOrDefault();

                if (Respiratory != null)
                {
                    _MedORDB.PHRespiratoryRates.DeleteObject(Respiratory);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (BloodPressure != null)
                {
                    _MedORDB.PHBloodPressures.DeleteObject(BloodPressure);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (Weight != null)
                {
                    _MedORDB.PHWeights.DeleteObject(Weight);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
               



                if (PHHEENTs != null)
                {
                    _MedORDB.PHHEENTs.DeleteObject(PHHEENTs);
                   _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (PHPulminaries != null)
                {
                    _MedORDB.PHPulminaries.DeleteObject(PHPulminaries);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (PHCardiovasculars != null)
                {
                    _MedORDB.PHCardiovasculars.DeleteObject(PHCardiovasculars);
                    _MedORDB.SaveChanges();
                }
                if (PHDigestives != null)
                {
                    _MedORDB.PHDigestives.DeleteObject(PHDigestives);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (PHUrogentals != null)
                {
                    _MedORDB.PHUrogentals.DeleteObject(PHUrogentals);
                     _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (PHNervousSystems != null)
                {
                    _MedORDB.PHNervousSystems.DeleteObject(PHNervousSystems);
                     _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (PHSkins != null)
                {
                    _MedORDB.PHSkins.DeleteObject(PHSkins);
                     _MedORDB.SaveChanges();
                    success = ValueTap;
                }
                if (PHMusculoskeletals != null)
                {
                    _MedORDB.PHMusculoskeletals.DeleteObject(PHMusculoskeletals);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }

                if (PHPsychosociales != null)
                {
                    _MedORDB.PHPsychosociales.DeleteObject(PHPsychosociales);
                    _MedORDB.SaveChanges();
                    success = ValueTap;
                }
            }


            if (ValueTap == "panel_tab13")
            {


                var BPConventionalRadiographies = _MedORDB.BPConventionalRadiographies.Where(x => x.ConventionalRadiographyID == DescriptionID).FirstOrDefault();
                var BPScans = _MedORDB.BPScans.Where(x => x.ScanID == DescriptionID).FirstOrDefault();
                var BPScintigraphies = _MedORDB.BPScintigraphies.Where(x => x.ScintigraphyID == DescriptionID).FirstOrDefault();
                var BPPetScans = _MedORDB.BPPetScans.Where(x => x.PetScanID == DescriptionID).FirstOrDefault();
                var BPMRIs = _MedORDB.BPMRIs.Where(x => x.MRIID == DescriptionID).FirstOrDefault();
                var BPOthers = _MedORDB.BPOthers.Where(x => x.OtherID == DescriptionID).FirstOrDefault();
                var BPEchographies = _MedORDB.BPEchographies.Where(x => x.EchographyID == DescriptionID).FirstOrDefault();


                if (BPConventionalRadiographies != null)
                {
                    _MedORDB.BPConventionalRadiographies.DeleteObject(BPConventionalRadiographies);
                    _MedORDB.SaveChanges();
                    success = "panel_tab13"; 
                }
                if (BPEchographies != null)
                {
                    _MedORDB.BPEchographies.DeleteObject(BPEchographies);
                    _MedORDB.SaveChanges();
                    success = "panel_tab13";
                }
                if (BPScans != null)
                {
                    _MedORDB.BPScans.DeleteObject(BPScans);
                    _MedORDB.SaveChanges();
                    success = "panel_tab13";
                }
                if (BPPetScans != null)
                {
                    _MedORDB.BPPetScans.DeleteObject(BPPetScans);
                    _MedORDB.SaveChanges();
                    success = "panel_tab13"; 
                }
                if (BPOthers != null)
                {
                    _MedORDB.BPOthers.DeleteObject(BPOthers);
                    _MedORDB.SaveChanges();
                    success = "panel_tab13";
                 
                }
                if (BPScintigraphies != null)
                {
                    _MedORDB.BPScintigraphies.DeleteObject(BPScintigraphies);
                    _MedORDB.SaveChanges();
                    success = "panel_tab13";
                 
                }
                if (BPMRIs != null)
                {
                    _MedORDB.BPMRIs.DeleteObject(BPMRIs);
                    success = "panel_tab13";
                    _MedORDB.SaveChanges();
                }
            }
            return success;
        }
        public string NewDescription(DataModel.IMedicneModel IMedicneModel)
        {
            string success = "";
            var CRVascularID = Guid.NewGuid();
            var IMCRVasculars = _MedORDB.IMCRVasculars.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            
            if (IMCRVasculars != null & (IMedicneModel.CRVDiabetesDescription != null || IMedicneModel.CRVDyslipidemiaDescription != null || IMedicneModel.CRVAlcoholDescription != null
                || IMedicneModel.CRVObesityDescription != null || IMedicneModel.CRVFamilialHistoryDescription != null || IMedicneModel.CRVFamilialHistoryDescription != null))
            {
                var CRVascularID1 = IMCRVasculars.CRVascularID;
                if (IMedicneModel.CRVDiabetesDescription != null)
                {
                    var newCDiabete = new DataAccess.CDiabete();
                    newCDiabete.DiabetesID = Guid.NewGuid();
                    newCDiabete.CadiovascularID = CRVascularID1;
                    newCDiabete.Description = IMedicneModel.CRVDiabetesDescription;
                    newCDiabete.CDiabetesCheck = true;
                    newCDiabete.CreatedOn = DateTime.Now;
                    _MedORDB.CDiabetes.AddObject(newCDiabete);
                }

                if (IMedicneModel.CRVDyslipidemiaDescription != null)
                {
                    var newDyslipidemia = new DataAccess.CDyslipidemia();
                    newDyslipidemia.CDyslipidemiaID = Guid.NewGuid();
                    newDyslipidemia.CadiovascularID = CRVascularID1;
                    newDyslipidemia.CheckBox = true;
                    newDyslipidemia.Description = IMedicneModel.CRVDyslipidemiaDescription;
                    newDyslipidemia.CreatedON = DateTime.Now;
                    _MedORDB.CDyslipidemias.AddObject(newDyslipidemia);
                }

                if (IMedicneModel.CRVAlcoholDescription != null)
                {
                    var newCAlcohol = new DataAccess.CAlcohol();
                    newCAlcohol.AlcoholID = Guid.NewGuid();
                    newCAlcohol.CadiovascularID = CRVascularID1;
                    newCAlcohol.CheckBox = true;
                    newCAlcohol.Description = IMedicneModel.CRVAlcoholDescription;
                    newCAlcohol.CreatedOn = DateTime.Now;
                    _MedORDB.CAlcohols.AddObject(newCAlcohol);
                }
                if (IMedicneModel.CRVObesityDescription != null)
                {
                    var newCObesity = new DataAccess.CObesity();
                    newCObesity.ObesityID = Guid.NewGuid();
                    newCObesity.CadiovascularID = CRVascularID1;
                    newCObesity.CheckBox = true;
                    newCObesity.Description = IMedicneModel.CRVObesityDescription;
                    newCObesity.CreatedOn = DateTime.Now;
                    _MedORDB.CObesities.AddObject(newCObesity);
                }
                if (IMedicneModel.CRVFamilialHistoryDescription != null)
                {
                    var newCFamilialHistory = new DataAccess.CFamilialHistory();
                    newCFamilialHistory.FamilialHistoryID = Guid.NewGuid();
                    newCFamilialHistory.CadiovascularID = CRVascularID1;
                    newCFamilialHistory.CheckBox = true;
                    newCFamilialHistory.Description = IMedicneModel.CRVFamilialHistoryDescription;
                    newCFamilialHistory.CreatedOn = DateTime.Now;
                    _MedORDB.CFamilialHistories.AddObject(newCFamilialHistory);
                }

               _MedORDB.SaveChanges();
                success = "panel_tab2";
            }






            //medical history

            var medicalHistoryID = Guid.NewGuid();
            var IMmedicalHistories = _MedORDB.IMmedicalHistories.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();

            if (IMmedicalHistories != null & (IMedicneModel.MHAllergies != null || IMedicneModel.MHGynecology != null || IMedicneModel.MHFamily != null
                || IMedicneModel.MHMedical != null || IMedicneModel.MHSurgical != null))
            {
                var medicalHistoryID1 = IMmedicalHistories.MedicaHistoryID;
                if (IMedicneModel.MHAllergies != null)
                {
                    var newAllergies = new DataAccess.MHAllergy();
                    newAllergies.MHAllergiesID = Guid.NewGuid();
                    newAllergies.MedicaHistoryID = medicalHistoryID1;
                    newAllergies.Description = IMedicneModel.MHAllergies;
                    newAllergies.CreatedOn = DateTime.Now;
                    _MedORDB.MHAllergies.AddObject(newAllergies);
                }

                if (IMedicneModel.MHGynecology != null)
                {
                    var newGynecology = new DataAccess.MHGynecology();
                    newGynecology.GynecologyID = Guid.NewGuid();
                    newGynecology.MedicaHistoryID = medicalHistoryID1;
                    newGynecology.Description = IMedicneModel.MHGynecology;
                    newGynecology.CreatedOn = DateTime.Now;
                    _MedORDB.MHGynecologies.AddObject(newGynecology);
                }
                if (IMedicneModel.MHFamily != null)
                {
                    var newFamily = new DataAccess.MHFamily();
                    newFamily.FamilyID = Guid.NewGuid();
                    newFamily.MedicaHistoryID = medicalHistoryID1;
                    newFamily.Description = IMedicneModel.MHFamily;
                    newFamily.CreatedOn = DateTime.Now;
                    _MedORDB.MHFamilies.AddObject(newFamily);
                }
                if (IMedicneModel.MHMedical != null)
                {
                    var newMedical = new DataAccess.MHMedical();
                    newMedical.MedicalID = Guid.NewGuid();
                    newMedical.MedicaHistoryID = medicalHistoryID1;
                    newMedical.Description = IMedicneModel.MHMedical;
                    newMedical.CreatedOn = DateTime.Now;
                    _MedORDB.MHMedicals.AddObject(newMedical);
                }
                if (IMedicneModel.MHSurgical != null)
                {
                    var newSurgical = new DataAccess.MHSurgical();
                    newSurgical.SurgicalID = Guid.NewGuid();
                    newSurgical.MedicaHistoryID = medicalHistoryID1;
                    newSurgical.Description = IMedicneModel.MHSurgical;
                    newSurgical.CreatedOn = DateTime.Now;
                    _MedORDB.MHSurgicals.AddObject(newSurgical);
                }
                
             _MedORDB.SaveChanges();
                success = "";
            }


            // checkbox review of system
            var ReviewSystemsID = Guid.NewGuid();
            var IMReviewSystems = _MedORDB.IMReviewSystems.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();

            if (IMReviewSystems != null & (IMedicneModel.RSHEENTDescription != null || IMedicneModel.RSSkinDescription != null || IMedicneModel.RSUrogenitalDescription != null
                || IMedicneModel.RSPsychosocialDescription != null || IMedicneModel.RSPulmonaryDescription != null || IMedicneModel.RSCardiovascularDescriptionical != null || IMedicneModel.RSNervouSystemDescription != null 
                || IMedicneModel.RSMusculoskeletalDescription != null || IMedicneModel.RSDigestiveDescription != null))
            {
                var ReviewSystemsID1 = IMReviewSystems.ReviewOfSystemID;
                if (IMedicneModel.RSHEENTDescription != null)
                {
                    var newRSHEENT = new DataAccess.RSHEENT();
                    newRSHEENT.HEENTID = Guid.NewGuid();
                    newRSHEENT.ReviewOfSystemID = ReviewSystemsID1;
                    newRSHEENT.Description = IMedicneModel.RSHEENTDescription;
                    newRSHEENT.CheckBox = true;
                    newRSHEENT.CreatedOn = DateTime.Now;
                    _MedORDB.RSHEENTs.AddObject(newRSHEENT);
                }

                if (IMedicneModel.RSSkinDescription != null)
                {
                    var newRSSkin = new DataAccess.RSSkin();
                    newRSSkin.SkinID = Guid.NewGuid();
                    newRSSkin.ReviewOfSystemID = ReviewSystemsID1;
                    newRSSkin.Description = IMedicneModel.RSSkinDescription;
                    newRSSkin.CheckBox = true;
                    newRSSkin.CreatedOn = DateTime.Now;
                    _MedORDB.RSSkins.AddObject(newRSSkin);
                }

                if (IMedicneModel.RSUrogenitalDescription != null)
                {
                    var newRSUrogental = new DataAccess.RSUrogental();
                    newRSUrogental.UrogentalID = Guid.NewGuid();
                    newRSUrogental.ReviewOfSystemID = ReviewSystemsID1;
                    newRSUrogental.Description = IMedicneModel.RSUrogenitalDescription;
                    newRSUrogental.CheckBox = true;
                    newRSUrogental.CreatedOn = DateTime.Now;
                    _MedORDB.RSUrogentals.AddObject(newRSUrogental);
                }
                if (IMedicneModel.RSPsychosocialDescription != null)
                {
                    var newRSPsychosociale = new DataAccess.RSPsychosociale();
                    newRSPsychosociale.PsychosocialeID = Guid.NewGuid();
                    newRSPsychosociale.ReviewOfSystemID = ReviewSystemsID1;
                    newRSPsychosociale.Description = IMedicneModel.RSPsychosocialDescription;
                    newRSPsychosociale.CheckBox = true;
                    newRSPsychosociale.CreatedOn = DateTime.Now;
                    _MedORDB.RSPsychosociales.AddObject(newRSPsychosociale);
                }
                if (IMedicneModel.RSPulmonaryDescription != null)
                {
                    var newRSPulminary = new DataAccess.RSPulminary();
                    newRSPulminary.PulminaryID = Guid.NewGuid();
                    newRSPulminary.ReviewOfSystemID = ReviewSystemsID1;
                    newRSPulminary.Description = IMedicneModel.RSPulmonaryDescription;
                    newRSPulminary.CheckBox = true;
                    newRSPulminary.CreatedOn = DateTime.Now;
                    _MedORDB.RSPulminaries.AddObject(newRSPulminary);
                }
                if (IMedicneModel.RSCardiovascularDescriptionical != null)
                {
                    var newRSCardiovascular = new DataAccess.RSCardiovascular();
                    newRSCardiovascular.CardiovascularID = Guid.NewGuid();
                    newRSCardiovascular.ReviewOfSystemID = ReviewSystemsID1;
                    newRSCardiovascular.Description = IMedicneModel.RSCardiovascularDescriptionical;
                    newRSCardiovascular.CheckBox = true;
                    newRSCardiovascular.CreatedOn = DateTime.Now;
                    _MedORDB.RSCardiovasculars.AddObject(newRSCardiovascular);
                }
                if (IMedicneModel.RSNervouSystemDescription != null)
                {
                    var newRSNervousSystem = new DataAccess.RSNervousSystem();
                    newRSNervousSystem.NervousSystemID = Guid.NewGuid();
                    newRSNervousSystem.ReviewOfSystemID = ReviewSystemsID1;
                    newRSNervousSystem.Description = IMedicneModel.RSNervouSystemDescription;
                    newRSNervousSystem.CheckBox = true;
                    newRSNervousSystem.CreatedOn = DateTime.Now;
                    _MedORDB.RSNervousSystems.AddObject(newRSNervousSystem);
                }
                if (IMedicneModel.RSMusculoskeletalDescription != null)
                {
                    var newRSMusculoskeletal = new DataAccess.RSMusculoskeletal();
                    newRSMusculoskeletal.MusculoskeletalID = Guid.NewGuid();
                    newRSMusculoskeletal.ReviewOfSystemID = ReviewSystemsID1;
                    newRSMusculoskeletal.Description = IMedicneModel.RSMusculoskeletalDescription;
                    newRSMusculoskeletal.CheckBox = true;
                    newRSMusculoskeletal.CreatedOn = DateTime.Now;
                    _MedORDB.RSMusculoskeletals.AddObject(newRSMusculoskeletal);
                }
                if (IMedicneModel.RSDigestiveDescription != null)
                {
                    var newRSDigestive = new DataAccess.RSDigestive();
                    newRSDigestive.DigestiveID = Guid.NewGuid();
                    newRSDigestive.ReviewOfSystemID = ReviewSystemsID1;
                    newRSDigestive.Description = IMedicneModel.RSDigestiveDescription;
                    newRSDigestive.CheckBox = true;
                    newRSDigestive.CreatedOn = DateTime.Now;
                    _MedORDB.RSDigestives.AddObject(newRSDigestive);
                }

                 _MedORDB.SaveChanges();
                success = "panel_tab5";
            }

            // checkbox physical of history
            var PhysicalhHistoryID = Guid.NewGuid();
            var IMPhysicalExamHistory = _MedORDB.IMPhysicalExamHistories.Where(x => x.IMconsultation == IMedicneModel.IMconsultationID).FirstOrDefault();

            if (IMPhysicalExamHistory != null & (IMedicneModel.PHHEENTDescription != null || IMedicneModel.PHSkinDescription != null || IMedicneModel.PHUrogenitalDescription != null
                || IMedicneModel.PHPsychosocialDescription != null || IMedicneModel.PHPulmonaryDescription != null || IMedicneModel.PHNervousSystemDescription != null
                || IMedicneModel.PHMusculoskeletalDescription != null || IMedicneModel.PHDigestiveDescription != null))
            {
                var PhysicalhHistoryID1 = IMPhysicalExamHistory.PhysicalExamHistoryID;
                if (IMedicneModel.PHHEENTDescription != null)
                {
                    var newPHHEENT = new DataAccess.PHHEENT();
                    newPHHEENT.HEENTID = Guid.NewGuid();
                    newPHHEENT.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHHEENT.Description = IMedicneModel.PHHEENTDescription;
                    newPHHEENT.CheckBox = true;
                    newPHHEENT.CreatedOn = DateTime.Now;
                    _MedORDB.PHHEENTs.AddObject(newPHHEENT);
                }

                if (IMedicneModel.PHSkinDescription != null)
                {
                    var newPHSkin = new DataAccess.PHSkin();
                    newPHSkin.SkinID = Guid.NewGuid();
                    newPHSkin.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHSkin.Description = IMedicneModel.PHSkinDescription;
                    newPHSkin.CheckBox = true;
                    newPHSkin.CreatedOn = DateTime.Now;
                    _MedORDB.PHSkins.AddObject(newPHSkin);
                }

                if (IMedicneModel.PHUrogenitalDescription != null)
                {
                    var newPHUrogental = new DataAccess.PHUrogental();
                    newPHUrogental.UrogentalID = Guid.NewGuid();
                    newPHUrogental.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHUrogental.Description = IMedicneModel.PHUrogenitalDescription;
                    newPHUrogental.CheckBox = true;
                    newPHUrogental.CreatedOn = DateTime.Now;
                    _MedORDB.PHUrogentals.AddObject(newPHUrogental);
                }
                if (IMedicneModel.PHPsychosocialDescription != null)
                {
                    var newPHPsychosociale = new DataAccess.PHPsychosociale();
                    newPHPsychosociale.PsychosocialeID = Guid.NewGuid();
                    newPHPsychosociale.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHPsychosociale.Description = IMedicneModel.PHPsychosocialDescription;
                    newPHPsychosociale.CheckBox = true;
                    newPHPsychosociale.CreatedOn = DateTime.Now;
                    _MedORDB.PHPsychosociales.AddObject(newPHPsychosociale);
                }
                if (IMedicneModel.PHPulmonaryDescription != null)
                {
                    var newPHPulminary = new DataAccess.PHPulminary();
                    newPHPulminary.PulminaryID = Guid.NewGuid();
                    newPHPulminary.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHPulminary.Description = IMedicneModel.PHPulmonaryDescription;
                    newPHPulminary.CheckBox = true;
                    newPHPulminary.CreatedOn = DateTime.Now;
                    _MedORDB.PHPulminaries.AddObject(newPHPulminary);
                }
                if (IMedicneModel.PHNervousSystemDescription != null)
                {
                    var newPHNervousSystem = new DataAccess.RSNervousSystem();
                    newPHNervousSystem.NervousSystemID = Guid.NewGuid();
                    newPHNervousSystem.ReviewOfSystemID = PhysicalhHistoryID1;
                    newPHNervousSystem.Description = IMedicneModel.PHNervousSystemDescription;
                    newPHNervousSystem.CheckBox = true;
                    newPHNervousSystem.CreatedOn = DateTime.Now;
                    _MedORDB.RSNervousSystems.AddObject(newPHNervousSystem);
                }
                if (IMedicneModel.PHMusculoskeletalDescription != null)
                {
                    var newPHMusculoskeletal = new DataAccess.PHMusculoskeletal();
                    newPHMusculoskeletal.MusculoskeletalID = Guid.NewGuid();
                    newPHMusculoskeletal.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHMusculoskeletal.Description = IMedicneModel.PHMusculoskeletalDescription;
                    newPHMusculoskeletal.CheckBox = true;
                    newPHMusculoskeletal.CreatedOn = DateTime.Now;
                    _MedORDB.PHMusculoskeletals.AddObject(newPHMusculoskeletal);
                }
                if (IMedicneModel.PHDigestiveDescription != null)
                {
                    var newPHDigestive = new DataAccess.PHDigestive();
                    newPHDigestive.DigestiveID = Guid.NewGuid();
                    newPHDigestive.PhysicalExamHistoryID = PhysicalhHistoryID1;
                    newPHDigestive.Description = IMedicneModel.PHDigestiveDescription;
                    newPHDigestive.CheckBox = true;
                    newPHDigestive.CreatedOn = DateTime.Now;
                    _MedORDB.PHDigestives.AddObject(newPHDigestive);
                }

              _MedORDB.SaveChanges();
                success = "";
            }



            if (IMCRVasculars == null & (IMedicneModel.CRVDiabetesDescription != null|| IMedicneModel.CRVDyslipidemiaDescription != null || IMedicneModel.CRVAlcoholDescription != null
                || IMedicneModel.CRVObesityDescription != null || IMedicneModel.CRVFamilialHistoryDescription != null || IMedicneModel.CRVFamilialHistoryDescription != null))
            {
                var newIMCRVascular = new DataAccess.IMCRVascular();
                newIMCRVascular.CRVascularID= Guid.NewGuid();
                newIMCRVascular.IMconsultationID = IMedicneModel.IMconsultationID;
                newIMCRVascular.CreatedOn = DateTime.Now;
                newIMCRVascular.PatientID = IMedicneModel.IMconsultationModelN.PatientID;
               
                _MedORDB.IMCRVasculars.AddObject(newIMCRVascular);

                if (IMedicneModel.CRVDiabetesDescription != null)
                {
                    var newCDiabete = new DataAccess.CDiabete();
                    newCDiabete.DiabetesID = Guid.NewGuid();
                    newCDiabete.CadiovascularID = newIMCRVascular.CRVascularID;
                    newCDiabete.Description = IMedicneModel.CRVDiabetesDescription;
                    newCDiabete.CreatedOn = DateTime.Now;
                    newCDiabete.CDiabetesCheck = true;
                    _MedORDB.CDiabetes.AddObject(newCDiabete);
                }

                if (IMedicneModel.CRVDyslipidemiaDescription != null)
                { 
                var newDyslipidemia = new DataAccess.CDyslipidemia();
                newDyslipidemia.CDyslipidemiaID = Guid.NewGuid();
                newDyslipidemia.CadiovascularID = newIMCRVascular.CRVascularID;
                newDyslipidemia.Description = IMedicneModel.CRVDyslipidemiaDescription;
                newDyslipidemia.CreatedON = DateTime.Now;
                _MedORDB.CDyslipidemias.AddObject(newDyslipidemia);
                }

                if (IMedicneModel.CRVAlcoholDescription != null)
                {
                    var newAlcohol = new DataAccess.CAlcohol();
                    newAlcohol.AlcoholID = Guid.NewGuid();
                    newAlcohol.CadiovascularID = newIMCRVascular.CRVascularID;
                    newAlcohol.Description = IMedicneModel.CRVDyslipidemiaDescription;
                    

                    newAlcohol.CreatedOn = DateTime.Now;
                    _MedORDB.CAlcohols.AddObject(newAlcohol);
                }

                if (IMedicneModel.CRVObesityDescription != null)
                {
                    var newCObesity = new DataAccess.CObesity();
                    newCObesity.ObesityID = Guid.NewGuid();
                    newCObesity.CadiovascularID = newIMCRVascular.CRVascularID;
                    newCObesity.Description = IMedicneModel.CRVObesityDescription;
                    newCObesity.CreatedOn = DateTime.Now;
                    _MedORDB.CObesities.AddObject(newCObesity);
                }

                if (IMedicneModel.CRVFamilialHistoryDescription != null)
                {
                    var newCFamilialHistory = new DataAccess.CFamilialHistory();
                    newCFamilialHistory.FamilialHistoryID = Guid.NewGuid();
                    newCFamilialHistory.CadiovascularID = newIMCRVascular.CRVascularID;
                    newCFamilialHistory.Description = IMedicneModel.CRVFamilialHistoryDescription;
                    newCFamilialHistory.CreatedOn = DateTime.Now;
                    _MedORDB.CFamilialHistories.AddObject(newCFamilialHistory);
                }

              _MedORDB.SaveChanges();
                success = "";

            }

            if (IMmedicalHistories == null & (IMedicneModel.MHAllergies != null || IMedicneModel.MHGynecology != null || IMedicneModel.MHFamily != null 
                || IMedicneModel.MHMedical != null || IMedicneModel.MHSurgical != null ))
            {
                var newmedicalHistory = new DataAccess.IMmedicalHistory();
                newmedicalHistory.MedicaHistoryID = Guid.NewGuid();
                newmedicalHistory.IMconsultationID = IMedicneModel.IMconsultationID;
                newmedicalHistory.CreatedOn = DateTime.Now;
                newmedicalHistory.PatientID = IMedicneModel.IMconsultationModelN.PatientID;
                
                _MedORDB.IMmedicalHistories.AddObject(newmedicalHistory);

                if (IMedicneModel.MHAllergies != null)
                {
                    var newAllergies = new DataAccess.MHAllergy();
                    newAllergies.MHAllergiesID = Guid.NewGuid();
                    newAllergies.MedicaHistoryID = newmedicalHistory.MedicaHistoryID;
                    newAllergies.Description = IMedicneModel.MHAllergies;
                    newAllergies.CreatedOn = DateTime.Now;
                    _MedORDB.MHAllergies.AddObject(newAllergies);
                }

                if (IMedicneModel.MHGynecology != null)
                {
                    var newGynecology = new DataAccess.MHGynecology();
                    newGynecology.GynecologyID = Guid.NewGuid();
                    newGynecology.MedicaHistoryID = newmedicalHistory.MedicaHistoryID;
                    newGynecology.Description = IMedicneModel.MHGynecology;
                    newGynecology.CreatedOn = DateTime.Now;
                    _MedORDB.MHGynecologies.AddObject(newGynecology);
                }
                if (IMedicneModel.MHFamily != null)
                {
                    var newFamily = new DataAccess.MHFamily();
                    newFamily.FamilyID = Guid.NewGuid();
                    newFamily.MedicaHistoryID = newmedicalHistory.MedicaHistoryID;
                    newFamily.Description = IMedicneModel.MHFamily;
                    newFamily.CreatedOn = DateTime.Now;
                    _MedORDB.MHFamilies.AddObject(newFamily);
                }
                if (IMedicneModel.MHMedical != null)
                {
                    var newMedical = new DataAccess.MHMedical();
                    newMedical.MedicalID = Guid.NewGuid();
                    newMedical.MedicaHistoryID = newmedicalHistory.MedicaHistoryID;
                    newMedical.Description = IMedicneModel.MHMedical;
                    newMedical.CreatedOn = DateTime.Now;
                    _MedORDB.MHMedicals.AddObject(newMedical);
                }
                if (IMedicneModel.MHSurgical != null)
                {
                    var newSurgical = new DataAccess.MHSurgical();
                    newSurgical.SurgicalID = Guid.NewGuid();
                    newSurgical.MedicaHistoryID = newmedicalHistory.MedicaHistoryID;
                    newSurgical.Description = IMedicneModel.MHSurgical;
                    newSurgical.CreatedOn = DateTime.Now;
                    _MedORDB.MHSurgicals.AddObject(newSurgical);
                }

                _MedORDB.SaveChanges();
                success = "";

            }

            //if description of review of system is new
            if (IMReviewSystems == null & (IMedicneModel.RSHEENTDescription != null || IMedicneModel.RSSkinDescription != null || IMedicneModel.RSUrogenitalDescription != null
               || IMedicneModel.RSPsychosocialDescription != null || IMedicneModel.RSPulmonaryDescription != null || IMedicneModel.RSNervouSystemDescription != null
               || IMedicneModel.RSMusculoskeletalDescription != null || IMedicneModel.RSDigestiveDescription != null))
            {

                var newIMReviewSystem = new DataAccess.IMReviewSystem();
                newIMReviewSystem.ReviewOfSystemID = Guid.NewGuid();
                newIMReviewSystem.IMconsultationID = IMedicneModel.IMconsultationID;
                newIMReviewSystem.CreadtedOn = DateTime.Now;
                newIMReviewSystem.PatientID = IMedicneModel.IMconsultationModelN.PatientID;

                if (IMedicneModel.RSHEENTDescription != null)
                {
                    var newRSHEENT = new DataAccess.RSHEENT();
                    newRSHEENT.HEENTID = Guid.NewGuid();
                    newRSHEENT.ReviewOfSystemID = ReviewSystemsID;
                    newRSHEENT.Description = IMedicneModel.RSHEENTDescription;
                    newRSHEENT.CheckBox = true;
                    newRSHEENT.CreatedOn = DateTime.Now;
                    _MedORDB.RSHEENTs.AddObject(newRSHEENT);
                }

                if (IMedicneModel.RSSkinDescription != null)
                {
                    var newRSSkin = new DataAccess.RSSkin();
                    newRSSkin.SkinID = Guid.NewGuid();
                    newRSSkin.ReviewOfSystemID = ReviewSystemsID;
                    newRSSkin.Description = IMedicneModel.RSSkinDescription;
                    newRSSkin.CheckBox = true;
                    newRSSkin.CreatedOn = DateTime.Now;
                    _MedORDB.RSSkins.AddObject(newRSSkin);
                }

                if (IMedicneModel.RSUrogenitalDescription != null)
                {
                    var newRSUrogental = new DataAccess.RSUrogental();
                    newRSUrogental.UrogentalID = Guid.NewGuid();
                    newRSUrogental.ReviewOfSystemID = ReviewSystemsID;
                    newRSUrogental.Description = IMedicneModel.RSUrogenitalDescription;
                    newRSUrogental.CheckBox = true;
                    newRSUrogental.CreatedOn = DateTime.Now;
                    _MedORDB.RSUrogentals.AddObject(newRSUrogental);
                }
                if (IMedicneModel.RSPsychosocialDescription != null)
                {
                    var newRSPsychosociale = new DataAccess.RSPsychosociale();
                    newRSPsychosociale.PsychosocialeID = Guid.NewGuid();
                    newRSPsychosociale.ReviewOfSystemID = ReviewSystemsID;
                    newRSPsychosociale.Description = IMedicneModel.RSPsychosocialDescription;
                    newRSPsychosociale.CheckBox = true;
                    newRSPsychosociale.CreatedOn = DateTime.Now;
                    _MedORDB.RSPsychosociales.AddObject(newRSPsychosociale);
                }
                if (IMedicneModel.RSPulmonaryDescription != null)
                {
                    var newRSPulminary = new DataAccess.RSPulminary();
                    newRSPulminary.PulminaryID = Guid.NewGuid();
                    newRSPulminary.ReviewOfSystemID = ReviewSystemsID;
                    newRSPulminary.Description = IMedicneModel.RSPulmonaryDescription;
                    newRSPulminary.CheckBox = true;
                    newRSPulminary.CreatedOn = DateTime.Now;
                    _MedORDB.RSPulminaries.AddObject(newRSPulminary);
                }
                if (IMedicneModel.RSNervouSystemDescription != null)
                {
                    var newRSNervousSystem = new DataAccess.RSNervousSystem();
                    newRSNervousSystem.NervousSystemID = Guid.NewGuid();
                    newRSNervousSystem.ReviewOfSystemID = ReviewSystemsID;
                    newRSNervousSystem.Description = IMedicneModel.RSNervouSystemDescription;
                    newRSNervousSystem.CheckBox = true;
                    newRSNervousSystem.CreatedOn = DateTime.Now;
                    _MedORDB.RSNervousSystems.AddObject(newRSNervousSystem);
                }
                if (IMedicneModel.RSMusculoskeletalDescription != null)
                {
                    var newRSMusculoskeletal = new DataAccess.RSMusculoskeletal();
                    newRSMusculoskeletal.MusculoskeletalID = Guid.NewGuid();
                    newRSMusculoskeletal.ReviewOfSystemID = ReviewSystemsID;
                    newRSMusculoskeletal.Description = IMedicneModel.RSMusculoskeletalDescription;
                    newRSMusculoskeletal.CheckBox = true;
                    newRSMusculoskeletal.CreatedOn = DateTime.Now;
                    _MedORDB.RSMusculoskeletals.AddObject(newRSMusculoskeletal);
                }
                if (IMedicneModel.RSDigestiveDescription != null)
                {
                    var newRSDigestive = new DataAccess.RSDigestive();
                    newRSDigestive.DigestiveID = Guid.NewGuid();
                    newRSDigestive.ReviewOfSystemID = ReviewSystemsID;
                    newRSDigestive.Description = IMedicneModel.RSPulmonaryDescription;
                    newRSDigestive.CheckBox = true;
                    newRSDigestive.CreatedOn = DateTime.Now;
                    _MedORDB.RSDigestives.AddObject(newRSDigestive);
                }

                _MedORDB.SaveChanges();
                success = "";
            }


            // if physical of history is new

            if (IMPhysicalExamHistory == null & (IMedicneModel.PHHEENTDescription != null || IMedicneModel.PHSkinDescription != null || IMedicneModel.PHUrogenitalDescription != null
              || IMedicneModel.PHPsychosocialDescription != null || IMedicneModel.PHPulmonaryDescription != null || IMedicneModel.PHNervousSystemDescription != null
              || IMedicneModel.PHMusculoskeletalDescription != null || IMedicneModel.PHDigestiveDescription != null))
            {

                var newIMReviewSystem = new DataAccess.IMReviewSystem();
                newIMReviewSystem.ReviewOfSystemID = Guid.NewGuid();
                newIMReviewSystem.IMconsultationID = IMedicneModel.IMconsultationID;
                newIMReviewSystem.CreadtedOn = DateTime.Now;
                newIMReviewSystem.PatientID = IMedicneModel.IMconsultationModelN.PatientID;

                if (IMedicneModel.PHHEENTDescription != null)
                {
                    var newPHHEENT = new DataAccess.PHHEENT();
                    newPHHEENT.HEENTID = Guid.NewGuid();
                    newPHHEENT.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHHEENT.Description = IMedicneModel.PHHEENTDescription;
                    newPHHEENT.CheckBox = true;
                    newPHHEENT.CreatedOn = DateTime.Now;
                    _MedORDB.PHHEENTs.AddObject(newPHHEENT);
                }

                if (IMedicneModel.PHSkinDescription != null)
                {
                    var newPHSkin = new DataAccess.PHSkin();
                    newPHSkin.SkinID = Guid.NewGuid();
                    newPHSkin.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHSkin.Description = IMedicneModel.PHSkinDescription;
                    newPHSkin.CheckBox = true;
                    newPHSkin.CreatedOn = DateTime.Now;
                    _MedORDB.PHSkins.AddObject(newPHSkin);
                }

                if (IMedicneModel.PHUrogenitalDescription != null)
                {
                    var newPHUrogental = new DataAccess.PHUrogental();
                    newPHUrogental.UrogentalID = Guid.NewGuid();
                    newPHUrogental.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHUrogental.Description = IMedicneModel.RSUrogenitalDescription;
                    newPHUrogental.CheckBox = true;
                    newPHUrogental.CreatedOn = DateTime.Now;
                    _MedORDB.PHUrogentals.AddObject(newPHUrogental);
                }
                if (IMedicneModel.PHPsychosocialDescription != null)
                {
                    var newPHPsychosociale = new DataAccess.PHPsychosociale();
                    newPHPsychosociale.PsychosocialeID = Guid.NewGuid();
                    newPHPsychosociale.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHPsychosociale.Description = IMedicneModel.PHPsychosocialDescription;
                    newPHPsychosociale.CheckBox = true;
                    newPHPsychosociale.CreatedOn = DateTime.Now;
                    _MedORDB.PHPsychosociales.AddObject(newPHPsychosociale);
                }
                if (IMedicneModel.PHPulmonaryDescription != null)
                {
                    var newPHPulminary = new DataAccess.PHPulminary();
                    newPHPulminary.PulminaryID = Guid.NewGuid();
                    newPHPulminary.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHPulminary.Description = IMedicneModel.PHPulmonaryDescription;
                    newPHPulminary.CheckBox = true;
                    newPHPulminary.CreatedOn = DateTime.Now;
                    _MedORDB.PHPulminaries.AddObject(newPHPulminary);
                }
                if (IMedicneModel.PHNervousSystemDescription != null)
                {
                    var newPHNervousSystem = new DataAccess.PHNervousSystem();
                    newPHNervousSystem.NervousSystemID = Guid.NewGuid();
                    newPHNervousSystem.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHNervousSystem.Description = IMedicneModel.PHNervousSystemDescription;
                    newPHNervousSystem.CheckBox = true;
                    newPHNervousSystem.CreatedOn = DateTime.Now;
                    _MedORDB.PHNervousSystems.AddObject(newPHNervousSystem);
                }
                if (IMedicneModel.PHMusculoskeletalDescription != null)
                {
                    var newPHMusculoskeletal = new DataAccess.PHMusculoskeletal();
                    newPHMusculoskeletal.MusculoskeletalID = Guid.NewGuid();
                    newPHMusculoskeletal.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHMusculoskeletal.Description = IMedicneModel.PHMusculoskeletalDescription;
                    newPHMusculoskeletal.CheckBox = true;
                    newPHMusculoskeletal.CreatedOn = DateTime.Now;
                    _MedORDB.PHMusculoskeletals.AddObject(newPHMusculoskeletal);
                }
                if (IMedicneModel.PHDigestiveDescription != null)
                {
                    var newPHDigestive = new DataAccess.PHDigestive();
                    newPHDigestive.DigestiveID = Guid.NewGuid();
                    newPHDigestive.PhysicalExamHistoryID = PhysicalhHistoryID;
                    newPHDigestive.Description = IMedicneModel.PHPulmonaryDescription;
                    newPHDigestive.CheckBox = true;
                    newPHDigestive.CreatedOn = DateTime.Now;
                    _MedORDB.PHDigestives.AddObject(newPHDigestive);
                }

                _MedORDB.SaveChanges();
                success = "";
            }

            return success;
        }
        public Int32 SetFeeConsultation(DataModel.IMedicneModel IMedicneModel)
        {
            //DataModel.InvoicesModel InvoicesModel =new DataModel.InvoicesModel();
            //DataModel.InvoiceItemModel InvoiceItemModel = new DataModel.InvoiceItemModel();

            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.ConsultationIDTap).FirstOrDefault();
            var lastfee = EditConsultation.Fees;
            EditConsultation.Fees = IMedicneModel.CFees;
            //if (IMedicneModel.CCheckBox == true)
            EditConsultation.Checkbox = IMedicneModel.CCheckBox; 
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.Currency = IMedicneModel.Ccurency;

            var success = _MedORDB.SaveChanges();
             var Result = _MedORDB.IMAccounts.Where(x => x.PatientID == IMedicneModel.PatientID );
             var Result2 = _MedORDB.IMAccounts.Where(x => x.PatientID == IMedicneModel.PatientID & (x.Currency == IMedicneModel.Ccurency)).FirstOrDefault();

            if (Result2 != null)
            {
                var e = Result2.TotalFees;
                var e1 = IMedicneModel.CFees;
                Guid Gu = new Guid("00000000-0000-0000-0000-000000000000");
                if (IMedicneModel.CmodifiedBy != Gu)
                {
                    Result2.TotalFees = e - lastfee;
                    Result2.TotalFees = Result2.TotalFees + e1;
                }
                else { Result2.TotalFees = e + e1; }
                success = _MedORDB.SaveChanges();

            }
            else
            {
                var NewAccount = new DataAccess.IMAccount();
                NewAccount.AccountID = Guid.NewGuid();
                NewAccount.CreateadBy = IMedicneModel.InCreatedBy;
                NewAccount.CreatedOn = DateTime.Now;
                NewAccount.PatientID = IMedicneModel.PatientID;
                NewAccount.TotalFees = IMedicneModel.CFees;
                NewAccount.Currency = IMedicneModel.Ccurency;

                _MedORDB.IMAccounts.AddObject(NewAccount);
                success = _MedORDB.SaveChanges();
            }

            return success;
        }
        public Int32 UpdateTapConsultation(DataModel.IMedicneModel IMedicneModel)
        {
            var EditConsultation  = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.Title = IMedicneModel.Ctitle;
            EditConsultation.Note = IMedicneModel.Cnote;
            //EditConsultation.CreatedOn = IMedicneModel.CcreatedOn;
            EditConsultation.ModifiedOn = IMedicneModel.CcreatedOn;
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateTapComplaint(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;
            var EditComplaint = _MedORDB.IMcomplaintHistories.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            if (EditComplaint == null)
            {
                var newComplaint = new DataAccess.IMcomplaintHistory();
                newComplaint.IMcomplaintHistoryID = Guid.NewGuid();
                newComplaint.IMconsultationID = IMedicneModel.IMconsultationID;
                newComplaint.PatientID = IMedicneModel.IMconsultationModelN.PatientID;

                newComplaint.Note = IMedicneModel.CHnote;
                newComplaint.Title = IMedicneModel.CHtitle;
                newComplaint.CreatedBy = IMedicneModel.CHmodifiedBy;
                newComplaint.ModifiedBy = IMedicneModel.CHmodifiedBy;
                newComplaint.CreatedOn = DateTime.Now;
                newComplaint.ModifiedOn = DateTime.Now;
                _MedORDB.IMcomplaintHistories.AddObject(newComplaint);

            }
            else
            {

                EditComplaint.Title = IMedicneModel.CHtitle;
                EditComplaint.Note = IMedicneModel.CHnote;
                EditComplaint.ModifiedOn = DateTime.Now;

                EditComplaint.ModifiedBy = IMedicneModel.CHmodifiedBy;


            }
            var success = _MedORDB.SaveChanges();
            return success;


        }
        public Int32 UpdateReviewSystem(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;
            var EditReviewSystem = _MedORDB.IMReviewSystems.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            if (EditReviewSystem == null)
            {
                var newReviewSystem = new DataAccess.IMReviewSystem();
                newReviewSystem.ReviewOfSystemID = Guid.NewGuid();
                newReviewSystem.IMconsultationID = IMedicneModel.IMconsultationID;
                newReviewSystem.PatientID = IMedicneModel.PatientID;
                newReviewSystem.ModifiedBy = IMedicneModel.CHmodifiedBy;
                newReviewSystem.CreadtedOn = DateTime.Now;
                _MedORDB.IMReviewSystems.AddObject(newReviewSystem);

                //update conultation
                EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
                EditConsultation.ModifiedOn = DateTime.Now;

            }
            else
            {
                //EditReviewSystem.Anorexia = IMedicneModel.RSAnorexia;
                //EditReviewSystem.Weight = IMedicneModel.RSWeight;
                //EditReviewSystem.Fatigue = IMedicneModel.RSFatigue;
                //EditReviewSystem.Fever = IMedicneModel.RSFever;

            }
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdatePhysicalHistory(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;

            var EditPhysicalHistory = _MedORDB.IMPhysicalExamHistories.Where(x => x.IMconsultation == IMedicneModel.IMconsultationID).FirstOrDefault();
            if (EditPhysicalHistory == null)
            {
                var newPhysicalHistory = new DataAccess.IMPhysicalExamHistory();
                newPhysicalHistory.PhysicalExamHistoryID = Guid.NewGuid();
                newPhysicalHistory.IMconsultation = IMedicneModel.IMconsultationID;
                newPhysicalHistory.PatientID = IMedicneModel.PatientID;
                newPhysicalHistory.BloodPressure = IMedicneModel.PHBloodPressure;
                newPhysicalHistory.Weight = IMedicneModel.PHWeight;
                newPhysicalHistory.RespiratoryRate = IMedicneModel.PHRespiratoryRate;
                newPhysicalHistory.ModifiedBy = IMedicneModel.CHmodifiedBy;
                newPhysicalHistory.CreadtedOn = DateTime.Now;
                _MedORDB.IMPhysicalExamHistories.AddObject(newPhysicalHistory);
             
            }
            else
            {
                EditPhysicalHistory.BloodPressure = IMedicneModel.PHBloodPressure;
                EditPhysicalHistory.Weight = IMedicneModel.PHWeight;
                EditPhysicalHistory.RespiratoryRate = IMedicneModel.PHRespiratoryRate;

            }
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateHospitalizations(DataModel.IMedicneModel IMedicneModel)
        {
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;

            var EditHospitalization = _MedORDB.IMHopitalizations.Where(x => x.ConsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            if (EditHospitalization == null)
            {
                var newHospitalization = new DataAccess.IMHopitalization();
                newHospitalization.HospitalizationID = Guid.NewGuid();
                newHospitalization.ConsultationID = IMedicneModel.IMconsultationID;
                newHospitalization.PatientID = IMedicneModel.PatientID;

                newHospitalization.HospitalName = IMedicneModel.HSAdmissionName;
                newHospitalization.AdmissionDate = IMedicneModel.HSAdmissionDate;
                newHospitalization.departuredate = IMedicneModel.HSDeparture;
                newHospitalization.lengthStay = IMedicneModel.HSLengthStay;
                newHospitalization.Description = IMedicneModel.HSDescription;

                newHospitalization.ModifiedBy = IMedicneModel.CHmodifiedBy;
                newHospitalization.CreatedOn = DateTime.Now;
                _MedORDB.IMHopitalizations.AddObject(newHospitalization);

             

            }
            else {

                EditHospitalization.HospitalName = IMedicneModel.HSAdmissionName;
                EditHospitalization.AdmissionDate = IMedicneModel.HSAdmissionDate;
                EditHospitalization.departuredate = IMedicneModel.HSDeparture;
                EditHospitalization.lengthStay = IMedicneModel.HSLengthStay;
                EditHospitalization.Description = IMedicneModel.HSDescription;

               
            }


            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateMedication(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;
            var newMedication = new DataAccess.IMMedication();
            newMedication.MedicationID = Guid.NewGuid();
            newMedication.IMconsultationID = IMedicneModel.IMconsultationID;
            newMedication.PatientID = IMedicneModel.IMconsultationModelN.PatientID;
            newMedication.MedicationName = IMedicneModel.MMedicationName;
            newMedication.DateStart = IMedicneModel.MDateStart;
            newMedication.DateStop = IMedicneModel.MDateStop;
            newMedication.Doze = IMedicneModel.MDoze;
            newMedication.Morning = IMedicneModel.MMorning;
            newMedication.afternoon = IMedicneModel.MAfternoon;
            newMedication.evening = IMedicneModel.MEvening;
            newMedication.BedTime = IMedicneModel.MBedTime;
            newMedication.Asneeded = IMedicneModel.MAsNeeded;
            newMedication.HowMuch = IMedicneModel.MHowMuch;
            newMedication.Reason = IMedicneModel.MReason;
            newMedication.AdditionalInformation = IMedicneModel.MAdditionalInformation;
            newMedication.Asneeded = IMedicneModel.MAsNeeded;

            newMedication.ModifiedBy = IMedicneModel.CHmodifiedBy;
            newMedication.CreatedOn = DateTime.Now;
            _MedORDB.IMMedications.AddObject(newMedication);

            //var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            //EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            //EditConsultation.ModifiedOn = DateTime.Now;
            //var newMedication = new DataAccess.IMMedication();
            //    newMedication.MedicationID = Guid.NewGuid();
            //    newMedication.IMconsultationID = IMedicneModel.IMconsultationID;
            //    newMedication.PatientID = IMedicneModel.PatientID;
            //    newMedication.MedicationName = IMedicneModel.MMedicationName;
            //    newMedication.DateStart = IMedicneModel.MDateStart;
            //    newMedication.DateStop = IMedicneModel.MDateStop;
            //    newMedication.Doze = IMedicneModel.MDoze;
            //    newMedication.Morning = IMedicneModel.MMorning;
            //    newMedication.afternoon = IMedicneModel.MAfternoon;
            //    newMedication.evening = IMedicneModel.MEvening;
            //    newMedication.BedTime = IMedicneModel.MBedTime;
            //    newMedication.Asneeded = IMedicneModel.MAsNeeded;
            //    newMedication.HowMuch = IMedicneModel.MHowMuch;
            //    newMedication.Reason = IMedicneModel.MReason;
            //    newMedication.AdditionalInformation = IMedicneModel.MAdditionalInformation;
            //    newMedication.Asneeded = IMedicneModel.MAsNeeded;

            //    newMedication.ModifiedBy = IMedicneModel.CHmodifiedBy;
            //    newMedication.CreatedOn = DateTime.Now;
            //    _MedORDB.IMMedications.AddObject(newMedication);

            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateTapManagement(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;
            var EditManagement = _MedORDB.IMManagements.Where(x => x.ConsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            if (EditManagement == null)
            {
                var newManagement = new DataAccess.IMManagement();

                newManagement.ManagementID = Guid.NewGuid();
                newManagement.ConsultationID = IMedicneModel.IMconsultationID;
                newManagement.PatientID = IMedicneModel.PatientID;

                newManagement.Descrition = IMedicneModel.Mdescription;
                newManagement.Title = IMedicneModel.MTitle;

                newManagement.ModifiedBy = IMedicneModel.McreatedBy;
                newManagement.CreatedOn = DateTime.Now;

                _MedORDB.IMManagements.AddObject(newManagement);
            }
            else
            {
                EditManagement.Title = IMedicneModel.MTitle;
                EditManagement.Descrition = IMedicneModel.Mdescription;
                EditManagement.ModifiedOn = DateTime.Now;

            }
            var success = _MedORDB.SaveChanges();
            return success;



        }
        public Int32 UpdateRHEntry(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;

            var EditReviewSystem = _MedORDB.IMReviewSystems.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();

            var newROS = new DataAccess.IMReviewSystem();
            newROS.PatientID = IMedicneModel.IMconsultationModelN.PatientID;
            newROS.ReviewOfSystemID = Guid.NewGuid();
            newROS.CreadtedOn = DateTime.Now;
            newROS.IMconsultationID = IMedicneModel.IMconsultationID;

            var newRSAnorexia = new DataAccess.RSAnorexia();
            var newRSFever = new DataAccess.RSFever();
            var newRSWeight = new DataAccess.RSWeight();
            var newRSFatigue = new DataAccess.RSFatigue();
           

            var success = _MedORDB.SaveChanges();

            if (EditReviewSystem == null)
            {
                _MedORDB.IMReviewSystems.AddObject(newROS);
                if (IMedicneModel.RSAnorexia != null)
                {
                    newRSAnorexia.ReviewOfSystemID = newROS.ReviewOfSystemID;
                    _MedORDB.RSAnorexias.AddObject(newRSAnorexia);
                    success = _MedORDB.SaveChanges();
                }

                if (IMedicneModel.RSFever != null)
                {
                    newRSFever.ReviewOfSystemID = newROS.ReviewOfSystemID;
                    _MedORDB.RSFevers.AddObject(newRSFever);
                    success = _MedORDB.SaveChanges();
                }

                if (IMedicneModel.RSWeight != null)
                {
                    newRSWeight.ReviewOfSystemID = newROS.ReviewOfSystemID;
                    _MedORDB.RSWeights.AddObject(newRSWeight);
                    success = _MedORDB.SaveChanges();
                }
                if (IMedicneModel.RSFatigue != null)
                {
                    newRSFatigue.ReviewOfSystemID = newROS.ReviewOfSystemID;
                    _MedORDB.RSFatigues.AddObject(newRSFatigue);
                    success = _MedORDB.SaveChanges();
                }
            }

            else
            {
               
                if (IMedicneModel.RSAnorexia != null)
                {
                    newRSAnorexia.ReviewOfSystemID = EditReviewSystem.ReviewOfSystemID;
                    newRSAnorexia.CreatedOn = DateTime.Now;
                    newRSAnorexia.AnorexiaID = Guid.NewGuid();
                    newRSAnorexia.Annorexia = IMedicneModel.RSAnorexia;
                    _MedORDB.RSAnorexias.AddObject(newRSAnorexia);
                    _MedORDB.SaveChanges();
                }
                if (IMedicneModel.RSFever != null)
                {
                    newRSFever.ReviewOfSystemID = EditReviewSystem.ReviewOfSystemID;
                    newRSFever.CreatedOn = DateTime.Now;
                    newRSFever.FeverID = Guid.NewGuid();
                    newRSFever.Fever = IMedicneModel.RSFever;
                    _MedORDB.RSFevers.AddObject(newRSFever);
                  _MedORDB.SaveChanges();
                }
                if (IMedicneModel.RSWeight != null)
                {
                    newRSWeight.ReviewOfSystemID = EditReviewSystem.ReviewOfSystemID;
                    newRSWeight.CreatedOn = DateTime.Now;
                    newRSWeight.WeightID = Guid.NewGuid();
                    newRSWeight.Weight = IMedicneModel.RSWeight;
                    _MedORDB.RSWeights.AddObject(newRSWeight);
                  _MedORDB.SaveChanges();
                }
                if (IMedicneModel.RSFatigue != null)
                {
                    newRSFatigue.ReviewOfSystemID = EditReviewSystem.ReviewOfSystemID;
                    newRSFatigue.CreatedOn = DateTime.Now;
                    newRSFatigue.FatigueID = Guid.NewGuid();
                    newRSFatigue.Fatigue = IMedicneModel.RSFatigue;
                    _MedORDB.RSFatigues.AddObject(newRSFatigue);
                     _MedORDB.SaveChanges();
                }
           

             

          

       

            }

            return success;
        }
        public DataModel.IMedicineModifiedModel GetLastModifiedConsultationByID(Guid ConsultationID)
        {
            DataModel.IMedicineModifiedModel medicalInfoAndHistory = new DataModel.IMedicineModifiedModel();

            medicalInfoAndHistory.IMconsultationModel = (from m in _MedORDB.IMconsultations
                                                          where (m.IMconsultationID == ConsultationID)
                                                     
                                                          select (new DataModel.IMConsultationModel
                                                          {
                                                              IMconsultationID = m.IMconsultationID,
                                                              note = m.Note,
                                                              title = m.Title,
                                                              PatientID = m.PatientID,
                                                              createdBy = m.CreatedBy,
                                                              createdOn = m.CreatedOn,
                                                              
                                                              modifiedBy = m.ModifiedBy,
                                                              modifiedOn = m.ModifiedOn
                                                          })).FirstOrDefault();

            medicalInfoAndHistory.IMcomplaintHistoryModel = (from XC in _MedORDB.IMcomplaintHistories
                                                              where XC.IMconsultationID == ConsultationID
                                                              orderby XC.CreatedOn descending
                                                              select new DataModel.IMcomplaintHistory { note = XC.Note, title = XC.Title, createdOn = XC.CreatedOn,modifiedOn = XC.ModifiedOn }).FirstOrDefault();

            medicalInfoAndHistory.CDiabetes = (from u in _MedORDB.IMCRVasculars
                                               where u.IMconsultationID == ConsultationID
                                              
                                               from p in _MedORDB.CDiabetes
                                               where p.CadiovascularID == u.CRVascularID
                                               orderby p.CreatedOn descending
                                               select new DataModel.CDiabetes { CreatedOn = p.CreatedOn, Description = p.Description}).FirstOrDefault(); ;

            medicalInfoAndHistory.CDyslipidemia = (from u1 in _MedORDB.IMCRVasculars
                                                   where u1.IMconsultationID == ConsultationID
                                                   orderby u1.ModifiedOn descending
                                                   from p1 in _MedORDB.CDyslipidemias
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   orderby p1.CreatedON descending
                                                   select new DataModel.CDyslipidemia { CreatedOn = p1.CreatedON, Description = p1.Description }).FirstOrDefault(); ;

            medicalInfoAndHistory.CAlcoholModel = (from u1 in _MedORDB.IMCRVasculars
                                                   where u1.IMconsultationID == ConsultationID
                                                   orderby u1.ModifiedOn descending
                                                   from p1 in _MedORDB.CAlcohols
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   orderby p1.CreatedOn descending
                                                   select new DataModel.CAlcoholModel { CreatedOn = p1.CreatedOn, Description = p1.Description }).FirstOrDefault(); ;

            medicalInfoAndHistory.CObesityModel = (from u1 in _MedORDB.IMCRVasculars
                                                   where u1.IMconsultationID == ConsultationID
                                                   from p1 in _MedORDB.CObesities
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   orderby p1.CreatedOn descending
                                                   select new DataModel.CObesityModel { CreatedOn = p1.CreatedOn, Description = p1.Description }).FirstOrDefault(); ;

            medicalInfoAndHistory.FamilialHistoryModel = (from u1 in _MedORDB.IMCRVasculars
                                                          where u1.IMconsultationID == ConsultationID
                                                          from p1 in _MedORDB.CFamilialHistories
                                                          where p1.CadiovascularID == u1.CRVascularID
                                                          orderby p1.CreatedOn descending
                                                          select new DataModel.FamilialHistoryModel { CreatedOn = p1.CreatedOn, Description = p1.Description }).FirstOrDefault(); 

            medicalInfoAndHistory.AllergiesModels = (from u in _MedORDB.IMmedicalHistories
                                                     where u.IMconsultationID == ConsultationID
                                                     from p in _MedORDB.MHAllergies
                                                     where p.MedicaHistoryID == u.MedicaHistoryID
                                                     orderby p.CreatedOn descending
                                                     select new DataModel.AllergiesModel { CreatedOn = p.CreatedOn, Description = p.Description, AllergiesID = p.MHAllergiesID, MedicalHistoryID = p.MedicaHistoryID }).FirstOrDefault(); 

            medicalInfoAndHistory.GynecologyModels = (from u1 in _MedORDB.IMmedicalHistories
                                                      where u1.IMconsultationID == ConsultationID
                                                      from p1 in _MedORDB.MHGynecologies
                                                      where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.GynecologyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, GynecologyID = p1.GynecologyID }).FirstOrDefault(); 

            medicalInfoAndHistory.FamilyModels = (from u1 in _MedORDB.IMmedicalHistories
                                                  where u1.IMconsultationID == ConsultationID
                                                  from p1 in _MedORDB.MHFamilies
                                                  where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.FamilyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, FamilyID = p1.FamilyID }).FirstOrDefault(); 

            medicalInfoAndHistory.MHMedicalModels = (from u1 in _MedORDB.IMmedicalHistories
                                                     where u1.IMconsultationID == ConsultationID
                                                     from p1 in _MedORDB.MHMedicals
                                                     where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                     orderby p1.CreatedOn descending
                                                     select new DataModel.MHMedicalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, MedicalID = p1.MedicalID }).FirstOrDefault(); 

            medicalInfoAndHistory.SurgicalModels = (from u1 in _MedORDB.IMmedicalHistories
                                                    where u1.IMconsultationID == ConsultationID
                                                    from p1 in _MedORDB.MHSurgicals
                                                    where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                    orderby p1.CreatedOn descending
                                                    select new DataModel.SurgicalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SurgicalID = p1.SurgicalID }).FirstOrDefault();


            // //ROS
            medicalInfoAndHistory.AnorexiasModel = (from u1 in _MedORDB.IMReviewSystems
                                                  where u1.IMconsultationID == ConsultationID
                                                  from p1 in _MedORDB.RSAnorexias
                                                  where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.AnorexiasModel { CreatedOn = p1.CreatedOn, Anorexia = p1.Annorexia, AnorexiaID = p1.AnorexiaID }).FirstOrDefault();
            medicalInfoAndHistory.RSFeverModel = (from u1 in _MedORDB.IMReviewSystems
                                                  where u1.IMconsultationID == ConsultationID
                                                  from p1 in _MedORDB.RSFevers 
                                                  where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.RSFeverModel { CreatedOn = p1.CreatedOn, Fever = p1.Fever, FeverID = p1.FeverID }).FirstOrDefault();
            medicalInfoAndHistory.RSFatigueModel = (from u1 in _MedORDB.IMReviewSystems
                                                    where u1.IMconsultationID == ConsultationID
                                                  from p1 in _MedORDB.RSFatigues
                                                  where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.RSFatigueModel { CreatedOn = p1.CreatedOn, Fatigue = p1.Fatigue, FatigueID = p1.FatigueID }).FirstOrDefault();
            medicalInfoAndHistory.RSWeightModel = (from u1 in _MedORDB.IMReviewSystems
                                                    where u1.IMconsultationID == ConsultationID
                                                    from p1 in _MedORDB.RSWeights
                                                    where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                    orderby p1.CreatedOn descending
                                                    select new DataModel.RSWeightModel { WCreatedOn = p1.CreatedOn, Weight = p1.Weight, WeightID = p1.WeightID }).FirstOrDefault();




            medicalInfoAndHistory.PHRespiratoryRateModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                  where u1.IMconsultation == ConsultationID
                                                  from p1 in _MedORDB.PHRespiratoryRates
                                                  where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.PHRespiratoryRateModel { CreatedOn = p1.CreatedOn, RespiratoryRate = p1.RespiratoryRate, RespiratoryRateID = p1.RespiratoryRateID }).FirstOrDefault();


            medicalInfoAndHistory.PHWeightModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                            where u1.IMconsultation == ConsultationID
                                                            from p1 in _MedORDB.PHWeights
                                                            where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                            orderby p1.createdOn descending
                                                            select new DataModel.PHWeightModel { CreatedOn = p1.createdOn, Weight = p1.Weight, WeightID = p1.WeightID }).FirstOrDefault();



            medicalInfoAndHistory.PHRespiratoryRateModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                            where u1.IMconsultation == ConsultationID
                                                            from p1 in _MedORDB.PHRespiratoryRates
                                                            where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                            orderby p1.CreatedOn descending
                                                            select new DataModel.PHRespiratoryRateModel { CreatedOn = p1.CreatedOn, RespiratoryRate = p1.RespiratoryRate, RespiratoryRateID = p1.RespiratoryRateID }).FirstOrDefault();





            medicalInfoAndHistory.RSHEENTModel = (from u1 in _MedORDB.IMReviewSystems
                                                  where u1.IMconsultationID == ConsultationID
                                                  from p1 in _MedORDB.RSHEENTs
                                                  where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.RSHEENTModel { CreatedOn = p1.CreatedOn, Description = p1.Description, HEENTID = p1.HEENTID }).FirstOrDefault();


            medicalInfoAndHistory.RSPulminaryModel = (from u1 in _MedORDB.IMReviewSystems
                                                      where u1.IMconsultationID == ConsultationID
                                                      from p1 in _MedORDB.RSPulminaries
                                                      where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.RSPulminaryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PulminaryID = p1.PulminaryID }).FirstOrDefault();

            medicalInfoAndHistory.RSCardiovascularModel = (from u1 in _MedORDB.IMReviewSystems
                                                           where u1.IMconsultationID == ConsultationID
                                                           from p1 in _MedORDB.RSCardiovasculars
                                                           where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                           orderby p1.CreatedOn descending
                                                           select new DataModel.RSCardiovascularModel { CreatedOn = p1.CreatedOn, Description = p1.Description, CardiovascularID = p1.CardiovascularID }).FirstOrDefault();
            medicalInfoAndHistory.RSDigestiveModel = (from u1 in _MedORDB.IMReviewSystems
                                                      where u1.IMconsultationID == ConsultationID
                                                      from p1 in _MedORDB.RSDigestives
                                                      where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.RSDigestiveModel { CreatedOn = p1.CreatedOn, Description = p1.Description, DigestiveID = p1.DigestiveID }).FirstOrDefault();

            medicalInfoAndHistory.RSUrogentalModel = (from u1 in _MedORDB.IMReviewSystems
                                                      where u1.IMconsultationID == ConsultationID
                                                      from p1 in _MedORDB.RSUrogentals
                                                      where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.RSUrogentalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, UrogentalID = p1.UrogentalID }).FirstOrDefault();

            medicalInfoAndHistory.RSNervousSystemModel = (from u1 in _MedORDB.IMReviewSystems
                                                          where u1.IMconsultationID == ConsultationID
                                                          from p1 in _MedORDB.RSNervousSystems
                                                          where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                          orderby p1.CreatedOn descending
                                                          select new DataModel.RSNervousSystemModel { CreatedOn = p1.CreatedOn, Description = p1.Description, NervousSystemID = p1.NervousSystemID }).FirstOrDefault();

            medicalInfoAndHistory.RSSkinModel = (from u1 in _MedORDB.IMReviewSystems
                                                 where u1.IMconsultationID == ConsultationID
                                                 from p1 in _MedORDB.RSSkins
                                                 where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                 select new DataModel.RSSkinModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SkinID = p1.SkinID }).FirstOrDefault();

            medicalInfoAndHistory.RSMusculoskeletalModel = (from u1 in _MedORDB.IMReviewSystems
                                                            where u1.IMconsultationID == ConsultationID
                                                            from p1 in _MedORDB.RSMusculoskeletals
                                                            where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                            orderby p1.CreatedOn descending
                                                            select new DataModel.RSMusculoskeletalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Musculoskeletal = p1.MusculoskeletalID }).FirstOrDefault();

            medicalInfoAndHistory.RSPsychosocialeModel = (from u1 in _MedORDB.IMReviewSystems
                                                          where u1.IMconsultationID == ConsultationID
                                                          from p1 in _MedORDB.RSPsychosociales
                                                          where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                          orderby p1.CreatedOn descending
                                                          select new DataModel.RSPsychosocialeModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PsychosocialeID = p1.PsychosocialeID }).FirstOrDefault();



            medicalInfoAndHistory.PHHEENTModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                  where u1.IMconsultation == ConsultationID
                                                  from p1 in _MedORDB.PHHEENTs
                                                  where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                  orderby p1.CreatedOn descending
                                                  select new DataModel.PHHEENTModel { CreatedOn = p1.CreatedOn, Description = p1.Description, HEENTID = p1.HEENTID }).FirstOrDefault(); ;

            medicalInfoAndHistory.PHPulminaryModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                      where u1.IMconsultation == ConsultationID
                                                      from p1 in _MedORDB.PHPulminaries
                                                      where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.PHPulminaryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PulminaryID = p1.PulminaryID }).FirstOrDefault(); ;
            medicalInfoAndHistory.PHCardiovascularModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                           where u1.IMconsultation == ConsultationID
                                                           from p1 in _MedORDB.PHCardiovasculars
                                                           where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                           orderby p1.CreatedOn descending
                                                           select new DataModel.PHCardiovascularModelcs { CreatedOn = p1.CreatedOn, Description = p1.Description, CardiovascularID = p1.CardiovascularID }).FirstOrDefault(); ;
            medicalInfoAndHistory.PHDigestiveModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                      where u1.IMconsultation == ConsultationID
                                                      from p1 in _MedORDB.PHDigestives
                                                      where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.PHDigestiveModel { CreatedOn = p1.CreatedOn, Description = p1.Description, DigestiveID = p1.DigestiveID }).FirstOrDefault(); ;
            medicalInfoAndHistory.PHUrogentalModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                      where u1.IMconsultation == ConsultationID
                                                      from p1 in _MedORDB.PHUrogentals
                                                      where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                      orderby p1.CreatedOn descending
                                                      select new DataModel.PHUrogentalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, UrogentalID = p1.UrogentalID }).FirstOrDefault(); ;
            medicalInfoAndHistory.PHNervousSystemModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                          where u1.IMconsultation == ConsultationID
                                                          from p1 in _MedORDB.PHNervousSystems
                                                          where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                          orderby p1.CreatedOn descending
                                                          select new DataModel.PHNervousSystemModel { CreatedOn = p1.CreatedOn, Description = p1.Description, NervousSystemID = p1.NervousSystemID }).FirstOrDefault(); ;

            medicalInfoAndHistory.PHSkinModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                 where u1.IMconsultation == ConsultationID
                                                 from p1 in _MedORDB.PHSkins
                                                 where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                 orderby p1.CreatedOn descending
                                                 select new DataModel.PHRSSkinModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SkinID = p1.SkinID }).FirstOrDefault(); ;
            medicalInfoAndHistory.PHMusculoskeletalModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                            where u1.IMconsultation == ConsultationID
                                                            from p1 in _MedORDB.PHMusculoskeletals
                                                            where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                            orderby p1.CreatedOn descending
                                                            select new DataModel.PHMusculoskeletalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Musculoskeletal = p1.MusculoskeletalID }).FirstOrDefault(); ;

            medicalInfoAndHistory.PHPsychosocialeModel = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                          where u1.IMconsultation == ConsultationID
                                                          from p1 in _MedORDB.PHPsychosociales
                                                          where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                          orderby p1.CreatedOn descending
                                                          select new DataModel.PHPsychosocialeModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PsychosocialeID = p1.PsychosocialeID }).FirstOrDefault(); ;



            medicalInfoAndHistory.IMMedicationModel = (from t in _MedORDB.IMMedications
                                                       where t.IMconsultationID == ConsultationID
                                                       orderby t.CreatedOn descending
                                                       select new DataModel.IMMedicationModel
                                                       {
                                                           MedicationName = t.MedicationName,
                                                           DateStart = t.DateStart,
                                                           DateStop = t.DateStart,
                                                           Doze = t.Doze,
                                                           Morning = t.Morning,
                                                           Afternoon = t.afternoon,
                                                           Evening = t.evening,
                                                           BedTime = t.BedTime,
                                                           AsNeeded = t.Asneeded,
                                                           HowMuch = t.HowMuch,
                                                           Reason = t.Reason,
                                                           AdditionalInformation = t.AdditionalInformation
                                                       }).FirstOrDefault();

            medicalInfoAndHistory.ICD10PatientModel = (from m in _MedORDB.ICD10_Patient
                                                       where (m.IMconsultationID == ConsultationID)
                                                       orderby m.CreatedOn descending
                                                       select (new DataModel.ICD10PatientModel
                                                       { CodeDescriptionICD10 = m.CodeDescriptionICD10, Note = m.Note, CreatedOn = m.CreatedOn, IMconsultationID = m.IMconsultationID, CreatedBy = m.CreatedBy, ICD10PatientID = m.ICD10PatientID })).FirstOrDefault();



            medicalInfoAndHistory.IMFileModel = (from t in _MedORDB.IMFiles
                                                 where t.Consultation == ConsultationID
                                                 orderby t.ModifiedOn descending
                                                 select new DataModel.IMFileModel
                                                 {
                                                     createdOn = t.CreatedOn,
                                                     PatientID = t.PatientID,
                                                     ConsultationID = t.Consultation,
                                                     FileName = t.FileName,
                                                     FileID = t.FileID
                                                 }).FirstOrDefault();


            medicalInfoAndHistory.IMHospitalSummaryModel = (from t in _MedORDB.IMHopitalizations
                                                            where t.ConsultationID == ConsultationID
                                                            orderby t.ModifiedOn descending
                                                            select new DataModel.IMHospitalSummaryModel
                                                            {
                                                                AdmissionName = t.HospitalName
                                                                ,
                                                                AdmissionDate = t.AdmissionDate
                                                                ,
                                                                Departure = t.departuredate
                                                                ,
                                                                Description = t.Description
                                                                 ,
                                                                LengthStay = t.lengthStay
                                                                ,
                                                                PatientID = t.PatientID
                                                                ,
                                                                ConsultationID = t.ConsultationID
                                                                ,
                                                                HospitalSummaryID = t.HospitalizationID
                                                                ,
                                                                FileName = t.FileName

                                                            }).FirstOrDefault();


            medicalInfoAndHistory.IMManagement = (from XC in _MedORDB.IMManagements
                                                  where XC.ConsultationID == ConsultationID
                                                  orderby XC.ModifiedOn descending
                                                  select new DataModel.IMManagement { Description = XC.Descrition, createdOn = XC.CreatedOn }).FirstOrDefault();


            return medicalInfoAndHistory;
        }
        public DataModel.IMedicneModel GetConsultationByID(Guid ConsultationID)
        {
            DataModel.IMedicneModel medicalInfoAndHistory = new DataModel.IMedicneModel();


            medicalInfoAndHistory.IMconsultationModels = (from m in _MedORDB.IMconsultations
                                                          where (m.IMconsultationID == ConsultationID)
                                                          orderby m.CreatedOn descending
                                                          select (new DataModel.IMConsultationModel
                                                          {
                                                              IMconsultationID = m.IMconsultationID,
                                                              note = m.Note,
                                                              title = m.Title,
                                                              PatientID = m.PatientID,
                                                              CreatedByName = (from u in _MedORDB.Users where u.UserID == m.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                              createdBy = m.CreatedBy,
                                                              createdOn = m.CreatedOn,
                                                              modifiedBy = m.ModifiedBy,
                                                              modifiedByName = (from u in _MedORDB.Users where u.UserID == m.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                              modifiedOn = m.ModifiedOn,
                                                              Fees = m.Fees
                                                              ,
                                                              Curency = m.Currency
                                                              ,
                                                              CheckBox = m.Checkbox

                                                          }));



            medicalInfoAndHistory.IMconsultationModelN = (from m in _MedORDB.IMconsultations
                                                          where (m.IMconsultationID == ConsultationID)
                                                          orderby m.CreatedOn descending
                                                          select (new DataModel.IMConsultationModel
                                                          {
                                                              IMconsultationID = m.IMconsultationID,
                                                              note = m.Note,
                                                              title = m.Title,
                                                         
                                                              PatientID = m.PatientID,
                                                              FullName= (from u in _MedORDB.IMconsultations where u.IMconsultationID == ConsultationID
                                                                         from u1 in _MedORDB.Patients
                                                                         where u.PatientID == u1.PatientID select u1.FirstName + " " + u1.LastName).FirstOrDefault(),
                                                              createdBy = m.CreatedBy,
                                                              createdOn = m.CreatedOn,
                                                              modifiedBy = m.ModifiedBy,
                                                              modifiedOn = m.ModifiedOn
                                                          })).OrderByDescending(m => m.createdOn).FirstOrDefault();

            medicalInfoAndHistory.IMcomplaintHistoryModelN = (from XC in _MedORDB.IMcomplaintHistories
                                                              where XC.IMconsultationID == ConsultationID
                                                              orderby XC.CreatedOn descending
                                                              select new DataModel.IMcomplaintHistory { note = XC.Note, title = XC.Title, createdOn = XC.CreatedOn }).FirstOrDefault();
      
   // CheckBox For Cardiovascular Tab
            medicalInfoAndHistory.CRVDiabetes = (from u1 in _MedORDB.IMCRVasculars
                                                 where u1.IMconsultationID == ConsultationID
                                                 from u2 in _MedORDB.CDiabetes
                                                 where u2.CDiabetesCheck == true & u2.CadiovascularID == u1.CRVascularID
                                                 select u2.CDiabetesCheck).FirstOrDefault();
            medicalInfoAndHistory.CRVObesity = (  from u1 in _MedORDB.IMCRVasculars
                                                 where u1.IMconsultationID == ConsultationID
                                                  from u2 in _MedORDB.CObesities
                                                  where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                 select u2.CheckBox).FirstOrDefault();
                           //medicalInfoAndHistory.CRVObesity = (from u in _MedORDB.IMCRVasculars where u.CRVobesity == true & u.IMconsultationID == ConsultationID select u.CRVobesity).FirstOrDefault();
            medicalInfoAndHistory.CRVDyslipidemia = (from u1 in _MedORDB.IMCRVasculars
                                                where u1.IMconsultationID == ConsultationID
                                                from u2 in _MedORDB.CDyslipidemias
                                                     where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.CRVFamilialHistory = (from u1 in _MedORDB.IMCRVasculars
                                                     where u1.IMconsultationID == ConsultationID
                                                     from u2 in _MedORDB.CFamilialHistories
                                                     where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                     select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.CRVAlcohol = (from u1 in _MedORDB.IMCRVasculars
                                                        where u1.IMconsultationID == ConsultationID
                                                        from u2 in _MedORDB.CAlcohols
                                                        where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                        select u2.CheckBox).FirstOrDefault();
         
   // Description For Cardiovascular Tab
            medicalInfoAndHistory.CDiabetes = (from u in _MedORDB.IMCRVasculars
                                               where u.IMconsultationID == ConsultationID
                                               orderby u.CreatedOn descending
                                               from p in _MedORDB.CDiabetes
                                               where p.CadiovascularID == u.CRVascularID
                                               select new DataModel.CDiabetes { CreatedOn = p.CreatedOn, Description = p.Description, DiabeteID = p.DiabetesID });
            medicalInfoAndHistory.CDyslipidemia = (from u1 in _MedORDB.IMCRVasculars
                                                   where u1.IMconsultationID == ConsultationID
                                                   orderby u1.CreatedOn descending
                                                   from p1 in _MedORDB.CDyslipidemias
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   select new DataModel.CDyslipidemia { CreatedOn = p1.CreatedON, Description = p1.Description, DyslipidemiaID = p1.CDyslipidemiaID });
            medicalInfoAndHistory.CAlcoholModel = (from u1 in _MedORDB.IMCRVasculars
                                                   where u1.IMconsultationID == ConsultationID
                                                   orderby u1.CreatedOn descending
                                                   from p1 in _MedORDB.CAlcohols
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   select new DataModel.CAlcoholModel { CreatedOn = p1.CreatedOn, Description = p1.Description, CAlcoholID = p1.AlcoholID });
            medicalInfoAndHistory.CObesityModel = (from u1 in _MedORDB.IMCRVasculars
                                                   where u1.IMconsultationID == ConsultationID
                                                   orderby u1.CreatedOn descending
                                                   from p1 in _MedORDB.CObesities
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   select new DataModel.CObesityModel { CreatedOn = p1.CreatedOn, Description = p1.Description,ObesityID=p1.ObesityID });

            medicalInfoAndHistory.FamilialHistoryModel = (from u1 in _MedORDB.IMCRVasculars
                                                          where u1.IMconsultationID == ConsultationID
                                                          orderby u1.CreatedOn descending
                                                          from p1 in _MedORDB.CFamilialHistories
                                                          where p1.CadiovascularID == u1.CRVascularID
                                                          select new DataModel.FamilialHistoryModel { CreatedOn = p1.CreatedOn, Description = p1.Description,FamilialHistoryID=p1.FamilialHistoryID });

           
            medicalInfoAndHistory.AllergiesModels = (from u in _MedORDB.IMmedicalHistories
                                                     where u.IMconsultationID == ConsultationID
                                                     orderby u.CreatedOn descending
                                                     from p in _MedORDB.MHAllergies
                                                     where p.MedicaHistoryID == u.MedicaHistoryID
                                                     select new DataModel.AllergiesModel { CreatedOn = p.CreatedOn, Description = p.Description,AllergiesID=p.MHAllergiesID });

            medicalInfoAndHistory.SurgicalModels = (from u in _MedORDB.IMmedicalHistories
                                                  where u.IMconsultationID == ConsultationID
                                                  orderby u.CreatedOn descending
                                                  from p in _MedORDB.MHSurgicals
                                                  where p.MedicaHistoryID == u.MedicaHistoryID
                                                  select new DataModel.SurgicalModel { CreatedOn = p.CreatedOn, Description = p.Description,SurgicalID=p.SurgicalID });
            medicalInfoAndHistory.GynecologyModels = (from u in _MedORDB.IMmedicalHistories
                                                      where u.IMconsultationID == ConsultationID
                                                      orderby u.CreatedOn descending
                                                      from p in _MedORDB.MHGynecologies
                                                      where p.MedicaHistoryID == u.MedicaHistoryID
                                                      select new DataModel.GynecologyModel { CreatedOn = p.CreatedOn, Description = p.Description,GynecologyID=p.GynecologyID });
            medicalInfoAndHistory.FamilyModels = (from u in _MedORDB.IMmedicalHistories
                                                  where u.IMconsultationID == ConsultationID
                                                  orderby u.CreatedOn descending
                                                  from p in _MedORDB.MHFamilies
                                                  where p.MedicaHistoryID == u.MedicaHistoryID
                                                  select new DataModel.FamilyModel { CreatedOn = p.CreatedOn, Description = p.Description,FamilyID=p.FamilyID });
            medicalInfoAndHistory.MHMedicalModels = (from u in _MedORDB.IMmedicalHistories
                                                     where u.IMconsultationID == ConsultationID
                                                     orderby u.CreatedOn descending
                                                     from p in _MedORDB.MHMedicals
                                                     where p.MedicaHistoryID == u.MedicaHistoryID
                                                     select new DataModel.MHMedicalModel { CreatedOn = p.CreatedOn, Description = p.Description,MedicalID=p.MedicalID });

            // CheckBox For Review of system Tab

            medicalInfoAndHistory.RSHEENT = (
                                             from u1 in _MedORDB.IMReviewSystems
                                             where u1.IMconsultationID == ConsultationID
                                             from u2 in _MedORDB.RSHEENTs
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.RSPulmonary = (
                                                 from u1 in _MedORDB.IMReviewSystems
                                                 where u1.IMconsultationID == ConsultationID
                                                 from u2 in _MedORDB.RSPulminaries
                                                 where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                 select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSCardiovascular = (
                                                      from u1 in _MedORDB.IMReviewSystems
                                                      where u1.IMconsultationID == ConsultationID
                                                      from u2 in _MedORDB.RSCardiovasculars
                                                      where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                      select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSDigestive = (
                                                 from u1 in _MedORDB.IMReviewSystems
                                                 where u1.IMconsultationID == ConsultationID
                                                 from u2 in _MedORDB.RSDigestives
                                                 where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                 select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSUrogenital = (
                                                  from u1 in _MedORDB.IMReviewSystems
                                                  where u1.IMconsultationID == ConsultationID
                                                  from u2 in _MedORDB.RSUrogentals
                                                  where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                  select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSNervouSystem = (
                                                    from u1 in _MedORDB.IMReviewSystems
                                                    where u1.IMconsultationID == ConsultationID
                                                    from u2 in _MedORDB.RSNervousSystems
                                                    where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                    select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.SRSkin = (
                                            from u1 in _MedORDB.IMReviewSystems
                                            where u1.IMconsultationID == ConsultationID
                                            from u2 in _MedORDB.RSSkins
                                            where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                            select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.RSMusculoskeletal = (
                                                       from u1 in _MedORDB.IMReviewSystems
                                                       where u1.IMconsultationID == ConsultationID
                                                       from u2 in _MedORDB.RSMusculoskeletals
                                                       where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                       select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.RSPsychosocial = (
                                                    from u1 in _MedORDB.IMReviewSystems
                                                    where u1.IMconsultationID == ConsultationID
                                                    from u2 in _MedORDB.RSPsychosociales
                                                    where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                    select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.RSEntryModels = from p in _MedORDB.IMconsultations
                                                  where p.IMconsultationID == ConsultationID
                                                  join p1 in _MedORDB.IMReviewSystems on p.IMconsultationID equals p1.IMconsultationID
                                                  join p2 in _MedORDB.RSAnorexias on p1.ReviewOfSystemID equals p2.ReviewOfSystemID
                                                  join p3 in _MedORDB.RSFevers on p1.ReviewOfSystemID equals p3.ReviewOfSystemID
                                                  join p4 in _MedORDB.RSWeights on p1.ReviewOfSystemID equals p4.ReviewOfSystemID
                                                  join p5 in _MedORDB.RSFatigues on p1.ReviewOfSystemID equals p5.ReviewOfSystemID
                                                  select new DataModel.RSEntryModel {

                                                      Anorexia = p2.Annorexia,
                                                      ACreatedOn = p2.CreatedOn,
                                                      AnorexiaID= p2.AnorexiaID,
                                                      

                                                      Fever = p3.Fever,
                                                      FCreatedOn = p3.CreatedOn,
                                                      FeverID= p3.FeverID,

                                                      Weight = p4.Weight,
                                                      WCreatedOn = p4.CreatedOn,
                                                      WeightID=p4.WeightID,

                                                      Fatigue = p5.Fatigue,
                                                      FaCreatedOn = p5.CreatedOn,
                                                      FatigueID= p5.FatigueID
                                                      
                                                  };

            medicalInfoAndHistory.AnorexiasModels = (
                                                     from u1 in _MedORDB.IMReviewSystems
                                                     where ConsultationID == u1.IMconsultationID
                                                     from u2 in _MedORDB.RSAnorexias
                                                     where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                     select new DataModel.AnorexiasModel
                                                     {
                                                         AnorexiaID = u2.AnorexiaID,
                                                         Anorexia = u2.Annorexia,
                                                         CreatedOn = u2.CreatedOn
                                                     });


            medicalInfoAndHistory.RSFeverModels = (
                                                   from u1 in _MedORDB.IMReviewSystems
                                                   where ConsultationID == u1.IMconsultationID
                                                   from u2 in _MedORDB.RSFevers
                                                   where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                   select new DataModel.RSFeverModel
                                                   {
                                                       FeverID = u2.FeverID,
                                                       Fever = u2.Fever,

                                                       CreatedOn = u2.CreatedOn
                                                   });

            medicalInfoAndHistory.RSWeightModels = (from u1 in _MedORDB.IMReviewSystems
                                                    where ConsultationID == u1.IMconsultationID
                                                    from u2 in _MedORDB.RSWeights
                                                    where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                    select new DataModel.RSWeightModel
                                                    {

                                                        WeightID = u2.WeightID,
                                                        Weight = u2.Weight,

                                                        WCreatedOn = u2.CreatedOn
                                                    });
            medicalInfoAndHistory.RSFatigueModels = (
                                                     from u1 in _MedORDB.IMReviewSystems
                                                     where ConsultationID == u1.IMconsultationID
                                                     from u2 in _MedORDB.RSFatigues
                                                     where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                     select new DataModel.RSFatigueModel
                                                     {

                                                         FatigueID = u2.FatigueID,
                                                         Fatigue = u2.Fatigue,

                                                         CreatedOn = u2.CreatedOn
                                                     });

            // Description for Review of sysytem

            medicalInfoAndHistory.RSHEENTModels = (from u1 in _MedORDB.IMReviewSystems
                                                   where u1.IMconsultationID == ConsultationID
                                                   from p1 in _MedORDB.RSHEENTs
                                                   where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                   select new DataModel.RSHEENTModel { CreatedOn = p1.CreatedOn, Description = p1.Description, HEENTID = p1.HEENTID });

            medicalInfoAndHistory.RSPulminaryModels = (from u1 in _MedORDB.IMReviewSystems
                                                       where u1.IMconsultationID == ConsultationID
                                                       from p1 in _MedORDB.RSPulminaries
                                                       where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                       select new DataModel.RSPulminaryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PulminaryID = p1.PulminaryID });
            medicalInfoAndHistory.RSCardiovascularModels = (from u1 in _MedORDB.IMReviewSystems
                                                            where u1.IMconsultationID == ConsultationID
                                                            from p1 in _MedORDB.RSCardiovasculars
                                                            where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                            select new DataModel.RSCardiovascularModel { CreatedOn = p1.CreatedOn, Description = p1.Description, CardiovascularID = p1.CardiovascularID });
            medicalInfoAndHistory.RSDigestiveModels = (from u1 in _MedORDB.IMReviewSystems
                                                       where u1.IMconsultationID == ConsultationID
                                                       from p1 in _MedORDB.RSDigestives
                                                       where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                       select new DataModel.RSDigestiveModel { CreatedOn = p1.CreatedOn, Description = p1.Description, DigestiveID = p1.DigestiveID });
            medicalInfoAndHistory.RSUrogentalModels = (from u1 in _MedORDB.IMReviewSystems
                                                       where u1.IMconsultationID == ConsultationID
                                                       from p1 in _MedORDB.RSUrogentals
                                                       where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                       select new DataModel.RSUrogentalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, UrogentalID = p1.UrogentalID });
            medicalInfoAndHistory.RSNervousSystemModels = (from u1 in _MedORDB.IMReviewSystems
                                                           where u1.IMconsultationID == ConsultationID
                                                           from p1 in _MedORDB.RSNervousSystems
                                                           where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                           select new DataModel.RSNervousSystemModel { CreatedOn = p1.CreatedOn, Description = p1.Description, NervousSystemID = p1.NervousSystemID });

            medicalInfoAndHistory.RSSkinModels = (from u1 in _MedORDB.IMReviewSystems
                                                  where u1.IMconsultationID == ConsultationID
                                                  from p1 in _MedORDB.RSSkins
                                                  where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                  select new DataModel.RSSkinModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SkinID = p1.SkinID });
            medicalInfoAndHistory.RSMusculoskeletalModels = (from u1 in _MedORDB.IMReviewSystems
                                                            where u1.IMconsultationID == ConsultationID
                                                            from p1 in _MedORDB.RSMusculoskeletals
                                                            where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                            select new DataModel.RSMusculoskeletalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Musculoskeletal = p1.MusculoskeletalID });

            medicalInfoAndHistory.RSPsychosocialeModels = (from u1 in _MedORDB.IMReviewSystems
                                                          where u1.IMconsultationID == ConsultationID
                                                          from p1 in _MedORDB.RSPsychosociales
                                                          where p1.ReviewOfSystemID == u1.ReviewOfSystemID
                                                          select new DataModel.RSPsychosocialeModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PsychosocialeID = p1.PsychosocialeID });


            medicalInfoAndHistory.IMPhysicalHistoryModels = (from t in _MedORDB.IMPhysicalExamHistories
                                                             where t.IMconsultation == ConsultationID
                                                             select new DataModel.IMPhysicalHistoryModel
                                                             {
                                                                 RespiratoryRate = t.RespiratoryRate,
                                                                 BloodPressure = t.BloodPressure,
                                                                 Weight = t.Weight
                                                             }).FirstOrDefault();





            medicalInfoAndHistory.PHEntryModels = from p in _MedORDB.IMconsultations
                                                  where p.IMconsultationID == ConsultationID
                                                  join p1 in _MedORDB.IMPhysicalExamHistories on p.IMconsultationID equals p1.IMconsultation
                                                  join p2 in _MedORDB.PHRespiratoryRates on p1.PhysicalExamHistoryID equals p2.PhysicalExamHistoryID
                                                  join p3 in _MedORDB.PHWeights on p1.PhysicalExamHistoryID equals p3.PhysicalExamHistoryID
                                                  join p4 in _MedORDB.PHBloodPressures on p1.PhysicalExamHistoryID equals p4.PhysicalExamHistoryID
                                                
                                                  select new DataModel.PHEntryModel
                                                  {
                                                      RespiratoryRate = p2.RespiratoryRate,
                                                      RCreatedOn = p2.CreatedOn,
                                                      RespiratoryRateID=p2.RespiratoryRateID,
                                                      Weight = p3.Weight,
                                                      WCreatedOn = p3.createdOn,
                                                      WeightID= p3.WeightID,
                                                      BloodPressure = p4.BloodPressure,
                                                      BCreatedOn = p4.CreatedOn,
                                                      BloodPressureID=p4.BloodPressureID
                                                  };

            medicalInfoAndHistory.BloodPressureModels = (
                                                         from u1 in _MedORDB.IMPhysicalExamHistories
                                                         where ConsultationID == u1.IMconsultation
                                                         from u2 in _MedORDB.PHBloodPressures
                                                         where u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                         select new DataModel.BloodPressureModel
                                                         {

                                                             BloodPressureID = u2.BloodPressureID,
                                                             BloodPressure = u2.BloodPressure,
                                                             CreatedOn = u2.CreatedOn

                                                         });
            medicalInfoAndHistory.PHWeightModels = (
                                                    from u1 in _MedORDB.IMPhysicalExamHistories
                                                    where ConsultationID == u1.IMconsultation
                                                    from u2 in _MedORDB.PHWeights
                                                    where u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                    select new DataModel.PHWeightModel
                                                    {
                                                        WeightID = u2.WeightID,
                                                        Weight = u2.Weight,
                                                        CreatedOn = u2.createdOn
                                                    });

            medicalInfoAndHistory.PHRespiratoryRateModels = (
                                                             from u1 in _MedORDB.IMPhysicalExamHistories
                                                             where ConsultationID == u1.IMconsultation
                                                             from u2 in _MedORDB.PHRespiratoryRates
                                                             where u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                             select new DataModel.PHRespiratoryRateModel
                                                             {

                                                                 RespiratoryRateID = u2.RespiratoryRateID,
                                                                 RespiratoryRate = u2.RespiratoryRate,

                                                                 CreatedOn = u2.CreatedOn
                                                             });


            // CheckBox For Physical history Tab

            medicalInfoAndHistory.PHHEENT = (
                                             from u1 in _MedORDB.IMPhysicalExamHistories
                                             where u1.IMconsultation == ConsultationID
                                             from u2 in _MedORDB.PHHEENTs
                                             where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                             select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHPulmonary = ( 
                                                 from u1 in _MedORDB.IMPhysicalExamHistories
                                                 where u1.IMconsultation == ConsultationID
                                                 from u2 in _MedORDB.PHPulminaries
                                                 where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                 select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHCardiovascular = (
                                                      from u1 in _MedORDB.IMPhysicalExamHistories
                                                      where u1.IMconsultation == ConsultationID
                                                      from u2 in _MedORDB.PHCardiovasculars
                                                      where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                      select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHDigestive = (
                                                 from u1 in _MedORDB.IMPhysicalExamHistories
                                                 where u1.IMconsultation == ConsultationID
                                                 from u2 in _MedORDB.PHDigestives
                                                 where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                 select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHUrogenital = (
                                                  from u1 in _MedORDB.IMPhysicalExamHistories
                                                  where u1.IMconsultation == ConsultationID
                                                  from u2 in _MedORDB.PHUrogentals
                                                  where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                  select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHNervouSystem = (
                                                    from u1 in _MedORDB.IMPhysicalExamHistories
                                                    where u1.IMconsultation == ConsultationID
                                                    from u2 in _MedORDB.PHNervousSystems
                                                    where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                    select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHSkin = (
                                            from u1 in _MedORDB.IMPhysicalExamHistories
                                            where u1.IMconsultation == ConsultationID
                                            from u2 in _MedORDB.PHSkins
                                            where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                            select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHMusculoskeletal = (
                                                       from u1 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultation == ConsultationID
                                                       from u2 in _MedORDB.PHMusculoskeletals
                                                       where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                       select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.PHPsychosocial = (
                                                    from u1 in _MedORDB.IMPhysicalExamHistories
                                                    where u1.IMconsultation == ConsultationID
                                                    from u2 in _MedORDB.PHPsychosociales
                                                    where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                    select u2.CheckBox).FirstOrDefault();


            // Description for physical history 



            medicalInfoAndHistory.PHHEENTModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                   where u1.IMconsultation == ConsultationID
                                                   from p1 in _MedORDB.PHHEENTs
                                                   where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                   select new DataModel.PHHEENTModel { CreatedOn = p1.CreatedOn, Description = p1.Description, HEENTID = p1.HEENTID });

            medicalInfoAndHistory.PHPulminaryModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultation == ConsultationID
                                                       from p1 in _MedORDB.PHPulminaries
                                                       where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                       select new DataModel.PHPulminaryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PulminaryID = p1.PulminaryID });
            medicalInfoAndHistory.PHCardiovascularModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                            where u1.IMconsultation == ConsultationID
                                                            from p1 in _MedORDB.PHCardiovasculars
                                                            where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                            select new DataModel.PHCardiovascularModelcs { CreatedOn = p1.CreatedOn, Description = p1.Description, CardiovascularID = p1.CardiovascularID });
            medicalInfoAndHistory.PHDigestiveModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultation == ConsultationID
                                                       from p1 in _MedORDB.PHDigestives
                                                       where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                       select new DataModel.PHDigestiveModel { CreatedOn = p1.CreatedOn, Description = p1.Description, DigestiveID = p1.DigestiveID });
            medicalInfoAndHistory.PHUrogentalModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultation == ConsultationID
                                                       from p1 in _MedORDB.PHUrogentals
                                                       where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                       select new DataModel.PHUrogentalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, UrogentalID = p1.UrogentalID });
            medicalInfoAndHistory.PHNervousSystemModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                           where u1.IMconsultation == ConsultationID
                                                           from p1 in _MedORDB.PHNervousSystems
                                                           where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                           select new DataModel.PHNervousSystemModel { CreatedOn = p1.CreatedOn, Description = p1.Description, NervousSystemID = p1.NervousSystemID });

            medicalInfoAndHistory.PHSkinModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                  where u1.IMconsultation == ConsultationID
                                                  from p1 in _MedORDB.PHSkins
                                                  where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                  select new DataModel.PHRSSkinModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SkinID = p1.SkinID });
            medicalInfoAndHistory.PHMusculoskeletalModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                            where u1.IMconsultation == ConsultationID
                                                            from p1 in _MedORDB.PHMusculoskeletals
                                                            where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                            select new DataModel.PHMusculoskeletalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Musculoskeletal = p1.MusculoskeletalID });

            medicalInfoAndHistory.PHPsychosocialeModels = (from u1 in _MedORDB.IMPhysicalExamHistories
                                                          where u1.IMconsultation == ConsultationID
                                                          from p1 in _MedORDB.PHPsychosociales
                                                          where p1.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                          select new DataModel.PHPsychosocialeModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PsychosocialeID = p1.PsychosocialeID });


            // Checkbox button for tap Biological Parameter

            medicalInfoAndHistory.BPEchography = (from u1 in _MedORDB.IMBiologicals
                                                  where ConsultationID == u1.ConsultaionID
                                                  from u2 in _MedORDB.BPEchographies
                                                  where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                                  select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BP_CRMoldels = ( from u1 in _MedORDB.IMBiologicals
                                                  where ConsultationID == u1.ConsultaionID
                                                  from u2 in _MedORDB.BPConventionalRadiographies
                                                  where u2.BiologicalID == u1.BiologicalID
                                                  from u3 in _MedORDB.BP_CR_Files
                                                  where u3.ConventionalRadiographyID == u2.ConventionalRadiographyID
                                                  select new DataModel.BP_CRMoldel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File }).ToList();


            medicalInfoAndHistory.BPMRI = ( from u1 in _MedORDB.IMBiologicals
                                           where ConsultationID == u1.ConsultaionID
                                           from u2 in _MedORDB.BPMRIs
                                           where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                           select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.BPOther = (
                                             from u1 in _MedORDB.IMBiologicals
                                             where ConsultationID == u1.ConsultaionID
                                             from u2 in _MedORDB.BPOthers
                                             where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.BPPetScan = (
                                               from u1 in _MedORDB.IMBiologicals
                                               where ConsultationID == u1.ConsultaionID
                                               from u2 in _MedORDB.BPPetScans
                                               where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                               select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.BPscintigraphy = (
                                                    from u1 in _MedORDB.IMBiologicals
                                                    where ConsultationID == u1.ConsultaionID
                                                    from u2 in _MedORDB.BPScintigraphies
                                                    where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                                    select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.BPScan = (
                                            from u1 in _MedORDB.IMBiologicals
                                            where ConsultationID == u1.ConsultaionID
                                            from u2 in _MedORDB.BPScans
                                            where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                            select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.BPconventionalRadiography = (
                                                               from u1 in _MedORDB.IMBiologicals
                                                               where ConsultationID == u1.ConsultaionID
                                                               from u2 in _MedORDB.BPConventionalRadiographies
                                                               where u2.CheckBox == true & u2.BiologicalID == u1.BiologicalID
                                                               select u2.CheckBox).FirstOrDefault();

            // New Descrpition for tap Biological Parameter
            medicalInfoAndHistory.Conventional_RadiographyModels = (from u1 in _MedORDB.IMBiologicals
                                                                    where u1.ConsultaionID == ConsultationID
                                                                    from p1 in _MedORDB.BPConventionalRadiographies
                                                                    where p1.BiologicalID == u1.BiologicalID
                                                                    select new DataModel.Conventional_RadiographyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Conventional_RadiographyID = p1.ConventionalRadiographyID });


            //files
            medicalInfoAndHistory.BP_CRMoldels = (from u1 in _MedORDB.IMBiologicals
                                                  where ConsultationID == u1.ConsultaionID

                                                  from u2 in _MedORDB.BPConventionalRadiographies
                                                  where u2.BiologicalID == u1.BiologicalID
                                                  from u3 in _MedORDB.BP_CR_Files
                                                  where u3.ConventionalRadiographyID == u2.ConventionalRadiographyID
                                                  select new DataModel.BP_CRMoldel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.ConventionalRadiographyID }).ToList();





            medicalInfoAndHistory.BPScintigraphyModels = (from u1 in _MedORDB.IMBiologicals
                                                          where u1.ConsultaionID == ConsultationID
                                                          from p1 in _MedORDB.BPScintigraphies
                                                          where p1.BiologicalID == u1.BiologicalID
                                                          select new DataModel.BPScintigraphyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, ScintigraphyID = p1.ScintigraphyID });
            //Files
            medicalInfoAndHistory.BP_scintigraphyModels = (from u1 in _MedORDB.IMBiologicals
                                                           where ConsultationID == u1.ConsultaionID

                                                           from u2 in _MedORDB.BPScintigraphies
                                                           where u2.BiologicalID == u1.BiologicalID
                                                           from u3 in _MedORDB.BP_scintigraphy_Files
                                                           where u3.scintigraphyID == u2.ScintigraphyID
                                                           select new DataModel.BP_scintigraphyModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.scintigraphyID }).ToList();

            medicalInfoAndHistory.BPScanModels = (from u1 in _MedORDB.IMBiologicals
                                                  where u1.ConsultaionID == ConsultationID
                                                  from p1 in _MedORDB.BPScans
                                                  where p1.BiologicalID == u1.BiologicalID
                                                  select new DataModel.BPScanModel { CreatedOn = p1.CreatedOn, Description = p1.Description, ScanID = p1.ScanID });
            //Files
            medicalInfoAndHistory.BP_ScanModels = (from u1 in _MedORDB.IMBiologicals
                                                   where ConsultationID == u1.ConsultaionID
                                                   from u2 in _MedORDB.BPScans
                                                   where u2.BiologicalID == u1.BiologicalID
                                                   from u3 in _MedORDB.BP_Scan_Files
                                                   where u3.ScanID == u2.ScanID
                                                   select new DataModel.BP_ScanModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.ScanID }).ToList();




            medicalInfoAndHistory.BPEchographyModels = (from u1 in _MedORDB.IMBiologicals
                                                        where u1.ConsultaionID == ConsultationID
                                                        from p1 in _MedORDB.BPEchographies
                                                        where p1.BiologicalID == u1.BiologicalID
                                                        select new DataModel.EchographyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, EchographyID = p1.EchographyID });
            //files
            medicalInfoAndHistory.BP_EchographyModels = (from u1 in _MedORDB.IMBiologicals
                                                         where ConsultationID == u1.ConsultaionID

                                                         from u2 in _MedORDB.BPEchographies
                                                         where u2.BiologicalID == u1.BiologicalID
                                                         from u3 in _MedORDB.BP_Echography_Files
                                                         where u3.Echography == u2.EchographyID
                                                         select new DataModel.BP_EchographyModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.Echography }).ToList();

            medicalInfoAndHistory.BPOtherModels = (from u1 in _MedORDB.IMBiologicals
                                                   where u1.ConsultaionID == ConsultationID
                                                   from p1 in _MedORDB.BPOthers
                                                   where p1.BiologicalID == u1.BiologicalID
                                                   select new DataModel.BPOtherModel { CreatedOn = p1.CreatedOn, Description = p1.Description, OtherID = p1.OtherID });
            //files
            medicalInfoAndHistory.BP_OtherModels = (from u1 in _MedORDB.IMBiologicals
                                                    where ConsultationID == u1.ConsultaionID

                                                    from u2 in _MedORDB.BPOthers
                                                    where u2.BiologicalID == u1.BiologicalID
                                                    from u3 in _MedORDB.BP_Other_Files
                                                    where u3.Other == u2.OtherID
                                                    select new DataModel.BP_OtherModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.Other }).ToList();

            medicalInfoAndHistory.BPPetScanModels = (from u1 in _MedORDB.IMBiologicals
                                                     where u1.ConsultaionID == ConsultationID
                                                     from p1 in _MedORDB.BPPetScans
                                                     where p1.BiologicalID == u1.BiologicalID
                                                     select new DataModel.BPPetScanModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PetScanID = p1.PetScanID });
            //files
            medicalInfoAndHistory.BP_PetScanModels = (from u1 in _MedORDB.IMBiologicals
                                                      where ConsultationID == u1.ConsultaionID
                                                      from u2 in _MedORDB.BPPetScans
                                                      where u2.BiologicalID == u1.BiologicalID
                                                      from u3 in _MedORDB.BP_PetScan_Files
                                                      where u3.PetScan == u2.PetScanID
                                                      select new DataModel.BP_PetScanModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.PetScan }).ToList();


            medicalInfoAndHistory.BPMRIModels = (from u1 in _MedORDB.IMBiologicals
                                                 where u1.ConsultaionID == ConsultationID
                                                 from p1 in _MedORDB.BPMRIs
                                                 where p1.BiologicalID == u1.BiologicalID
                                                 select new DataModel.BPMRIModel { CreatedOn = p1.CreatedOn, Description = p1.Description, MRIID = p1.MRIID });

            medicalInfoAndHistory.BP_MRIModels = (from u1 in _MedORDB.IMBiologicals
                                                  where ConsultationID == u1.ConsultaionID

                                                  from u2 in _MedORDB.BPMRIs
                                                  where u2.BiologicalID == u1.BiologicalID
                                                  from u3 in _MedORDB.BP_MRI_Files
                                                  where u3.MRIID == u2.MRIID
                                                  select new DataModel.BP_MRIModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.MRIID }).ToList();



            medicalInfoAndHistory.IMMedicationModels = (from t in _MedORDB.IMMedications
                                                        where t.IMconsultationID == ConsultationID
                                                        select new DataModel.IMMedicationModel
                                                        {
                                                            MedicationName = t.MedicationName  ,
                                                            MedicationID=t.MedicationID,
                                                            PatientID=t.PatientID,
                                                            ConsultationID=t.IMconsultationID,
                                                            DateStart = t.DateStart
                                                            ,
                                                            DateStop = t.DateStart
                                                            ,
                                                            Doze = t.Doze
                                                            ,
                                                            Morning = t.Morning
                                                            ,
                                                            Afternoon = t.afternoon
                                                            ,
                                                            Evening = t.evening
                                                            ,
                                                            BedTime = t.BedTime
                                                            ,
                                                            AsNeeded = t.Asneeded
                                                            ,
                                                            HowMuch = t.HowMuch
                                                            ,
                                                            Reason = t.Reason
                                                            ,
                                                            AdditionalInformation = t.AdditionalInformation
                                                        }).ToList();

            medicalInfoAndHistory.IMHospitalSummaryModels = (from t in _MedORDB.IMHopitalizations
                                                             where t.ConsultationID == ConsultationID
                                                             select new DataModel.IMHospitalSummaryModel
                                                             {
                                                                 AdmissionName = t.HospitalName
                                                                 ,
                                                                 AdmissionDate = t.AdmissionDate
                                                                 ,
                                                                 Departure = t.departuredate
                                                                 ,
                                                                 Description = t.Description
                                                                  ,
                                                                 LengthStay = t.lengthStay
                                                                 ,
                                                                 PatientID = t.PatientID
                                                                 ,
                                                                 ConsultationID = t.ConsultationID
                                                                 ,
                                                                 HospitalSummaryID = t.HospitalizationID

                                                             });
            medicalInfoAndHistory.ICD10PatientModels = (from m in _MedORDB.ICD10_Patient
                                                        where (m.IMconsultationID == ConsultationID)
                                                        orderby m.CreatedOn ascending
                                                        select (new DataModel.ICD10PatientModel
                                                        { CodeDescriptionICD10 = m.CodeDescriptionICD10, Note = m.Note, CreatedOn = m.CreatedOn, IMconsultationID = m.IMconsultationID, CreatedBy = m.CreatedBy, ICD10PatientID = m.ICD10PatientID,PatientID = m.PatientID }));
            medicalInfoAndHistory.IMFileModels = (from t in _MedORDB.IMFiles
                                                  where t.Consultation == ConsultationID
                                                  select new DataModel.IMFileModel
                                                  {
                                                      createdOn = t.CreatedOn,
                                                      PatientID = t.PatientID,
                                                      ConsultationID = t.Consultation,
                                                      FileName = t.FileName,
                                                      FileID = t.FileID
                                                  });

            medicalInfoAndHistory.IMManagement = (from XC in _MedORDB.IMManagements
                                                  where XC.ConsultationID == ConsultationID
                                                  orderby XC.CreatedOn descending
                                                  select new DataModel.IMManagement { Description = XC.Descrition, createdOn = XC.CreatedOn,ConsultationID=ConsultationID }).FirstOrDefault();
            medicalInfoAndHistory.IMManagementMOdels = (from XC in _MedORDB.IMManagements
                                                        where XC.ConsultationID == ConsultationID
                                                        orderby XC.CreatedOn descending
                                                        select new DataModel.IMManagement { Description = XC.Descrition, createdOn = XC.CreatedOn, ManagementID = XC.ManagementID, PatientID = XC.PatientID,ConsultationID=XC.ConsultationID });


            return medicalInfoAndHistory;
        }
        public DataModel.IMedicneModel GetMedicalInternalByPatientID(Guid PatientID, Guid UserID)
        {
            DataModel.IMedicneModel medicalInfoAndHistory = new DataModel.IMedicneModel();
            medicalInfoAndHistory.DoctorModels = (
           
                from ud in _MedORDB.UserDoctors

                where ud.UserID == UserID
                from d in _MedORDB.Doctors

                where ud.DoctorID == d.DoctorID

                select new DataModel.DoctorModel { FullName = d.FirstName + " " + d.LastName, DoctorID = d.DoctorID }).OrderBy(o => o.FullName);

            medicalInfoAndHistory.IMconsultationModels = (from m in _MedORDB.IMconsultations
                                                          where m.PatientID == PatientID
                                                          from UD in _MedORDB.UserDoctors
                                                          where m.DoctorID==UD.DoctorID && UserID==UD.UserID
                                                          orderby m.CreatedOn descending
                                                          select (new DataModel.IMConsultationModel
                                                          {
                                                              IMconsultationID = m.IMconsultationID,
                                                              note = m.Note,
                                                              title = m.Title,
                                                              PatientID = m.PatientID,
                                                              CreatedByName = (from u in _MedORDB.Users where u.UserID == m.CreatedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                              createdBy = m.CreatedBy,
                                                              createdOn = m.CreatedOn,
                                                              modifiedBy = m.ModifiedBy,
                                                              modifiedByName = (from u in _MedORDB.Users where u.UserID == m.ModifiedBy select u.FirstName + " " + u.LastName).FirstOrDefault(),
                                                              modifiedOn = m.ModifiedOn,
                                                              Fees=m.Fees
                                                              ,Curency=m.Currency
                                                              ,CheckBox=m.Checkbox
                                                             
                                                          }));

            medicalInfoAndHistory.SecurityModel = (from u in _MedORDB.Users
                                                   where u.UserID == UserID
                                                   from r in _MedORDB.Roles
                                                   where r.RoleID == u.RoleID
                                                   from p in _MedORDB.Permissions
                                                   where p.PermissionName == "Consultations"
                                                   from s in _MedORDB.Securities
                                                   where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                                   select new DataModel.SecurityModel
                                                   {
                                                       View = s.View,
                                                       Create = s.Create,
                                                       Edit = s.Edit,
                                                       Delete = s.Delete
                                                   }).FirstOrDefault();

            medicalInfoAndHistory.PatientID = PatientID;

            medicalInfoAndHistory.IMcomplaintHistoryModels = (from XC in _MedORDB.IMcomplaintHistories
                                                              where XC.PatientID == PatientID
                                                              from m in _MedORDB.IMconsultations
                                                              where m.IMconsultationID == XC.IMconsultationID
                                                              from UD in _MedORDB.UserDoctors
                                                              where m.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                              orderby XC.CreatedOn descending 
                                                              select new DataModel.IMcomplaintHistory { note = XC.Note, title = XC.Title, createdOn = XC.CreatedOn });

            medicalInfoAndHistory.CRVDiabetes = (from u in _MedORDB.IMconsultations
                                                 where u.PatientID == PatientID
                                                 from UD in _MedORDB.UserDoctors
                                                 where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                 from u1 in _MedORDB.IMCRVasculars
                                                 where u.IMconsultationID == u1.IMconsultationID
                                                 from u2 in _MedORDB.CDiabetes
                                                 where u2.CDiabetesCheck == true & u2.CadiovascularID == u1.CRVascularID
                                                 select u2.CDiabetesCheck).FirstOrDefault();

            medicalInfoAndHistory.CRVObesity = (from u in _MedORDB.IMconsultations
                                                 where u.PatientID == PatientID
                                                from UD in _MedORDB.UserDoctors
                                                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                from u1 in _MedORDB.IMCRVasculars
                                                 where u.IMconsultationID == u1.IMconsultationID
                                                 from u2 in _MedORDB.CObesities
                                                 where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                 select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.CRVDyslipidemia = (from u in _MedORDB.IMconsultations
                                                where u.PatientID == PatientID
                                                from UD in _MedORDB.UserDoctors
                                                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                from u1 in _MedORDB.IMCRVasculars
                                                where u.IMconsultationID == u1.IMconsultationID
                                                from u2 in _MedORDB.CDyslipidemias
                                                where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.CRVFamilialHistory = (from u in _MedORDB.IMconsultations
                                                     where u.PatientID == PatientID
                                                        from UD in _MedORDB.UserDoctors
                                                        where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                        from u1 in _MedORDB.IMCRVasculars
                                                     where u.IMconsultationID == u1.IMconsultationID
                                                     from u2 in _MedORDB.CFamilialHistories
                                                     where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                     select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.CRVAlcohol = (from u in _MedORDB.IMconsultations
                                                        where u.PatientID == PatientID
                                                from UD in _MedORDB.UserDoctors
                                                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                from u1 in _MedORDB.IMCRVasculars
                                                        where u.IMconsultationID == u1.IMconsultationID
                                                        from u2 in _MedORDB.CAlcohols
                                                        where u2.CheckBox == true & u2.CadiovascularID == u1.CRVascularID
                                                        select u2.CheckBox).FirstOrDefault();
       
            // Description for Tab CardioVaqscular 
            medicalInfoAndHistory.CDiabetes = (from u in _MedORDB.IMconsultations
                                               where u.PatientID == PatientID
                                               from UD in _MedORDB.UserDoctors
                                               where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                               from u1 in _MedORDB.IMCRVasculars
                                               where u.IMconsultationID == u1.IMconsultationID
                                               from p in _MedORDB.CDiabetes
                                               where p.CadiovascularID == u1.CRVascularID
                                               orderby p.CreatedOn descending
                                               select new DataModel.CDiabetes { CreatedOn = p.CreatedOn, Description = p.Description, DiabeteID = p.DiabetesID, CardiovascularID = p.CadiovascularID });

            medicalInfoAndHistory.CDyslipidemia = (from u in _MedORDB.IMconsultations
                                                   where u.PatientID == PatientID
                                                   from UD in _MedORDB.UserDoctors
                                                   where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                   from u1 in _MedORDB.IMCRVasculars
                                                       //where u1.PatientID == PatientID
                                                   where u.IMconsultationID == u1.IMconsultationID
                                                   from p1 in _MedORDB.CDyslipidemias
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   orderby p1.CreatedON descending

                                                   select new DataModel.CDyslipidemia { CreatedOn = p1.CreatedON, Description = p1.Description, DyslipidemiaID = p1.CDyslipidemiaID });
            medicalInfoAndHistory.CAlcoholModel = (
                                                   from u in _MedORDB.IMconsultations
                                                   where u.PatientID == PatientID
                                                   from UD in _MedORDB.UserDoctors
                                                   where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                   from u1 in _MedORDB.IMCRVasculars
                                                   where u.IMconsultationID == u1.IMconsultationID
                                                   where u1.PatientID == PatientID
                                                   from p1 in _MedORDB.CAlcohols
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   select new DataModel.CAlcoholModel { CreatedOn = p1.CreatedOn, Description = p1.Description, CAlcoholID = p1.AlcoholID });
            medicalInfoAndHistory.CObesityModel = (
                                                   from u in _MedORDB.IMconsultations
                                                   where u.PatientID == PatientID
                                                   from UD in _MedORDB.UserDoctors
                                                   where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                   from u1 in _MedORDB.IMCRVasculars
                                                   where u.IMconsultationID == u1.IMconsultationID
                                                  // where u1.PatientID == PatientID
                                                   from p1 in _MedORDB.CObesities
                                                   where p1.CadiovascularID == u1.CRVascularID
                                                   select new DataModel.CObesityModel { CreatedOn = p1.CreatedOn, Description = p1.Description, ObesityID = p1.ObesityID });
            medicalInfoAndHistory.FamilialHistoryModel = (
                                                          from u in _MedORDB.IMconsultations
                                                          where u.PatientID == PatientID
                                                          from UD in _MedORDB.UserDoctors
                                                          where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                          from u1 in _MedORDB.IMCRVasculars
                                                          where u.IMconsultationID == u1.IMconsultationID
                                                          //where u1.PatientID == PatientID
                                                          from p1 in _MedORDB.CFamilialHistories
                                                          where p1.CadiovascularID == u1.CRVascularID
                                                          select new DataModel.FamilialHistoryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, FamilialHistoryID = p1.FamilialHistoryID });

            medicalInfoAndHistory.AllergiesModels = (
                                                from u1 in _MedORDB.IMconsultations
                                                where u1.PatientID == PatientID
                                                from UD in _MedORDB.UserDoctors
                                                where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                from u in _MedORDB.IMmedicalHistories
                                                    // where u.PatientID == PatientID
                                                where u.IMconsultationID == u1.IMconsultationID
                                                from p in _MedORDB.MHAllergies
                                               where p.MedicaHistoryID == u.MedicaHistoryID
                                               select new DataModel.AllergiesModel { CreatedOn = p.CreatedOn, Description = p.Description, AllergiesID = p.MHAllergiesID, MedicalHistoryID = p.MedicaHistoryID });
            medicalInfoAndHistory.GynecologyModels = (
                                                   from u in _MedORDB.IMconsultations
                                                   where u.PatientID == PatientID
                                                   from UD in _MedORDB.UserDoctors
                                                   where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                   from u1 in _MedORDB.IMmedicalHistories
                                                       // where u1.PatientID == PatientID
                                                   where u.IMconsultationID == u1.IMconsultationID
                                                   from p1 in _MedORDB.MHGynecologies
                                                   where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                   select new DataModel.GynecologyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, GynecologyID = p1.GynecologyID });
            medicalInfoAndHistory.FamilyModels = (
                from u in _MedORDB.IMconsultations
                where u.PatientID == PatientID
                from UD in _MedORDB.UserDoctors
                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                from u1 in _MedORDB.IMmedicalHistories
                where u.IMconsultationID == u1.IMconsultationID
                //where u1.PatientID == PatientID
                from p1 in _MedORDB.MHFamilies
                                                   where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                   select new DataModel.FamilyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, FamilyID = p1.FamilyID });
            medicalInfoAndHistory.MHMedicalModels = (
                from u in _MedORDB.IMconsultations
                where u.PatientID == PatientID
                from UD in _MedORDB.UserDoctors
                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                from u1 in _MedORDB.IMmedicalHistories
                    //where u1.PatientID == PatientID
                where u.IMconsultationID == u1.IMconsultationID
                from p1 in _MedORDB.MHMedicals
                                                   where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                   select new DataModel.MHMedicalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, MedicalID = p1.MedicalID });
            medicalInfoAndHistory.SurgicalModels = (

                from u in _MedORDB.IMconsultations
                where u.PatientID == PatientID
                from UD in _MedORDB.UserDoctors
                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                from u1 in _MedORDB.IMmedicalHistories
                    // where u1.PatientID == PatientID
                where u.IMconsultationID == u1.IMconsultationID
                from p1 in _MedORDB.MHSurgicals
                                                          where p1.MedicaHistoryID == u1.MedicaHistoryID
                                                          select new DataModel.SurgicalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SurgicalID = p1.SurgicalID });
       //XXXX
            medicalInfoAndHistory.RSHEENT = (

                from u1 in _MedORDB.IMconsultations
                where u1.PatientID == PatientID
                from UD in _MedORDB.UserDoctors
                where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                 from u in _MedORDB.IMReviewSystems
                                                 where u.IMconsultationID == u.IMconsultationID
                                                 from u2 in _MedORDB.RSHEENTs
                                                 where u2.CheckBox == true & u2.ReviewOfSystemID == u.ReviewOfSystemID
                                                 select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSPulmonary = (from u in _MedORDB.IMconsultations
               where u.PatientID == PatientID
                from UD in _MedORDB.UserDoctors
                where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                from u1 in _MedORDB.IMconsultations
                                             where u1.PatientID == PatientID
                                             from RS in _MedORDB.IMReviewSystems
                                             where RS.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSPulminaries
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == RS.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSCardiovascular = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                                      from UD in _MedORDB.UserDoctors
                                                      where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                      from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSCardiovasculars
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSDigestive = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                             from UD in _MedORDB.UserDoctors
                                             where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                             from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID 
                                                 from u2 in _MedORDB.RSDigestives
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSUrogenital = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSUrogentals
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSNervouSystem = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSNervousSystems
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.SRSkin = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                            from UD in _MedORDB.UserDoctors
                                            where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                            from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSSkins
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSMusculoskeletal = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSMusculoskeletals
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();
            medicalInfoAndHistory.RSPsychosocial = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u1 in _MedORDB.IMReviewSystems
                                             where u.IMconsultationID == u1.IMconsultationID
                                             from u2 in _MedORDB.RSPsychosociales
                                             where u2.CheckBox == true & u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                             select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.AnorexiasModels = (from u in _MedORDB.IMconsultations
                                                     where u.PatientID == PatientID
                                                     from UD in _MedORDB.UserDoctors
                                                     where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                     from u1 in _MedORDB.IMReviewSystems
                                                     where u.IMconsultationID == u1.IMconsultationID
                                                     from u2 in _MedORDB.RSAnorexias
                                                where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                     select new DataModel.AnorexiasModel { 

                                                         AnorexiaID = u2.AnorexiaID,
                                                         Anorexia = u2.Annorexia,
                                                         CreatedOn = u2.CreatedOn

                                                     });
            medicalInfoAndHistory.RSFeverModels = (from u in _MedORDB.IMconsultations
                                                   where u.PatientID == PatientID
                                                   from UD in _MedORDB.UserDoctors
                                                   where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                     from u1 in _MedORDB.IMReviewSystems
                                                     where u.IMconsultationID == u1.IMconsultationID
                                                     from u2 in _MedORDB.RSFevers
                                                     where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                     select new DataModel.RSFeverModel
                                                     {

                                                         FeverID = u2.FeverID,
                                                         Fever = u2.Fever,

                                                         CreatedOn = u2.CreatedOn
                                                     });
            medicalInfoAndHistory.RSWeightModels = (from u in _MedORDB.IMconsultations
                                                    where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                   from u1 in _MedORDB.IMReviewSystems
                                                   where u.IMconsultationID == u1.IMconsultationID
                                                   from u2 in _MedORDB.RSWeights
                                                   where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                   select new DataModel.RSWeightModel
                                                   {

                                                       WeightID = u2.WeightID,
                                                       Weight = u2.Weight,
                                                       WCreatedOn = u2.CreatedOn
                                                   });

            medicalInfoAndHistory.RSFatigueModels = (from u in _MedORDB.IMconsultations
                                                     where u.PatientID == PatientID
                                                     from UD in _MedORDB.UserDoctors
                                                     where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u1 in _MedORDB.IMReviewSystems
                                                    where u.IMconsultationID == u1.IMconsultationID
                                                    from u2 in _MedORDB.RSFatigues
                                                    where u2.ReviewOfSystemID == u1.ReviewOfSystemID
                                                    select new DataModel.RSFatigueModel
                                                    {

                                                        FatigueID = u2.FatigueID,
                                                        Fatigue = u2.Fatigue,

                                                        CreatedOn = u2.CreatedOn
                                                    });




            medicalInfoAndHistory.RSHEENTModels = (
                   from u in _MedORDB.IMconsultations
                                                   where u.PatientID == PatientID
                                                   from UD in _MedORDB.UserDoctors
                                                   where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                   from rs in _MedORDB.IMReviewSystems
                                                       //where u1.PatientID == PatientID
                   where u.IMconsultationID == rs.IMconsultationID
                   from p1 in _MedORDB.RSHEENTs
                                                   where p1.ReviewOfSystemID == rs.ReviewOfSystemID
                                                   select new DataModel.RSHEENTModel { CreatedOn = p1.CreatedOn, Description = p1.Description, HEENTID = p1.HEENTID });

            medicalInfoAndHistory.RSPulminaryModels = (
                from u1 in _MedORDB.IMconsultations
                where u1.PatientID == PatientID
                from UD in _MedORDB.UserDoctors
                where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                from u2 in _MedORDB.IMReviewSystems
                    //where u2.PatientID == PatientID
                where u1.IMconsultationID == u2.IMconsultationID
                from p1 in _MedORDB.RSPulminaries
                                                       where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                       select new DataModel.RSPulminaryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PulminaryID = p1.PulminaryID });

            medicalInfoAndHistory.RSCardiovascularModels = (from u1 in _MedORDB.IMconsultations
                                                            where u1.PatientID == PatientID
                                                            from UD in _MedORDB.UserDoctors
                                                            where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                            from u2 in _MedORDB.IMReviewSystems
                                                                //where u2.PatientID == PatientID
                                                            where u1.IMconsultationID == u2.IMconsultationID
                                                            from p1 in _MedORDB.RSCardiovasculars
                                                            where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                            select new DataModel.RSCardiovascularModel { CreatedOn = p1.CreatedOn, Description = p1.Description, CardiovascularID = p1.CardiovascularID });
            medicalInfoAndHistory.RSDigestiveModels = (from u1 in _MedORDB.IMconsultations
                                                       where u1.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u2 in _MedORDB.IMReviewSystems
                                                           //where u2.PatientID == PatientID
                                                       where u1.IMconsultationID == u2.IMconsultationID
                                                       from p1 in _MedORDB.RSDigestives
                                                       where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                       select new DataModel.RSDigestiveModel { CreatedOn = p1.CreatedOn, Description = p1.Description, DigestiveID = p1.DigestiveID });
            medicalInfoAndHistory.RSUrogentalModels = (from u1 in _MedORDB.IMconsultations
                                                       where u1.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u2 in _MedORDB.IMReviewSystems
                                                           //where u2.PatientID == PatientID
                                                       where u1.IMconsultationID == u2.IMconsultationID
                                                       from p1 in _MedORDB.RSUrogentals
                                                       where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                       select new DataModel.RSUrogentalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, UrogentalID = p1.UrogentalID });

            medicalInfoAndHistory.RSNervousSystemModels = (from u1 in _MedORDB.IMconsultations
                                                           where u1.PatientID == PatientID
                                                           from UD in _MedORDB.UserDoctors
                                                           where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                           from u2 in _MedORDB.IMReviewSystems
                                                               //where u2.PatientID == PatientID
                                                           where u1.IMconsultationID == u2.IMconsultationID
                                                           from p1 in _MedORDB.RSNervousSystems
                                                           where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                           select new DataModel.RSNervousSystemModel { CreatedOn = p1.CreatedOn, Description = p1.Description, NervousSystemID = p1.NervousSystemID });

            medicalInfoAndHistory.RSSkinModels = (from u1 in _MedORDB.IMconsultations
                                                  where u1.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  from u2 in _MedORDB.IMReviewSystems
                                                      //where u2.PatientID == PatientID
                                                  where u1.IMconsultationID == u2.IMconsultationID
                                                  from p1 in _MedORDB.RSSkins
                                                  where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                  select new DataModel.RSSkinModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SkinID = p1.SkinID });
            medicalInfoAndHistory.RSMusculoskeletalModels = (from u1 in _MedORDB.IMconsultations
                                                             where u1.PatientID == PatientID
                                                             from UD in _MedORDB.UserDoctors
                                                             where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                             from u2 in _MedORDB.IMReviewSystems
                                                                 //where u2.PatientID == PatientID
                                                             where u1.IMconsultationID == u2.IMconsultationID
                                                             from p1 in _MedORDB.RSMusculoskeletals
                                                            where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                            select new DataModel.RSMusculoskeletalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Musculoskeletal = p1.MusculoskeletalID });

            medicalInfoAndHistory.RSPsychosocialeModels = (from u1 in _MedORDB.IMconsultations
                                                           where u1.PatientID == PatientID
                                                           from UD in _MedORDB.UserDoctors
                                                           where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                           from u2 in _MedORDB.IMReviewSystems
                                                               //where u2.PatientID == PatientID
                                                           where u1.IMconsultationID == u2.IMconsultationID
                                                           from p1 in _MedORDB.RSPsychosociales
                                                          where p1.ReviewOfSystemID == u2.ReviewOfSystemID
                                                          select new DataModel.RSPsychosocialeModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PsychosocialeID = p1.PsychosocialeID });

            // chechbox botton for tap physical erxam history
            medicalInfoAndHistory.PHEntryModels = from p in _MedORDB.IMconsultations
                                                  where p.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where p.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  join p1 in _MedORDB.IMPhysicalExamHistories on p.IMconsultationID equals p1.IMconsultation
                                                  join p2 in _MedORDB.PHWeights on p1.PhysicalExamHistoryID equals p2.PhysicalExamHistoryID
                                                  join p3 in _MedORDB.PHBloodPressures on p1.PhysicalExamHistoryID equals p3.PhysicalExamHistoryID
                                                  join p4 in _MedORDB.PHRespiratoryRates on p1.PhysicalExamHistoryID equals p4.PhysicalExamHistoryID
                                                  select new DataModel.PHEntryModel { RespiratoryRate = p4.RespiratoryRate, BloodPressure = p3.BloodPressure, Weight = p2.Weight, WCreatedOn = p2.createdOn, BCreatedOn = p3.CreatedOn, RCreatedOn = p4.CreatedOn };
           
            medicalInfoAndHistory.BloodPressureModels = (from u in _MedORDB.IMconsultations
                                                     where u.PatientID == PatientID
                                                         from UD in _MedORDB.UserDoctors
                                                         where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                         from u1 in _MedORDB.IMPhysicalExamHistories
                                                     where u.IMconsultationID == u1.IMconsultation
                                                     from u2 in _MedORDB.PHBloodPressures
                                                     where u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                     select new DataModel.BloodPressureModel
                                                     {
                                                         BloodPressureID = u2.BloodPressureID,
                                                         BloodPressure = u2.BloodPressure,
                                                         CreatedOn = u2.CreatedOn
                                                     });

            medicalInfoAndHistory.PHWeightModels = (from u in _MedORDB.IMconsultations
                                                    where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u1 in _MedORDB.IMPhysicalExamHistories
                                                   where u.IMconsultationID == u1.IMconsultation
                                                   from u2 in _MedORDB.PHWeights
                                                   where u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                   select new DataModel.PHWeightModel
                                                   {
                                                       WeightID = u2.WeightID,
                                                       Weight = u2.Weight,
                                                       CreatedOn = u2.createdOn
                                                   });

            medicalInfoAndHistory.PHRespiratoryRateModels = (from u in _MedORDB.IMconsultations
                                                             where u.PatientID == PatientID
                                                             from UD in _MedORDB.UserDoctors
                                                             where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                             from u1 in _MedORDB.IMPhysicalExamHistories
                                                    where u.IMconsultationID == u1.IMconsultation
                                                    from u2 in _MedORDB.PHRespiratoryRates
                                                    where u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                    select new DataModel.PHRespiratoryRateModel
                                                    {
                                                        RespiratoryRateID = u2.RespiratoryRateID,
                                                        RespiratoryRate = u2.RespiratoryRate,
                                                        CreatedOn = u2.CreatedOn
                                                    });

            medicalInfoAndHistory.PHHEENT = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                             from UD in _MedORDB.UserDoctors
                                             where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                             from u1 in _MedORDB.IMPhysicalExamHistories
                                             where u.IMconsultationID == u1.IMconsultation
                                             from u2 in _MedORDB.PHHEENTs
                                             where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                             select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHPulmonary = (from u in _MedORDB.IMconsultations
                                                 where u.PatientID == PatientID
                                                 from UD in _MedORDB.UserDoctors
                                                 where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                 from u1 in _MedORDB.IMPhysicalExamHistories
                                                 where u.IMconsultationID == u1.IMconsultation
                                                 from u2 in _MedORDB.PHPulminaries
                                                 where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                 select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHCardiovascular = (from u in _MedORDB.IMconsultations
                                                      where u.PatientID == PatientID
                                                      from UD in _MedORDB.UserDoctors
                                                      where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                      from u1 in _MedORDB.IMPhysicalExamHistories
                                                      where u.IMconsultationID == u1.IMconsultation
                                                      from u2 in _MedORDB.PHCardiovasculars
                                                      where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                      select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHDigestive = (from u in _MedORDB.IMconsultations
                                                 where u.PatientID == PatientID
                                                 from UD in _MedORDB.UserDoctors
                                                 where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                 from u1 in _MedORDB.IMPhysicalExamHistories
                                                 where u.IMconsultationID == u1.IMconsultation
                                                 from u2 in _MedORDB.PHDigestives
                                                 where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                 select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHUrogenital = (from u in _MedORDB.IMconsultations
                                                  where u.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  from u1 in _MedORDB.IMPhysicalExamHistories
                                                  where u.IMconsultationID == u1.IMconsultation
                                                  from u2 in _MedORDB.PHUrogentals
                                                  where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                  select u2.CheckBox).FirstOrDefault();
         
            medicalInfoAndHistory.PHNervouSystem = (from u in _MedORDB.IMconsultations
                                                    where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u1 in _MedORDB.IMPhysicalExamHistories
                                                    where u.IMconsultationID == u1.IMconsultation
                                                    from u2 in _MedORDB.PHNervousSystems
                                                    where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                    select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHSkin = (from u in _MedORDB.IMconsultations
                                            where u.PatientID == PatientID
                                            from UD in _MedORDB.UserDoctors
                                            where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                            from u1 in _MedORDB.IMPhysicalExamHistories
                                            where u.IMconsultationID == u1.IMconsultation
                                            from u2 in _MedORDB.PHSkins
                                            where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                            select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHMusculoskeletal = (from u in _MedORDB.IMconsultations
                                                       where u.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u1 in _MedORDB.IMPhysicalExamHistories
                                                       where u.IMconsultationID == u1.IMconsultation
                                                       from u2 in _MedORDB.PHMusculoskeletals
                                                       where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                       select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHPsychosocial = (from u in _MedORDB.IMconsultations
                                                 where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u1 in _MedORDB.IMPhysicalExamHistories
                                                 where u.IMconsultationID == u1.IMconsultation
                                                 from u2 in _MedORDB.PHPsychosociales
                                                 where u2.CheckBox == true & u2.PhysicalExamHistoryID == u1.PhysicalExamHistoryID
                                                 select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.PHHEENTModels = (
                                         from u1 in _MedORDB.IMconsultations
                                         where u1.PatientID == PatientID
                                         from UD in _MedORDB.UserDoctors
                                         where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                         from u2 in _MedORDB.IMPhysicalExamHistories
                                         where u1.IMconsultationID == u2.IMconsultation
                                         from p1 in _MedORDB.PHHEENTs
                                                   where u2.PhysicalExamHistoryID == p1.PhysicalExamHistoryID
                                                   select new DataModel.PHHEENTModel { CreatedOn = p1.CreatedOn, Description = p1.Description, HEENTID = p1.HEENTID });

            medicalInfoAndHistory.PHPulminaryModels = (from u1 in _MedORDB.IMconsultations
                                                       where u1.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u2 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultationID == u2.IMconsultation
                                                       from p1 in _MedORDB.PHPulminaries
                                                       where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                       select new DataModel.PHPulminaryModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PulminaryID = p1.PulminaryID });

            medicalInfoAndHistory.PHCardiovascularModels = (from u1 in _MedORDB.IMconsultations
                                                            where u1.PatientID == PatientID
                                                            from UD in _MedORDB.UserDoctors
                                                            where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                            from u2 in _MedORDB.IMPhysicalExamHistories
                                                            where u1.IMconsultationID == u2.IMconsultation
                                                            from p1 in _MedORDB.PHCardiovasculars
                                                            where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                            select new DataModel.PHCardiovascularModelcs { CreatedOn = p1.CreatedOn, Description = p1.Description, CardiovascularID = p1.CardiovascularID });

            medicalInfoAndHistory.PHDigestiveModels = (from u1 in _MedORDB.IMconsultations
                                                       where u1.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u2 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultationID == u2.IMconsultation
                                                       from p1 in _MedORDB.PHDigestives
                                                       where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                       select new DataModel.PHDigestiveModel { CreatedOn = p1.CreatedOn, Description = p1.Description, DigestiveID = p1.DigestiveID });

            medicalInfoAndHistory.PHUrogentalModels = (from u1 in _MedORDB.IMconsultations
                                                       where u1.PatientID == PatientID
                                                       from UD in _MedORDB.UserDoctors
                                                       where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                       from u2 in _MedORDB.IMPhysicalExamHistories
                                                       where u1.IMconsultationID == u2.IMconsultation
                                                       from p1 in _MedORDB.PHUrogentals
                                                       where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                       select new DataModel.PHUrogentalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, UrogentalID = p1.UrogentalID });

            medicalInfoAndHistory.PHNervousSystemModels = (from u1 in _MedORDB.IMconsultations
                                                           where u1.PatientID == PatientID
                                                           from UD in _MedORDB.UserDoctors
                                                           where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                           from u2 in _MedORDB.IMPhysicalExamHistories
                                                           where u1.IMconsultationID == u2.IMconsultation
                                                           from p1 in _MedORDB.PHNervousSystems
                                                           where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                           select new DataModel.PHNervousSystemModel { CreatedOn = p1.CreatedOn, Description = p1.Description, NervousSystemID = p1.NervousSystemID });

            medicalInfoAndHistory.PHSkinModels = (from u1 in _MedORDB.IMconsultations
                                                  where u1.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  from u2 in _MedORDB.IMPhysicalExamHistories
                                                  where u1.IMconsultationID == u2.IMconsultation
                                                  from p1 in _MedORDB.PHSkins
                                                  where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                  select new DataModel.PHRSSkinModel { CreatedOn = p1.CreatedOn, Description = p1.Description, SkinID = p1.SkinID });

            medicalInfoAndHistory.PHMusculoskeletalModels = (from u1 in _MedORDB.IMconsultations
                                                             where u1.PatientID == PatientID
                                                             from UD in _MedORDB.UserDoctors
                                                             where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                             from u2 in _MedORDB.IMPhysicalExamHistories
                                                             where u1.IMconsultationID == u2.IMconsultation
                                                             from p1 in _MedORDB.PHMusculoskeletals
                                                            where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                            select new DataModel.PHMusculoskeletalModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Musculoskeletal = p1.MusculoskeletalID });

            medicalInfoAndHistory.PHPsychosocialeModels = (from u1 in _MedORDB.IMconsultations
                                                           where u1.PatientID == PatientID
                                                           from UD in _MedORDB.UserDoctors
                                                           where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                           from u2 in _MedORDB.IMPhysicalExamHistories
                                                           where u1.IMconsultationID == u2.IMconsultation
                                                           from p1 in _MedORDB.PHPsychosociales
                                                          where p1.PhysicalExamHistoryID == u2.PhysicalExamHistoryID
                                                          select new DataModel.PHPsychosocialeModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PsychosocialeID = p1.PsychosocialeID });

            medicalInfoAndHistory.IMMedicationModels = (from u1 in _MedORDB.IMconsultations
                                                        where u1.PatientID == PatientID
                                                        from UD in _MedORDB.UserDoctors
                                                        where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                        from t in _MedORDB.IMMedications
                                                        where u1.IMconsultationID ==t.IMconsultationID
                                                   
                                                        select new DataModel.IMMedicationModel
                                                        {
                                                            PatientID = t.PatientID,
                                                            MedicationID = t.MedicationID  ,
                                                            MedicationName = t.MedicationName ,
                                                            DateStart = t.DateStart,
                                                            DateStop = t.DateStart,
                                                            Doze = t.Doze,
                                                            Morning = t.Morning ,
                                                            Afternoon = t.afternoon,
                                                            Evening = t.evening,
                                                            BedTime = t.BedTime,
                                                            AsNeeded = t.Asneeded ,
                                                            HowMuch = t.HowMuch,
                                                            Reason = t.Reason ,
                                                            AdditionalInformation = t.AdditionalInformation
                                                        }).ToList();


            medicalInfoAndHistory.IMHospitalSummaryModels = (
               from u1 in _MedORDB.IMconsultations
               where u1.PatientID == PatientID
               from UD in _MedORDB.UserDoctors
               where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
               from t in _MedORDB.IMHopitalizations
               where u1.IMconsultationID == t.ConsultationID
             
                                                             select new DataModel.IMHospitalSummaryModel
                                                             {
                                                                 AdmissionName = t.HospitalName,
                                                                 AdmissionDate = t.AdmissionDate,
                                                                 Departure = t.departuredate,
                                                                 Description = t.Description,
                                                                 LengthStay = t.lengthStay,
                                                                 PatientID = t.PatientID,
                                                                 ConsultationID = t.ConsultationID,
                                                                 HospitalSummaryID = t.HospitalizationID
                                                                 ,FileName=t.FileName

                                                             });



            medicalInfoAndHistory.ICD10PatientModels = (
                     from u1 in _MedORDB.IMconsultations
                     where u1.PatientID == PatientID
                     from UD in _MedORDB.UserDoctors
                     where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                     from m in _MedORDB.ICD10_Patient
                     where u1.IMconsultationID == m.IMconsultationID
                                                        orderby m.CreatedOn ascending
                                                        select (new DataModel.ICD10PatientModel
                                                        { CodeDescriptionICD10 = m.CodeDescriptionICD10, Note = m.Note, CreatedOn = m.CreatedOn, IMconsultationID = m.IMconsultationID, CreatedBy = m.CreatedBy,ICD10PatientID=m.ICD10PatientID,PatientID=PatientID }));

            medicalInfoAndHistory.IMBiologicalModels = (from t in _MedORDB.IMBiologicals
                                                        where t.PatientID == PatientID
                                                        select new DataModel.IMBiologicalModel
                                                        {
                                                            conventionalRadiography = t.BPconventionalRadiography,
                                                            Echography = t.BPEchography,
                                                            scintigraphy = t.BPscintigraphy,
                                                            Scan = t.BPScan,
                                                            Other = t.BPOther,
                                                            MRI = t.BPMRI,
                                                            PetScan = t.BPPetScan,
                                                        }).FirstOrDefault();

            // Checkbox button for tap Biological Parameter


            medicalInfoAndHistory.BPBiology = (from u in _MedORDB.Patients
                                                  where u.PatientID == PatientID
                                                  from u1 in _MedORDB.BPParameter_value
                                                  where u.PatientID == u1.PatientID
                                                  
                                                  where u1.CheckBox == true & u.PatientID == u.PatientID
                                                  select u1.CheckBox).FirstOrDefault();


            medicalInfoAndHistory.BPEchography = (from u in _MedORDB.IMconsultations
                                                  where u.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  from u3 in _MedORDB.IMBiologicals
                                                  where u.IMconsultationID == u3.ConsultaionID
                                                  from u2 in _MedORDB.BPEchographies
                                             where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                                  select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BPMRI = (from u in _MedORDB.IMconsultations
                                           where u.PatientID == PatientID
                                           from UD in _MedORDB.UserDoctors
                                           where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                           from u3 in _MedORDB.IMBiologicals
                                           where u.IMconsultationID == u3.ConsultaionID
                                           from u2 in _MedORDB.BPMRIs
                                                 where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                                 select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BPOther = (from u in _MedORDB.IMconsultations
                                             where u.PatientID == PatientID
                                             from UD in _MedORDB.UserDoctors
                                             where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                             from u3 in _MedORDB.IMBiologicals
                                             where u.IMconsultationID == u3.ConsultaionID
                                             from u2 in _MedORDB.BPOthers
                                                      where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                                      select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BPPetScan = (from u in _MedORDB.IMconsultations
                                               where u.PatientID == PatientID
                                               from UD in _MedORDB.UserDoctors
                                               where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                               from u3 in _MedORDB.IMBiologicals
                                               where u.IMconsultationID == u3.ConsultaionID
                                               from u2 in _MedORDB.BPPetScans
                                                 where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                                 select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BPscintigraphy = (from u in _MedORDB.IMconsultations
                                                    where u.PatientID == PatientID
                                                    from UD in _MedORDB.UserDoctors
                                                    where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                    from u3 in _MedORDB.IMBiologicals
                                                    where u.IMconsultationID == u3.ConsultaionID
                                                    from u2 in _MedORDB.BPScintigraphies
                                                  where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                                  select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BPScan = (from u in _MedORDB.IMconsultations
                                            where u.PatientID == PatientID
                                            from UD in _MedORDB.UserDoctors
                                            where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                            from u3 in _MedORDB.IMBiologicals
                                            where u.IMconsultationID == u3.ConsultaionID
                                            from u2 in _MedORDB.BPScans
                                                    where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                                    select u2.CheckBox).FirstOrDefault();

            medicalInfoAndHistory.BPconventionalRadiography = (from u in _MedORDB.IMconsultations
                                                               where u.PatientID == PatientID
                                                               from UD in _MedORDB.UserDoctors
                                                               where u.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                               from u3 in _MedORDB.IMBiologicals
                                            where u.IMconsultationID == u3.ConsultaionID
                                            from u2 in _MedORDB.BPConventionalRadiographies
                                            where u2.CheckBox == true & u2.BiologicalID == u3.BiologicalID
                                            select u2.CheckBox).FirstOrDefault();

           

            // New Descrpition for tap Biological Parameter
            medicalInfoAndHistory.Conventional_RadiographyModels = (
           from u1 in _MedORDB.IMconsultations
           where u1.PatientID == PatientID
           from UD in _MedORDB.UserDoctors
           where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
           from u2 in _MedORDB.IMBiologicals
                                                                    where u1.IMconsultationID ==u2.ConsultaionID
                                                                    from p1 in _MedORDB.BPConventionalRadiographies
                                                                    where p1.BiologicalID == u2.BiologicalID
                                                                    select new DataModel.Conventional_RadiographyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, Conventional_RadiographyID = p1.ConventionalRadiographyID,FileName=p1.FileName });

            medicalInfoAndHistory.BP_CRMoldels = (from u1 in _MedORDB.IMBiologicals
                                                  where PatientID == u1.PatientID
                                                  from u2 in _MedORDB.BPConventionalRadiographies
                                                  where u2.BiologicalID == u1.BiologicalID
                                                  from u3 in _MedORDB.BP_CR_Files
                                                  where u3.ConventionalRadiographyID == u2.ConventionalRadiographyID
                                                  select new DataModel.BP_CRMoldel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.ConventionalRadiographyID }).ToList();

            medicalInfoAndHistory.BPScintigraphyModels = (from u1 in _MedORDB.IMconsultations
                                                          where u1.PatientID == PatientID
                                                          from UD in _MedORDB.UserDoctors
                                                          where u1.DoctorID == UD.DoctorID && UserID == UD.UserID


                                                          from u2 in _MedORDB.IMBiologicals
                                                          where u1.IMconsultationID == u2.ConsultaionID
                                                          from p1 in _MedORDB.BPScintigraphies
                                                          where p1.BiologicalID == u2.BiologicalID
                                                          select new DataModel.BPScintigraphyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, ScintigraphyID = p1.ScintigraphyID });

            medicalInfoAndHistory.BP_scintigraphyModels = (from u1 in _MedORDB.IMBiologicals
                                                  where PatientID == u1.PatientID

                                                  from u2 in _MedORDB.BPScintigraphies
                                                  where u2.BiologicalID == u1.BiologicalID
                                                  from u3 in _MedORDB.BP_scintigraphy_Files
                                                           where u3.scintigraphyID == u2.ScintigraphyID
                                                  select new DataModel.BP_scintigraphyModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.scintigraphyID }).ToList();

            medicalInfoAndHistory.BPScanModels = (from u1 in _MedORDB.IMconsultations
                                                  where u1.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u1.DoctorID == UD.DoctorID && UserID == UD.UserID

                                                  from u2 in _MedORDB.IMBiologicals
                                                  where u1.IMconsultationID == u2.ConsultaionID
                                                  from p1 in _MedORDB.BPScans
                                                  where p1.BiologicalID == u2.BiologicalID
                                                  select new DataModel.BPScanModel { CreatedOn = p1.CreatedOn, Description = p1.Description, ScanID = p1.ScanID });

            medicalInfoAndHistory.BP_ScanModels = (from u1 in _MedORDB.IMBiologicals
                                                           where PatientID == u1.PatientID

                                                           from u2 in _MedORDB.BPScans
                                                           where u2.BiologicalID == u1.BiologicalID
                                                           from u3 in _MedORDB.BP_Scan_Files
                                                           where u3.ScanID == u2.ScanID
                                                           select new DataModel.BP_ScanModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.ScanID }).ToList();

            medicalInfoAndHistory.BPEchographyModels = (from u1 in _MedORDB.IMconsultations
                                                        where u1.PatientID == PatientID
                                                        from UD in _MedORDB.UserDoctors
                                                        where u1.DoctorID == UD.DoctorID && UserID == UD.UserID

                                                        from u2 in _MedORDB.IMBiologicals
                                                        where u1.IMconsultationID == u2.ConsultaionID
                                                        from p1 in _MedORDB.BPEchographies
                                                        where p1.BiologicalID == u2.BiologicalID
                                                        select new DataModel.EchographyModel { CreatedOn = p1.CreatedOn, Description = p1.Description, EchographyID = p1.EchographyID });
            medicalInfoAndHistory.BP_EchographyModels = (from u1 in _MedORDB.IMBiologicals
                                                   where PatientID == u1.PatientID

                                                   from u2 in _MedORDB.BPEchographies
                                                   where u2.BiologicalID == u1.BiologicalID
                                                   from u3 in _MedORDB.BP_Echography_Files
                                                   where u3.Echography == u2.EchographyID
                                                   select new DataModel.BP_EchographyModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.Echography }).ToList();

            medicalInfoAndHistory.BPOtherModels =( from u1 in _MedORDB.IMconsultations
                                                  where u1.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u1.DoctorID == UD.DoctorID && UserID == UD.UserID

                                                  from u2 in _MedORDB.IMBiologicals
                                                  where u1.IMconsultationID == u2.ConsultaionID
                                                  from p1 in _MedORDB.BPOthers
                                                   where p1.BiologicalID == u2.BiologicalID
                                                   select new DataModel.BPOtherModel { CreatedOn = p1.CreatedOn, Description = p1.Description, OtherID = p1.OtherID });

            medicalInfoAndHistory.BP_OtherModels = (from u1 in _MedORDB.IMBiologicals
                                                   where PatientID == u1.PatientID

                                                   from u2 in _MedORDB.BPOthers
                                                   where u2.BiologicalID == u1.BiologicalID
                                                   from u3 in _MedORDB.BP_Other_Files
                                                   where u3.Other == u2.OtherID
                                                   select new DataModel.BP_OtherModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.Other }).ToList();

            medicalInfoAndHistory.BPPetScanModels = (from u1 in _MedORDB.IMconsultations
                                                     where u1.PatientID == PatientID
                                                     from UD in _MedORDB.UserDoctors
                                                     where u1.DoctorID == UD.DoctorID && UserID == UD.UserID

                                                     from u2 in _MedORDB.IMBiologicals
                                                     where u1.IMconsultationID == u2.ConsultaionID
                                                     from p1 in _MedORDB.BPPetScans
                                                     where p1.BiologicalID == u2.BiologicalID
                                                     select new DataModel.BPPetScanModel { CreatedOn = p1.CreatedOn, Description = p1.Description, PetScanID = p1.PetScanID });

            medicalInfoAndHistory.BP_PetScanModels = (from u1 in _MedORDB.IMBiologicals
                                                    where PatientID == u1.PatientID

                                                    from u2 in _MedORDB.BPPetScans
                                                    where u2.BiologicalID == u1.BiologicalID
                                                    from u3 in _MedORDB.BP_PetScan_Files
                                                    where u3.PetScan == u2.PetScanID
                                                    select new DataModel.BP_PetScanModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.PetScan }).ToList();

            medicalInfoAndHistory.BPMRIModels = (from u1 in _MedORDB.IMconsultations
                                                 where u1.PatientID == PatientID
                                                 from UD in _MedORDB.UserDoctors
                                                 where u1.DoctorID == UD.DoctorID && UserID == UD.UserID

                                                 from u2 in _MedORDB.IMBiologicals
                                                 where u1.IMconsultationID == u2.ConsultaionID
                                                 from p1 in _MedORDB.BPMRIs
                                                 where p1.BiologicalID == u2.BiologicalID
                                         select new DataModel.BPMRIModel { CreatedOn = p1.CreatedOn, Description = p1.Description, MRIID = p1.MRIID });

            medicalInfoAndHistory.BP_MRIModels = (from u1 in _MedORDB.IMBiologicals
                                                      where PatientID == u1.PatientID

                                                      from u2 in _MedORDB.BPMRIs
                                                      where u2.BiologicalID == u1.BiologicalID
                                                      from u3 in _MedORDB.BP_MRI_Files
                                                      where u3.MRIID == u2.MRIID
                                                      select new DataModel.BP_MRIModel { ID = u3.FileID, FileName = u3.FileName, Data = u3.File, BP_ID = u3.MRIID }).ToList();

            medicalInfoAndHistory.DaynParametersModels = (from u1 in _MedORDB.BPParameters
                                                  select new DataModel.DaynParametersModel { ParameterID = u1.ParameterID, value = u1.Value}).ToList();

  

            medicalInfoAndHistory.Dyn_Parameter_ValueModelst = ((from u1 in _MedORDB.BPParameter_value
                                                               where u1.PatientID == PatientID
                                                               from u2 in _MedORDB.ParameterValueDates


                                                               where u2.ParameterDateID == u1.ParameterDateID 
                                                               // group  u1 by u1.ParameterDateID into u1.e
                                                               select new DataModel.Dyn_Parameter_ValueModel
                                                               {

                                                                   //Dyn_Parameter_ValueModelst=(from u1 in _MedORDB.ParameterValueDates where u1.ParameterDateID==u2.ParameterDateID select { u1.ParameterDateID }),
                                                                   ParameterID = u1.ParameterID,
                                                                   Date = (from u1 in _MedORDB.ParameterValueDates where u1.ParameterDateID == u2.ParameterDateID select u2.Date).FirstOrDefault(),
                                                                   ParameterName = (from u in _MedORDB.BPParameters where u.ParameterID == u1.ParameterID select u.Value).FirstOrDefault(),
                                                                   Value = u1.Value,
                                                                   PatientID = u1.PatientID,
                                                                   ParameterDateID=u1.ParameterDateID
                                                               })
                                                             // .GroupBy(uu => uu.ParameterDateID).Select(uu)
                                                               .ToList());

            medicalInfoAndHistory.Dyn_Parameter_ValueModels = ((from u1 in _MedORDB.BPParameter_value
                                                                 where u1.PatientID == PatientID
                                                                 from u2 in _MedORDB.ParameterValueDates
                                                                 where u2.ParameterDateID == u1.ParameterDateID
                                                                 //group  u1.ParameterID by u1.ParameterDateID
                                                                 select new DataModel.Dyn_Parameter_ValueModel
                                                                 {
                                                                     ParameterID = u1.ParameterID,
                                                                     Date = (from u1 in _MedORDB.ParameterValueDates where u1.ParameterDateID == u1.ParameterDateID select u2.Date).FirstOrDefault(),
                                                                     ParameterName = (from u in _MedORDB.BPParameters where u.ParameterID == u1.ParameterID select u.Value).FirstOrDefault(),
                                                                     Value = u1.Value,
                                                                     PatientID = u1.PatientID,
                                                                     ParameterDateID = u1.ParameterDateID
                                                                 })
                                                     
                                                           .ToList());
            
          

            medicalInfoAndHistory.IMFileModels = (from u1 in _MedORDB.IMconsultations
                                                  where u1.PatientID == PatientID
                                                  from UD in _MedORDB.UserDoctors
                                                  where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                  from t in _MedORDB.IMFiles
                                                  where u1.IMconsultationID == t.Consultation
                                                  select new DataModel.IMFileModel
                                                  {
                                                      createdOn = t.CreatedOn,
                                                      PatientID = t.PatientID,
                                                      ConsultationID = t.Consultation,
                                                      FileName = t.FileName,
                                                      FileID = t.FileID
                                                  });

            medicalInfoAndHistory.IMManagementMOdels = (
                from u1 in _MedORDB.IMconsultations
                 where u1.PatientID == PatientID
                 from UD in _MedORDB.UserDoctors
                 where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                 from XC in _MedORDB.IMManagements
                 where u1.IMconsultationID == XC.ConsultationID
                                                        where XC.PatientID == PatientID
                                                        orderby XC.CreatedOn descending
                                                        select new DataModel.IMManagement { Description = XC.Descrition, createdOn = XC.CreatedOn ,ManagementID=XC.ManagementID,PatientID=XC.PatientID});

            medicalInfoAndHistory.IMAccountModels = (from u1 in _MedORDB.IMconsultations
                                                     where u1.PatientID == PatientID
                                                     from UD in _MedORDB.UserDoctors
                                                     where u1.DoctorID == UD.DoctorID && UserID == UD.UserID
                                                     from t in _MedORDB.IMAccounts
                                                             where t.PatientID == u1.PatientID
                                                             select new DataModel.IMAccountModel
                                                             {
                                                                AccountID = t.AccountID,
                                                                 TotalFees = t.TotalFees,
                                                                 CreatedON = t.CreatedOn,
                                                                 CreatedBy = t.CreateadBy,
                                                                 ModifiedON = t.ModifiedOn,
                                                                 ModifiedBy = t.ModifiedBy,
                                                                 PatientID = t.PatientID,
                                                                 Currency= t.Currency
                                                             });





            return medicalInfoAndHistory;
        }
        public Int32 SetNewComlaint(DataModel.IMcomplaintHistory IMcomplaintHistory)
        {
            var newComplaint = new DataAccess.IMcomplaintHistory();

            newComplaint.IMcomplaintHistoryID = Guid.NewGuid();
            newComplaint.IMconsultationID = IMcomplaintHistory.ConsultationID;
            newComplaint.PatientID = IMcomplaintHistory.PatientID;

            newComplaint.Note = IMcomplaintHistory.note;
            newComplaint.Title = IMcomplaintHistory.title;

            newComplaint.CreatedBy = IMcomplaintHistory.createdBy;
            newComplaint.CreatedOn = IMcomplaintHistory.createdOn;

            _MedORDB.IMcomplaintHistories.AddObject(newComplaint);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewDiabetes(DataModel.IMCRVascularModel IMCRVascularModel)
        {
            var newDiabetes = new DataAccess.CDiabete();

            newDiabetes.DiabetesID = Guid.NewGuid();
            newDiabetes.CadiovascularID = IMCRVascularModel.CRVascularID;
            newDiabetes.CDiabetesCheck = IMCRVascularModel.Diabetes;

            newDiabetes.Description = IMCRVascularModel.DiabetesDescription;
            newDiabetes.CreatedOn = IMCRVascularModel.createdOn;

           
            _MedORDB.CDiabetes.AddObject(newDiabetes);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewDyslipidemia(DataModel.IMCRVascularModel IMCRVascularModel)
        {
            var newDyslipidemia = new DataAccess.CDyslipidemia();
            newDyslipidemia.CadiovascularID = IMCRVascularModel.CRVascularID;
            newDyslipidemia.CDyslipidemiaID = Guid.NewGuid();
            newDyslipidemia.CheckBox = IMCRVascularModel.Dyslipidemia;
            newDyslipidemia.Description = IMCRVascularModel.DyslipidemiaDescription;
            newDyslipidemia.CreatedON = IMCRVascularModel.createdOn;
            _MedORDB.CDyslipidemias.AddObject(newDyslipidemia);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewAlcohol(DataModel.IMCRVascularModel IMCRVascularModel)
        {
            var newCAlcohol = new DataAccess.CAlcohol();
            newCAlcohol.CadiovascularID = IMCRVascularModel.CRVascularID;
            newCAlcohol.AlcoholID = Guid.NewGuid();
            newCAlcohol.CheckBox = IMCRVascularModel.Dyslipidemia;
            newCAlcohol.Description = IMCRVascularModel.DyslipidemiaDescription;
            newCAlcohol.CreatedOn = IMCRVascularModel.createdOn;
            _MedORDB.CAlcohols.AddObject(newCAlcohol);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewObesity(DataModel.IMCRVascularModel IMCRVascularModel)
        {
            var newObesity = new DataAccess.CObesity();
            newObesity.CadiovascularID = IMCRVascularModel.CRVascularID;
            newObesity.ObesityID = Guid.NewGuid();
            newObesity.CheckBox = IMCRVascularModel.Dyslipidemia;
            newObesity.Description = IMCRVascularModel.DyslipidemiaDescription;
            newObesity.CreatedOn = IMCRVascularModel.createdOn;
            _MedORDB.CObesities.AddObject(newObesity);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewFamilialHistory(DataModel.IMCRVascularModel IMCRVascularModel)
        {
            var newFamilialHistory= new DataAccess.CFamilialHistory();
            newFamilialHistory.CadiovascularID = IMCRVascularModel.CRVascularID;
            newFamilialHistory.FamilialHistoryID = Guid.NewGuid();
            newFamilialHistory.CheckBox = IMCRVascularModel.Dyslipidemia;
            newFamilialHistory.Description = IMCRVascularModel.FamilialHistoryDescription;
            newFamilialHistory.CreatedOn = IMCRVascularModel.createdOn;
            _MedORDB.CFamilialHistories.AddObject(newFamilialHistory);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Guid SetNewMedicalHistory(DataModel.IMmedicalHistoryModel IMmedicalHistoryModel)
        {
            var newMedicalHistory= new DataAccess.IMmedicalHistory();
            newMedicalHistory.MedicaHistoryID = Guid.NewGuid();
            newMedicalHistory.PatientID = IMmedicalHistoryModel.PatientID;

            newMedicalHistory.Medical = IMmedicalHistoryModel.Medical;
            newMedicalHistory.Gynecology = IMmedicalHistoryModel.Gynecology;
            newMedicalHistory.Family = IMmedicalHistoryModel.Family;
            newMedicalHistory.Allergies = IMmedicalHistoryModel.Allergies;
            newMedicalHistory.Surgical = IMmedicalHistoryModel.Surgical;
            newMedicalHistory.IMconsultationID = IMmedicalHistoryModel.ConsultationID;

            newMedicalHistory.CreatedBy = IMmedicalHistoryModel.createdBy;
            newMedicalHistory.CreatedOn = IMmedicalHistoryModel.createdOn;
            _MedORDB.IMmedicalHistories.AddObject(newMedicalHistory);
            var success = _MedORDB.SaveChanges();
            return newMedicalHistory.MedicaHistoryID;
        }
        public Int32 SetNewAllergies(DataModel.IMmedicalHistoryModel IMmedicalHistoryModel)
        {
            var NewAllergies = new DataAccess.MHAllergy();
            NewAllergies.MedicaHistoryID = IMmedicalHistoryModel.MedicalHistoryID;
            NewAllergies.MHAllergiesID = Guid.NewGuid();
            
            NewAllergies.Description = IMmedicalHistoryModel.Allergies;
            NewAllergies.CreatedOn = IMmedicalHistoryModel.createdOn;
            _MedORDB.MHAllergies.AddObject(NewAllergies);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewGynecology(DataModel.IMmedicalHistoryModel IMmedicalHistoryModel)
        {
            var NewGynecology = new DataAccess.MHGynecology();
            NewGynecology.MedicaHistoryID = IMmedicalHistoryModel.MedicalHistoryID;
            NewGynecology.GynecologyID = Guid.NewGuid();

            NewGynecology.Description = IMmedicalHistoryModel.Gynecology;
            NewGynecology.CreatedOn = IMmedicalHistoryModel.createdOn;
            _MedORDB.MHGynecologies.AddObject(NewGynecology);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewFamily(DataModel.IMmedicalHistoryModel IMmedicalHistoryModel)
        {
            var NewFamily = new DataAccess.MHFamily();
            NewFamily.MedicaHistoryID = IMmedicalHistoryModel.MedicalHistoryID;
            NewFamily.FamilyID = Guid.NewGuid();

            NewFamily.Description = IMmedicalHistoryModel.Family;
            NewFamily.CreatedOn = IMmedicalHistoryModel.createdOn;
            _MedORDB.MHFamilies.AddObject(NewFamily);
            var success = _MedORDB.SaveChanges();
            return success;
        
    }
        public Int32 SetNewMedical(DataModel.IMmedicalHistoryModel IMmedicalHistoryModel)
    {
        var NewMedical = new DataAccess.MHMedical();
        NewMedical.MedicaHistoryID = IMmedicalHistoryModel.MedicalHistoryID;
        NewMedical.MedicalID = Guid.NewGuid();

        NewMedical.Description = IMmedicalHistoryModel.Medical;
        NewMedical.CreatedOn = IMmedicalHistoryModel.createdOn;
        _MedORDB.MHMedicals.AddObject(NewMedical);
        var success = _MedORDB.SaveChanges();
        return success;
    }
        public Int32 SetNewSurgical(DataModel.IMmedicalHistoryModel IMmedicalHistoryModel)
    {
        var NewSurgical = new DataAccess.MHSurgical();
        NewSurgical.MedicaHistoryID = IMmedicalHistoryModel.MedicalHistoryID;
        NewSurgical.SurgicalID = Guid.NewGuid();
        NewSurgical.Description = IMmedicalHistoryModel.Surgical;
        NewSurgical.CreatedOn = IMmedicalHistoryModel.createdOn;
        _MedORDB.MHSurgicals.AddObject(NewSurgical);
        var success = _MedORDB.SaveChanges();
        return success;
    }
        public Guid SetNewCardiovascular(DataModel.IMCRVascularModel IMCRVascularModel)
        {
            var newVascular = new DataAccess.IMCRVascular();
            newVascular.CRVascularID = Guid.NewGuid();
            newVascular.IMconsultationID = IMCRVascularModel.ConsultationID;
            newVascular.PatientID = IMCRVascularModel.PatientID;

           newVascular.CRVdiabetes = IMCRVascularModel.Diabetes;
            //newVascular.CRVdiabetesDescription = IMCRVascularModel.DiabetesDescription;

            newVascular.CRVdyslipidemia= IMCRVascularModel.Dyslipidemia;
            //newVascular.CRVdyslipidemiaDescrition = IMCRVascularModel.DyslipidemiaDescription;

            newVascular.CRValcohol = IMCRVascularModel.Alcohol;
            //newVascular.CRValcoholDescription = IMCRVascularModel.AlcoholDescription;

            newVascular.CRVobesity = IMCRVascularModel.Obesity;
            //newVascular.CRVobesityDescription = IMCRVascularModel.ObesityDescription;

            newVascular.CRVfamilialHistory = IMCRVascularModel.FamilialHistory;
            //newVascular.CRVfamilialHistoryDescription = IMCRVascularModel.FamilialHistoryDescription;
            newVascular.CreatedBy = IMCRVascularModel.createdBy;
                newVascular.CreatedOn = IMCRVascularModel.createdOn;
            _MedORDB.IMCRVasculars.AddObject(newVascular);
            var success = _MedORDB.SaveChanges();
            return newVascular.CRVascularID;
        }
        public Guid SetNewREviewSystem(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newROS = new DataAccess.IMReviewSystem();
            newROS.ReviewOfSystemID = Guid.NewGuid();
            newROS.IMconsultationID = IMreviewSystemModel.ConsultationID;
            newROS.PatientID = IMreviewSystemModel.PatientID;

            //newROS.Anorexia = IMreviewSystemModel.Anorexia;
            //newROS.Fatigue = IMreviewSystemModel.Fatigue;
            //newROS.Fever = IMreviewSystemModel.Fever;
            //newROS.Weight = IMreviewSystemModel.Weight;

            newROS.HEENT = IMreviewSystemModel.HEENT;
            //newROS.HEENTdescription = IMreviewSystemModel.HEENTDescription;

            newROS.Digestive = IMreviewSystemModel.Digestive;
            //newROS.DigestiveDescription = IMreviewSystemModel.DigestiveDescription;

            newROS.Skin = IMreviewSystemModel.Skin;
            //newROS.SkinDescription = IMreviewSystemModel.SkinDescription;

            newROS.Psychosocial = IMreviewSystemModel.Psychosocial;
            //newROS.PsychosocialDescription = IMreviewSystemModel.PsychosocialDescription;

            newROS.Pulmonary = IMreviewSystemModel.Pulmonary;
            //newROS.PulmonaryDescription = IMreviewSystemModel.PulmonaryDescription;

            newROS.NervouSystem = IMreviewSystemModel.NervouSystem;
            //newROS.NervousSystemDescription = IMreviewSystemModel.NervousSystemDescription;

            newROS.Cardiovascular = IMreviewSystemModel.Cardiovascular;
            //newROS.CardiovascularDescription = IMreviewSystemModel.CardiovascularDescriptionical;

            newROS.Musculoskeletal = IMreviewSystemModel.Musculoskeletal;
            //newROS.MusculoskeletalDescription = IMreviewSystemModel.MusculoskeletalDescription;

            newROS.Urogenital = IMreviewSystemModel.Urogenital;
            //newROS.UrogenitalDescription = IMreviewSystemModel.UrogenitalDescription;


            newROS.CreatedBy = IMreviewSystemModel.createdBy;
            newROS.CreadtedOn = DateTime.Now;

            _MedORDB.IMReviewSystems.AddObject(newROS);
            var success = _MedORDB.SaveChanges();
            return newROS.ReviewOfSystemID;
        }
        public Int32 SetNewRSEntry(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {

            if (IMreviewSystemModel.Anorexia != null)
            {
            var newRSAnorexia = new DataAccess.RSAnorexia();
            newRSAnorexia.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSAnorexia.AnorexiaID = Guid.NewGuid();
            newRSAnorexia.Annorexia = IMreviewSystemModel.Anorexia;

            newRSAnorexia.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSAnorexias.AddObject(newRSAnorexia);
            }

            if (IMreviewSystemModel.Fever != null)
            {
                var newRSFever = new DataAccess.RSFever();
                newRSFever.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
                newRSFever.FeverID = Guid.NewGuid();
                newRSFever.Fever = IMreviewSystemModel.Fever;
                newRSFever.CreatedOn = IMreviewSystemModel.createdOn;
                _MedORDB.RSFevers.AddObject(newRSFever);
            }
            if (IMreviewSystemModel.Weight != null)
            {
                var newRSWeight = new DataAccess.RSWeight();
                newRSWeight.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
                newRSWeight.WeightID = Guid.NewGuid();
                newRSWeight.Weight = IMreviewSystemModel.Weight;
                newRSWeight.CreatedOn = IMreviewSystemModel.createdOn;
                _MedORDB.RSWeights.AddObject(newRSWeight);
            }
            if (IMreviewSystemModel.Fatigue != null)
            {
                var newRSFatigue = new DataAccess.RSFatigue();
                newRSFatigue.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
                newRSFatigue.FatigueID = Guid.NewGuid();
                newRSFatigue.Fatigue = IMreviewSystemModel.Fatigue;
                newRSFatigue.CreatedOn = IMreviewSystemModel.createdOn;
                _MedORDB.RSFatigues.AddObject(newRSFatigue);
            }

            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdatePHEntry(DataModel.IMedicneModel IMedicneModel)
        {
            //update conultation
            var EditConsultation = _MedORDB.IMconsultations.Where(x => x.IMconsultationID == IMedicneModel.IMconsultationID).FirstOrDefault();
            EditConsultation.ModifiedBy = IMedicneModel.CmodifiedBy;
            EditConsultation.ModifiedOn = DateTime.Now;

            var EditPhysicalExamHistory = _MedORDB.IMPhysicalExamHistories.Where(x => x.IMconsultation == IMedicneModel.IMconsultationID).FirstOrDefault();

            var newPEH = new DataAccess.IMPhysicalExamHistory();
            newPEH.PatientID = IMedicneModel.IMconsultationModelN.PatientID;
            newPEH.PhysicalExamHistoryID = Guid.NewGuid();
            newPEH.CreadtedOn = DateTime.Now;
            newPEH.IMconsultation = IMedicneModel.IMconsultationID;

            var newRespiratoryRate = new DataAccess.PHRespiratoryRate();
            var newPHWeight = new DataAccess.PHWeight();
            var newPHBloodPressure = new DataAccess.PHBloodPressure();
           
            var success = _MedORDB.SaveChanges();


            if (EditPhysicalExamHistory == null)
            {
                _MedORDB.IMPhysicalExamHistories.AddObject(newPEH);
                if (IMedicneModel.PHRespiratoryRate != null)
                {
                    newRespiratoryRate.PhysicalExamHistoryID = newPEH.PhysicalExamHistoryID;
                    newRespiratoryRate.CreatedOn = DateTime.Now;
                    newRespiratoryRate.RespiratoryRateID = Guid.NewGuid();
                    newRespiratoryRate.RespiratoryRate = IMedicneModel.PHRespiratoryRate;
                    _MedORDB.PHRespiratoryRates.AddObject(newRespiratoryRate);
                    success = _MedORDB.SaveChanges();
                }

                if (IMedicneModel.PHWeight != null)
                {
                    newPHWeight.PhysicalExamHistoryID = newPEH.PhysicalExamHistoryID;
                    newPHWeight.createdOn = DateTime.Now;
                    newPHWeight.WeightID = Guid.NewGuid();
                    newPHWeight.Weight = IMedicneModel.PHWeight;
                    _MedORDB.PHWeights.AddObject(newPHWeight);
                    success = _MedORDB.SaveChanges();
                }

                if (IMedicneModel.PHBloodPressure != null)
                {
                    newPHBloodPressure.PhysicalExamHistoryID = newPEH.PhysicalExamHistoryID;
                    newPHBloodPressure.CreatedOn = DateTime.Now;
                    newPHBloodPressure.BloodPressureID = Guid.NewGuid();
                    newPHBloodPressure.BloodPressure = IMedicneModel.PHBloodPressure;
                    _MedORDB.PHBloodPressures.AddObject(newPHBloodPressure);
                    success = _MedORDB.SaveChanges();
                }
               
            }

            else
            {
                if (IMedicneModel.PHRespiratoryRate != null)
                {
                    newRespiratoryRate.PhysicalExamHistoryID = EditPhysicalExamHistory.PhysicalExamHistoryID;
                    newRespiratoryRate.CreatedOn = DateTime.Now;
                    newRespiratoryRate.RespiratoryRateID = Guid.NewGuid();
                    newRespiratoryRate.RespiratoryRate = IMedicneModel.PHRespiratoryRate;
                    _MedORDB.PHRespiratoryRates.AddObject(newRespiratoryRate);
                    success = _MedORDB.SaveChanges();
                }

                if (IMedicneModel.PHWeight != null)
                {
                    newPHWeight.PhysicalExamHistoryID = EditPhysicalExamHistory.PhysicalExamHistoryID;
                    newPHWeight.createdOn = DateTime.Now;
                    newPHWeight.WeightID = Guid.NewGuid();
                    newPHWeight.Weight = IMedicneModel.PHWeight;
                    _MedORDB.PHWeights.AddObject(newPHWeight);
                    success = _MedORDB.SaveChanges();
                }

                if (IMedicneModel.PHBloodPressure != null)
                {
                    newPHBloodPressure.PhysicalExamHistoryID = EditPhysicalExamHistory.PhysicalExamHistoryID;
                    newPHBloodPressure.CreatedOn = DateTime.Now;
                    newPHBloodPressure.BloodPressureID = Guid.NewGuid();
                    newPHBloodPressure.BloodPressure = IMedicneModel.PHBloodPressure;
                    _MedORDB.PHBloodPressures.AddObject(newPHBloodPressure);
                    success = _MedORDB.SaveChanges();
                }

            }

            return success;
        }
        public Int32 SetNewPHEntry(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {

            if (IMPhysicalHistoryModel.Weight != null)
            {
                var newPHWeight = new DataAccess.PHWeight();
                newPHWeight.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
                newPHWeight.WeightID = Guid.NewGuid();
                newPHWeight.Weight = IMPhysicalHistoryModel.Weight;

                newPHWeight.createdOn = IMPhysicalHistoryModel.createdOn;
                _MedORDB.PHWeights.AddObject(newPHWeight);
            }

            if (IMPhysicalHistoryModel.BloodPressure != null)
            {
                var newPHBloodPressure = new DataAccess.PHBloodPressure();
                newPHBloodPressure.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
                newPHBloodPressure.BloodPressureID = Guid.NewGuid();
                newPHBloodPressure.BloodPressure = IMPhysicalHistoryModel.BloodPressure;
                newPHBloodPressure.CreatedOn = IMPhysicalHistoryModel.createdOn;
                _MedORDB.PHBloodPressures.AddObject(newPHBloodPressure);
            }
            if (IMPhysicalHistoryModel.RespiratoryRate != null)
            {
                var newPHRespiratoryRate = new DataAccess.PHRespiratoryRate();
                newPHRespiratoryRate.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
                newPHRespiratoryRate.RespiratoryRateID = Guid.NewGuid();
                newPHRespiratoryRate.RespiratoryRate = IMPhysicalHistoryModel.RespiratoryRate;
                newPHRespiratoryRate.CreatedOn = IMPhysicalHistoryModel.createdOn;
                _MedORDB.PHRespiratoryRates.AddObject(newPHRespiratoryRate);
            }
        

            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSHEENT(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSHEENT = new DataAccess.RSHEENT();
            newRSHEENT.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSHEENT.HEENTID = Guid.NewGuid();
            newRSHEENT.CheckBox = IMreviewSystemModel.HEENT;
            newRSHEENT.Description = IMreviewSystemModel.HEENTDescription;
            newRSHEENT.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSHEENTs.AddObject(newRSHEENT);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSPulminary(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSPulminary = new DataAccess.RSPulminary();
            newRSPulminary.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSPulminary.PulminaryID = Guid.NewGuid();
            newRSPulminary.CheckBox = IMreviewSystemModel.Pulmonary;
            newRSPulminary.Description = IMreviewSystemModel.PulmonaryDescription;
            newRSPulminary.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSPulminaries.AddObject(newRSPulminary);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSCardiovascular(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSCardiovascular = new DataAccess.RSCardiovascular();
            newRSCardiovascular.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSCardiovascular.CardiovascularID = Guid.NewGuid();
            newRSCardiovascular.CheckBox = IMreviewSystemModel.Cardiovascular;
            newRSCardiovascular.Description = IMreviewSystemModel.CardiovascularDescriptionical;
            newRSCardiovascular.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSCardiovasculars.AddObject(newRSCardiovascular);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSDigestive(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSDigestive = new DataAccess.RSDigestive();
            newRSDigestive.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSDigestive.DigestiveID = Guid.NewGuid();
            newRSDigestive.CheckBox = IMreviewSystemModel.Digestive;
            newRSDigestive.Description = IMreviewSystemModel.DigestiveDescription;
            newRSDigestive.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSDigestives.AddObject(newRSDigestive);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSUrogental(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSUrogental = new DataAccess.RSUrogental();
            newRSUrogental.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSUrogental.UrogentalID = Guid.NewGuid();
            newRSUrogental.CheckBox = IMreviewSystemModel.Urogenital;
            newRSUrogental.Description = IMreviewSystemModel.UrogenitalDescription;
            newRSUrogental.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSUrogentals.AddObject(newRSUrogental);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSNervousSystem(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSNervousSystem = new DataAccess.RSNervousSystem();
            newRSNervousSystem.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSNervousSystem.NervousSystemID = Guid.NewGuid();
            newRSNervousSystem.CheckBox = IMreviewSystemModel.NervouSystem;
            newRSNervousSystem.Description = IMreviewSystemModel.NervousSystemDescription;
            newRSNervousSystem.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSNervousSystems.AddObject(newRSNervousSystem);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSSkin(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSSkin = new DataAccess.RSSkin();
            newRSSkin.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSSkin.SkinID = Guid.NewGuid();
            newRSSkin.CheckBox = IMreviewSystemModel.Skin;
            newRSSkin.Description = IMreviewSystemModel.SkinDescription;
            newRSSkin.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSSkins.AddObject(newRSSkin);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewRSMusculoskeletal(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSMusculoskeletal = new DataAccess.RSMusculoskeletal();
            newRSMusculoskeletal.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSMusculoskeletal.MusculoskeletalID = Guid.NewGuid();
            newRSMusculoskeletal.CheckBox = IMreviewSystemModel.Musculoskeletal;
            newRSMusculoskeletal.Description = IMreviewSystemModel.MusculoskeletalDescription;
            newRSMusculoskeletal.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSMusculoskeletals.AddObject(newRSMusculoskeletal);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPsychosociale(DataModel.IMreviewSystemModel IMreviewSystemModel)
        {
            var newRSPsychosociale = new DataAccess.RSPsychosociale();
            newRSPsychosociale.ReviewOfSystemID = IMreviewSystemModel.ReviewSystemID;
            newRSPsychosociale.PsychosocialeID = Guid.NewGuid();
            newRSPsychosociale.CheckBox = IMreviewSystemModel.Psychosocial;
            newRSPsychosociale.Description = IMreviewSystemModel.PsychosocialDescription;
            newRSPsychosociale.CreatedOn = IMreviewSystemModel.createdOn;
            _MedORDB.RSPsychosociales.AddObject(newRSPsychosociale);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Guid SetPhisycalHistory(DataModel.IMPhysicalHistoryModel IMreviewSystemModel)
        {
            var newPH = new DataAccess.IMPhysicalExamHistory();
            newPH.PhysicalExamHistoryID = Guid.NewGuid();
            newPH.IMconsultation = IMreviewSystemModel.ConsultationID;
            newPH.PatientID = IMreviewSystemModel.PatientID;

            newPH.RespiratoryRate = IMreviewSystemModel.RespiratoryRate;
            newPH.Weight = IMreviewSystemModel.Weight;
            newPH.BloodPressure = IMreviewSystemModel.BloodPressure;


            newPH.HEENT = IMreviewSystemModel.HEENT;
            newPH.HEENTdescription = IMreviewSystemModel.HEENTDescription;

            newPH.Digestive = IMreviewSystemModel.Digestive;
            newPH.DigestiveDescription = IMreviewSystemModel.DigestiveDescription;

            newPH.Skin = IMreviewSystemModel.Skin;
            newPH.SkinDescription = IMreviewSystemModel.SkinDescription;

            newPH.Psychosocial = IMreviewSystemModel.Psychosocial;
            newPH.PsychosocialDescription = IMreviewSystemModel.PsychosocialDescription;

            newPH.Pulmonary = IMreviewSystemModel.Pulmonary;
            newPH.PulmonaryDescription = IMreviewSystemModel.PulmonaryDescription;

            newPH.NervouSystem = IMreviewSystemModel.NervouSystem;
            newPH.NervousSystemDescription = IMreviewSystemModel.NervousSystemDescription;

            newPH.Cardiovascular = IMreviewSystemModel.Cardiovascular;
            newPH.CardiovascularDescription = IMreviewSystemModel.CardiovascularDescriptionical;

            newPH.Musculoskeletal = IMreviewSystemModel.Musculoskeletal;
            newPH.MusculoskeletalDescription = IMreviewSystemModel.MusculoskeletalDescription;

            newPH.Urogenital = IMreviewSystemModel.Urogenital;
            newPH.UrogenitalDescription = IMreviewSystemModel.UrogenitalDescription;


            newPH.CreatedBy = IMreviewSystemModel.createdBy;
            newPH.CreadtedOn = DateTime.Now;

            _MedORDB.IMPhysicalExamHistories.AddObject(newPH);
            var success = _MedORDB.SaveChanges();
            return newPH.PhysicalExamHistoryID;
        }
        public Int32 SetNewPHHEENT(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSHEENT = new DataAccess.PHHEENT();
            newRSHEENT.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSHEENT.HEENTID = Guid.NewGuid();
            newRSHEENT.CheckBox = IMPhysicalHistoryModel.HEENT;
            newRSHEENT.Description = IMPhysicalHistoryModel.HEENTDescription;
            newRSHEENT.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHHEENTs.AddObject(newRSHEENT);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHPulminary(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSPulminary = new DataAccess.PHPulminary();
            newRSPulminary.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSPulminary.PulminaryID = Guid.NewGuid();
            newRSPulminary.CheckBox = IMPhysicalHistoryModel.Pulmonary;
            newRSPulminary.Description = IMPhysicalHistoryModel.PulmonaryDescription;
            newRSPulminary.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHPulminaries.AddObject(newRSPulminary);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHCardiovascular(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSCardiovascular = new DataAccess.PHCardiovascular();
            newRSCardiovascular.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSCardiovascular.CardiovascularID = Guid.NewGuid();
            newRSCardiovascular.CheckBox = IMPhysicalHistoryModel.Cardiovascular;
            newRSCardiovascular.Description = IMPhysicalHistoryModel.CardiovascularDescriptionical;
            newRSCardiovascular.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHCardiovasculars.AddObject(newRSCardiovascular);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHDigestive(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSDigestive = new DataAccess.PHDigestive();
            newRSDigestive.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSDigestive.DigestiveID = Guid.NewGuid();
            newRSDigestive.CheckBox = IMPhysicalHistoryModel.Digestive;
            newRSDigestive.Description = IMPhysicalHistoryModel.DigestiveDescription;
            newRSDigestive.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHDigestives.AddObject(newRSDigestive);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHUrogental(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSUrogental = new DataAccess.PHUrogental();
            newRSUrogental.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSUrogental.UrogentalID = Guid.NewGuid();
            newRSUrogental.CheckBox = IMPhysicalHistoryModel.Urogenital;
            newRSUrogental.Description = IMPhysicalHistoryModel.UrogenitalDescription;
            newRSUrogental.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHUrogentals.AddObject(newRSUrogental);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHNervousSystem(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSNervousSystem = new DataAccess.PHNervousSystem();
            newRSNervousSystem.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSNervousSystem.NervousSystemID = Guid.NewGuid();
            newRSNervousSystem.CheckBox = IMPhysicalHistoryModel.NervouSystem;
            newRSNervousSystem.Description = IMPhysicalHistoryModel.NervousSystemDescription;
            newRSNervousSystem.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHNervousSystems.AddObject(newRSNervousSystem);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHSkin(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSSkin = new DataAccess.PHSkin();
            newRSSkin.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSSkin.SkinID = Guid.NewGuid();
            newRSSkin.CheckBox = IMPhysicalHistoryModel.Skin;
            newRSSkin.Description = IMPhysicalHistoryModel.SkinDescription;
            newRSSkin.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHSkins.AddObject(newRSSkin);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHMusculoskeletal(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSMusculoskeletal = new DataAccess.PHMusculoskeletal();
            newRSMusculoskeletal.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSMusculoskeletal.MusculoskeletalID = Guid.NewGuid();
            newRSMusculoskeletal.CheckBox = IMPhysicalHistoryModel.Musculoskeletal;
            newRSMusculoskeletal.Description = IMPhysicalHistoryModel.MusculoskeletalDescription;
            newRSMusculoskeletal.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHMusculoskeletals.AddObject(newRSMusculoskeletal);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewPHPsychosociale(DataModel.IMPhysicalHistoryModel IMPhysicalHistoryModel)
        {
            var newRSPsychosociale = new DataAccess.PHPsychosociale();
            newRSPsychosociale.PhysicalExamHistoryID = IMPhysicalHistoryModel.IMPhysicalHistoryID;
            newRSPsychosociale.PsychosocialeID = Guid.NewGuid();
            newRSPsychosociale.CheckBox = IMPhysicalHistoryModel.Psychosocial;
            newRSPsychosociale.Description = IMPhysicalHistoryModel.PsychosocialDescription;
            newRSPsychosociale.CreatedOn = IMPhysicalHistoryModel.createdOn;
            _MedORDB.PHPsychosociales.AddObject(newRSPsychosociale);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Guid SetNewBiological(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var newBiological = new DataAccess.IMBiological();
          newBiological.BiologicalID = IMBiologicalModel.BiologicalID;
            newBiological.ConsultaionID = IMBiologicalModel.ConsultationID;
            newBiological.PatientID = IMBiologicalModel.PatientID;

            newBiological.Biology = IMBiologicalModel.Biology;

            newBiological.BPconventionalRadiography = IMBiologicalModel.conventionalRadiography;
            newBiological.BPEchography = IMBiologicalModel.Echography;
            newBiological.BPScan = IMBiologicalModel.Scan;

            newBiological.BPconventionalRadiographyNote = IMBiologicalModel.conventionalRadiographyNote;
            newBiological.BPEchographyNote = IMBiologicalModel.EchographyNote;
            newBiological.BPScanFileNote = IMBiologicalModel.ScanNote;

            newBiological.BPMRI = IMBiologicalModel.MRI;
            newBiological.BPscintigraphy = IMBiologicalModel.scintigraphy;
            newBiological.BPPetScan = IMBiologicalModel.PetScan;
            newBiological.BPOther = IMBiologicalModel.Other;

            newBiological.BPMRINote = IMBiologicalModel.MRINote;
            newBiological.BPscintigraphyNote = IMBiologicalModel.scintigraphyNote;
            newBiological.BPPetScanNote = IMBiologicalModel.PetScanNote;
            newBiological.BPOther = IMBiologicalModel.Other;



            newBiological.CreatedBy = IMBiologicalModel.createdBy;
            newBiological.CreatedOn = IMBiologicalModel.createdOn;
            _MedORDB.IMBiologicals.AddObject(newBiological);
            var success = _MedORDB.SaveChanges();
            return newBiological.BiologicalID;
        }
        public Guid checkFileID_BP(Guid FileID_BP, string source)
        {
            var resultConventional = new DataAccess.BPConventionalRadiography();
            var resultEchography = new DataAccess.BPEchography();
            var resultScan = new DataAccess.BPScan();
            var resultMRI = new DataAccess.BPMRI();
            var resultPScintigraphy = new DataAccess.BPScintigraphy();
            var resultPetScan = new DataAccess.BPPetScan();
            var resultOther = new DataAccess.BPOther();
            Guid result1 = Guid.NewGuid();

            if (source == "BPCFiles")
            {
                resultConventional = _MedORDB.BPConventionalRadiographies.Where(x => x.ConventionalRadiographyID == FileID_BP).FirstOrDefault();
                if (resultConventional == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultConventional.ConventionalRadiographyID; }
            }

            if (source == "Echography")
            {

                resultEchography = _MedORDB.BPEchographies.Where(x => x.EchographyID == FileID_BP).FirstOrDefault();
                if (resultEchography == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultEchography.EchographyID; }

                //result1 = resultEchography.EchographyID;
            }

            if (source == "Scan")
            { resultScan = _MedORDB.BPScans.Where(x => x.ScanID == FileID_BP).FirstOrDefault();
                if (resultScan == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultScan.ScanID; } }

            if (source == "MRI")
            {
                resultMRI = _MedORDB.BPMRIs.Where(x => x.MRIID == FileID_BP).FirstOrDefault();
                if (resultMRI == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultScan.ScanID; }
            }

            if (source == "scintigraphy")
            {
                resultPScintigraphy = _MedORDB.BPScintigraphies.Where(x => x.ScintigraphyID == FileID_BP).FirstOrDefault();
                if (resultPScintigraphy == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultScan.ScanID; }
            }

            if (source == "PetScan")
            {
                resultPetScan = _MedORDB.BPPetScans.Where(x => x.PetScanID == FileID_BP).FirstOrDefault();
                if (resultPetScan == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultScan.ScanID; }
            }

            if (source == "Other")
            {
                resultOther = _MedORDB.BPOthers.Where(x => x.OtherID == FileID_BP).FirstOrDefault();
                if (resultOther == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultScan.ScanID; }
            }

            return result1;
        }
        public Guid CheckIMBiology(Guid ConsultationID)
        {
            var IMBiological = new DataAccess.IMBiological();
            
            Guid result1 = Guid.NewGuid();

         var   resultIMBiological = _MedORDB.IMBiologicals.Where(x => x.ConsultaionID == ConsultationID).FirstOrDefault();
                if (resultIMBiological == null)
                    result1 = new Guid("00000000-0000-0000-0000-000000000000");
                else
                { result1 = resultIMBiological.BiologicalID; }
          

            return result1;
        }
        public DataAccess.IMBiological checkConsultation(Guid Consultation)
        {
            var result3 = _MedORDB.IMBiologicals.Where(x => x.ConsultaionID == Consultation).FirstOrDefault();
            return result3;
        }
        public Guid SetNewBP_Files(DataModel.BP_FilesModel BP_FilesModel, string source)
        {

            Guid ConventionalRadiographyID = Guid.NewGuid();
            if ( source=="BPCFiles")
            {
               var SetNewBPCFiles = new DataAccess.BP_CR_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.ConventionalRadiographyID = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_CR_Files.AddObject(SetNewBPCFiles);
            }
            if (source == "Echography")
            {
                var SetNewBPCFiles = new DataAccess.BP_Echography_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.Echography = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_Echography_Files.AddObject(SetNewBPCFiles);
            }
            if (source == "Scan")
            {
                var SetNewBPCFiles = new DataAccess.BP_Scan_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.ScanID = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_Scan_Files.AddObject(SetNewBPCFiles);
            }
            if (source == "MRI")
            {
                var SetNewBPCFiles = new DataAccess.BP_MRI_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.MRIID = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_MRI_Files.AddObject(SetNewBPCFiles);
            }
            if (source == "scintigraphy")
            {
                var SetNewBPCFiles = new DataAccess.BP_scintigraphy_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.scintigraphyID = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_scintigraphy_Files.AddObject(SetNewBPCFiles);
            }
            if (source == "PetScan")
            {
                var SetNewBPCFiles = new DataAccess.BP_PetScan_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.PetScan = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_PetScan_Files.AddObject(SetNewBPCFiles);
            }
            if (source == "Other")
            {
                var SetNewBPCFiles = new DataAccess.BP_Other_Files();

                SetNewBPCFiles.FileID = Guid.NewGuid();
                SetNewBPCFiles.Other = BP_FilesModel.BP_ID;
                SetNewBPCFiles.FileName = BP_FilesModel.FileName;
                SetNewBPCFiles.ContentType_File = BP_FilesModel.ContentType;
                SetNewBPCFiles.File = BP_FilesModel.Data;
                SetNewBPCFiles.CreatedOn = DateTime.Now;
                _MedORDB.BP_Other_Files.AddObject(SetNewBPCFiles);
            }
            var success = _MedORDB.SaveChanges();
            return ConventionalRadiographyID;
        }
        public Guid SetNewBPconventionalRadiography(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var newConventional_Radiography = new DataAccess.BPConventionalRadiography();
            Guid FileID = Guid.NewGuid();

            //var newIMBiological = new DataAccess.IMBiological();
            //newIMBiological.CreatedBy = IMBiologicalModel.createdBy;
            //newIMBiological.ConsultaionID = IMBiologicalModel.ConsultationID;
            //newIMBiological.PatientID = IMBiologicalModel.PatientID;
            //newIMBiological.BiologicalID = Guid.NewGuid();
            //newIMBiological.CreatedOn = IMBiologicalModel.createdOn;
            //_MedORDB.IMBiologicals.AddObject(newIMBiological);

            FileID = newConventional_Radiography.ConventionalRadiographyID = Guid.NewGuid();
            newConventional_Radiography.BiologicalID = IMBiologicalModel.BiologicalID;//
            newConventional_Radiography.CheckBox = true;//
            newConventional_Radiography.File = IMBiologicalModel.Data;
            newConventional_Radiography.ContentType_File = IMBiologicalModel.ContentType;
            newConventional_Radiography.FileName = IMBiologicalModel.FileName;
            newConventional_Radiography.CreatedOn = IMBiologicalModel.createdOn;
            newConventional_Radiography.Description=IMBiologicalModel.conventionalRadiographyNote;
            _MedORDB.BPConventionalRadiographies.AddObject(newConventional_Radiography);
            var success = _MedORDB.SaveChanges();
            return FileID;
        }
        public Guid SetNewBPEchography(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var NewBPEchography = new DataAccess.BPEchography();

            Guid FileID = Guid.NewGuid();
            FileID = NewBPEchography.EchographyID = Guid.NewGuid();
            NewBPEchography.BiologicalID = IMBiologicalModel.BiologicalID;//
            NewBPEchography.CheckBox = true;
            NewBPEchography.File = IMBiologicalModel.Data;
            NewBPEchography.Description = IMBiologicalModel.EchographyNote;
            NewBPEchography.ContentType_File = IMBiologicalModel.ContentType; 
            NewBPEchography.FileName = IMBiologicalModel.FileName;
            NewBPEchography.CreatedOn = IMBiologicalModel.createdOn;

            _MedORDB.BPEchographies.AddObject(NewBPEchography);
            var success = _MedORDB.SaveChanges();
            return FileID;

        }
        public Guid  SetNewBPScan(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var newBPScan = new DataAccess.BPScan();

            Guid FileID = Guid.NewGuid();
            FileID = newBPScan.ScanID = Guid.NewGuid();
            newBPScan.BiologicalID = IMBiologicalModel.BiologicalID;
            newBPScan.CheckBox = true;
            newBPScan.Description = IMBiologicalModel.ScanNote;
            newBPScan.ContentType_File = IMBiologicalModel.ContentType;
            newBPScan.FileName = IMBiologicalModel.FileName;
            newBPScan.CreatedOn = IMBiologicalModel.createdOn;
            newBPScan.File = IMBiologicalModel.Data;

            _MedORDB.BPScans.AddObject(newBPScan);
            var success = _MedORDB.SaveChanges();
            return FileID;
        }
        public Guid SetNewBPMRI(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var NewBPMRI = new DataAccess.BPMRI();

            Guid FileID = Guid.NewGuid();
            FileID = NewBPMRI.MRIID = Guid.NewGuid();
            NewBPMRI.BiologicalID = IMBiologicalModel.BiologicalID;
            NewBPMRI.CheckBox = true;
            NewBPMRI.Description = IMBiologicalModel.MRINote;
            NewBPMRI.ContentType_File = IMBiologicalModel.ContentType;
            NewBPMRI.FileName = IMBiologicalModel.FileName;
            NewBPMRI.File = IMBiologicalModel.Data;
            NewBPMRI.CreatedOn = IMBiologicalModel.createdOn;

            _MedORDB.BPMRIs.AddObject(NewBPMRI);
            var success = _MedORDB.SaveChanges();
            return FileID;
        }
        public Guid SetNewBPscintigraphy(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var NewBPscintigraphy = new DataAccess.BPScintigraphy();

            Guid FileID = Guid.NewGuid();
            FileID = NewBPscintigraphy.ScintigraphyID = Guid.NewGuid();
            NewBPscintigraphy.BiologicalID = IMBiologicalModel.BiologicalID;
            NewBPscintigraphy.CheckBox = true;
            NewBPscintigraphy.Description = IMBiologicalModel.scintigraphyNote;
            NewBPscintigraphy.ContentType_File = IMBiologicalModel.ContentType;
            NewBPscintigraphy.FileName = IMBiologicalModel.FileName;
            NewBPscintigraphy.File = IMBiologicalModel.Data;
            NewBPscintigraphy.CreatedOn = IMBiologicalModel.createdOn;

            _MedORDB.BPScintigraphies.AddObject(NewBPscintigraphy);
            var success = _MedORDB.SaveChanges();
            return FileID;
        }
        public Guid SetNewBPPetScan(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var NewBPPetScan = new DataAccess.BPPetScan();

            Guid FileID = Guid.NewGuid();
            FileID = NewBPPetScan.PetScanID = Guid.NewGuid();
            NewBPPetScan.BiologicalID = IMBiologicalModel.BiologicalID;
            NewBPPetScan.CheckBox = true;
            NewBPPetScan.Description = IMBiologicalModel.PetScanNote;
            NewBPPetScan.ContentType_File = IMBiologicalModel.ContentType;
            NewBPPetScan.FileName = IMBiologicalModel.FileName;
            NewBPPetScan.File = IMBiologicalModel.Data;
            NewBPPetScan.CreatedOn = IMBiologicalModel.createdOn;

            _MedORDB.BPPetScans.AddObject(NewBPPetScan);
            var success = _MedORDB.SaveChanges();
            return FileID;
        }
        public Guid SetNewBPOther(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var NewBPOthe = new DataAccess.BPOther();


            Guid FileID = Guid.NewGuid();
            FileID = NewBPOthe.OtherID = Guid.NewGuid();
            NewBPOthe.BiologicalID = IMBiologicalModel.BiologicalID;
            NewBPOthe.CheckBox = true;
            NewBPOthe.Description = IMBiologicalModel.OtherNote;
            NewBPOthe.ContentType_File = IMBiologicalModel.ContentType;
            NewBPOthe.FileName = IMBiologicalModel.FileName;
            NewBPOthe.File = IMBiologicalModel.Data;
            NewBPOthe.CreatedOn = IMBiologicalModel.createdOn;
            _MedORDB.BPOthers.AddObject(NewBPOthe);
            var success = _MedORDB.SaveChanges();
            return FileID;
        }
        public Int32 UpdateBPconventionalRadiography(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var editConventional = _MedORDB.BPConventionalRadiographies.Where(x => x.BiologicalID == IMBiologicalModel.BiologicalID).FirstOrDefault();
            int success = 0;
            if (editConventional==null)
            {
                var newConventional_Radiography = new DataAccess.BPConventionalRadiography();
                Guid FileID = Guid.NewGuid();
               
               newConventional_Radiography.ConventionalRadiographyID = Guid.NewGuid();
                newConventional_Radiography.BiologicalID = IMBiologicalModel.BiologicalID;//
                newConventional_Radiography.CheckBox = true;//
                newConventional_Radiography.File = IMBiologicalModel.Data;
                newConventional_Radiography.ContentType_File = IMBiologicalModel.ContentType;
                newConventional_Radiography.FileName = IMBiologicalModel.FileName;
                newConventional_Radiography.CreatedOn = IMBiologicalModel.createdOn;
                newConventional_Radiography.Description = IMBiologicalModel.conventionalRadiographyNote;
                _MedORDB.BPConventionalRadiographies.AddObject(newConventional_Radiography);
                 success = _MedORDB.SaveChanges();



            }
            else
            {
                editConventional.Description = IMBiologicalModel.conventionalRadiographyNote;
                editConventional.CheckBox = true;
                editConventional.CreatedOn = IMBiologicalModel.createdOn;
                success = _MedORDB.SaveChanges();
            }
            
            return success;
        }
        public Int32 UpdateBPEchographies(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var editEchographies = _MedORDB.BPEchographies.Where(x => x.BiologicalID == IMBiologicalModel.BiologicalID).FirstOrDefault();
            int success = 0;

            if (editEchographies==null)
            {
                var NewBPEchography = new DataAccess.BPEchography();

                Guid FileID = Guid.NewGuid();
                FileID = NewBPEchography.EchographyID = Guid.NewGuid();
                NewBPEchography.BiologicalID = IMBiologicalModel.BiologicalID;//
                NewBPEchography.CheckBox = true;
                NewBPEchography.File = IMBiologicalModel.Data;
                NewBPEchography.Description = IMBiologicalModel.EchographyNote;
                NewBPEchography.ContentType_File = IMBiologicalModel.ContentType;
                NewBPEchography.FileName = IMBiologicalModel.FileName;
                NewBPEchography.CreatedOn = IMBiologicalModel.createdOn;

                _MedORDB.BPEchographies.AddObject(NewBPEchography);
                 success = _MedORDB.SaveChanges();
            }
            else { 
            editEchographies.Description = IMBiologicalModel.EchographyNote;
            editEchographies.CheckBox = true;
            editEchographies.CreatedOn = IMBiologicalModel.createdOn;
            }

            success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 UpdateBPScan(DataModel.IMBiologicalModel IMBiologicalModel)
        {
            var editScan = _MedORDB.BPScans.Where(x => x.BiologicalID == IMBiologicalModel.BiologicalID).FirstOrDefault();
            int success = 0;

            if (editScan == null)
            {
                var newBPScan = new DataAccess.BPScan();
                Guid FileID = Guid.NewGuid();
                FileID = newBPScan.ScanID = Guid.NewGuid();
                newBPScan.BiologicalID = IMBiologicalModel.BiologicalID;
                newBPScan.CheckBox = true;
                newBPScan.Description = IMBiologicalModel.ScanNote;
                newBPScan.ContentType_File = IMBiologicalModel.ContentType;
                newBPScan.FileName = IMBiologicalModel.FileName;
                newBPScan.CreatedOn = IMBiologicalModel.createdOn;
                newBPScan.File = IMBiologicalModel.Data;
                _MedORDB.BPScans.AddObject(newBPScan);
                 success = _MedORDB.SaveChanges();

            }
            else
            {
                editScan.Description = IMBiologicalModel.ScanNote;
                editScan.CheckBox = true;
                editScan.CreatedOn = IMBiologicalModel.createdOn;
                success = _MedORDB.SaveChanges();

            }


           
            return success;
        }
        public string Dynamic(string valueparameter,Guid ConsultationID,Guid PatientID )
        {
            var parameterinput= _MedORDB.BPParameters.Where(x => x.Value == valueparameter).FirstOrDefault();
            if (parameterinput==null)
            
            {
                var Newparameter = new DataAccess.BPParameter();
                Newparameter.ParameterID = Guid.NewGuid();
                Newparameter.Value = valueparameter;
                _MedORDB.BPParameters.AddObject(Newparameter);
                _MedORDB.SaveChanges();

            }
    
            return valueparameter;
        }
        public Guid DynamicInput(string valueparameter1, Guid ConsultationID, Guid PatientID,DateTime DateParametersvalue,Guid DaynParametersvalueID, Guid ParameterDateID)
        {
            //var parameterinput = _MedORDB.BPParameter_value.Where(x => x.PatientID == PatientID).FirstOrDefault();
            //if (parameterinput == null)

            //{

            var CheckDateID= _MedORDB.ParameterValueDates.Where(x => x.ParameterDateID == ParameterDateID).FirstOrDefault();
        
            if (CheckDateID !=null)
            {


            }
            else
            {
                var NewparameterDate = new DataAccess.ParameterValueDate();
                NewparameterDate.ParameterDateID = Guid.NewGuid();
                NewparameterDate.Date = DateParametersvalue;
                ParameterDateID = NewparameterDate.ParameterDateID;
                _MedORDB.ParameterValueDates.AddObject(NewparameterDate);
                var success=_MedORDB.SaveChanges();
            }




            //  var checkparameterValue =
            var Newparameter = new DataAccess.BPParameter_value();
                Newparameter.Parameter_valueID = Guid.NewGuid();
            Newparameter.PatientID = PatientID;
            Newparameter.Value = valueparameter1;
            Newparameter.CheckBox = true;
            //Newparameter.Date = DateTime.Now;
            Newparameter.Date = DateParametersvalue;
            Newparameter.ParameterID = DaynParametersvalueID;
            Newparameter.ParameterDateID = ParameterDateID;
                _MedORDB.BPParameter_value.AddObject(Newparameter);

             _MedORDB.SaveChanges();

            //}

            return Newparameter.ParameterDateID;
        }
        public Int32 SetIMDiagnostic(DataModel.ICD10PatientModel ICD10PatientModel)
        {
            var newConsultation = new DataAccess.IMconsultation();
            if (ICD10PatientModel.CodeDescriptionICD10 != null)
            {
                var newICD10Patient = new DataAccess.ICD10_Patient();
                newICD10Patient.ICD10PatientID = Guid.NewGuid();
                newICD10Patient.IMconsultationID = ICD10PatientModel.IMconsultationID;
                newICD10Patient.PatientID = ICD10PatientModel.PatientID;

                newICD10Patient.CodeDescriptionICD10 = ICD10PatientModel.CodeDescriptionICD10;
                newICD10Patient.Note = ICD10PatientModel.Note;
                newICD10Patient.CreatedOn = ICD10PatientModel.CreatedOn;
                newICD10Patient.CreatedBy = ICD10PatientModel.CreatedBy;

                _MedORDB.ICD10_Patient.AddObject(newICD10Patient);

            };


            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetHospitalization(DataModel.IMHospitalSummaryModel HospitalizationModel,string source)
        {
            var newHospitalization = new DataAccess.IMHopitalization();
            if (
                source == "Hospitalization")
            {
                newHospitalization.HospitalizationID = Guid.NewGuid();
                newHospitalization.ConsultationID = HospitalizationModel.ConsultationID;
                newHospitalization.PatientID = HospitalizationModel.PatientID;
                

                newHospitalization.File = HospitalizationModel.Data;
                newHospitalization.ContentType_File = HospitalizationModel.ContentType;
                newHospitalization.FileName = HospitalizationModel.FileName;

                newHospitalization.CreatedBy = HospitalizationModel.CreatedBy;
                newHospitalization.CreatedOn = HospitalizationModel.CreatedOn;
                _MedORDB.IMHopitalizations.AddObject(newHospitalization);
            }
             
            if (source == "") {

                //newHospitalization.HospitalizationID = Guid.NewGuid();
                var EditHospitalization = _MedORDB.IMHopitalizations.Where(x => x.ConsultationID == HospitalizationModel.ConsultationID).FirstOrDefault();

                if (EditHospitalization != null) {

                    EditHospitalization.HospitalName = HospitalizationModel.AdmissionName;
                    EditHospitalization.Description = HospitalizationModel.Description;
                    EditHospitalization.AdmissionDate = HospitalizationModel.AdmissionDate;
                    EditHospitalization.departuredate = HospitalizationModel.Departure;
                    EditHospitalization.lengthStay = HospitalizationModel.LengthStay;
                    //_MedORDB.IMHopitalizations.AddObject(EditHospitalization);
                }
               
                else {
                    newHospitalization.HospitalizationID = Guid.NewGuid();
                    newHospitalization.ConsultationID = HospitalizationModel.ConsultationID;
                    newHospitalization.PatientID = HospitalizationModel.PatientID;

                    newHospitalization.Description = HospitalizationModel.Description;
                    newHospitalization.HospitalName = HospitalizationModel.AdmissionName;
                    newHospitalization.AdmissionDate = HospitalizationModel.AdmissionDate;
                    newHospitalization.departuredate = HospitalizationModel.Departure;
                    newHospitalization.lengthStay = HospitalizationModel.LengthStay;

                    newHospitalization.CreatedOn = HospitalizationModel.CreatedOn;
                    newHospitalization.CreatedBy = HospitalizationModel.CreatedBy;
                    _MedORDB.IMHopitalizations.AddObject(newHospitalization);

                }
            
            }
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetIMManagement(DataModel.IMManagement IMManagementModel)
        {
            var newManagement = new DataAccess.IMManagement();
            newManagement.ManagementID = Guid.NewGuid();
            newManagement.ConsultationID = IMManagementModel.ConsultationID;
            newManagement.PatientID = IMManagementModel.PatientID;
            newManagement.Descrition = IMManagementModel.Description;

            newManagement.CreatedOn = IMManagementModel.createdOn;
            newManagement.CreatedBy = IMManagementModel.createdBy;
            _MedORDB.IMManagements.AddObject(newManagement);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetNewMedicationntModel(DataModel.IMMedicationModel IMMedicationModel)
        {
            var newMedication = new DataAccess.IMMedication();
            newMedication.MedicationID = Guid.NewGuid();
            newMedication.IMconsultationID = IMMedicationModel.ConsultationID;
            newMedication.PatientID = IMMedicationModel.PatientID;
            newMedication.MedicationName = IMMedicationModel.MedicationName;
            newMedication.AdditionalInformation = IMMedicationModel.AdditionalInformation;
            newMedication.Reason = IMMedicationModel.Reason;
            newMedication.Morning = IMMedicationModel.Morning;
            newMedication.evening = IMMedicationModel.Evening;
            newMedication.afternoon = IMMedicationModel.Afternoon;
            newMedication.BedTime = IMMedicationModel.BedTime;
            newMedication.Asneeded = IMMedicationModel.AsNeeded;
            newMedication.DateStart = IMMedicationModel.DateStart;
            newMedication.DateStop = IMMedicationModel.DateStop;
            newMedication.Doze = IMMedicationModel.Doze;
            newMedication.HowMuch = IMMedicationModel.HowMuch;
            newMedication.CreatedOn = IMMedicationModel.CreatedOn;
            newMedication.CreatedBy = IMMedicationModel.CreatedBy;
            _MedORDB.IMMedications.AddObject(newMedication);
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public Int32 SetIMConsultation(DataModel.IMedicneModel model, DataModel.ICD10PatientModel ICD10PatientModel, DataModel.IMMedicationModel IMMedicationModel)
        {
            var newConsultation = new DataAccess.IMconsultation();
            newConsultation.IMconsultationID = model.IMconsultationID;
            newConsultation.PatientID = ICD10PatientModel.PatientID;
            newConsultation.DoctorID = model.SelectedDoctorID;
            newConsultation.Note = model.Cnote;
            newConsultation.Title = model.Ctitle;
            newConsultation.CreatedBy = model.CcreatedBy;
            newConsultation.CreatedOn =model.CcreatedOn;

            _MedORDB.IMconsultations.AddObject(newConsultation);

            var success = _MedORDB.SaveChanges();
            return success;
        }
     
        #endregion  #region IMedicine
        #region Conultation




        #endregion
        #region ViewCreateModify
        public Int32 AddNewCreateModify(DataModel.ViewCreateModifyModel model)
        {
            DataAccess.CreateModifyView newViewCreateModify = new DataAccess.CreateModifyView();
            newViewCreateModify.CreateModifyViewID = Guid.NewGuid();
            newViewCreateModify.UserID = model.UserID;
            newViewCreateModify.TableName = model.TableName;
            newViewCreateModify.CreatedByView = model.ViewCreatedBy;
            newViewCreateModify.CreatedOnView = model.ViewCreatedOn;
            newViewCreateModify.ModifiedByView = model.ViewModifiedBy;
            newViewCreateModify.ModifiedOnView = model.ViewModifiedOn;
            _MedORDB.CreateModifyViews.AddObject(newViewCreateModify);
            var success = _MedORDB.SaveChanges();

            return success;
        }
        public Int32 UpdateCreateModify(DataModel.ViewCreateModifyModel model)
        {
            var editCreateModifyView = _MedORDB.CreateModifyViews.Where(x => x.UserID == model.UserID && x.TableName == model.TableName).FirstOrDefault();
            editCreateModifyView.CreatedByView = model.ViewCreatedBy;
            editCreateModifyView.CreatedOnView = model.ViewCreatedOn;
            editCreateModifyView.ModifiedByView = model.ViewModifiedBy;
            editCreateModifyView.ModifiedOnView = model.ViewModifiedOn;
            var success = _MedORDB.SaveChanges();
            return success;
        }
        public DataModel.ViewCreateModifyModel GetViewCreateModifyByUserAndTableName(Guid UserID, String TableName)
        {
            var viewCreateModifyModel = (from cm in _MedORDB.CreateModifyViews
                                         where cm.UserID == UserID && cm.TableName == TableName
                                         select new DataModel.ViewCreateModifyModel
                                         {
                                             UserID = cm.UserID,
                                             TableName = cm.TableName,
                                             ViewCreatedBy = cm.CreatedByView,
                                             ViewCreatedOn = cm.CreatedOnView,
                                             ViewModifiedBy = cm.ModifiedByView,
                                             ViewModifiedOn = cm.ModifiedOnView,
                                             //SecurityModel = (from u in _MedORDB.Users
                                             //                 where u.UserID == UserID
                                             //                 from r in _MedORDB.Roles
                                             //                 where r.RoleID == u.RoleID
                                             //                 from p in _MedORDB.Permissions
                                             //                 where p.PermissionName == "Appointments"
                                             //                 from s in _MedORDB.Securities
                                             //                 where s.PermissionID == p.PermissionID && s.RoleID == r.RoleID
                                             //                 select new DataModel.SecurityModel
                                             //                 {
                                             //                     View = s.View,
                                             //                     Create = s.Create,
                                             //                     Edit = s.Edit,
                                             //                     Delete = s.Delete
                                             //                 }).FirstOrDefault()
                                         }).FirstOrDefault();
            return viewCreateModifyModel;
        }
        public Boolean GetViewCreateModifyExist(Guid UserID, String TableName)
        {
            var viewCreateModifyExist = (from cm in _MedORDB.CreateModifyViews
                                         where cm.UserID == UserID && cm.TableName == TableName
                                         select cm).Any();

            return viewCreateModifyExist;
        }

        #endregion
        #region ICD_10
        public IEnumerable<DataModel.ICD10Model> GetICD10()
        {
            var ICD10Model = (from pi in _MedORDB.ICD_10

                              select new DataModel.ICD10Model
                              {
                                  IDICD10 = pi.ID_ICD10,
                                  CodeICD10 = pi.Code_ICD10,
                                  CodeCategory = pi.Code_Category,
                                  CodeDescription = pi.Code_Description

                              }).OrderBy(o => o.IDICD10).ThenBy(o => o.IDICD10); 
            return ICD10Model;
        }
        public DataModel.PediatricMedicalModel GetAllICD_10()
        {
            var ICD10Model = new DataModel.PediatricMedicalModel
            {
                ICD10Models = (from d in _MedORDB.ICD_10 select new DataModel.ICD10Model { CodeCategory = d.Code_Category, CodeICD10 = d.Code_ICD10, CodeDescription = d.Code_Description }).OrderBy(o => o.CodeDescription),

            };
            return ICD10Model;
        }
        #endregion

    }
}
